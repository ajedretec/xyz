﻿using LINQtoCSV;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;

public class CLanguage
{
    public class LangLine
    {
        [CsvColumn(FieldIndex = 1)]
        public string key;
        [CsvColumn(FieldIndex = 2)]
        public string value;
    }
    // Constants
    const string EXTENSION = ".csv";
    const string IGNORE_CHAR = "!";
    const string FILENAME = "texts";
    const string DIR = "lang";
    const string LANG = "es";
    const string REGEX = @"\[((\s*?.*?)*?)\]";

    Dictionary<string, string> _strings;

    static CLanguage _inst;
    public static CLanguage Inst
    {
        get
        {
            if (_inst == null)
                _inst = new CLanguage();
            return _inst;
        }
    }

    public CLanguage()
    {
        _strings = new Dictionary<string, string>();
        /* Look for langs
        string[] tDirs = Directory.GetDirectories(
            Application.streamingAssetsPath + "/" + DIR);
        for (int i = 0; i < tDirs.Length; i++)
        {
            string tDir = tDirs[i];
            if (!tDir.StartsWith(IGNORE_CHAR))
            {

            }
        }
        */

        CsvContext cc = new CsvContext();
        CsvFileDescription tInputFileDescription = new CsvFileDescription
        {
            MaximumNbrExceptions = 50,
            FirstLineHasColumnNames = false,
            IgnoreUnknownColumns = true,
            EnforceCsvColumnAttribute = true,
        };

        string tText = Application.streamingAssetsPath + "/" + DIR + "/" + LANG + "/"
            + FILENAME + EXTENSION;

#if UNITY_ANDROID
        WWW reader = new WWW(tText);
        while (!reader.isDone) { }
        using (StreamReader s = GenerateStreamFromString(reader.text))
#else
        using (StreamReader s = new StreamReader(tText)) 
#endif
        {
            IEnumerable<LangLine> tLines = cc.Read<LangLine>(s,
                tInputFileDescription);
            _strings = new Dictionary<string, string>();
            foreach (LangLine tLine in tLines)
            {
                _strings.Add(tLine.key, tLine.value);
            }
        }
    }

    public static StreamReader GenerateStreamFromString(string s)
    {
        MemoryStream stream = new MemoryStream();
        StreamWriter writer = new StreamWriter(stream);
        writer.Write(s);
        writer.Flush();
        stream.Position = 0;
        return new StreamReader(stream, System.Text.Encoding.UTF8, true); ;
    }

    public static string GetText(string aKey)
    {
        if (Inst._strings.ContainsKey(aKey))
            return Inst._strings[aKey].Replace("\\n", "\n");
        return aKey;
    }

    public static string FillText(string aText)
    {
        return Regex.Replace(aText, REGEX, Replace);
    }

    static string Replace(Match tMatch)
    {
        string tStr = tMatch.Value.Substring(1, tMatch.Value.Length - 2);
        return GetText(tStr);
        //return CGameStrings.Inst.GetGeneral(tStr);
    }
}
