﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CGameText : MonoBehaviour
{
    [SerializeField]
    bool _allCaps;

    string _key;
    Text _text;
    TextMeshProUGUI _tmpUI;
    TextMeshPro _tmp;
    
    void Awake()
    {
        _text = GetComponent<Text>();
        if (_text != null)
            _key = _text.text;
        else
        {
            _tmpUI = GetComponent<TextMeshProUGUI>();
            if (_tmpUI != null)
                _key = _tmpUI.text;
            else
            {
                _tmp = GetComponent<TextMeshPro>();
                if (_tmp != null)
                    _key = _tmp.text;
            }
        }
    }

    private void OnEnable()
    {
        string tText = CLanguage.FillText(_key);
        if (_allCaps)
            tText = tText.ToUpper();
        if (_text != null)
            _text.text = tText;
        else if (_tmp != null)
            _tmp.text = tText;
        else if (_tmpUI != null)
            _tmpUI.text = tText;
    }

    public void ChangeKey(string aNewKey)
    {
        _key = aNewKey;
        OnEnable();
    }
}
