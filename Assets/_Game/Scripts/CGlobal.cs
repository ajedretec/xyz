﻿using System.Collections;
using UnityEngine;

public class CGlobal : MonoBehaviour
{
    const string PREF_PATH = "Prefab/Global";
    static CGlobal _inst;
    public static CGlobal Inst
    {
        get
        {
            if (_inst == null)
                _inst = FindObjectOfType<CGlobal>();
            if (_inst == null)
                _inst = Instantiate(Resources.Load<GameObject>(PREF_PATH)).GetComponent<CGlobal>();
            return _inst;
        }
    }

    //Serialized
    [SerializeField]
    CSceneList _sceneList;
    [SerializeField]
    int _mainMenuScene;

    private void Awake()
    {
        if (_inst != null && _inst != this)
        {
            Destroy(gameObject);
            return;
        }
        _inst = this;
        DontDestroyOnLoad(gameObject);
    }

    public static void LoadScene(int aSceneIndex)
    {
        Time.timeScale = 1;
        CLoadingScreen.LoadScene(Inst._sceneList.sceneIndices[aSceneIndex]);
    }

    public static void GoToMainMenu()
    {
        LoadScene(Inst._mainMenuScene);
    }

    public static void ExecuteNextFrame(System.Action aCallback, int aFrames = 1)
    {
        Inst.StartCoroutine(Inst.ExecuteNextCoroutine(aCallback, aFrames));
    }

    public static void ExecuteAfterTime(System.Action aCallback, float aTime)
    {
        Inst.StartCoroutine(Inst.ExecuteAfterTimeCoroutine(aCallback, aTime));
    }

    IEnumerator ExecuteNextCoroutine(System.Action aCallback, int aFrames = 1)
    {
        for (int i = 0; i < aFrames; i++)
            yield return null;
        aCallback();
    }

    IEnumerator ExecuteAfterTimeCoroutine(System.Action aCalback, float aTime)
    {
        float tTime = 0;
        while (tTime < aTime)
        {
            tTime += Time.deltaTime;
            yield return null;
        }
        aCalback();
    }
}
