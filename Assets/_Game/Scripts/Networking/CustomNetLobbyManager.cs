﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;

public class CustomNetLobbyManager : NetworkLobbyManager
{
    System.Action<NetworkConnection> _onDisconnect;
    string _originalLobbyScene = "";

    public override void OnLobbyStartHost()
    {
        base.OnLobbyStartHost();
        SecretObjectiveLobbyUI.Inst.ShowLobby();
    }

    public override void OnLobbyClientConnect(NetworkConnection conn)
    {
        base.OnLobbyClientConnect(conn);
        SecretObjectiveLobbyUI.Inst.ShowLobby();
    }

    public override void OnServerAddPlayer(NetworkConnection conn, short playerControllerId)
    {
        string name = SceneManager.GetActiveScene().name;
        if (autoCreatePlayer && name != lobbyScene) //This means we're in the game scene
        {
            GameObject player = Instantiate(gamePlayerPrefab,
                Vector3.zero, Quaternion.identity);

            NetworkServer.AddPlayerForConnection(conn, player, playerControllerId);
            return;
        }

        base.OnServerAddPlayer(conn, playerControllerId);
    }

    public static void DestroyObj()
    {
        if (singleton != null)
            Destroy(singleton.gameObject);
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        if (_onDisconnect != null)
            _onDisconnect(conn);
    }

    public override void OnServerDisconnect(NetworkConnection conn)
    {
        base.OnServerDisconnect(conn);
        if (_onDisconnect != null)
            _onDisconnect(conn);
    }

    public void AddOnDisconnectCallback(System.Action<NetworkConnection> aCallback)
    {
        _onDisconnect -= aCallback;
        _onDisconnect += aCallback;
    }

    public void RemoveOnDisconnectCallback(System.Action<NetworkConnection> aCallback)
    {
        _onDisconnect -= aCallback;
    }

    public override void ServerChangeScene(string sceneName)
    {
        if (sceneName == playScene)
            base.ServerChangeScene(sceneName);
    }

    public override void OnStartClient(NetworkClient lobbyClient)
    {
        if (_originalLobbyScene != "")
            lobbyScene = _originalLobbyScene; // Ensures the client loads correctly
        base.OnStartClient(lobbyClient);
    }
    public override void OnStopClient()
    {
        if (lobbyScene != "")
            _originalLobbyScene = lobbyScene;
        lobbyScene = ""; // Ensures we don't reload the scene after quitting
        base.OnStopClient();
    }
    public override void OnStartServer()
    {
        if (_originalLobbyScene != "")
            lobbyScene = _originalLobbyScene; // Ensures the server loads correctly
        base.OnStartServer();
    }
    public override void OnStopServer()
    {
        if (lobbyScene != "")
            _originalLobbyScene = lobbyScene;
        lobbyScene = ""; // Ensures we don't reload the scene after quitting
        base.OnStopServer();
    }
}
