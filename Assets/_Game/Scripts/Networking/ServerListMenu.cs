﻿using System.Collections.Generic;
using UnityEngine;

public class ServerListMenu : MonoBehaviour
{
    [SerializeField]
    ServerData _serverDataPref;
    [SerializeField]
    Transform _serverListRoot;

    List<ServerData> _hosts;

    private void Awake()
    {
        _hosts = new List<ServerData>();
    }

    private void Start()
    {
        SecretObjectiveLobbyUI.Inst.AddClientFindServerCallback(ReceivedBroadcast);
    }

    private void ReceivedBroadcast(string address, string data)
    {
        string[] items = data.Split(':');
        if (items.Length == 3 && items[0] == "NetworkManager")
        {
            string tAddress = items[1];
            int tPort;
            bool tParsed = int.TryParse(items[2], out tPort);
            if (!tParsed)
                tPort = UnityEngine.Networking.NetworkManager.singleton.networkPort;
            if (!ServerListed(tAddress, tPort))
            {
                ServerData tData = Instantiate(_serverDataPref, _serverListRoot, false);
                tData.SetData(tAddress, tPort);
                _hosts.Add(tData);
            }
        }
    }

    private bool ServerListed(string aServer, int port)
    {
        for(int i = 0; i < _hosts.Count; i++)
        {
            ServerData tData = _hosts[i];
            if (tData.address == aServer && tData.port == port)
                return true;
        }
        return false;
    }
}
