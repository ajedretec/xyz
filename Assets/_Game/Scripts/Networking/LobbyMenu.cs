﻿using UnityEngine;

public class LobbyMenu : MonoBehaviour
{
    [SerializeField]
    Transform _lobbyPlayersRoot;

    public static LobbyMenu Inst { get; private set; }

    private void Awake()
    {
        Inst = this;
    }

    public void AddPlayer(CLobbyPlayer aPlayer)
    {
        aPlayer.transform.SetParent(_lobbyPlayersRoot);
    }
}
