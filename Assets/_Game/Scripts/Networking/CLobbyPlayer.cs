﻿using UnityEngine;
using UnityEngine.Networking;

public class CLobbyPlayer : NetworkLobbyPlayer
{
    [SerializeField]
    GameObject _readyObj;
    [SerializeField]
    string _readyKey = "[READY]";
    [SerializeField]
    string _notReadyKey = "[NOT_READY]";
    [SerializeField]
    CGameText _readyBtnText;
    [SerializeField]
    GameObject _readyBtn;

    private void Start()
    {
        _readyBtn.SetActive(isLocalPlayer);
        _readyObj.SetActive(false);
        _readyBtnText.ChangeKey(_readyKey);

        LobbyMenu.Inst.AddPlayer(this);
        transform.localScale = Vector3.one;
    }

    public void Ready(bool aReady)
    {
        if (aReady)
            SendReadyToBeginMessage();
        else
            SendNotReadyToBeginMessage();
    }

    public override void OnClientReady(bool readyState)
    {
        base.OnClientReady(readyState);
        if (readyState)
        {
            _readyObj.SetActive(true);
            _readyBtnText.ChangeKey(_notReadyKey);
        }
        else
        {
            _readyObj.SetActive(false);
            _readyBtnText.ChangeKey(_readyKey);
        }
    }

    public void ToggleReady()
    {
        Ready(!readyToBegin);
    }
}
