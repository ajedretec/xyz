﻿using TMPro;
using UnityEngine;

public class ServerData : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _text;
    
    public string address { get; private set; }
    public int port { get; private set; }

    public void SetData(string aAddress, int aPort)
    {
        address = aAddress;
        port = aPort;
        _text.text = address + ":" + port.ToString();
    }

    public void Connect()
    {
        SecretObjectiveLobbyUI.Inst.StartClient(address, port);
    }
}
