﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CambioReglas : CGameManager
{
    [SerializeField]
    TextMeshProUGUI _title;
    [SerializeField]
    TextMeshProUGUI _description;
    [SerializeField]
    CStringList _stringList;
    [SerializeField]
    float _minTime = 10;
    [SerializeField]
    float _maxTime = 15;
    [SerializeField]
    int _targetScene = 0;
    [SerializeField]
    Image _background;

    private int rand = 0;
    private float t = 0.0f;
    private int z;
    GameObject fondo;

    Sprite[] r = new Sprite[9];

    Sprite r1, r2, r3, r4, r5, r6,r7,r8;

    void Start()
    {
        rand = Random.Range(1, 9);
        t = Random.Range(30, 50);
        z = Random.Range(3, 9);

        SetRandomRule();


        r[1] = Resources.Load("1", typeof(Sprite)) as Sprite;
        r[2] = Resources.Load("2", typeof(Sprite)) as Sprite;
        r[3] = Resources.Load("3", typeof(Sprite)) as Sprite;
        r[4] = Resources.Load("4", typeof(Sprite)) as Sprite;
        r[5] = Resources.Load("5", typeof(Sprite)) as Sprite;
        r[6] = Resources.Load("6", typeof(Sprite)) as Sprite;
        r[7] = Resources.Load("7", typeof(Sprite)) as Sprite;
        r[8] = Resources.Load("8", typeof(Sprite)) as Sprite;

        rand = Random.Range(1, 9);
        fondo = GameObject.Find("GameCanvas/Image");
        fondo.transform.GetComponent<UnityEngine.UI.Image>().sprite = r[rand];
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameUI.IsBlocked())
            return;

        t -= Time.deltaTime;

        z = Random.Range(3, 8);
        if (t.CompareTo(z) == -1)
        {
            int i = (int)t;

            if ((i % 2).Equals(0))
            {
                _background.color = Color.white;
            }
            else
            {
                _background.color = Color.gray;
            }
        }

        if (t.CompareTo(0) == -1)
        {
            SetRandomRule();
        }

        //print (t);
    }

    void SetRandomRule()
    {
        //rand = Random.Range(1, _stringList.titles.Length + 1);

        rand = Random.Range(1, 9);
        t = Random.Range(_minTime, _maxTime);
        _background.color = Color.white;

        //if (rand < _stringList.titles.Length)
        if (rand < 9)
        {
            _title.text = CLanguage.GetText(_stringList.titles[rand]);
            _description.text = CLanguage.GetText(_stringList.strings[rand]);
            fondo = GameObject.Find("GameCanvas/Image");
            fondo.transform.GetComponent<UnityEngine.UI.Image>().sprite = r[rand];
            //fondo.transform.GetComponent<UnityEngine.UI.Image>().sprite = r[1];

        }
        else
        {

            fondo = GameObject.Find("GameCanvas/Image");
            fondo.transform.GetComponent<UnityEngine.UI.Image>().sprite = r[rand];
            // Turnos.SetCambioR(true);
            //CGlobal.LoadScene(_targetScene);
        }
    }

    /*
    void OnGUI()
    {
        Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 24;

        GUIStyle myStyle2 = new GUIStyle();
        myStyle2.font = myFont;
        myStyle2.fontSize = 56;

        GUI.color = Color.black;

        GUI.skin.label.fontSize = 32;

        switch (rand)
        {

            case 1:
                GUI.Label(new Rect(x, y2, 180, 200), " CAMBIO DE COLOR", myStyle2);
                GUI.skin.label.fontSize = 14;

                GUI.Label(new Rect(x, y, 180, 200), "Se da vuelta el tablero y mueve el color  \n de quien era el turno.", myStyle);

                break;
            case 2:
                GUI.Label(new Rect(x, y2, 180, 200), "   TORNADO", myStyle2);
                GUI.skin.label.fontSize = 14;
                GUI.Label(new Rect(x, y, 200, 200), "Todos los jugadores cambian hacia la \n silla de la derecha.", myStyle);
                break;
            case 3:
                GUI.Label(new Rect(x, y2, 200, 200), "  PASAPIEZAS", myStyle2);
                GUI.skin.label.fontSize = 14;
                GUI.Label(new Rect(x, y, 200, 200), "Todas las piezas que habíamos atrapado con \n anterioridad, pueden ser pasadas sin más \n al compañero de al lado.", myStyle);
                break;
            case 4:
                GUI.Label(new Rect(x, y2, 180, 200), "    MUTANTE", myStyle2);
                GUI.skin.label.fontSize = 14;
                GUI.Label(new Rect(x, y, 180, 200), "De aquí en adelante, las piezas \n mueven de acuerdo  a la columna que ocupen. \n" +
                "Si un caballo está en la columna \n del alfil (“c” o “f”),  pues mueve \n como alfil," +
                "pero si desde ella se desplaza \n hasta  la columna “h”, inmediatamente \n pasa a ser torre…", myStyle);
                break;
            case 5:
                GUI.Label(new Rect(x, y2, 200, 200), "   COME COME", myStyle2);
                GUI.skin.label.fontSize = 14;
                GUI.Label(new Rect(x, y, 180, 200), "La partida ahora cambia de objetivo, \n y habrá que entregar todas las piezas, \n "
                    + "excepto el rey, ya que si tenemos \n nuevo cambio de " +
                       "reglas y hemos \n entregado el rey, no podremos \n seguir en las nuevas condiciones.", myStyle);
                break;
            case 6:
                GUI.Label(new Rect(x, y2, 200, 200), "  MARSELLES", myStyle2);
                GUI.skin.label.fontSize = 14;
                GUI.Label(new Rect(x, y, 180, 200), "Cada jugador mueve dos veces por turno.", myStyle);
                break;
            case 7:
                GUI.Label(new Rect(x, y2, 200, 200), " AGREGAR PUNTOS", myStyle2);
                GUI.skin.label.fontSize = 14;
                GUI.Label(new Rect(x, y, 180, 200), "Se tira un dado y el número que este \n indique determinará cuantos “puntos” \n" +
                    "agrega cada jugador a su ejército, \n en la casilla que desee,  \n" +
                     "y antes de retomar las acciones.", myStyle);
                break;
            case 8:
                GUI.Label(new Rect(x, y2, 200, 200), "   PROGRESIVO", myStyle2);
                GUI.skin.label.fontSize = 14;
                GUI.Label(new Rect(x, y, 200, 200), "De aquí en adelante, a quien corresponde \n mover arranca  con dos jugadas," +
                    "su oponente con tres, y así \n sucesivamente", myStyle);
                break;
            case 9: //tn.setTipoCarta ("");
                //tn.setCambioR(true);
                Application.LoadLevel("Cartas");
                break;

        }

    }*/
}
