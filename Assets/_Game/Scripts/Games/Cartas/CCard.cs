﻿using UnityEngine;

public class CCard : MonoBehaviour
{
    [SerializeField]
    SpriteRenderer _sprite;
    [SerializeField]
    SpriteRenderer _shade;
    [SerializeField]
    float _shadingTime = .3f;
    [SerializeField]
    float _flipTime = .3f;
    [SerializeField]
    float _zDist = -1;

    enum State
    {
        Idle,
        Flipping,
        Shading,
    }
    State _state;
    float _timer;

    Color _shadeColor;
    Color _shadeTransparent;

    Color _colorFrom;
    Color _colorTo;
    bool _doShade;
    bool _doFlip;
    Vector3 _scale;
    Sprite _newSprite;
    bool _flipped;
    Vector3 _origin;
    float _targetZ;

    private void Awake()
    {
        _shadeColor = _shade.color;
        _shadeTransparent = _shade.color;
        _shadeTransparent.a = 0;
        _shade.color = _shadeTransparent;
        _scale = _sprite.transform.localScale;
    }

    private void SetState(State aState)
    {
        _state = aState;
        _timer = 0;
        if (_state == State.Idle)
        {

        }
        else if (_state == State.Flipping)
        {
            _flipped = false;
            _doFlip = false;
            _origin = transform.position;
        }
        else if (_state == State.Shading)
        {
            _doShade = false;
        }
    }

    private void Update()
    {
        if (_state == State.Idle)
        {
            if (_doShade)
            {
                SetState(State.Shading);
                return;
            }
            if (_doFlip)
            {
                SetState(State.Flipping);
                return;
            }
        }
        else if (_state == State.Flipping)
        {
            float tHalfTime = _flipTime * .5f;
            if (_timer < tHalfTime)
            {
                _sprite.transform.localScale = Vector2.up * _scale.y
                    + Vector2.right * Mathf.Lerp(_scale.x, 0, _timer / tHalfTime );
                transform.position = _origin
                    + Vector3.forward * Mathf.Lerp(0, _zDist, _timer / tHalfTime);
            }
            else if (!_flipped)
            {
                _sprite.transform.localScale = Vector2.up;
                _sprite.sprite = _newSprite;
                _flipped = true;
                transform.position = _origin + Vector3.forward * _zDist;
            }
            else if (_timer < _flipTime)
            {
                _sprite.transform.localScale = Vector2.up * _scale.x
                    + Vector2.right * Mathf.Lerp(0, _scale.x, (_timer - tHalfTime) / tHalfTime);
                transform.position = _origin
                    + Vector3.forward * Mathf.Lerp(_zDist, 0, (_timer - tHalfTime) / tHalfTime);
            }
            else
            {
                _sprite.transform.localScale = _scale;
                transform.position = _origin;
                SetState(State.Idle);
                return;
            }
        }
        else if (_state == State.Shading)
        {
            if (_timer < _shadingTime)
            {
                _shade.color = Color.Lerp(_colorFrom, _colorTo, _timer / _shadingTime);
            }
            else
            {
                _shade.color = _colorTo;
                SetState(State.Idle);
                return;
            }
        }
        _timer += Time.deltaTime;
    }

    public void ShadeDark()
    {
        _colorTo = _shadeColor;
        _colorFrom = _shade.color;
        _doShade = true;
    }

    public void ShadeHide()
    {
        _colorTo = _shadeTransparent;
        _colorFrom = _shade.color;
        _doShade = true;
    }

    public void Flip(Sprite aSprite)
    {
        _newSprite = aSprite;
        _doFlip = true;
    }

    public void SetSprite(Sprite aSprite)
    {
        _sprite.sprite = aSprite;
    }

    private void OnMouseDown()
    {
        CardsManager.Inst.CardTouched(this);
    }
}
