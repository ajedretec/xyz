﻿using UnityEngine;

public class CardsManager : CGameManager
{
    [SerializeField]
    CSpriteList _cardsList;
    [SerializeField]
    CSpriteList _powersList;
    [SerializeField]
    CCard _card1;
    [SerializeField]
    CCard _card2;
    [SerializeField]
    Color _cardsBG;
    [SerializeField]
    Color _powerBG;
    [SerializeField]
    Color _cardsPlusPowerBG;
    [SerializeField]
    Sprite _cardPowerBack;
    [SerializeField]
    Sprite _cardSimpleBack;
    [SerializeField]
    Sprite _cardCombinedBack;

    Sprite _cardBackSprite;
    Sprite[] _sprites;
    int _currentCard = -1;

    public static CardsManager Inst { get; private set; }

    protected override void Start()
    {

        if (Turnos.geTipoCarta() == MainMenu.CARDS)
        {
            _gameName = "[CARDS_NAME]";
            _sprites = _cardsList.sprites;
            Camera.main.backgroundColor = _cardsBG;
            _cardBackSprite = _cardSimpleBack;
        }
        else if (Turnos.geTipoCarta() == MainMenu.POWER)
        {
            _cardBackSprite = _cardPowerBack;
            GameObject shade = GameObject.Find("Card1");

            _cardBackSprite = _cardPowerBack;
            _cardBackSprite = _cardPowerBack;
            _gameName = "[POWER_CARDS_NAME]";
            _sprites = _powersList.sprites;
            Camera.main.backgroundColor = _powerBG;
        }
        else if (Turnos.geTipoCarta() == MainMenu.CARDS_PLUS_POWER)
        {
            _cardBackSprite = _cardCombinedBack;
            _gameName = "[CARDS_PLUS_POWER_NAME]";
            _sprites = new Sprite[_cardsList.sprites.Length + _powersList.sprites.Length];
            for (int i = 0; i < _cardsList.sprites.Length; i++)
                _sprites[i] = _cardsList.sprites[i];
            for (int i = 0; i < _powersList.sprites.Length; i++)
                _sprites[_cardsList.sprites.Length + i] = _powersList.sprites[i];
            Camera.main.backgroundColor = _cardsPlusPowerBG;
            
        }

        _card1.SetSprite(_cardBackSprite);
        _card2.SetSprite(_cardBackSprite);

        base.Start();
    }

 

    private void Awake()
    {
        Inst = this;
    }

    Sprite RandomCard()
    {
        return _sprites[Random.Range(0, _sprites.Length)];
    }

    public void CardTouched(CCard aCard)
    {
        if (_gameUI.IsBlocked())
            return;
        if (aCard == _card1 && _currentCard != 0)
        {
            _card1.Flip(RandomCard());
            _card2.SetSprite(_cardBackSprite);
            //_card1.ShadeHide();
            //_card2.ShadeDark();
            _currentCard = 0;
        }
        else if (aCard == _card2 && _currentCard != 1)
        {
            _card2.Flip(RandomCard());
            _card1.SetSprite(_cardBackSprite);
            //_card2.ShadeHide();
            //_card1.ShadeDark();
            _currentCard = 1;
        }
    }
}
