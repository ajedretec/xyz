﻿using UnityEngine;
using System.Collections;

public class cambio : MonoBehaviour {

    private SpriteRenderer sr;
    private SpriteRenderer alfil;
    private SpriteRenderer sr2;
    private SpriteRenderer sr3;
    private bool derb=false;
    private string der;
    private string izq;
    private string d = "der";
    private string i = "izq";
    private string turno;
    private bool aux = false;
    private string TipoCarta;
    private Color c = Color.grey;
    private bool cambioR= false;
    private int rand=0;
    private GameObject myObj;
    private GameObject actual;
    private GameObject myObj2;
    private GameObject myObj3;
    private GameObject fondo; 
    private Turnos tn;
    private float t;
    private int t2;
    private bool color = false;
    
    void Start () {
        sr = this.GetComponent<SpriteRenderer> ();
        tn = GameObject.Find ("pFondo").GetComponent<Turnos> ();
        cambioR= Turnos.GetCambioR();
        if (cambioR){
            t = Random.Range(20,20);
        }
    }

    void Update () {

        if(cambioR){

            print (t);
            t-=Time.deltaTime;

            if (t.CompareTo(0).Equals(-1)){
                Application.LoadLevel("CambioDeReglas");
            }
            if (t.CompareTo(5).Equals(-1)){
                color= true;
            }

        }
    }
    
    void OnMouseDown(){

        c.a=1.0f;
        c.b=0.2f;
        c.g=0.2f;
        c.r=0.2f;

        Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
        mousePosition.z = 0;

        tn = GameObject.Find ("pFondo").GetComponent<Turnos> ();
        der = tn.getDer ();
        izq = tn.getIzq ();
        turno = tn.getTurno ();

        if ((mousePosition.x < 0)) {

            if (!(turno.Equals ("der"))) {
                derb = false;
                myObj2 = GameObject.Find (der);		
                sr2 = myObj2.GetComponent<SpriteRenderer> ();
                sr2.color = c;
                myObj3 = GameObject.Find (izq);
                sr3 = myObj3.GetComponent<SpriteRenderer> ();
                sr3.color = c;
                tn.setTurno ("der");
                aux = true;
            }

        } else if ((mousePosition.x > 0)) {

            if (!(turno.Equals ("izq"))) {
                derb = true;
                myObj2 = GameObject.Find (izq);	
                sr2 = myObj2.GetComponent<SpriteRenderer> ();
                sr2.color = c;
                myObj3 = GameObject.Find (der);
                sr3 = myObj3.GetComponent<SpriteRenderer> ();
                sr3.color = c;
                tn.setTurno ("izq");
                aux = true;
            }   
        }
         

        if (aux) {

            TipoCarta = Turnos.geTipoCarta ();
            if (TipoCarta.Equals ("cartas")) {
                rand = Random.Range (0, 12);
            } else if (TipoCarta.Equals ("poder")) {
                rand = Random.Range (11, 24);
            } else if (TipoCarta.Equals ("cartasmaspoder")) {
                rand = Random.Range (0, 24);
            }



            switch (rand) {
            case 0:
                myObj = GameObject.Find ("pAlfil");
                break;
            case 1:
                myObj = GameObject.Find ("pRey");
                break;
            case 2:
                myObj = GameObject.Find ("pTorreCaballo");
                break;
            case 3:
                myObj = GameObject.Find ("pPeon");
                break;
            case 4:
                myObj = GameObject.Find ("pComodin");
                break;
            case 5:
                myObj = GameObject.Find ("pPeonTorre");
                break;
            case 6:
                myObj = GameObject.Find ("pReinaAlfil");
                break;
            case 7:
                myObj = GameObject.Find ("pReina");
                break;
            case 8:
                myObj = GameObject.Find ("pReyPeon");
                break;
            case 9:
                myObj = GameObject.Find ("pTorre");
                break;

            case 10:
                myObj = GameObject.Find("pCaballoReyReina");
                break;

            case 11:
                myObj = GameObject.Find ("pPeonCaballoAlfil");
                break;
            case 12:
                myObj = GameObject.Find ("p_AgregarAlfil");
                break;
            case 13:
                myObj = GameObject.Find ("p_AgregarTorre");
                break;
            case 14:
                myObj = GameObject.Find ("p_AgregarPeon");
                break;
            case 15:
                myObj = GameObject.Find ("p_AgregarCaballo");
                break;
            case 16:
                myObj = GameObject.Find ("p_CasillaProhibida");
                break;
            case 17:
                myObj = GameObject.Find ("p_DestruirPeon");
                break;
            case 18:
                myObj = GameObject.Find ("p_Mover2Veces");
                break;
            case 19:
                myObj = GameObject.Find ("p_PiezaQueSalta");
                break;
            case 20:
                myObj = GameObject.Find ("p_RetrocederJugada");
                break;
            case 21:
                myObj = GameObject.Find ("p_CongelarPieza");
                break;
            case 22:
                myObj = GameObject.Find ("p_ColocarPared");
                break;
            case 23:
                myObj = GameObject.Find ("p_PiezaIntocable");
                break;

            }

            GameObject nuevo = (GameObject)Instantiate (myObj, transform.position, transform.rotation);



            if (derb) {
                nuevo.name = (nuevo.name + "_der");
                tn.setDer (nuevo.name); //nuevo valor derecha
            } else {
                nuevo.name = (nuevo.name + "_izq");
                tn.setIzq (nuevo.name); //nuevo valor izquierda
            }
    
            print ("der: " + der + " izq: " + izq);
            Destroy (this.gameObject);
        }
        aux=false;
    }

    void OnGUI() {

        if (cambioR){
            t2= (int) t;
            GUI.skin.label.fontSize = 30;

            if(!color){
                    GUI.color = Color.black; 
                }
            else
            {
                if((t2%2).Equals(0)){
                    GUI.color = Color.blue; 
                }
                else{
                    GUI.color = Color.red; 
                }
            }	
            if (cambioR){
                GUI.Label (new Rect (Screen.width/7,Screen.height/1.2f, 400, 400),"AJEDREZ CON CARTAS");
            }
        }
    }
}


