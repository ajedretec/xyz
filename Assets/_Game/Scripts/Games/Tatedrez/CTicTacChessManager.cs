﻿using DoozyUI;
using TMPro;
using UnityEngine;

public class CTicTacChessManager : CGameManager
{
    [SerializeField]
    TextMeshProUGUI _lvlTxt;
    [SerializeField]
    GameObject _lvlContainer;
    [SerializeField]
    int _maxLvl = 2;
    [SerializeField]
    UIElement _lvlSelectMenu;
    [SerializeField]
    UIElement _messageBox;
    [SerializeField]
    float _msgShowTime = 3f;
    [SerializeField]
    TextMeshProUGUI _msgText;

    Tatedrez _controller;

    public static CTicTacChessManager Inst { get; private set; }

    private void Awake()
    {
        Inst = this;
        _controller = FindObjectOfType<Tatedrez>();
        _controller.onSetLvl -= ShowLvl;
        _controller.onSetLvl += ShowLvl;
        _lvlSelectMenu.gameObject.SetActive(true);
    }

    protected override void Start()
    {
        base.Start();
        ShowLvl(_controller.level);
    }

    void ShowLvl(int aLevel)
    {
        if (aLevel < 0 || aLevel > _maxLvl)
            _lvlContainer.SetActive(false);
        else
        {
            _lvlContainer.SetActive(true);
            _lvlTxt.text = (aLevel + 1).ToString();
        }
    }

    public void PlayVsHuman()
    {
        _controller.setLevel(_maxLvl + 1);
        _lvlSelectMenu.Hide(false);
    }

    public void PlayVsCom(int aLvl)
    {
        _controller.setLevel(aLvl);
        _lvlSelectMenu.Hide(false);
    }

    public void PrintMessage(string aMsg)
    {
        _msgText.text = aMsg;
        _messageBox.Show(false);
        CGlobal.ExecuteAfterTime(() =>
        {
            _messageBox.Hide(false);
        }, _msgShowTime);
    }
}
