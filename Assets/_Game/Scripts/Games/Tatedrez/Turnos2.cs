﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Turnos2 : MonoBehaviour
{

    //Turnos 2 controla los turnos de Tatedrez
    /*
        static bool blancas = true;
        SpriteRenderer n;
        SpriteRenderer b;
        static int counter = 0;
        static bool posible;
        static string juego = "";
        GameObject myObj;
        GameObject nuevo;
        */


    static bool blancas;
    static System.Action<bool> _onChange;

    [SerializeField]
    bool _white;
    Image _image;
    static int counter = 0;
    static bool posible;
    static string juego = "";
    GameObject myObj;
    GameObject nuevo;


    //las blancas siempre tienen turnos pares, las negras impares.


    void Start()
    {
        blancas = true;
       // n = GameObject.Find("turno_n").GetComponent<SpriteRenderer>();
       // b = GameObject.Find("turno_b").GetComponent<SpriteRenderer>();
       // blancas = GameObject.Find("pFondo").GetComponent<Turnos>().getBlancas();
      //  counter = GameObject.Find("pFondo").GetComponent<Turnos>().getCounter();

        // myObj = GameObject.Find("ptNeutral");
        //nuevo = (GameObject)Instantiate(myObj, transform.position, transform.rotation);

        _image = GetComponent<Image>();
        _onChange -= OnTurn;
        _onChange += OnTurn;
       // blancas = FindObjectOfType<Turnos>().getBlancas();
        counter = FindObjectOfType<Turnos>().getCounter();
        OnTurn(blancas);
        /*
        n = GameObject.Find("turno_n").GetComponent<SpriteRenderer>();
        b = GameObject.Find("turno_b").GetComponent<SpriteRenderer>();
        */
        // myObj = GameObject.Find("ptNeutral");
        //nuevo = (GameObject)Instantiate(myObj, transform.position, transform.rotation);


    }

    // Update is called once per frame
    void Update()
    {

       /*     
        if (blancas)
        {

            b.enabled = true;
            n.enabled = false;

        }
        else
        {
            b.enabled = false;
            n.enabled = true;
        }
        */
    }






    private void OnDestroy()
    {
        _onChange -= OnTurn;
    }
    /*
    // Update is called once per frame
    void Update()
    {
        if (blancas)
        {
            b.enabled = true;
            n.enabled = false;
        }
        else
        {
            b.enabled = false;
            n.enabled = true;
        }

    }*/

    void OnTurn(bool aTurn)
    {
        if (aTurn == _white)
        {
            //blancas = true;
            _image.color = Color.white;
        }
        else
        {
            //blancas = false;
            _image.color = Color.clear;
        }
    }


    public void SetTurno(bool b)
    {
        blancas = b;
        counter++;
        if (_onChange != null)
            _onChange(b);
    }

    public bool getTurno()
    {
        return blancas;
    }

    public void setPosible(bool aux)
    {
        posible = aux;
    }

    public bool getPosible()
    {
        return posible;
    }

    public int getCounter()
    {
        return counter;
    }

    public void setCounter(int n)
    {
        counter = n;
    }

    public string getJuego()
    {
        return juego;
    }

    public void setJuego(string j)
    {
        juego = j;
    }

    public void setTurno(bool b)
    {

        blancas = b;
        counter++;
    }

}
