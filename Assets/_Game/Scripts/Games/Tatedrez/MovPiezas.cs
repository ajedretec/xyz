﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class MovPiezas : MonoBehaviour
{

    private GameObject a1, a2, a3, b1, b2, b3, c1, c2, c3;
    private GameObject myObj2;
    private Vector3 v, u;
    Transform startParent;
    GameObject pieza, slot;
    Vector3 startPosition;
    //string pieza;

    GridLayoutGroup aux;
    Canvas canvas;
    string cosa;
    private int i = 0;
    private int tope = 0;
    private Cas[] posibles = new Cas[15];
    string p;

    Turnos2 tn2;
    Transform po;
    Tatedrez t;
    bool alfil;
    bool caballo;
    bool torre;
    //Turnos2 tn2;


    void Start()
    {

        alfil = true;
        caballo = true;
        torre = true;

        t = GameObject.Find("pFondo").GetComponent<Tatedrez>();
        //tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();

        tn2 = FindObjectOfType<Turnos2>();

        a1 = GameObject.Find("a1/t_a1");
        a1.SetActive(false);

        a2 = GameObject.Find("a2/t_a2");
        a2.SetActive(false);

        a3 = GameObject.Find("a3/t_a3");
        a3.SetActive(false);

        b1 = GameObject.Find("b1/t_b1");
        b1.SetActive(false);

        b2 = GameObject.Find("b2/t_b2");
        b2.SetActive(false);

        b3 = GameObject.Find("b3/t_b3");
        b3.SetActive(false);

        c1 = GameObject.Find("c1/t_c1");
        c1.SetActive(false);

        c2 = GameObject.Find("c2/t_c2");
        c2.SetActive(false);

        c3 = GameObject.Find("c3/t_c3");
        c3.SetActive(false);
        //tn2.SetTurno(true);

    }

    void mover(int xx, int yy, string p)
    {

        print("MOVER" + p);
        int aux = 0;


        if (xx.Equals(1) && yy.Equals(1))
        {
            aux = 1;
        }
        else if (xx.Equals(1) && yy.Equals(2))
        {
            aux = 2;
        }
        else if (xx.Equals(1) && yy.Equals(3))
        {
            aux = 3;
        }
        else if (xx.Equals(2) && yy.Equals(1))
        {
            aux = 4;
        }
        else if (xx.Equals(2) && yy.Equals(2))
        {
            aux = 5;
        }
        else if (xx.Equals(2) && yy.Equals(3))
        {
            aux = 6;
        }
        else if (xx.Equals(3) && yy.Equals(1))
        {
            aux = 7;
        }
        else if (xx.Equals(3) && yy.Equals(2))
        {
            aux = 8;
        }
        else if (xx.Equals(3) && yy.Equals(3))
        {
            aux = 9;
        }

        switch (aux)
        {
            case 1:
                this.a1.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = a1.transform.parent;
                a1.SetActive(false);
                t.setCasilla("a1");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 2:
                this.a2.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = a2.transform.parent;
                a2.SetActive(false);
                t.setCasilla("a2");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 3:
                this.a3.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = a3.transform.parent;
                a3.SetActive(false);
                t.setCasilla("a3");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 4:

                this.b1.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = b1.transform.parent;
                b1.SetActive(false);
                t.setCasilla("b1");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 5:
                b2.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = b2.transform.parent;
                b2.SetActive(false);
                t.setCasilla("b2");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 6:
                b3.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = b3.transform.parent;
                b3.SetActive(false);
                t.setCasilla("b3");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 7:
                c1.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = c1.transform.parent;
                c1.SetActive(false);
                t.setCasilla("c1");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 8:
                c2.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = c2.transform.parent;
                c2.SetActive(false);
                t.setCasilla("c2");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
            case 9:
                c3.SetActive(true);
                pieza = GameObject.Find(p);
                pieza.transform.parent = c3.transform.parent;
                c3.SetActive(false);
                t.setCasilla("c3");
                t.insertar(p); //ojo con insertar pieza
                tn2.SetTurno(true);

                break;
        }
    }


    void Update()
    {

        if ((tn2.getTurno() == false) && (!(t.getLevel().Equals(3))))
        {
            i++;
            if ((i == 100))
            {

                posibles = t.get_Mov_legal_n();
                tope = t.getTope_n();
                Cas c;

                if (!(t.getLevel().Equals(0)))
                {
                    c = t.getCasGanadora();
                }
                else
                {
                    int rand = Random.Range(0, tope);
                    c = posibles[rand];

                }

                print("[" + c.getX() + "," + c.getY() + "] " + c.getPieza());

                p = c.getPieza();

                int xx = c.getX();
                int yy = c.getY();

                if (p.Equals("Alfil"))
                {
                    p = "bB";

                }
                else
                    if (p.Equals("Caballo"))
                {
                    p = "bN";

                }
                else
                    if (p.Equals("Torre"))
                {
                    p = "bR";

                }


                mover(xx, yy, p);


                i = 0;
            }
        }


    }

    void moverpieza_t(int x, int y, int i, int j)
    {

    }





}
