﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tatedrez : MonoBehaviour
{
    [SerializeField]
    string _moveToWinMsg = "[MOVE_TO_WIN]";
    [SerializeField]
    string _stopVictoryMsg = "[STOP_VICTORY]";
    private Turnos2 tn2;
    private string[,] board = new string[4, 4];
    private string[,] movPos = new string[4, 4];
    private string casilla = "null";
    private int i, j;
    private bool term = false;
    private string color = "";
    private bool empate = false;
    private Cas[] mov_legal = new Cas[50];
    private Cas[] mov_legal_n = new Cas[50];
    private int topeLegal = 0;
    private int topeLegal_n = 0;
    private int tope_c = 0;
    private int tope_a = 0;
    private int tope_t = 0;
    private int ntope_c = 0;
    private int ntope_a = 0;
    private int ntope_t = 0;
    private Cas[] mov_alfil = new Cas[10];
    private Cas[] mov_caballo = new Cas[10];
    private Cas[] mov_torre = new Cas[10];
    private Cas[] n_mov_alfil = new Cas[10];
    private Cas[] n_mov_caballo = new Cas[10];
    private Cas[] n_mov_torre = new Cas[10];
    private Cas[] mov_posibles = new Cas[15];
    private int[] pos_alfil = new int[2];
    private int[] pos_caballo = new int[2];
    private int[] pos_torre = new int[2];
    private Cas[] lista = new Cas[15];
    private int tope = 0;
    private int tope_ini = 0;
    private Cas[] historial = new Cas[50];
    private int th = 0;
    private Cas item;
    private Cas[] libres = new Cas[20];
    private int tope_libre = 0;
    private bool ntorre, nalfil, ncaballo, torre, caballo, alfil; //para indicar si se ha movido las piezas en los primeros turnos
    private float kk = 1.0f;
    private int ss = 0;
    private bool mensaje = false; //variable auxiliar
    private bool mensaje2 = false; //variable auxiliar
    private Font myFont;
    private Cas CasGanadora;
    private Cas CasPerdedora;
    private int iii = 0;//borrar
    private int jjj = 0;//borrar despues
    private string pp = ""; //borrae despues
    private bool meter = false;
    public int level { get; private set; }
    private string pieza;
    public System.Action<int> onSetLvl;
    private bool game_finished = false;

    public void setPieza(string aux)
    {
        pieza = aux;
    }

    public string getPieza()
    {
        return pieza;
    }

    private void Awake()
    {
        level = 0;
    }

    void Start()
    {
        item = new Cas(0, 0);
        tope_libre = 0;
        myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        tn2 = FindObjectOfType<Turnos2>();
        tn2.setJuego("t");

        ntorre = true;
        nalfil = true;
        ncaballo = true;
        torre = true;
        alfil = true;
        caballo = true;

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                board[i, j] = "";
            }
        }
    }

    public void setLevel(int aux)
    {
        level = aux;
        if (onSetLvl != null)
            onSetLvl(level);
    }


    public void insertar(string p)
    {
        eliminar(p);

        int a = 0;
        int b = 2;
        int k;
        bool aux = false;

        if ((p.Equals("bN")))
        {
            ncaballo = false;
        }
        else
        if ((p.Equals("bB")))
        {
            nalfil = false;
        }
        else
        if ((p.Equals("bR")))
        {
            ntorre = false;
        }
        else
        if ((p.Equals("wN")))
        {
            caballo = false;
        }
        else
        if ((p.Equals("wB")))
        {
            alfil = false;
        }
        else
        if ((p.Equals("wR")))
        {
            torre = false;
        }

        mensaje = false;
        mensaje2 = false;

        switch (casilla) //Cada vez que inserto una pieza blanca pongo a pensar a la maquina
        {
            case "a1":
                board[1, 1] = p;
                i = 1;
                j = 1;

                break;
            case "a2":
                board[1, 2] = p;
                i = 1;
                j = 2;

                break;
            case "a3":
                board[1, 3] = p;
                i = 1;
                j = 3;

                break;
            case "b1":
                board[2, 1] = p;
                i = 2;
                j = 1;

                break;
            case "b2":
                board[2, 2] = p;
                i = 2;
                j = 2;

                break;
            case "b3":
                board[2, 3] = p;
                i = 2;
                j = 3;

                break;
            case "c1":
                board[3, 1] = p;
                i = 3;
                j = 1;

                break;
            case "c2":
                board[3, 2] = p;
                i = 3;
                j = 2;

                break;
            case "c3":
                board[3, 3] = p;
                i = 3;
                j = 3;
                break;
        }

        if ((p.Contains("w")) && (level.Equals(0)))
        {
            n_movLegales();

        }
        if ((p.Contains("w")) && (!level.Equals(0)) && (!(level.Equals(3)))) // NO SON NECESARIOS TANTOS IF
        {
            th++;
            historial[th] = item;
            pieza = p;

            if (tn2.getCounter() > 1)
            {
                k = _alfaBeta(item, level, a, b, aux, p);
            }
            else
            {
                k = _alfaBeta(item, 1, a, b, aux, p);
            }
            th--;
        }
        else
        {
            //movLegales();
            //th++;
            //  historial[th] = item;
            // k = _alfaBeta(item, 1, b, a, true, p);
            // th--;
            //print("----------------------------------------------------------Algoritmo:" + k);

        }
         terminado(color, false, tn2.getCounter());
         terminado(color, true, tn2.getCounter());

    }

    public void CalcularVariantes(string pieza)
    {
        int a = 0;
        int b = 1;
        int k;
        bool aux = false;
        n_movLegales();
        th++;
        historial[th] = item;
        k = _alfaBeta(item, 2, a, b, aux, pieza);
        th--;
        print("----------------------------------------------------------Algoritmo:" + k);

    }

    public int[] getCoord(string p)
    {
        int[] pos = new int[2];
        switch (casilla)
        {
            case "a1":
                pos[0] = 1;
                pos[1] = 1;

                break;
            case "a2":

                pos[0] = 1;
                pos[1] = 2;
                break;
            case "a3":

                pos[0] = 1;
                pos[1] = 3;
                break;
            case "b1":

                pos[0] = 2;
                pos[1] = 1;
                break;
            case "b2":

                pos[0] = 2;
                pos[1] = 2;
                break;
            case "b3":

                pos[0] = 2;
                pos[1] = 3;
                break;
            case "c1":

                pos[0] = 3;
                pos[1] = 1;
                break;
            case "c2":
                pos[0] = 3;
                pos[1] = 2;
                break;
            case "c3":

                pos[0] = 3;
                pos[1] = 3;
                break;
        }
        return pos;
    }


    public string getContenido(string casilla)
    {

        int[] pos = new int[2];

        pos = getCoord("");


        return board[pos[0], pos[1]];
    }

    public void eliminar(string p)
    {

        for (int i = 1; i < 4; i++)
        {
            for (int j = 1; j < 4; j++)
            {
                if (board[i, j].Equals(p))
                {
                    board[i, j] = "";
                }
            }
        }
    }

    public bool EnLista(int x, int y)
    {

        bool aux = false;
        int i = 0;

        while ((!aux) && (i < tope))
        {
            aux = ((lista[i].getX().Equals(x)) && (lista[i].getY().Equals(y)));
            i++;
        }

        return aux;
    }

    public int[] getPos(string p)
    {
        int[] pos = new int[2];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 1; j < 4; j++)
            {
                if (board[i, j].Equals(p))
                {
                    pos[0] = i;
                    pos[1] = j;
                }
            }
        }

        return pos;
    }


    //Usar backtracking para mayor eficiencia

    public void MovTorre(int x, int y)
    {

        tope = 0;
        int i, j, x_ini, y_ini;
        bool libre;
        i = x;
        j = y;
        x_ini = x;
        y_ini = y;
        libre = true;

        //primero consulto abajo, itero sobre la columna j
        while ((j > 0) && (libre))
        {

            j--;

            if ((j > 0) && (board[x, j].Equals("")))
            {
                Cas item = new Cas(x, j);
                item.set_bx(x_ini);
                item.set_by(y_ini);
                item.setPieza("Torre");
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia arriba

        while ((j < 4) && (libre))
        {

            j++;
            if ((j < 4) && (board[x, j].Equals("")))
            { 
                Cas item = new Cas(x, j);
                item.setPieza("Torre");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;

        while ((i < 4) && (libre))
        {
            i++;
            if ((i < 4) && (board[i, y].Equals("")))
            { 
                Cas item = new Cas(i, y);
                item.setPieza("Torre");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;
        libre = true;

        while ((i > 0) && (libre))
        {
            i--;
            if ((i > 0) && (board[i, y].Equals("")))
            { //guardo la x y la j

                // print("IZQUIERDA" + i + " " + y);
                Cas item = new Cas(i, y);
                item.setPieza("Torre");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }


    }


    public void MovAlfil(int x, int y)
    {

        tope = 0;
        int i, j, x_ini, y_ini; ;

        bool libre;
        i = x;
        j = y;
        x_ini = x;
        y_ini = y;


        libre = true;

        //primero consulto abaja a la izquierda
        while ((j > 0) && (i > 0) && (libre))
        {

            j--;
            i--;

            if ((j > 0) && (i > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                // print("ABAJO - IZQUIERDA" + i + " " + j);

                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia ARRIBA - DERECHA

        while ((j < 4) && (i < 4) && (libre))
        {

            j++;
            i++;

            if ((j < 4) && (i < 4) && (board[i, j].Equals("")))
            { //guardo la x y la j
              // print("ARRIBA-DERECHA" + i + " " + j);

                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;


        //Subo la i y decremento la j
        while ((i < 4) && (j > 0) && (libre))
        {

            i++;
            j--;

            if ((i < 4) && (j > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("DERECHA- ABAJO" + i + " " + j);

                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;

        libre = true;

        //subo la i y decremento la j
        while ((i > 0) && (j < 4) && (libre))
        {
            i--;
            j++;
            if ((i > 0) && (j < 4) && (board[i, j].Equals("")))
            { //guardo la x y la j

                // print("IZQUIERDA - ARRIBA" + i + " " + j);
                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        //print("MovTorre: " + tope);
    }




    public void SaltoPosible(int i, int j)
    {
        int x_ini = i;
        int y_ini = j;

        if ((j < 4) && (i < 4) && (i > 0) && (j > 0) && (board[i, j].Equals("")))
        {
            Cas item = new Cas(i, j);
            item.setPieza("Caballo");
            item.set_bx(x_ini);
            item.set_by(y_ini);
            lista[tope] = item;
            tope++;
        }
    }


    public void MovCaballo(int x, int y)
    {

        tope = 0;

        if (tope < 3)
        {
            SaltoPosible(x + 2, y + 1);
            SaltoPosible(x + 2, y - 1);
        }

        if (tope < 3)
        {
            SaltoPosible(x - 2, y + 1);
            SaltoPosible(x - 2, y - 1);
        }

        if (tope < 3)
        {
            SaltoPosible(x + 1, y + 2);
            SaltoPosible(x - 1, y + 2);

        }

        if (tope < 3)
        {
            SaltoPosible(x + 1, y - 2);
            SaltoPosible(x - 1, y - 2);
        }


    }


    public bool HayEmpate(bool turno)
    {
        //dependiendo del turno, debo verificar que las piezas posean movimientos posibles.


        int[] b_pos = new int[2];
        int[] n_pos = new int[2];
        int[] r_pos = new int[2];

        if (turno)
        {
            b_pos = getPos("wB");
            n_pos = getPos("wN");
            r_pos = getPos("wR");
        }
        else
        {
            b_pos = getPos("bB");
            n_pos = getPos("bN");
            r_pos = getPos("bR");
        }

        MovAlfil(b_pos[0], b_pos[1]);
        if ((tope.Equals(0)))
        {
            MovCaballo(n_pos[0], n_pos[1]);
            if ((tope.Equals(0)))
            {
                MovTorre(r_pos[0], r_pos[1]);
                if ((tope.Equals(0)))
                {
                    //print("HAY EMPATE!!!!");
                    empate = true;
                }
            }

        }

        return empate;
    }



    public bool terminado(string c, bool turno, int jugadas)
    {
        bool aux = false;
        bool empate = false;

        //verticales
        if ((board[1, 1].Contains(c)) && (board[1, 2].Contains(c)) && (board[1, 3].Contains(c)))
        {
            aux = true;
            //print("[" + board[1, 1] + " " + board[1, 2] + " " + board[1, 3] + "]");
        }
        else
        if ((board[2, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[2, 3].Contains(c)))
        {
            aux = true;
            //print("[" + board[2, 1] + " " + board[2, 2] + " " + board[2, 3] + "]");
        }
        else
        if ((board[3, 1].Contains(c)) && (board[3, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = true;
         //   print("[" + board[3, 1] + " " + board[3, 2] + " " + board[3, 3] + "]");
        }
        else
        //laterales
        if ((board[1, 1].Contains(c)) && (board[2, 1].Contains(c)) && (board[3, 1].Contains(c)))
        {
            aux = true;
           // print("[" + board[1, 1] + " " + board[2, 1] + " " + board[3, 1] + "]");
        }
        else
        if ((board[1, 2].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 2].Contains(c)))
        {
            aux = true;
            //print("[" + board[1, 2] + " " + board[2, 2] + " " + board[3, 2] + "]");
        }
        else
        if ((board[1, 3].Contains(c)) && (board[2, 3].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = true;
            //print("[" + board[1, 3] + " " + board[2, 3] + " " + board[3, 3] + "]");
        }
        else

        //diagonales
        if ((board[1, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = true;
            //print("[" + board[1, 1] + " " + board[2, 2] + " " + board[3, 3] + "]");
        }
        else
        if ((board[1, 3].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 1].Contains(c)))
        {
            aux = true;
            //print("[" + board[1, 3] + " " + board[2, 2] + " " + board[3, 1] + "]");
        }

        string tMsg = "";
        if (aux)
        {
            tMsg = CLanguage.GetText(CGlobalTexts.VICTORY_MSG);
            //lista = MovTorre(2, 2);
            if (c.Equals("b"))
            {
                tMsg = tMsg.Replace(CGlobalTexts.COLOR_VAR,
                    CLanguage.GetText(CGlobalTexts.BLACK_PIECES));
                color = "NEGRAS";
            }
            else
            {
                tMsg = tMsg.Replace(CGlobalTexts.COLOR_VAR,
                    CLanguage.GetText(CGlobalTexts.WHITE_PIECES));
                color = "BLANCAS";
            }
        }
        else
        {
            if (jugadas > 4)
            {
                empate = HayEmpate(turno);
                if (empate)
                {
                    // print("COLOR = EMPATE");
                    tMsg = CLanguage.GetText(CGlobalTexts.TIE_MSG);
                    color = "EMPATE";
                }
            }
        }


        term = (aux) | (empate); // no terminó pero hay empate
        if (term)
            CTicTacChessManager.Inst.PrintMessage(tMsg);

        return term;
    }


    public Cas[] getListaPosible()
    {

        return lista;
    }

    public int getTope()
    {

        return this.tope;
    }

    public int getTope_n()
    {

        return this.topeLegal_n;
    }

    //una opcion es siempre ejectarlo independientemente de si es blancas o negras
    public Cas[] movLegales() //tengo que saber DE DONDE VENGO!!!
    {

        pos_alfil = getPos("wB");
        pos_caballo = getPos("wN");
        pos_torre = getPos("wR");


        if (!(pos_alfil[0].Equals(0)) && !(pos_alfil[1].Equals(0)))
        {
            MovAlfil(pos_alfil[0], pos_alfil[1]);
            mov_alfil = (Cas[])getListaPosible().Clone();
            tope_a = getTope();
        }

        if (!(pos_caballo[0].Equals(0)) && !(pos_caballo[1].Equals(0)))
        {
            MovCaballo(pos_caballo[0], pos_caballo[1]);
            mov_caballo = (Cas[])getListaPosible().Clone();
            tope_c = getTope();
        }

        if (!(pos_torre[0].Equals(0)) && !(pos_torre[1].Equals(0)))
        {
            MovTorre(pos_torre[0], pos_torre[1]);
            mov_torre = (Cas[])getListaPosible().Clone();


            tope_t = getTope();
        }

        topeLegal = tope_c + tope_a + tope_t;

        for (int i = 0; i < tope_a; i++)
        {
            Cas cas = new Cas(mov_alfil[i].getX(), mov_alfil[i].getY());
            cas.setPieza(mov_alfil[i].getPieza());

            cas.set_bx(mov_alfil[i].get_bx());
            cas.set_by(mov_alfil[i].get_by());

            mov_legal[i] = cas;
        }

        for (int i = 0; i < tope_c; i++)
        {
            Cas cas = new Cas(mov_caballo[i].getX(), mov_caballo[i].getY());
            cas.setPieza(mov_caballo[i].getPieza());

            cas.set_bx(mov_caballo[i].get_bx());
            cas.set_by(mov_caballo[i].get_by());

            mov_legal[tope_a + i] = cas;
        }

        for (int i = 0; i < tope_t; i++)
        {
            Cas cas = new Cas(mov_torre[i].getX(), mov_torre[i].getY());
            cas.setPieza(mov_torre[i].getPieza());

            cas.set_bx(mov_torre[i].get_bx());
            cas.set_by(mov_torre[i].get_by());

            mov_legal[tope_a + i + tope_c] = cas;
        }

        topeLegal = tope_c + tope_a + tope_t;
        mov_legal_ini();

        return mov_legal;
    }


    public Cas[] n_movLegales()
    {

        int c = tn2.getCounter();
        if (c > 4)
        {
            pos_alfil = getPos("bB");
            pos_caballo = getPos("bN");
            pos_torre = getPos("bR");

            if (!(pos_alfil[0].Equals(0)) && !(pos_alfil[1].Equals(0)))
            {
                MovAlfil(pos_alfil[0], pos_alfil[1]);
                n_mov_alfil = (Cas[])getListaPosible().Clone();
                ntope_a = getTope();
            }

            if (!(pos_caballo[0].Equals(0)) && !(pos_caballo[1].Equals(0)))
            {
                MovCaballo(pos_caballo[0], pos_caballo[1]);
                n_mov_caballo = (Cas[])getListaPosible().Clone();
                ntope_c = getTope();
            }

            if (!(pos_torre[0].Equals(0)) && !(pos_torre[1].Equals(0)))
            {
                MovTorre(pos_torre[0], pos_torre[1]);
                n_mov_torre = (Cas[])getListaPosible().Clone();
                ntope_t = getTope();
            }


            for (int i = 0; i < ntope_a; i++)
            {
                Cas cas = new Cas(n_mov_alfil[i].getX(), n_mov_alfil[i].getY());
                cas.setPieza(n_mov_alfil[i].getPieza());
                mov_legal_n[i] = cas;
            }

            for (int i = 0; i < ntope_c; i++)
            {
                Cas cas = new Cas(n_mov_caballo[i].getX(), n_mov_caballo[i].getY());
                cas.setPieza(n_mov_caballo[i].getPieza());
                mov_legal_n[ntope_a + i] = cas;
            }

            for (int i = 0; i < ntope_t; i++)
            {
                Cas cas = new Cas(n_mov_torre[i].getX(), n_mov_torre[i].getY());
                cas.setPieza(n_mov_torre[i].getPieza());
                mov_legal_n[ntope_a + i + ntope_c] = cas;
            }

            topeLegal_n = ntope_c + ntope_a + ntope_t;
        }
        else
        {
            topeLegal_n = 0;
            n_mov_legal_ini();
        }
        return mov_legal_n;
    }


    public Cas[] get_Mov_legal_n()
    {

        return mov_legal_n;
    }



    //al inicio siempre hay una 
    void mov_legal_ini()
    {
        int k = 0;
        tn2.setJuego("t");
        int c = tn2.getCounter();
        if (c < 5)
        {
            for (int i = 1; i < 4; i++)
            {
                for (int j = 1; j < 4; j++)
                {
                    if (board[i, j].Equals(""))
                    {/*
                        Cas cas = new Cas(i, j);
                        cas.setPieza("-");
                        cas.set_bx(0);
                        cas.set_by(0);
                        mov_legal[topeLegal + k] = cas;
                        k = k + 1;*/

                        if (alfil)
                        {
                            Cas cas = new Cas(i, j);
                            cas.setPieza("Alfil");
                            cas.set_bx(0);
                            cas.set_by(0);
                            mov_legal[topeLegal + k] = cas;
                            k = k + 1;
                        }
                        if (caballo)
                        {
                            Cas cas2 = new Cas(i, j);
                            cas2.setPieza("Caballo");
                            cas2.set_bx(0);
                            cas2.set_by(0);
                            mov_legal[topeLegal + k] = cas2;
                            k = k + 1;
                        }
                        if (torre)
                        {
                            Cas cas3 = new Cas(i, j);
                            cas3.setPieza("Torre"); //hay que cambiarlo!
                            cas3.set_bx(0);
                            cas3.set_by(0);
                            mov_legal[topeLegal + k] = cas3;
                            k = k + 1;
                        }
                    }
                }
            }
        }
        topeLegal = topeLegal + k;
    }

    void n_mov_legal_ini()
    {
        int k = 0;
        tn2.setJuego("t");
        int c = tn2.getCounter();
        if (c < 6)
        {
            for (int i = 1; i < 4; i++)
            {
                for (int j = 1; j < 4; j++)
                {
                    if (board[i, j].Equals(""))
                    {
                        if (nalfil)
                        {
                            Cas cas = new Cas(i, j);
                            cas.setPieza("Alfil");
                            cas.set_bx(0);
                            cas.set_by(0);
                            mov_legal_n[topeLegal_n + k] = cas;
                            k = k + 1;
                        }
                        if (ncaballo)
                        {
                            Cas cas2 = new Cas(i, j);
                            cas2.setPieza("Caballo");
                            cas2.set_bx(0);
                            cas2.set_by(0);
                            mov_legal_n[topeLegal_n + k] = cas2;
                            k = k + 1;
                        }
                        if (ntorre)
                        {
                            Cas cas3 = new Cas(i, j);
                            cas3.setPieza("Torre");
                            cas3.set_bx(0);
                            cas3.set_by(0);
                            mov_legal_n[topeLegal_n + k] = cas3;
                            k = k + 1;
                        }
                    }
                }
            }
            topeLegal_n = topeLegal_n + k;
        }
    }


    public Cas[] getLibres()
    {
        int k = 0;
        tope_libre = 0;
        {
            for (int i = 1; i < 4; i++)
            {
                for (int j = 1; j < 4; j++)
                {
                    if (board[i, j].Equals(""))
                    {
                        Cas cas = new Cas(i, j);
                        cas.setPieza("");
                        tope_libre++;
                        libres[tope_libre] = cas;

                    }
                }
            }
        }
        return libres;
    }

    private int evaluar(string c)
    {

        int aux = 0;

        if ((board[1, 1].Contains(c)) && (board[1, 2].Contains(c)) && (board[1, 3].Contains(c)))
        {
            aux = 1;
            print("#[1,1]:" + board[1, 1] + " [1,2]:" + board[1, 2] + " [1,3]:" + board[1, 3]);
        }
        else
       if ((board[2, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[2, 3].Contains(c)))
        {
            aux = 1;
            print("#[2,1]:" + board[2, 1] + " [2,2]:" + board[2, 2] + " [2,3]:" + board[2, 3]);

        }
        else
       if ((board[3, 1].Contains(c)) && (board[3, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = 1;
            print("#[3,1]:" + board[3, 1] + " [3,2]:" + board[3, 2] + "[3,3]:" + board[3, 3]);

        }
        else
       //laterales
       if ((board[1, 1].Contains(c)) && (board[2, 1].Contains(c)) && (board[3, 1].Contains(c)))
        {
            aux = 1;
            print("#[1,1]: " + board[1, 1] + " [2,1]: " + board[2, 1] + " [3,1]: " + board[3, 1]);

        }
        else
       if ((board[1, 2].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 2].Contains(c)))
        {

            aux = 1;
            print("#[1,2] " + board[1, 2] + "[2,2] " + board[2, 2] + " [3,3]" + board[3, 3]);

        }
        else
       if ((board[1, 3].Contains(c)) && (board[2, 3].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = 1;
            print("#[1,3]: " + board[1, 3] + "[2,3]: " + board[2, 3] + "[3,3]: " + board[3, 3]);

        }

        if ((board[1, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = 1;
            print("#[1,1]: " + board[1, 1] + "[2,2]: " + board[2, 2] + " [3,3]:" + board[3, 3]);

        }
        if ((board[3, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[1, 3].Contains(c)))
        {
            aux = 1;
            print("#[3,1]: " + board[3, 1] + " [2,2]:" + board[2, 2] + "[1,3]" + board[1, 3]);

        }



        return aux;
    }


    private Cas borrarPieza(string pieza, string p)
    {

        Cas c = new Cas(0, 0);
        c.setPieza("-");

        if (pieza.Equals("Alfil"))
        {
            pieza = p + "B";
        }
        else if (pieza.Equals("Caballo"))
        {
            pieza = p + "k";
        }
        else if (pieza.Equals("Torre"))
        {
            pieza = p + "R";
        }

        if (tn2.getCounter() > 4)
        {

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    if (board[i, j].Equals(pieza)) //elimino pieza de donde estaba anteriormente, porque es la que voy a mover.
                    {
                        board[i, j] = "";
                        print("ELIMINO PIEZA==========================================================" + pieza);
                        c = new Cas(i, j);
                        c.setPieza(pieza);
                     
                    };
                }
            }
        }
        return c;
    }




    private void insertarCasTablero(Cas cas, string c)
    {

        string pieza = "";

        string aux = cas.getPieza();

        if (aux.Equals("Alfil"))
        {

            pieza = c + "B";
        }
        else if (aux.Equals("Caballo"))
        {

            pieza = c + "k";

        }
        else if (aux.Equals("Torre"))
        {

            pieza = c + "R";

        }

        th++;
        historial[th] = cas;

        //eliminar(pieza); // a la hora de calcular, debo eliminar la pieza del escaque anterior, PERO TAMBIEN DEBO AGREGARLA DESPUES!!!
        print("Insertada pieza " + cas.getPieza() + " que estaba en: " + "[" + cas.get_bx() + "," + cas.get_by() + "]" + " en: " + "[" + cas.getX() + "," + cas.getY() + "]");
        //board[cas.get_bx(), cas.get_by()] = "";

        board[cas.getX(), cas.getY()] = pieza;
    }

    private void eliminarCasTablero(Cas cas, Cas cas_aux)
    {
        th--;
        board[cas.getX(), cas.getY()] = "";

        // if (meter) //Quito el movimiento anterior vuelvo a poner la pieza. Ojo con esa variable, puedo estar necesitando meter dos veces
        //{
        if (tn2.getCounter() > 4)
        {
            board[cas_aux.getX(), cas_aux.getY()] = cas_aux.getPieza();

        }

    }


    public int _alfaBeta(Cas node, int depth, int alpha = int.MinValue, int beta = int.MaxValue, bool maximizing = true, string c = "")
    {

        print("-----------------------------------------------------------------------------------------------------------------------");
        if (depth == 0)
        {

            int val = evaluar("w");
            int val2 = evaluar("b");

            for (int i = 1; i <= th; i++)
            {
                print("-[" + historial[i].getPieza() + historial[i].getX() + historial[i].getY() + "]");
            }
            if (val.Equals(1))
            {
                //mensaje2 = true; // Con esto descarto esta jugada.            
                CTicTacChessManager.Inst.PrintMessage(CLanguage.FillText(_stopVictoryMsg));                            
                val = 1;
            }
            else
            if (val2.Equals(1))
            {
                val = -1;
                //mensaje = true; //Con esto elijo la que gano   
                CTicTacChessManager.Inst.PrintMessage(CLanguage.FillText(_moveToWinMsg));                            
            }
            else
            {
                val = 0;
            }

            return val;
        }

        if (maximizing)
        {
            print("Entro a Maximizar");
            int v = int.MinValue;
            Cas[] replies = movLegales();

            for (int i = 0; i < topeLegal; i++)
            {
                //borrarPieza(replies[i].getPieza(), "w");
                Cas cas_aux_2 = borrarPieza(replies[i].getPieza(), "w");
                insertarCasTablero(replies[i], "w"); //hay que tener cuidado con esto
                int candidate = _alfaBeta(replies[i], depth - 1, alpha, beta, false, "b");
                eliminarCasTablero(replies[i], cas_aux_2);

                v = candidate > v ? candidate : v;

                alpha = alpha > v ? alpha : v;
                if (beta < alpha)
                {
                    break;
                }
            }
            // _board.revert();
            return v;
        }
        else
        {
            print("Entro a Minimizar");
            int v = int.MaxValue;
            Cas[] replies = n_movLegales();

            for (int i = 0; i < topeLegal_n; i++)
            {
                //borrarPieza(replies[i].getPieza());
                Cas cas_aux = borrarPieza(replies[i].getPieza(), "b");
                insertarCasTablero(replies[i], "b");
                int candidate = _alfaBeta(replies[i], depth - 1, alpha, beta, true, "w");
                eliminarCasTablero(replies[i], cas_aux);

                v = candidate < v ? candidate : v;

                if (beta > v)
                {
                    beta = v;
                    CasGanadora = replies[i];//Va guardando la casilla ganadora (la que tiene menos riesgo)

                    //node.bestResponse = reply; ACA ES DONDE GUARDA EL MOVIMIENTO...
                }
                if (beta < alpha) //Pareceria que fuera una poda.
                {
                    //mensaje = true;
                    break;
                }
            }
            //_board.revert();
            return v;
        }
    }


    public int getLevel()
    {
        //term = terminado(cas);
        return level;
    }


    public void setCasilla(string cas)
    {
        //term = terminado(cas);
        casilla = cas;
    }
    public string getCasilla()
    {
        return casilla;
    }

    public int getTope_libre()
    {
        return tope_libre;
    }

    public bool esGanador()
    {
        return mensaje;
    }

    public Cas getCasGanadora()
    {
        return CasGanadora;
    }

    public void setEsGanador(bool aux)
    {
        mensaje = aux;
    }


    public bool esPerdedor()
    {
        return mensaje2;
    }

    public Cas getCasPerdedor()
    {
        return CasPerdedora;
    }

    public void setPerdedor(bool aux)
    {
        mensaje2 = aux;
    }


}
