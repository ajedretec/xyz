﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class caballo_t : MonoBehaviour
{
    [SerializeField]
    GridLayoutGroup _boardRoot;
    private GameObject[,] board2 = new GameObject[7, 7];
    private string[,] board = new string[7, 7];
    private int[,] board3 = new int[7, 7];
    private string[,] movPos = new string[7, 7];
    private string casilla = "null";
    private int i, j;
    private bool term = false;
    private string color = "";
    private bool empate = false;
    static bool neutral_n = false;
    static bool neutral_b = false;
    private bool ganan_blancas = false;
    private bool ganan_negras = false;
    private string juego;
    private int p_blancas = 0;
    private int p_negras = 0;
    private Font myFont;
    Turnos2 tn2;
    monedas[,] _coins;
    int ii, jj;
    private Cas[] lista = new Cas[37];//hay casillas de más
    private int tope = 0;
    private GameObject pieza;
    Sprite sp;
    private Cas[] posibles = new Cas[37];
    Cas CasGanadora;
    int c_temp = 0;
    int counter = 0;
    int c_w = 0;
    int c_w_temp = 0;
    int iii = 0;
    int jjj = 0;
    string global_pieza = "";
    private Cas[] historial = new Cas[50];
    int th = 0;
    int tope_b;
    int tope_n;
    public int level { get; private set; }
    public System.Action<int> onSetLvl;


    void Start()
    {
        ii = 5;
        jj = 6;

        myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        tn2 = FindObjectOfType<Turnos2>();
        tn2.setJuego("c"); //Setea el Juego

        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {          
                board[i, j] = "c" + i.ToString() + j.ToString();
                board2[i, j] = GameObject.Find("c" + i.ToString() + j.ToString());
            }
        }

        board2[2, 1].GetComponent<monedas>().SetValue(0);
        board2[5, 6].GetComponent<monedas>().SetValue(0);
        board3[2, 1] = 0;
        board3[5, 6] = 0;



        for (int i = 1; i < 7; i++)
        {
            for (int j = 1; j < 7; j++)
            {
                board3[i, j] = board2[i, j].GetComponent<monedas>().GetValue();
            }
        }

        board2[2, 1].SetActive(false);
        board2[5, 6].SetActive(false);

        board[2, 1] = "wk1";
        board[5, 6] = "bk2";

        pieza = GameObject.Find("e6/bk2");
        juego = tn2.getJuego();
        _coins = new monedas[_boardRoot.constraintCount, _boardRoot.constraintCount];

        int x = 0;
        int y = 0;
        for (int i = 0; i < _boardRoot.transform.childCount; i++)
        {
            _coins[x, y] = _boardRoot.transform.GetChild(i).GetComponent<monedas>();
            x++;
            if (x >= _boardRoot.constraintCount)
            {
                x = 0;
                y++;
            }
        }
    }

    void Update()
    {

        if (!(level.Equals(4)))
        {

            if (tn2.getTurno() == false)
            {
                i++;
                counter = 0;
                if (i == 50)
                {

                    Cas item = new Cas(ii, jj);
                    item.setPieza("bk2");
                    item.set_coin(board2[ii, jj].GetComponent<monedas>().GetValue());

                    int i2 = ii;
                    int j2 = jj;
                    c_w = 0;
                    int res = 0;

                    if (!(level.Equals(0)))
                    {
                        historial[th] = item;
                        th++;
                        res = _alfaBeta(item, level, 0, 1, false, 0);
                        th--;
                    }
                    else
                    {

                        MovCaballo(ii, jj);
                        int rand = Random.Range(0, tope - 1);
                        CasGanadora = lista[rand];
                    }

                    int c3 = c_temp;

                    if ((res.Equals(0)) && !(level.Equals(0)))
                    {
                        MovCaballo(ii, jj);
                        int rand = Random.Range(0, tope - 1);
                        CasGanadora = lista[rand];
                    }


                    GameObject token = board2[CasGanadora.getX(), CasGanadora.getY()];

                    token.SetActive(true);
                    pieza.transform.parent = token.transform.parent;
                    token.SetActive(false);
                    ii = CasGanadora.getX();
                    jj = CasGanadora.getY();

                    GameObject coin = board2[ii, jj];

                    monedas md = coin.GetComponent<monedas>();
                    p_negras = p_negras + md.GetValue();
                    md.SetValue(0);

                    board3[ii, jj] = 0;

                    board[ii, jj] = "bk2";
                    board[i2, j2] = "";
                    token.SetActive(false);
                    tn2.SetTurno(true);
                    i = 0;

                    c_temp = 0;
                    counter = 0;
                }
            }
        }
    }


    public void setLevel(int aux)
    {
        level = aux;
        if (onSetLvl != null)
            onSetLvl(level);
    }

    private void Awake()
    {
        level = 0;
    }


    private int evaluar(Cas node, int moneda)
    {
        int aux = 0;
        int valor;

        valor = board2[node.getX(), node.getY()].GetComponent<monedas>().GetValue();
        // valor = board3[node.getX(), node.getY()];
        aux = moneda - valor;

        return aux;
    }


    public string get_space(int deep)
    {

        string aux = "";

        if (deep.Equals(0))
        {
            aux = "_____________________";
        }
        else
        if (deep.Equals(1))
        {
            aux = "__________";
        }
        else
        if (deep.Equals(2))
        {
            aux = "_";
        }

        return aux;
    }

    public int _alfaBeta(Cas node, int depth, int alpha = int.MinValue, int beta = int.MaxValue, bool maximizing = true, int c = 0, string turn = "negras")
    {
        if (depth == 0)
        {
            int val = evaluar(node, c);

            return val;
        }

        if (maximizing) //Aca les toca a las blancas
        {
            int[] cc = getPos("wk1");
            int v = int.MinValue;
            Cas item = new Cas(cc[0], cc[1]);
            item.setPieza("wk1");
            MovCaballo(item.getX(), item.getY());
            int tope_b = tope;
            Cas[] replies = (Cas[])lista.Clone();

            for (int i = 0; i < tope_b; i++)
            {
                replies[i].set_coin(board2[replies[i].getX(), replies[i].getY()].GetComponent<monedas>().GetValue());
                replies[i].setPieza("wk1");
                historial[th] = replies[i];
                th++;

                c_w = c;

                int candidate = _alfaBeta(replies[i], depth - 1, alpha, beta, false, c_w, "blancas");
                th--;

                v = candidate > v ? candidate : v;
                alpha = alpha > v ? alpha : v;

                if (beta < alpha)
                {
                    //    break;
                }
            }
            return v;
        }
        else
        {
            int[] cc = getPos("bk2");
            int v = int.MaxValue;

            MovCaballo(cc[0], cc[1]);

           // print("Depth " + depth + "POS: " + cc[0] + " " + cc[1]);
            Cas[] replies = (Cas[])lista.Clone();
            int tope_n = tope;
            int c_aux = c;

            for (int i = 0; i < tope_n; i++)
            {
                if (!(depth.Equals(1)))
                {
                    board[node.getX(), node.getY()] = "";
                }

                string anterior = board[replies[i].getX(), replies[i].getY()];
                board[replies[i].getX(), replies[i].getY()] = "bk2";

                if (!(depth.Equals(1)))
                {
                    c = -board2[replies[i].getX(), replies[i].getY()].GetComponent<monedas>().GetValue();
                }

                replies[i].set_coin(board2[replies[i].getX(), replies[i].getY()].GetComponent<monedas>().GetValue());
                historial[th] = replies[i];
                th++;

                int candidate = _alfaBeta(replies[i], depth - 1, alpha, beta, true, c, "negras");

                board[replies[i].getX(), replies[i].getY()] = anterior;

                if (!(depth.Equals(1)))
                {
                    //  board[node.getX(), node.getY()] = "bk2";
                }

                th--;

                v = candidate < v ? candidate : v;

                if ((beta > v))
                {

                    beta = v;

                    if (depth.Equals(1))
                    {
                        if ((board3[replies[i].getX(), replies[i].getY()].Equals(0) && !(c.Equals(0))))
                        {                           
                            CasGanadora = replies[i];
                        }
                        else
                        {
                            if (level.Equals(3))
                            {
                                print("^^^^^^^^^^^^^^");
                                CasGanadora = historial[th - 2];
                            }
                            else if (level.Equals(1)) {
                                CasGanadora = historial[th];
                            }

                            print("-------------------------------------------------------------------------------------" + CasGanadora.getPieza() + "_" + CasGanadora.getX() + "_" + CasGanadora.getY() + "candidate: " + candidate);
                          //  print("C AUX :" + c_aux);
                           // for (int ii = 0; ii < th + 1; ii++)
                            //{
                             // print(ii + " ->[" + historial[ii].getPieza() + historial[ii].getX() + historial[ii].getY() + "]  " + historial[ii].get_coin() + "<-");
                            //}

                            for (int ii = 0; ii < tope; ii++)
                            {
                              //  print(lista[ii].getX() + " " + lista[i].getY());
                            }
                        }

                    }
                    else
                    {
                        print("HOLA CARACOLA!!");
                        CasGanadora = replies[i];//Va guardando la casilla ganadora (la que tiene menos riesgo)
                    }

                }
                if (beta < alpha)
                {
                    //  break;
                }
                c = c_aux;
            }

            return v;
        }
    }

    public void insertar(string p)
    {

        //print(p);
        switch (casilla)
        {
            case "a1":
                board[1, 1] = p;
                i = 1;
                j = 1;
                break;
            case "a2":
                board[1, 2] = p;
                i = 1;
                j = 2;
                break;
            case "a3":
                board[1, 3] = p;
                i = 1;
                j = 3;
                break;
            case "a4":
                board[1, 4] = p;
                i = 1;
                j = 4;
                break;
            case "a5":
                board[1, 5] = p;
                i = 1;
                j = 5;
                break;
            case "a6":
                board[1, 6] = p;
                i = 1;
                j = 6;
                break;
            case "b1":
                board[2, 1] = p;
                i = 2;
                j = 1;
                break;
            case "b2":
                board[2, 2] = p;
                i = 2;
                j = 2;
                break;
            case "b3":
                board[2, 3] = p;
                i = 2;
                j = 3;
                break;
            case "b4":
                board[2, 4] = p;
                i = 2;
                j = 4;
                break;
            case "b5":
                board[2, 5] = p;
                i = 2;
                j = 5;
                break;
            case "b6":
                board[2, 6] = p;
                i = 2;
                j = 6;
                break;
            case "c1":
                board[3, 1] = p;
                i = 3;
                j = 1;
                break;
            case "c2":
                board[3, 2] = p;
                i = 3;
                j = 2;
                break;
            case "c3":
                board[3, 3] = p;
                i = 3;
                j = 3;
                break;
            case "c4":
                board[3, 4] = p;
                i = 3;
                j = 4;
                break;
            case "c5":
                board[3, 5] = p;
                i = 3;
                j = 5;
                break;
            case "c6":
                board[3, 6] = p;
                i = 3;
                j = 6;
                break;
            case "d1":
                board[4, 1] = p;
                i = 4;
                j = 1;
                break;
            case "d2":
                board[4, 2] = p;
                i = 4;
                j = 2;
                break;
            case "d3":
                board[4, 3] = p;
                i = 4;
                j = 3;
                break;
            case "d4":
                board[4, 4] = p;
                i = 4;
                j = 4;
                break;
            case "d5":
                board[4, 5] = p;
                i = 4;
                j = 5;
                break;
            case "d6":
                board[4, 6] = p;
                i = 4;
                j = 6;
                break;
            case "e1":
                board[5, 1] = p;
                i = 5;
                j = 1;
                break;
            case "e2":
                board[5, 2] = p;
                i = 5;
                j = 2;
                break;
            case "e3":
                board[5, 3] = p;
                i = 5;
                j = 3;
                break;
            case "e4":
                board[5, 4] = p;
                i = 5;
                j = 4;
                break;
            case "e5":
                board[5, 5] = p;
                i = 5;
                j = 5;
                break;
            case "e6":
                board[5, 6] = p;
                i = 5;
                j = 6;
                break;
            case "f1":
                board[6, 1] = p;
                i = 6;
                j = 1;
                break;
            case "f2":
                board[6, 2] = p;
                i = 6;
                j = 2;
                break;
            case "f3":
                board[6, 3] = p;
                i = 6;
                j = 3;
                break;
            case "f4":
                board[6, 4] = p;
                i = 6;
                j = 4;
                break;
            case "f5":
                board[6, 5] = p;
                i = 6;
                j = 5;
                break;
            case "f6":
                board[6, 6] = p;
                i = 6;
                j = 6;
                break;
        }


        if (p.Contains("w"))
        {

            // Cas item = new Cas(0, 0);
            // _alfaBeta(item, 1, 0, 2, false, 0);




        }

    }





    public int[] getCoord(string p)
    {
        int[] pos = new int[2];
        switch (casilla)
        {
            case "a1":
                pos[0] = 1;
                pos[1] = 1;

                break;
            case "a2":

                pos[0] = 1;
                pos[1] = 2;
                break;
            case "a3":

                pos[0] = 1;
                pos[1] = 3;
                break;
            case "a4":

                pos[0] = 1;
                pos[1] = 4;
                break;
            case "a5":

                pos[0] = 1;
                pos[1] = 5;
                break;
            case "a6":

                pos[0] = 1;
                pos[1] = 6;
                break;
            case "b1":

                pos[0] = 2;
                pos[1] = 1;
                break;
            case "b2":

                pos[0] = 2;
                pos[1] = 2;
                break;
            case "b3":

                pos[0] = 2;
                pos[1] = 3;
                break;
            case "b4":

                pos[0] = 2;
                pos[1] = 4;
                break;
            case "b5":

                pos[0] = 2;
                pos[1] = 5;
                break;
            case "b6":

                pos[0] = 2;
                pos[1] = 6;
                break;
            case "c1":

                pos[0] = 3;
                pos[1] = 1;
                break;
            case "c2":
                pos[0] = 3;
                pos[1] = 2;
                break;
            case "c3":
                pos[0] = 3;
                pos[1] = 3;
                break;
            case "c4":
                pos[0] = 3;
                pos[1] = 4;
                break;
            case "c5":
                pos[0] = 3;
                pos[1] = 5;
                break;
            case "c6":

                pos[0] = 3;
                pos[1] = 6;
                break;
            case "d1":

                pos[0] = 4;
                pos[1] = 1;
                break;
            case "d2":

                pos[0] = 4;
                pos[1] = 2;
                break;
            case "d3":

                pos[0] = 4;
                pos[1] = 3;
                break;
            case "d4":

                pos[0] = 4;
                pos[1] = 4;
                break;
            case "d5":

                pos[0] = 4;
                pos[1] = 5;
                break;
            case "d6":

                pos[0] = 4;
                pos[1] = 6;
                break;
            case "e1":

                pos[0] = 5;
                pos[1] = 1;
                break;
            case "e2":
                pos[0] = 5;
                pos[1] = 2;
                break;
            case "e3":

                pos[0] = 5;
                pos[1] = 3;
                break;
            case "e4":
                pos[0] = 5;
                pos[1] = 4;
                break;
            case "e5":

                pos[0] = 5;
                pos[1] = 5;
                break;
            case "e6":

                pos[0] = 5;
                pos[1] = 6;
                break;

            case "f1":
                pos[0] = 6;
                pos[1] = 1;
                break;
            case "f2":
                pos[0] = 6;
                pos[1] = 2;
                break;
            case "f3":
                pos[0] = 6;
                pos[1] = 3;
                break;
            case "f4":
                pos[0] = 6;
                pos[1] = 4;
                break;
            case "f5":

                pos[0] = 6;
                pos[1] = 5;
                break;
            case "f6":
                Debug.Log(pos);
                pos[0] = 6;
                pos[1] = 6;
                break;

        }
        return pos;
    }




    public int getValorMoneda(string name)
    {

        int valor = 0;

        if (name.Contains("1"))
            valor = 1;
        else if (name.Contains("2"))
            valor = 2;
        else if (name.Contains("3"))
            valor = 3;
        else if (name.Contains("4"))
            valor = 4;
        else if (name.Contains("5"))
            valor = 5;

        return valor;
    }


    public bool capturar_moneda(string casilla)
    {
        int[] pos = getCoord(casilla);

        if (board[pos[0], pos[1]].Contains("c"))
        {

            GameObject coin = GameObject.Find(board[pos[0], pos[1]]);
            // string name = coin.GetComponent<UnityEngine.UI.Image>().sprite.name;

            //print("Capturar moneda: " + name);
            int valor = 0;
            //valor = coin.GetComponent<monedas>().GetValue();
            valor = coin.GetComponent<monedas>().GetValue();

            if (!tn2.getTurno())
            {
                p_blancas = p_blancas + valor;
            }
            else
            {
                p_negras = p_negras + valor;
            }

            coin.GetComponent<monedas>().SetValue(0);
            coin.SetActive(false);
            // Destroy(coin);
            board[pos[0], pos[1]] = ""; //elimino la moneda del array tablero.
            board3[pos[0], pos[1]] = 0;

            return true;
        }
        else
            return false;


    }

    public void eliminar(string p)
    {

        for (int i = 1; i < 7; i++)
        {
            for (int j = 1; j < 7; j++)
            {
                if (board[i, j].Equals(p))
                {
                    board[i, j] = "";
                }
            }
        }
    }


    public void SaltoPosible(int i, int j)
    {

        if ((j < 7) && (i < 7) && (i > 0) && (j > 0) && ((board[i, j].Equals("")) || (board[i, j].Contains("c"))))
        {

            Cas item = new Cas(i, j);
            item.setPieza("bk2");//cambiar a futuro
            lista[tope] = item;
            tope++;
        }
    }


    public void MovCaballo(int x, int y)
    {

        tope = 0;


        SaltoPosible(x + 2, y + 1);
        SaltoPosible(x + 2, y - 1);

        SaltoPosible(x - 2, y + 1);
        SaltoPosible(x - 2, y - 1);

        SaltoPosible(x + 1, y + 2);
        SaltoPosible(x - 1, y + 2);

        SaltoPosible(x + 1, y - 2);
        SaltoPosible(x - 1, y - 2);

    }



    public bool EnLista(int x, int y)
    {

        bool aux = false;
        int i = 0;

        while ((!aux) && (i < tope))
        {
            aux = ((lista[i].getX().Equals(x)) && (lista[i].getY().Equals(y))); //lo que hace es buscar en una lista                 
            i++;
        }

        return aux;
    }


    public int[] getPos(string p)
    {
        int[] pos = new int[2];
        for (int i = 1; i < 7; i++)
        {
            for (int j = 1; j < 7; j++)
            {
                if (board[i, j].Equals(p))
                {
                    pos[0] = i;
                    pos[1] = j;
                }
            }
        }

        return pos;
    }


    //Usar backtracking para mayor eficiencia


    public bool HayEmpate(bool turno)
    {
        //dependiendo del turno, debo verificar que las piezas posean movimientos posibles.


        int[] b_pos = new int[2];
        int[] n_pos = new int[2];
        int[] r_pos = new int[2];

        if (turno)
        {
            //  b_pos = getPos("wB");
            // n_pos = getPos("wN");
            // r_pos = getPos("wR");
        }
        else
        {
            // b_pos = getPos("bB");
            // n_pos = getPos("bN");
            //  r_pos = getPos("bR");
        }

        //MovAlfil(b_pos[0], b_pos[1]);
        if ((tope.Equals(0)))
        {
            // MovCaballo(n_pos[0], n_pos[1]);
            if ((tope.Equals(0)))
            {
                //   MovTorre(r_pos[0], r_pos[1]);
                if ((tope.Equals(0)))
                {
                    print("HAY EMPATE!!!!");
                    empate = true;
                }
            }

        }

        return empate;
    }



    public bool terminado(string c, bool turno, int jugadas)
    {
        bool aux = false;
        bool empate = false;
        ganan_blancas = false;
        ganan_negras = false;

        //verticales
        if ((turno) && ((board[1, 1].Contains("nQ")) || (board[2, 1].Contains("nQ")) || (board[3, 1].Contains("nQ")) || (board[4, 1].Contains("nQ")) || (board[5, 1].Contains("nQ"))))
        {
            aux = true;
            print("GANAN BLANCAS");
            ganan_blancas = true;
        }
        else
         if ((!turno) && ((board[1, 5].Contains("nQ")) || (board[2, 5].Contains("nQ")) || (board[3, 5].Contains("nQ")) || (board[4, 5].Contains("nQ")) || (board[5, 5].Contains("nQ"))))
        {
            aux = true;
            print("GANAN NEGRAS");
            ganan_negras = true;
        }

        if (aux)
        {

            if (c.Equals("w"))
            {
                color = "NEGRAS";
            }
            else
            {
                color = "BLANCAS";
            }
        }

        term = aux;
        return aux;
    }


    public Cas[] getListaPosible()
    {

        return lista;
    }


    public int getTope()
    {

        return this.tope;
    }


    void OnGUI()
    {
        float x = Screen.width / 5f;
        float y = Screen.height / 4f;
        GUI.color = Color.red;
        GUI.skin.label.fontSize = 8;
        int h = 0;
        x = Screen.width / 5;
        y = Screen.height;
        //print (y);
        int k = 0;

        //GUI.Label(new Rect(x * 3.8f, 0.3f * x + 20, 90, 1000), "level: " + level);
       //- GUI.Label(new Rect(x * 3.8f, 0.3f * x + 20, 90, 1000), "[" + iii + "," + jjj + "]" + "c_w " + c_w);

        for (int i = 1; i < 7; i++)
        {

            k++;
            for (int j = 1; j < 7; j++)
            {
                k++;
               //- GUI.Label(new Rect(x * 0.3f, 0.005f * x + k * 10, 80, 1000), "[" + i + "," + j + "]: " + board3[i, j] + "=" + board2[i, j].GetComponent<monedas>().GetValue());
                // GUI.Label(new Rect(x * 0.3f, 0.005f * x + k * 10, 80, 1000), "[" + i + "," + j + "]: " + "=" + board[i, j]);
            }

        }

        for (int f = 0; f < tope; f++)
        {

           //- GUI.Label(new Rect(x * 4.3f, 0.3f * x + 20 * f, 90, 1000), "[" + lista[f].getX().ToString() + " " + lista[f].getY().ToString() + "]");

        }



        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 36;
        myStyle.fontStyle = FontStyle.Bold;
        GUI.Label(new Rect(10, 10, 10, 10), juego);
        // print(juego);

        GUI.Label(new Rect(x * 4.2f, x * 2.5f, 300, 300), "b : " + p_blancas.ToString(), myStyle);
        GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 320), "n : " + p_negras.ToString(), myStyle);

        if (term)
        {
            //GUI.skin.label.fontSize = 36;
            //GUI.color = Color.green;

            if (ganan_negras)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "N\nE\nG\nR\nA\nS", myStyle);

            }
            else
                if (ganan_blancas)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "B\nL\nA\nN\nC\nA\nS", myStyle);

            }
        }
    }

    public void setCasilla(string cas)
    {
        //term = terminado(cas);
        casilla = cas;
    }
    public string getCasilla()
    {
        return casilla;
    }

    public void setNeutral_n(bool aux)
    {
        neutral_n = aux;
    }
    public void setNeutral_b(bool aux)
    {
        neutral_b = aux;
    }
    public bool getNeutral_n()
    {
        return neutral_n;
    }
    public bool getNeutral_b()
    {
        return neutral_b;
    }

}

