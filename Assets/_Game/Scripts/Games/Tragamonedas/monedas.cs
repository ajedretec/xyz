﻿using UnityEngine;
using UnityEngine.UI;

public class monedas : MonoBehaviour
{

    private int puntaje_b = 0;
    private int puntaje_a = 0;

    int i;
    [SerializeField]
    Image _image;
    [SerializeField]
    CCoinList _coins;
    int _value;
    //public Sprite s_coin1, s_coin2, s_coin3, s_coin4, s_coin5;


    // Use this for initialization
    void Start()
    {
        float j = 0;

        //j = Random.Range(0.0f, 360.0f);
        //transform.Rotate(0, 0, j);
        /*
        s_coin1 = Resources.Load("s_coin1", typeof(Sprite)) as Sprite;
        s_coin2 = Resources.Load("s_coin2", typeof(Sprite)) as Sprite;
        s_coin3 = Resources.Load("s_coin3", typeof(Sprite)) as Sprite;
        s_coin4 = Resources.Load("s_coin4", typeof(Sprite)) as Sprite;
        s_coin5 = Resources.Load("s_coin5", typeof(Sprite)) as Sprite;
        */

        i = Random.Range(0, _coins.coins.Length);
        CCoinList.Coin tCoin = _coins.coins[i];
        _image.sprite = tCoin.sprite;
        _value = tCoin.value;
        /*
        switch (i)
        {
            case 5:
                this.transform.GetComponent<UnityEngine.UI.Image>().sprite = s_coin5;
                break;
            case 4:
                this.transform.GetComponent<UnityEngine.UI.Image>().sprite = s_coin4;
                break;
            case 3:
                this.transform.GetComponent<UnityEngine.UI.Image>().sprite = s_coin3;
                break;
            case 2:
                this.transform.GetComponent<UnityEngine.UI.Image>().sprite = s_coin2;
                break;
            case 1:
                this.transform.GetComponent<UnityEngine.UI.Image>().sprite = s_coin1;
                break;
            default:
                
                break;
        }
        */
    }

    public void eliminar()
    {
        Destroy(gameObject);
    }

    public int GetValue()
    {
        return _value;
    }

    public void SetValue(int aux)
    {
        _value = aux;
    }
}
