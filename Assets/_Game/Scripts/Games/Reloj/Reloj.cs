﻿using UnityEngine;

public class Reloj : MonoBehaviour {


    private bool aux2 = false;
    private bool aux3 = false;
    private SpriteRenderer sr;
    private SpriteRenderer sr2;
    private SpriteRenderer sr3;
    private float Timer = 0.0f; 
    private bool aux=true;
    static int minutes;
    static int seconds;
    private string niceTime;
    private GameObject myObj;
    private GameObject actual;
    private GameObject myObj2;
    private GameObject myObj3;
    private GameObject fondo; 
    private Turnos tn;
    private Reloj2 r2;
    private string der;
    private string izq;
    private string d = "der";
    private string i = "izq";
    private string turno;
    private int inc=0;
    private float x;
    private float y;
    private Font myFont;

    void Start () {

        myObj2 = GameObject.Find ("pReloj1");	
        sr2 = myObj2.GetComponent<SpriteRenderer> ();
        sr2.color = Color.gray;


        tn = GameObject.Find ("pFondo").GetComponent<Turnos> ();
        minutes = tn.getM1 ();
        inc = tn.getInc1 ();
        //Vector3 v = myObj2.transform.position;
        x= (float)Screen.width/5.1f;
        y= (float)Screen.height/2.4f;
        myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));

    }

    void Update (){

        turno = tn.getTurno ();
    

        if ((turno.Equals ("stop"))) {
            aux3=false;
            myObj2 = GameObject.Find ("pReloj1");	
            sr2 = myObj2.GetComponent<SpriteRenderer> ();
            sr2.color = Color.gray;

        }
    
            if (aux3) {
                Timer -= Time.deltaTime;  
            }


    }
    
    void OnGUI() {

        GUI.color = Color.black; 
        GUI.skin.label.fontSize = 100;
        seconds =  Mathf.FloorToInt(Timer);
        tn = GameObject.Find ("pFondo").GetComponent<Turnos> ();

        

       
        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 116;



        if ((minutes.Equals (0))&&(seconds.Equals(-1))){
            aux = false;
        }
        
        if ((seconds.Equals (-1)) && !(minutes.Equals (0))) {
            Timer = Timer + 60;
            minutes = minutes - 1;
        } 

        if (aux) {
            niceTime = string.Format ("{0:0}:{1:00}", minutes, seconds);

        } else {
            niceTime = string.Format ("{0:0}:{1:00}", 0, 0);
            tn.setTurno("fin");
        }
             
          //85 //130
          GUI.Label (new Rect (x, y, 250, 100), niceTime , myStyle);
    }

    public void setReloj(bool r){
        aux3 = true;
        tn = GameObject.Find ("pFondo").GetComponent<Turnos> ();
        tn.setTurno("izq");//mientras el blanco sea el izquierdo entonces el turno es del izquierdo
    }

    void OnMouseDown(){

        tn = GameObject.Find ("pFondo").GetComponent<Turnos> ();
        turno = tn.getTurno ();
        Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);

        if ((mousePosition.x < 0)) {

        mousePosition.z = 0;
        if (!(turno.Equals ("fin"))) {

            if ((turno.Equals ("izq"))||(turno.Equals ("inicio"))||(turno.Equals ("stop"))) { //preciona izquierda


                aux3 = false;
                myObj2 = GameObject.Find ("pReloj1");		
                sr2 = myObj2.GetComponent<SpriteRenderer> ();
                sr2.color = Color.gray;
                myObj3 = GameObject.Find ("pReloj1 (1)");
                r2 = myObj3.GetComponent<Reloj2> ();
                
                sr3 = myObj3.GetComponent<SpriteRenderer> ();
                sr3.color = Color.white;
                Timer = Timer + inc;


                if (Timer > 59) {
                    minutes= minutes+1;
                    Timer=Timer-60;
                }
                    r2.setReloj2 (true);
            }
        }
        }
    }
}//borre un aux3=false en algun lado atento
