﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ConfReloj : MonoBehaviour {
    

    public GUISkin skin;
    public Rect textArea;
    public int cursorIndex;
    public string myText;
    private Turnos tn;
    private float x;
    private float y;

    void Start () {
        tn = GameObject.Find ("pFondo").GetComponent<Turnos> ();
        x = Screen.width/3.6f;

    }
    
    // Update is called once per frame
    void Update () {
    
    }

    void OnGUI() {
        
        GUI.color = Color.black;
        GUI.skin.label.fontSize = 30;

    
    
        Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        GUIStyle myStyle = new GUIStyle();

       
        myStyle.font = myFont;
        myStyle.fontSize = 36;
        myStyle.fontStyle = FontStyle.Bold;
        

        if (GUI.Button (new Rect (x, 200, 400, 50), "Blitz 10|0",myStyle)) {
            tn.setM1(10);
            tn.setM2(10);
            tn.setInc1(0);
            tn.setInc2(0);
            SceneManager.LoadScene("Reloj");

        }

        if (GUI.Button (new Rect (x, 260, 400, 50), "Blitz 5|0", myStyle)) {
            tn.setM1(5);
            tn.setM2(5);
            tn.setInc1(0);
            tn.setInc2(0);
            SceneManager.LoadScene("Reloj");
           
        }


        if (GUI.Button (new Rect (x, 320, 400, 50), "Blitz 3|2",myStyle)) {
            tn.setM1(3);
            tn.setM2(3);
            tn.setInc1(2);
            tn.setInc2(2);
            SceneManager.LoadScene("Reloj");
        }

        if (GUI.Button (new Rect (x, 380, 400, 50), "Blitz 1|0",myStyle)) {
            tn.setM1(1);
            tn.setM2(1);
            tn.setInc1(0);
            tn.setInc2(0);
            SceneManager.LoadScene("Reloj");
        }

        if (GUI.Button (new Rect (x, 440, 400, 50), "Volver",myStyle)) {
            SceneManager.LoadScene("Reloj");
        }
        


        
//		GUI.skin = skin;
//		GUI.Label (textArea, myText);
//		cursorIndex = GUI.skin.GetStyle ("MyLabel").GetCursorStringIndex (textArea, new GUIContent (myText), Event.current.mousePosition);

    }

//	void OnMouseDown(){

//		if (this.gameObject.name.Equals ("pBack")){
//			Application.LoadLevel("Reloj");
//		}
    //}

}
