﻿using UnityEngine;
using UnityEngine.UI;

public class CChessClockManager : CGameManager
{
    [SerializeField]
    Image _clock1BG;
    [SerializeField]
    Text _clock1Text;
    [SerializeField]
    Image _clock2BG;
    [SerializeField]
    Text _clock2Text;

    Turnos2 _tn2;
    float _timer1;
    float _timer2;

    protected override void Start()
    {
        base.Start();
        _tn2 = FindObjectOfType<Turnos2>();

        Restart();
    }

    public override void Restart()
    {
        base.Restart();

    }

    private void Update()
    {
        
    }
}
