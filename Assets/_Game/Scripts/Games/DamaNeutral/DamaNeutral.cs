﻿using UnityEngine;
using System.Collections;

public class DamaNeutral : CGameManager
{
   //Se utilizan las clases Drag y Slot de Dama Neutral

    private string[,] board = new string[6, 6];
    private GameObject[,] board2 = new GameObject[6, 6];
    private string[,] movPos = new string[6, 6];
    private string casilla = "null";
    private int i, j;
    private bool term = false;
    private string color = "";
    private bool empate = false;
    private bool neutron = false; 
    static bool neutral_n = false;
    static bool neutral_b = false;
    private bool ganan_blancas = false;
    private bool ganan_negras = false;
    private Cas[] lista = new Cas[16];
    private int tope = 0;
    private Turnos2 tn2;
    int ii, jj;
    int i2, j2;
    string pnegra;
    TurnoNeutral tNeutral;



    private GameObject pieza, pieza2;

    override protected void Start()
    {
        base.Start();
        tn2 = FindObjectOfType<Turnos2>();
        tn2.setJuego("d"); // 
        tNeutral = FindObjectOfType<TurnoNeutral>();




        ii = 3;
        jj = 3;

        i2 = 1;
        j2 = 5;

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                board[i, j] = "";
                board2[i,j] = GameObject.Find("t" + i.ToString() + j.ToString());

                if ((i > 0) && (j > 0))
                {
                    board2[i, j].SetActive(false);
                }
            }
        }

          board[3, 3] = "nQ";
          board[1, 1] = "wQ1";
          board[2, 1] = "wQ2";
          board[3, 1] = "wQ3";
          board[4, 1] = "wQ4";
          board[5, 1] = "wQ5";

          board[1, 5] = "bQ1";
          board[2, 5] = "bQ2";
          board[3, 5] = "bQ3";
          board[4, 5] = "bQ4";
          board[5, 5] = "bQ5";

          pieza = GameObject.Find("c3/nQ");
        //  pieza2 = GameObject.Find("a5/bQ1"); Aca esta la posta

        
                board2[3, 3].SetActive(false);
                board2[1, 1].SetActive(false);
                board2[2, 1].SetActive(false);
                board2[3, 1].SetActive(false);
                board2[4, 1].SetActive(false);

                board2[5, 1].SetActive(false);


                board2[1, 5].SetActive(false);
                board2[2, 5].SetActive(false);
                board2[3, 5].SetActive(false);
                board2[4, 5].SetActive(false);
                board2[5, 5].SetActive(false);
                

    }

    void obtenerPieza() {




    }


    void Update()
    {
        i++;

        if ((tn2.getTurno() == false))
        {
            //i++;
            //j++;
            if (i == 100)
            {
                if (neutral_n == false)
                {
                    int[] pos_n = new int[2];
                    pos_n = getPos("nQ");
                    lista = new Cas[16];
                    int r = Random.Range(0, 2);

                    if (r.Equals(0))
                    {
                        MovAlfil(pos_n[0], pos_n[1]);
                        MovTorre(pos_n[0], pos_n[1]);
                    }
                    else
                    {
                        MovTorre(pos_n[0], pos_n[1]);
                        MovAlfil(pos_n[0], pos_n[1]);
                    }

                    board[pos_n[0], pos_n[1]] = "";
                    int rand = Random.Range(0, tope - 1);
                    Cas c = lista[rand];
                    print("TOPE: " + tope);
                    GameObject token = board2[c.getX(), c.getY()];  
                    token.SetActive(true);
                    pieza.transform.parent = token.transform.parent;                    
                    token.SetActive(false);                   
                    ii = c.getX();
                    jj = c.getY();

                    board[ii, jj] = "nQ";
                    print("[" + c.getX() + "," + c.getY() + "] " + c.getPieza());
                    //neutral_n = true;

                    neutral_b = false;
                    neutral_n = true;
                    tNeutral.SetNeutron("");

                }
            }
        }
       
        if (((tn2.getTurno() == false)) && (neutral_n == true) && (i == 150)  )
        {
            
            int[] pos = new int[2];
            int r3 = Random.Range(0, 5);

            switch (r3)
            {
                case 0:
                    pnegra = "bQ1";
                    break;
                case 1:
                    pnegra = "bQ2";
                    break;
                case 2:
                    pnegra = "bQ3";
                    break;
                case 3:
                    pnegra = "bQ4";
                    break;
                case 4:
                    pnegra = "bQ5";
                    break;
            }


            pos = getPos(pnegra);
            int r2 = Random.Range(0, 2);
            lista = new Cas[16];

            if (r2.Equals(0))
            {
                MovAlfil(pos[0], pos[1]);
                MovTorre(pos[0], pos[1]);
            }
            else
            {
                MovTorre(pos[0], pos[1]);
                MovAlfil(pos[0], pos[1]);
            }
            int rand2 = Random.Range(0, tope - 1);
            Cas c2 = lista[rand2];
            print("TOPE: " + tope);
            int u = c2.getX();
            int v = c2.getY();
            print("POS N:" + u.ToString() + v.ToString());

            //pieza2 = GameObject.Find(c2.getPieza());
            print("Pieza:" + c2.getPieza());

            pieza2 = GameObject.FindGameObjectsWithTag(pnegra)[0]; //

            board2[u, v].SetActive(true);
            pieza2.transform.parent = board2[u, v].transform.parent;
            board2[u, v].SetActive(false);


            board[u, v] = pnegra;
            board[pos[0], pos[1]] = "";
            i = 0;
            //neutral_n = false;
            tn2.SetTurno(true);

            tNeutral.SetNeutron("blancas");

        }


    }

    public void insertar(string p)
    {

        print(p);
        switch (casilla)
        {
            case "a1":
                board[1, 1] = p;
                i = 1;
                j = 1;
                break;
            case "a2":
                board[1, 2] = p;
                i = 1;
                j = 2;
                break;
            case "a3":
                board[1, 3] = p;
                i = 1;
                j = 3;
                break;
            case "a4":
                board[1, 4] = p;
                i = 1;
                j = 4;
                break;
            case "a5":
                board[1, 5] = p;
                i = 1;
                j = 5;
                break;
            case "b1":
                board[2, 1] = p;
                i = 2;
                j = 1;
                break;
            case "b2":
                board[2, 2] = p;
                i = 2;
                j = 2;
                break;
            case "b3":
                board[2, 3] = p;
                i = 2;
                j = 3;
                break;
            case "b4":
                board[2, 4] = p;
                i = 2;
                j = 4;
                break;
            case "b5":
                board[2, 5] = p;
                i = 2;
                j = 5;
                break;
            case "c1":
                board[3, 1] = p;
                i = 3;
                j = 1;
                break;
            case "c2":
                board[3, 2] = p;
                i = 3;
                j = 2;
                break;
            case "c3":
                board[3, 3] = p;
                i = 3;
                j = 3;
                break;
            case "c4":
                board[3, 4] = p;
                i = 3;
                j = 4;
                break;
            case "c5":
                board[3, 5] = p;
                i = 3;
                j = 5;
                break;
            case "d1":
                board[4, 1] = p;
                i = 4;
                j = 1;
                break;
            case "d2":
                board[4, 2] = p;
                i = 4;
                j = 2;
                break;
            case "d3":
                board[4, 3] = p;
                i = 4;
                j = 3;
                break;
            case "d4":
                board[4, 4] = p;
                i = 4;
                j = 4;
                break;
            case "d5":
                board[4, 5] = p;
                i = 4;
                j = 5;
                break;
            case "e1":
                board[5, 1] = p;
                i = 5;
                j = 1;
                break;
            case "e2":
                board[5, 2] = p;
                i = 5;
                j = 2;
                break;
            case "e3":
                board[5, 3] = p;
                i = 5;
                j = 3;
                break;
            case "e4":
                board[5, 4] = p;
                i = 5;
                j = 4;
                break;
            case "e5":
                board[5, 5] = p;
                i = 5;
                j = 5;
                break;
        }

    }


    public int[] getCoord(string p)
    {
        int[] pos = new int[2];
        switch (casilla)
        {
            case "a1":
                pos[0] = 1;
                pos[1] = 1;

                break;
            case "a2":

                pos[0] = 1;
                pos[1] = 2;
                break;
            case "a3":

                pos[0] = 1;
                pos[1] = 3;
                break;
            case "a4":

                pos[0] = 1;
                pos[1] = 4;
                break;
            case "a5":

                pos[0] = 1;
                pos[1] = 5;
                break;
            case "b1":

                pos[0] = 2;
                pos[1] = 1;
                break;
            case "b2":

                pos[0] = 2;
                pos[1] = 2;
                break;
            case "b3":

                pos[0] = 2;
                pos[1] = 3;
                break;
            case "b4":

                pos[0] = 2;
                pos[1] = 4;
                break;
            case "b5":

                pos[0] = 2;
                pos[1] = 5;
                break;
            case "c1":

                pos[0] = 3;
                pos[1] = 1;
                break;
            case "c2":
                pos[0] = 3;
                pos[1] = 2;
                break;
            case "c3":

                pos[0] = 3;
                pos[1] = 3;
                break;
            case "c4":
                pos[0] = 3;
                pos[1] = 4;
                break;
            case "c5":

                pos[0] = 3;
                pos[1] = 5;
                break;
            case "d1":

                pos[0] = 4;
                pos[1] = 1;
                break;
            case "d2":

                pos[0] = 4;
                pos[1] = 2;
                break;
            case "d3":

                pos[0] = 4;
                pos[1] = 3;
                break;
            case "d4":

                pos[0] = 4;
                pos[1] = 4;
                break;
            case "d5":

                pos[0] = 4;
                pos[1] = 5;
                break;
            case "e1":

                pos[0] = 5;
                pos[1] = 1;
                break;
            case "e2":
                pos[0] = 5;
                pos[1] = 2;
                break;
            case "e3":

                pos[0] = 5;
                pos[1] = 3;
                break;
            case "e4":
                pos[0] = 5;
                pos[1] = 4;
                break;
            case "e5":

                pos[0] = 5;
                pos[1] = 5;
                break;


        }
        return pos;
    }

    public void eliminar(string p)
    {

        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                if (board[i, j].Equals(p))
                {
                    board[i, j] = "";
                }
            }
        }
    }

    public bool EnLista(int x, int y)
    {

        bool aux = false;
        int i = 0;

        while ((!aux) && (i < tope))
        {
            aux = ((lista[i].getX().Equals(x)) && (lista[i].getY().Equals(y)));
            i++;
        }

        return aux;
    }

    public int[] getPos(string p)
    {
        int[] pos = new int[2];
        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                if (board[i, j].Equals(p))
                {
                    pos[0] = i;
                    pos[1] = j;
                }
            }
        }

        return pos;
    }


    //Nota: Mejor usar backtracking para mayor eficiencia

    public void MovTorre(int x, int y)
    {

        tope = 0;
        int i, j;
        bool libre;
        i = x;
        j = y;
        libre = true;

        //primero consulto abajo, itero sobre la columna j
        while ((j > 0) && (libre))
        {

            j--;

            if ((j > 0) && (board[x, j].Equals("")))
            { //guardo la x y la j

                //print("ABAJO" + x + " " + j);

                Cas item = new Cas(x, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia arriba

        while ((j < 6) && (libre))
        {

            j++;
            if ((j < 6) && (board[x, j].Equals("")))
            { //guardo la x y la j
                //print("ARRIBA" + x + " " + j);

                Cas item = new Cas(x, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;

        while ((i < 6) && (libre))
        {

            i++;
            if ((i < 6) && (board[i, y].Equals("")))
            { //guardo la x y la j
                //print("DERECHA" + i + " " + y);

                Cas item = new Cas(i, y);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;
        libre = true;

        while ((i > 0) && (libre))
        {
            i--;
            if ((i > 0) && (board[i, y].Equals("")))
            { //guardo la x y la j

                //print("IZQUIERDA" + i + " " + y);
                Cas item = new Cas(i, y);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }


    }


    public void MovAlfil(int x, int y)
    {

        tope = 0;
        int i, j;
        bool libre;
        i = x;
        j = y;
        libre = true;

        //primero consulto abaja a la izquierda
        while ((j > 0) && (i > 0) && (libre))
        {

            j--;
            i--;

            if ((j > 0) && (i > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("ABAJO - IZQUIERDA" + i + " " + j);

                Cas item = new Cas(i, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia ARRIBA - DERECHA

        while ((j < 6) && (i < 6) && (libre))
        {

            j++;
            i++;

            if ((j < 6) && (i < 6) && (board[i, j].Equals("")))
            { //guardo la x y la j
               // print("ARRIBA-DERECHA" + i + " " + j);

                Cas item = new Cas(i, j);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;


        //Subo la i y decremento la j
        while ((i < 6) && (j > 0) && (libre))
        {

            i++;
            j--;

            if ((i < 6) && (j > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("DERECHA- ABAJO" + i + " " + j);

                Cas item = new Cas(i, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;

        libre = true;

        //subo la i y decremento la j
        while ((i > 0) && (j < 6) && (libre))
        {
            i--;
            j++;
            if ((i > 0) && (j < 6) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("IZQUIERDA - ARRIBA" + i + " " + j);
                Cas item = new Cas(i, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        //print("MovTorre: " + tope);
    }
    
    public bool HayEmpate(bool turno)
    {
        //dependiendo del turno, debo verificar que las piezas posean movimientos posibles.


        int[] b_pos = new int[2];
        int[] n_pos = new int[2];
        int[] r_pos = new int[2];

        if (turno)
        {
            b_pos = getPos("wB");
            n_pos = getPos("wN");
            r_pos = getPos("wR");
        }
        else
        {
            b_pos = getPos("bB");
            n_pos = getPos("bN");
            r_pos = getPos("bR");
        }

        MovAlfil(b_pos[0], b_pos[1]);
        if ((tope.Equals(0)))
        {
           // MovCaballo(n_pos[0], n_pos[1]);
            if ((tope.Equals(0)))
            {
                MovTorre(r_pos[0], r_pos[1]);
                if ((tope.Equals(0)))
                {
                    print("HAY EMPATE!!!!");
                    empate = true;
                }
            }

        }

        return empate;
    }
    


    public bool terminado(string c, bool turno, int jugadas)
    {
        bool aux = false;
        bool empate = false;

        ganan_blancas = false;
        ganan_negras = false;

        //verticales
        if ((turno) && ((board[1, 1].Contains("nQ")) || (board[2, 1].Contains("nQ")) || (board[3, 1].Contains("nQ"))|| (board[4, 1].Contains("nQ")) || (board[5, 1].Contains("nQ"))))
        {
            aux = true;
            print("GANAN BLANCAS");
            ganan_blancas = true;
        }
        else
         if ((!turno) && ((board[1, 5].Contains("nQ")) || (board[2, 5].Contains("nQ")) || (board[3, 5].Contains("nQ")) || (board[4, 5].Contains("nQ")) || (board[5, 5].Contains("nQ"))))
            {
             aux = true;
             print("GANAN NEGRAS");
             ganan_negras = true;
           }

        if (aux)
        {
            
            if (c.Equals("w"))
            {
                color = "NEGRAS";
            }
            else
            {
                color = "BLANCAS";
            }
        }

        term = aux;
        return aux;
    }


    public Cas[] getListaPosible()
    {

        return lista;
    }


    public int getTope()
    {

        return this.tope;
    }


    void OnGUI()
    {
        float x = Screen.width / 5f;
        float y = Screen.height / 4f;
        int h = 0;
        x = Screen.width / 5;
        y = Screen.height;
        int k = 0;
        Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 16;
        myStyle.fontStyle = FontStyle.Bold;

        if (term)
        {
           
            if (ganan_negras)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "N\nE\nG\nR\nA\nS", myStyle);

            }
            else
                if (ganan_blancas)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "B\nL\nA\nN\nC\nA\nS", myStyle);
              
            }
        }

       // float x = Screen.width / 5f;
        //float y = Screen.height / 4f;
        GUI.color = Color.red;
        GUI.skin.label.fontSize = 10;

        for (int i = 1; i < 6; i++)
        {

            k++;
            for (int j = 1; j < 6; j++)
            {
                k++;
                GUI.Label(new Rect(x * 0.3f, 0.005f * x + k * 10, 80, 1000), "[" + i + "," + j + "]: " + board[i, j]);
            }
        }

    }

    public void setCasilla(string cas)
    {
        casilla = cas;
    }
    public string getCasilla()
    {
        return casilla;
    }

    public void setNeutral_n(bool aux) {
        neutral_n = aux;
    }
    public void setNeutral_b(bool aux)
    {
        neutral_b = aux;
    }
    public bool getNeutral_n()
    {
        return neutral_n;
    }
    public bool getNeutral_b()
    {
        return neutral_b;
    }

    public override void Restart()
    {
        base.Restart();
        UnityEngine.SceneManagement.SceneManager.LoadScene(
            UnityEngine.SceneManagement.SceneManager.GetActiveScene().buildIndex);
    }
}

