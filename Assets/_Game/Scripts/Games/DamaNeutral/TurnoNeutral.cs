﻿using UnityEngine;
using UnityEngine.UI;

public class TurnoNeutral : MonoBehaviour
{
    [SerializeField]
    private Vector2 _posBlack;
    [SerializeField]
    Vector2 _anchorMinBlack;
    [SerializeField]
    Vector2 _anchorMaxBlack;
    [SerializeField]
    private Vector2 _posWhite;
    [SerializeField]
    Vector2 _anchorMinWhite;
    [SerializeField]
    Vector2 _anchorMaxWhite;

    RectTransform _transform;
    Image _image;

    void Start()
    {
        _image = GetComponent<Image>();
        _transform = GetComponent<RectTransform>();
        SetNeutron("");
    }

    public void SetNeutron(string aux)
    {
        if (aux == "")
        {
            _image.color = Color.clear;
        }
        else if (aux == "negras")
        {
            _image.color = Color.white;
            _transform.anchorMin = _anchorMinBlack;
            _transform.anchorMax = _anchorMaxBlack;
            _transform.anchoredPosition = _posBlack;
        }
        else if (aux == "blancas")
        {
            _image.color = Color.white;
            _transform.anchorMin = _anchorMinWhite;
            _transform.anchorMax = _anchorMaxWhite;
            _transform.anchoredPosition = _posWhite;
        }
    }

}
