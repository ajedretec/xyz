﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class CGameManager : MonoBehaviour
{
    [SerializeField]
    protected string _gameName;
    [SerializeField]
    Color _titleColor = Color.cyan;
    [SerializeField]
    Color _titleBGColor = Color.cyan;
    [SerializeField]
    protected CGameUI _gameUI;
    [SerializeField]
    GameObject _tutorialPref;
    [SerializeField]
    protected bool _reloadScene;

    protected virtual void Start()
    {
        _gameUI.SetTitle(CLanguage.FillText(_gameName), _titleColor, _titleBGColor);
        _gameUI.SetManager(this);
        _gameUI.SetTutorial(_tutorialPref);
        //CGlobal.ExecuteNextFrame(_gameUI.ShowTutorial);
    }

    public virtual void Restart()
    {
        if (_reloadScene)
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
