﻿using UnityEngine;
using UnityEngine.Networking;

public class CSecretObjectivePlayer : NetworkBehaviour
{
    [SyncVar]
    int _objective;
    public int Objective { get { return _objective; } }

    const short CLAIM_VICTORY_ID = MsgType.InternalHighest + 1;
    const short RESTART_VOTE_ID = MsgType.InternalHighest + 3;
    const short RESTART_ORDER_ID = MsgType.InternalHighest + 4;
    const short SHOW_RESTART_MSG_ID = MsgType.InternalHighest + 5;

    int _restartVotes;
    int _restartYes;

    private void Start()
    {
        if (ObjetivosSecretos.Inst != null)
        {
            if (isLocalPlayer)
            {
                Restart();
                ObjetivosSecretos.Inst.SetLocalPlayer(this);

                NetworkManager.singleton.client.RegisterHandler(RESTART_ORDER_ID,
                    ReceiveRestartOrder);
                NetworkManager.singleton.client.RegisterHandler(SHOW_RESTART_MSG_ID,
                    ClientShowRestartMsg);
                if (isServer)
                {
                    NetworkServer.RegisterHandler(CLAIM_VICTORY_ID, ServerReceiveVictory);
                    NetworkServer.RegisterHandler(RESTART_VOTE_ID, ReceiveRestartVote);
                }
            }
            else
            {
                NetworkManager.singleton.client.RegisterHandler(CLAIM_VICTORY_ID, VictoryMsgReceived);
            }
        }
    }

    private void ClientShowRestartMsg(NetworkMessage netMsg)
    {
        ObjetivosSecretos.Inst.ShowRestartConfirm();
    }

    [Command]
    private void CmdShowRestartMsg()
    {
        NetworkServer.SendToAll(SHOW_RESTART_MSG_ID, new RestartVote());
    }

    private void Restart()
    {
        _objective = ObjetivosSecretos.Inst.GetRandomObjective();
        ObjetivosSecretos.Inst.ShowObjectives(_objective);
    }

    private void ReceiveRestartOrder(NetworkMessage netMsg)
    {
        if (netMsg.ReadMessage<RestartVote>().value)
            Restart();
        ObjetivosSecretos.Inst.HideWaitingPanel();
    }

    private void ReceiveRestartVote(NetworkMessage netMsg)
    {
        _restartVotes++;
        _restartYes += netMsg.ReadMessage<RestartVote>().value ? 1 : 0;
        Debug.Log("Votes: " + _restartVotes + ", YES: " + _restartYes);
        if (_restartVotes >= 2)
        {
            NetworkServer.SendToAll(RESTART_ORDER_ID, new RestartVote
            {
                value = _restartYes >= _restartVotes
            });
            _restartYes = 0;
            _restartVotes = 0;
        }
    }

    private void ServerReceiveVictory(NetworkMessage netMsg)
    {
        NetworkServer.SendToAll(CLAIM_VICTORY_ID,
            netMsg.reader.ReadMessage<ClaimVictoryMsg>());
    }

    private void VictoryMsgReceived(NetworkMessage netMsg)
    {
        int tID = netMsg.reader.ReadMessage<ClaimVictoryMsg>().id;
        if (tID != NetworkManager.singleton.client.connection.connectionId)
        {
            ObjetivosSecretos.Inst.ShowWinnerObjectives(_objective);
        }
    }

    public void ClaimVictory()
    {
        ClaimVictoryMsg tMsg = new ClaimVictoryMsg();
        tMsg.id = NetworkManager.singleton.client.connection.connectionId;
        NetworkManager.singleton.client.Send(CLAIM_VICTORY_ID, tMsg);
    }

    public void SendRestartVote(bool aValue)
    {
        RestartVote tMsg = new RestartVote
        {
            value = aValue,
        };
        NetworkManager.singleton.client.Send(RESTART_VOTE_ID, tMsg);
    }

    public void SendShowRestartMsg()
    {
        CmdShowRestartMsg();
    }

    public void GoToMenu()
    {
        if (isServer)
            NetworkManager.singleton.StopServer();
        else
            NetworkManager.singleton.StopClient();
    }
}

public class ClaimVictoryMsg : MessageBase
{
    public int id;
}

public class RestartVote : MessageBase
{
    public bool value;
}