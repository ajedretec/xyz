﻿using DoozyUI;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class SecretObjectiveLobbyUI : UIMenu
{
    [SerializeField]
    UIElement _lobbyMenu;

    public static SecretObjectiveLobbyUI Inst { get; private set; }
    NetworkDiscovery _discovery;

    private void Awake()
    {
        Inst = this;
        _discovery = FindObjectOfType<NetworkDiscovery>();
    }

    protected override void Start()
    {
        base.Start();
        if (!_discovery.IsInitialized())
            _discovery.Initialize();
        StopConnection();
    }

    public void GoToMainMenu()
    {
        CustomNetLobbyManager.DestroyObj();
        CGlobal.GoToMainMenu();
    }

    public void StartClient(string host, int port)
    {
        if (NetworkManager.singleton != null && NetworkManager.singleton.client == null)
        {
            NetworkManager.singleton.networkAddress = host;
            NetworkManager.singleton.networkPort = port;
            NetworkManager.singleton.StartClient();
        }
    }

    public void StartHost()
    {
        NetworkManager.singleton.StartHost();
    }

    public void StartClientDiscovery()
    {
        _discovery.StartAsClient();
    }

    public void StartServerBroadcast()
    {
        _discovery.StartAsServer();
    }

    public void StopDiscovery()
    {
        if (_discovery.isClient || _discovery.isServer)
            _discovery.StopBroadcast();
    }

    public void AddClientFindServerCallback(NetworkDiscovery.OnRecieveBroadcastDelegate aCallback)
    {
        _discovery._delegate -= aCallback;
        _discovery._delegate += aCallback;
    }

    public void ShowLobby()
    {
        ShowMenu(_lobbyMenu);
    }

    public void StopConnection()
    {
        StopDiscovery();

        NetworkManager.singleton.StopHost();
    }

    public void OfflineMode()
    {
        CLoadingScreen.LoadScene((NetworkManager.singleton as NetworkLobbyManager).playScene);
        CustomNetLobbyManager.DestroyObj();
    }

    public void ForceRebuildLayout(RectTransform aTransform)
    {
        CGlobal.ExecuteNextFrame(() =>
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(aTransform);
        }, 2);
    }
}
