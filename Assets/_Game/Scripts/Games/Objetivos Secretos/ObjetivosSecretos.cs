﻿using DoozyUI;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class ObjetivosSecretos : CGameManager
{
    [SerializeField]
    CStringList _objectives;
    [SerializeField]
    CSecretObjectiveItem _itemPref;
    [SerializeField]
    Transform _root;
    [SerializeField]
    Transform _winnerRoot;
    [SerializeField]
    UIElement _winnerPanel;
    [SerializeField]
    UIElement _claimVictoryMenu;
    [SerializeField]
    UIElement _confirmRestartPanel;
    [SerializeField]
    UIElement _waitingPanel;
    [SerializeField]
    int _menuScene;
    [SerializeField]
    UIElement _disconnectMsg;

    public static ObjetivosSecretos Inst;
    CSecretObjectiveItem[] _items;
    CSecretObjectivePlayer _localPlayer;

    Dictionary<int, CSecretObjectivePlayer> _players;

    private void Awake()
    {
        Inst = this;
        _players = new Dictionary<int, CSecretObjectivePlayer>();
    }

    override protected void Start()
    {
        base.Start();
        _claimVictoryMenu.Hide(true);
        if (NetworkManager.singleton == null)
        {
            _reloadScene = true;
            ShowObjectives(GetRandomObjective());
        }
        _gameUI.SetAlternateBackFunction(GoToMenu);

        if (NetworkManager.singleton != null)
        {
            (NetworkManager.singleton as CustomNetLobbyManager).AddOnDisconnectCallback(OnDisconnection);
            _disconnectMsg.gameObject.SetActive(true);
        }
        else
            _claimVictoryMenu.gameObject.SetActive(false);
    }

    private void OnDestroy()
    {
        if (NetworkManager.singleton != null)
        {
            (NetworkManager.singleton as CustomNetLobbyManager).RemoveOnDisconnectCallback(OnDisconnection);
        }
    }

    void OnDisconnection(NetworkConnection conn)
    {
        _disconnectMsg.Show(false);
    }

    public void GoToMenu()
    {
        if (NetworkManager.singleton != null)
            _localPlayer.GoToMenu();
        
        CGlobal.LoadScene(_menuScene);
    }

    public void ConfirmGoToMenu()
    {
        _gameUI.ConfirmGoToMenu();
    }

    public int GetRandomObjective()
    {
        return Random.Range(0, _objectives.strings.Length);
    }

    public void ShowObjectives(int index)
    {
        string[] tObj = CLanguage.GetText(_objectives.strings[index]).Split(';');
        InstantiateItems(_root, tObj, true);
    }

    void InstantiateItems(Transform aRoot, string[] aObj, bool aMain = false)
    {
        if (aMain)
            _items = new CSecretObjectiveItem[aObj.Length];
        for (int i = aRoot.childCount - 1; i >= 0; i--)
            Destroy(aRoot.GetChild(i).gameObject);
        for (int i = 0; i < aObj.Length; i++)
        {
            CSecretObjectiveItem tItem = Instantiate(_itemPref, aRoot, false);
            tItem.SetState(false);
            tItem.SetText(aObj[i]);
            if (aMain)
                _items[i] = tItem;
        }
        CGlobal.ExecuteNextFrame(() =>
        {
            LayoutRebuilder.ForceRebuildLayoutImmediate(aRoot as RectTransform);
        });
    }

    bool AllObjectivesDone()
    {
        for (int i = 0; i < _items.Length; i++)
        {
            if (!_items[i].state)
                return false;
        }
        return true;
    }

    private void Update()
    {
        if (NetworkManager.singleton == null)
            return;

        if (_items != null)
        {
            if (!_claimVictoryMenu.isVisible && AllObjectivesDone())
                _claimVictoryMenu.Show(false);
            else if (_claimVictoryMenu.isVisible && !AllObjectivesDone())
                _claimVictoryMenu.Hide(false);
        }
    }

    public void SetLocalPlayer(CSecretObjectivePlayer aPlayer)
    {
        _localPlayer = aPlayer;
    }

    public void RegisterPlayer(int aID, CSecretObjectivePlayer aPlayer)
    {
        _players.Add(aID, aPlayer);
    }

    public void ClaimVictory()
    {
        if (_localPlayer != null)
        {
            _localPlayer.ClaimVictory();
        }
    }

    public CSecretObjectivePlayer GetPlayer(int aConnID)
    {
        if (_players.ContainsKey(aConnID))
            return _players[aConnID];
        return null;
    }

    public void ShowWinnerObjectives(int aIndex)
    {
        string[] tObj = CLanguage.GetText(_objectives.strings[aIndex]).Split(';');
        InstantiateItems(_winnerRoot, tObj);
        _winnerPanel.gameObject.SetActive(true);
        _winnerPanel.Show(false);
    }

    public override void Restart()
    {
        if (NetworkManager.singleton != null)
            _localPlayer.SendShowRestartMsg();
        else
            base.Restart();
    }

    public void ShowRestartConfirm()
    {
        _confirmRestartPanel.gameObject.SetActive(true);
        _confirmRestartPanel.Show(false);
    }

    public void SendRestart(bool aValue)
    {
        _localPlayer.SendRestartVote(aValue);
        ShowWaitingPanel();
    }

    public void ShowWaitingPanel()
    {
        _waitingPanel.gameObject.SetActive(true);
        _waitingPanel.Show(false);
    }

    public void HideWaitingPanel()
    {
        _waitingPanel.Hide(false);
        _confirmRestartPanel.Hide(false);
    }
}
