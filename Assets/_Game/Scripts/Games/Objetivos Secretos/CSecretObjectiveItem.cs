﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CSecretObjectiveItem : MonoBehaviour
{
    [SerializeField]
    Image _checkedImg;
    [SerializeField]
    TextMeshProUGUI _text;

    public bool state { get; private set; }

    public void ToggleState()
    {
        SetState(!state);
    }

    public void SetState(bool aState)
    {
        state = aState;
        _checkedImg.color = state ? Color.white : Color.clear;
    }

    public void SetText(string aText)
    {
        _text.text = aText;
    }
}
