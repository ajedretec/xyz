﻿using System.Collections.Generic;
using UnityEngine;

public class PuzzleManager : CGameManager
{
    [SerializeField]
    float _adjustTime;
    [SerializeField]
    float _velocityThreshold = .1f;
    [SerializeField]
    float _scale = 1.5f;
    [SerializeField]
    int _numOfItems = 10;
    [SerializeField]
    int _maxLives;
    [SerializeField]
    GameObject _lifePref;
    [SerializeField]
    Transform _livesRoot;

    [SerializeField]
    ScrollControl _control1;
    [SerializeField]
    ScrollControl _control2;
    [SerializeField]
    PuzzleData _data;
    [SerializeField]
    GameObject _item1Pref;
    [SerializeField]
    GameObject _item2Pref;
    [SerializeField]
    GameObject _confirmMsg;

    public static PuzzleManager Inst { get; private set; }
    bool _msgVisible;
    int _lives;

    private void Awake()
    {
        Inst = this;

        for (int i = 0; i < _maxLives; i++)
        {
            Instantiate(_lifePref, _livesRoot, false);
        }

        _control1.scale = _scale;
        _control1.adjustTime = _adjustTime;
        _control1.threshold = _velocityThreshold;
        _control2.scale = _scale;
        _control2.adjustTime = _adjustTime;
        _control2.threshold = _velocityThreshold;

        Restart();
    }

    public void OnScrollStop()
    {
        if (_control1.IsStopped() && _control2.IsStopped())
        {
            SetConfirmMsgVisible(true);
        }
    }

    void SetConfirmMsgVisible(bool aVisible, bool aInstant = false)
    {
        _confirmMsg.SetActive(aVisible);
        _msgVisible = aVisible;
    }

    void UpdateLives()
    {
        for (int i = 0; i < _lives; i++)
            _livesRoot.GetChild(i).gameObject.SetActive(true);
        for (int i = _lives; i < _livesRoot.childCount; i++)
            _livesRoot.GetChild(i).gameObject.SetActive(false);
    }

    public void ConfirmSelection()
    {
        PuzzleItem tItem1 = _control1.GetSelectedObject().GetComponent<PuzzleItem>();
        PuzzleItem tItem2 = _control2.GetSelectedObject().GetComponent<PuzzleItem>();
        if (tItem1.ID == tItem2.ID)
        {
            if (_control1.Content.childCount == 1 && _control2.Content.childCount == 1)
            {
                // TODO: Game over
                _gameUI.ShowVictory();
            }
            tItem1.Remove();
            tItem2.Remove();
            _gameUI.PositiveFeedback();
            CGlobal.ExecuteNextFrame(() =>
            {
                _control1.EndHandle();
                _control2.EndHandle();
            });
        }
        else
        {
            _lives--;
            UpdateLives();
            _gameUI.NegativeFeedback();
            if (_lives == 0)
            {
                _gameUI.ShowDefeat();
            }
        }
    }

    private void Update()
    {
        if (_msgVisible)
        {
            if (!_control1.IsStopped() || !_control2.IsStopped())
            {
                SetConfirmMsgVisible(false);
            }
        }
    }

    public override void Restart()
    {
        base.Restart();

        // Delete remaining items
        for (int i = 0; i < _control1.Content.childCount; i++)
            Destroy(_control1.Content.GetChild(i).gameObject);
        for (int i = 0; i < _control2.Content.childCount; i++)
            Destroy(_control2.Content.GetChild(i).gameObject);

        // Set pairs
        if (_numOfItems > _data.pairs.Length)
            _numOfItems = _data.pairs.Length;
        List<PuzzleData.Pair> tPairs = new List<PuzzleData.Pair>();
        for (int i = 0; i < _data.pairs.Length; i++)
            tPairs.Add(_data.pairs[i]);

        // Instantiate items
        for (int i = 0; i < _numOfItems; i++)
        {
            int tIndex = Random.Range(0, tPairs.Count);
            PuzzleData.Pair tPair = tPairs[tIndex];
            tPairs.RemoveAt(tIndex);
            // Create item 1
            PuzzleItem tItem = Instantiate(_item1Pref, _control1.Content, false).GetComponent<PuzzleItem>();
            tItem.SetData(i, tPair.image1);
            // Create item 2
            tItem = Instantiate(_item2Pref, _control2.Content, false).GetComponent<PuzzleItem>();
            tItem.SetData(i, tPair.image2);
        }
        // Shuffle second list
        _control2.Shuffle();

        _lives = _maxLives;
        UpdateLives();

        SetConfirmMsgVisible(false, true);
        _gameUI.ResetFeedback();

        CGlobal.ExecuteAfterTime(() =>
        {
            _control1.Select(0, true);
            _control2.Select(0, true);
        }, .1f);
    }
}
