﻿using UnityEngine;
using UnityEngine.EventSystems;

public class PuzzleScrollHandler : MonoBehaviour, IBeginDragHandler, IEndDragHandler
{
    ScrollControl _control;

    public void SetControl(ScrollControl aControl)
    {
        _control = aControl;
    }

    public void OnEndDrag(PointerEventData data)
    {
        _control.EndHandle();
    }

    public void OnBeginDrag(PointerEventData data)
    {
        _control.StartHandle();
    }
}
