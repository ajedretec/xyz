﻿using UnityEngine;

[CreateAssetMenu(fileName = "PuzzleData", menuName = "Data/Puzzle data")]
public class PuzzleData : ScriptableObject
{
    [System.Serializable]
    public class Pair
    {
        public Sprite image1;
        public Sprite image2;
    }

    public Pair[] pairs;
}
