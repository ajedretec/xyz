﻿using UnityEngine;
using UnityEngine.UI;

public class ScrollControl : MonoBehaviour
{
    [SerializeField]
    ScrollRect _scroll;
    [SerializeField]
    GridLayoutGroup _grid;

    public RectTransform Content { get { return _scroll.content; } }
    public float scale = 1.5f;
    public float adjustTime = .3f;
    public float threshold = 10f;
    enum State
    {
        Idle,
        Handled,
        Scrolling,
        Adjusting,
    }
    State _state;
    float _time;

    float _fromPosition;
    float _targetPosition;

    private void Awake()
    {
        _scroll.gameObject.AddComponent<PuzzleScrollHandler>().SetControl(this);
    }

    private void Start()
    {
        SetState(State.Adjusting);
    }

    int GetStep(float aPosition)
    {
        aPosition *= -1;
        if (aPosition < 0)
            aPosition = 0;
        else if (aPosition > _scroll.content.rect.width)
            aPosition = _scroll.content.rect.width;
        return Mathf.Clamp(Mathf.RoundToInt((aPosition - _grid.cellSize.x / 2)
            / (_grid.cellSize.x + _grid.spacing.x)), 0, Content.childCount - 1);
    }

    float GetPosition(int i)
    {
        if (i < 0)
            i = 0;
        else if (i >= _scroll.content.childCount)
            i = _scroll.content.childCount - 1;

        return -(_grid.cellSize.x / 2 + i * (_grid.cellSize.x + _grid.spacing.x));
    }

    void SetState(State aState)
    {
        _state = aState;
        _time = 0;
        if (_state == State.Idle)
        {
            PuzzleManager.Inst.OnScrollStop();
        }
        else if (_state == State.Scrolling)
        {

        }
        else if (_state == State.Adjusting)
        {
            _scroll.velocity = Vector2.zero;
        }
    }

    public void Update()
    {
        if (_state == State.Scrolling)
        {
            if (Mathf.Abs(_scroll.velocity.x) < threshold)
            {
                // Set adjust positions
                _fromPosition = _scroll.content.anchoredPosition.x;
                int tPos = GetStep(_fromPosition);
                _targetPosition = GetPosition(tPos);

                SetState(State.Adjusting);
                return;
            }
        }
        else if (_state == State.Adjusting)
        {
            if (_time < adjustTime)
            {
                _scroll.content.anchoredPosition = new Vector2(Mathf.Lerp(
                    _fromPosition, _targetPosition, _time / adjustTime),
                    _scroll.content.anchoredPosition.y);
            }
            else
            {
                _scroll.content.anchoredPosition = new Vector2(_targetPosition,
                    _scroll.content.anchoredPosition.y);
                SetState(State.Idle);
                return;
            }
        }

        // Adjust size
        if (_scroll.content.childCount > 0)
        {
            Transform tTransform;
            int tCurstep = GetStep(_scroll.content.anchoredPosition.x);
            for (int i = 0; i < tCurstep; i++)
            {
                tTransform = _scroll.content.GetChild(i);
                tTransform.localScale = Vector3.one;
            }
            tTransform = _scroll.content.GetChild(tCurstep);
            tTransform.localScale = Vector3.one * Mathf.Lerp(1, scale,
                (1 - (Mathf.Abs(_scroll.content.anchoredPosition.x - GetPosition(tCurstep))
                / (_grid.cellSize.x + _grid.spacing.x))));
            for( int i = tCurstep + 1; i < _scroll.content.childCount; i++)
            {
                tTransform = _scroll.content.GetChild(i);
                tTransform.localScale = Vector3.one;
            }
        }
            
        _time += Time.deltaTime;
    }

    public void StartHandle()
    {
        SetState(State.Handled);
    }

    public void EndHandle()
    {
        SetState(State.Scrolling);
    }

    public void Shuffle()
    {
        for (int i = 0; i < Content.childCount; i++)
            Content.GetChild(i).SetSiblingIndex(Random.Range(0, Content.childCount));
    }

    public bool IsStopped()
    {
        return _state == State.Idle;
    }

    public Transform GetSelectedObject()
    {
        return Content.GetChild(GetStep(Content.anchoredPosition.x));
    }

    public void Select(int aIndex, bool aInstant)
    {
        if (aInstant)
        {
            _scroll.content.anchoredPosition = new Vector2(GetPosition(aIndex),
                    _scroll.content.anchoredPosition.y);
        }
        else
        {
            _fromPosition = _scroll.content.anchoredPosition.x;
            _targetPosition = GetPosition(aIndex);
        }
    }
}
