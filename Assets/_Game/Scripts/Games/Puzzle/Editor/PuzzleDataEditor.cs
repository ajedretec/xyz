﻿using UnityEditor;
using UnityEngine;
using RhoTools.ReorderableList;

[CustomEditor(typeof(PuzzleData))]
public class PuzzleDataEditor : Editor
{
    SerializedObject _obj;
    SerializedProperty _pairs;

    private void OnEnable()
    {
        _obj = new SerializedObject(target);
        _pairs = _obj.FindProperty("pairs");
    }

    public override void OnInspectorGUI()
    {
        if (_obj != null)
        {
            ReorderableListGUI.Title("Pairs");
            ReorderableListGUI.ListField(_pairs);
            _obj.ApplyModifiedProperties();
        }
    }
}

[CustomPropertyDrawer(typeof(PuzzleData.Pair))]
public class PuzzleDataPairDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight * 2;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        position.height = EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("image1"));
        position.y += position.height;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("image2"));
    }
}
