﻿using UnityEngine;
using UnityEngine.UI;

public class PuzzleItem : MonoBehaviour
{
    public int ID { get; private set; }
    [SerializeField]
    Image _image;

    public void SetData(int aID, Sprite aImage)
    {
        ID = aID;
        _image.sprite = aImage;
    }

    public void Remove()
    {
        Destroy(gameObject);
    }
}
