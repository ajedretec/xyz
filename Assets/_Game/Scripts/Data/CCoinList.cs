﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data/New CoinsList", menuName = "Coins list")]
public class CCoinList : ScriptableObject
{
    [System.Serializable]
    public class Coin
    {
        public Sprite sprite;
        public int value;
    }

    public Coin[] coins;
}
