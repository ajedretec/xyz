﻿using UnityEngine;

[CreateAssetMenu(fileName = "SceneList", menuName = "Data/Scene list")]
public class CSceneList : ScriptableObject
{
    [SerializeField]
    public Object[] scenes;
    [SerializeField]
    public int[] sceneIndices;
}
