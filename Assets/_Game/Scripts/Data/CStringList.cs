﻿using UnityEngine;

[CreateAssetMenu(fileName = "StringList", menuName = "Data/String list")]
public class CStringList : ScriptableObject
{
    public string[] titles;
    public string[] strings;
}
