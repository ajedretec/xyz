﻿using UnityEditor;
using RhoTools.ReorderableList;
using RhoTools;
using UnityEngine;

[CustomEditor(typeof(CSpriteList))]
public class CSpriteListEditor : Editor
{
    SerializedObject _object;
    SerializedProperty _sprites;

    private void OnEnable()
    {
        _object = new SerializedObject(target);
        _sprites = _object.FindProperty("sprites");
    }

    public override void OnInspectorGUI()
    {
        CEditorGUILayout.DropAreaGUI("Drop images", 50, OnDrop);
        if (_object != null)
        {
            ReorderableListGUI.Title("Sprites");
            ReorderableListGUI.ListField(_sprites);
            _object.ApplyModifiedProperties();
        }
    }

    void OnDrop(Object aObj)
    {
        if (aObj is Sprite)
        {
            AddSprite(aObj);
        }
        else
        {
            string spriteSheet = AssetDatabase.GetAssetPath(aObj);
            Object[] sprites = AssetDatabase.LoadAllAssetsAtPath(spriteSheet);

            for (int i = 0; i < sprites.Length; i++)
            {
                AddSprite(sprites[i]);
            }
        }
        _object.ApplyModifiedProperties();
        Repaint();
    }

    void AddSprite(Object aObj)
    {
        if (aObj is Sprite)
        {
            _sprites.arraySize++;
            _sprites.GetArrayElementAtIndex(_sprites.arraySize - 1)
                .objectReferenceValue = aObj;
        }
    }
}
