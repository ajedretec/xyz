﻿using UnityEngine;
using UnityEditor;
using RhoTools.ReorderableList;
using UnityEngine.SceneManagement;

[CustomEditor(typeof(CSceneList))]
public class CSceneListEditor : Editor
{
    SerializedObject _obj;
    ScenesAdaptor _scenes;
    CSceneList _target;

    private void OnEnable()
    {
        _obj = new SerializedObject(target);
        _target = target as CSceneList;
        _scenes = new ScenesAdaptor(_obj.FindProperty("scenes"), _target);
    }

    public override void OnInspectorGUI()
    {
        if (_target.scenes == null)
            _target.scenes = new Object[0];
        if (_target.sceneIndices == null || _target.sceneIndices.Length < _target.scenes.Length)
            UpdateNames();
        if (_obj != null)
        {
            EditorGUI.BeginChangeCheck();
            ReorderableListGUI.Title("Scenes");
            ReorderableListGUI.ListField(_scenes,
                ReorderableListFlags.ShowIndices|ReorderableListFlags.DisableReordering);
            _obj.ApplyModifiedProperties();
            if (EditorGUI.EndChangeCheck())
            {
                UpdateNames();
            }

            if (GUILayout.Button("Force update"))
            {
                UpdateNames();
            }
        }
    }

    void UpdateNames()
    {
        _target.sceneIndices = new int[_scenes.Count];
        
        for (int i = 0; i < _target.sceneIndices.Length; i++)
        {
            SceneAsset tScene = _scenes.Get(i);
            if (tScene != null)
            {
                for (int j = 0; j < SceneManager.sceneCountInBuildSettings; j++)
                {
                    if (SceneUtility.GetScenePathByBuildIndex(j)
                        == AssetDatabase.GetAssetPath(tScene))
                    {
                        _target.sceneIndices[i] = j;
                        break;
                    }
                }
            }
            else
                _target.sceneIndices[i] = -1;
        }

        EditorUtility.SetDirty(target);
        AssetDatabase.SaveAssets();
    }

    class ScenesAdaptor : IReorderableListAdaptor
    {
        SerializedProperty _prop;
        CSceneList _target;

        public int Count
        {
            get
            {
                return _prop.arraySize;
            }
        }

        public ScenesAdaptor(SerializedProperty aProp, CSceneList aTarget)
        {
            _prop = aProp;
            _target = aTarget;
        }

        public bool CanDrag(int index)
        {
            return true;
        }

        public bool CanRemove(int index)
        {
            return true;
        }

        public void Add()
        {
            _prop.InsertArrayElementAtIndex(_prop.arraySize);
        }

        void ArrayGUI(SerializedObject obj, string name)
        {
            int size = obj.FindProperty(name + ".Array.size").intValue;

            int newSize = EditorGUILayout.IntField(name + " Size", size);

            if (newSize != size)
                obj.FindProperty(name + ".Array.size").intValue = newSize;

            EditorGUI.indentLevel = 3;

            for (int i = 0; i < newSize; i++)
            {
                var prop = obj.FindProperty(string.Format("{0}.Array.data[{1}]", name, i));
                EditorGUILayout.PropertyField(prop);
            }
        }

        public void Insert(int index)
        {
            _prop.InsertArrayElementAtIndex(index);
        }

        public void Duplicate(int index)
        {
            Add();
            _prop.GetArrayElementAtIndex(Count - 1).objectReferenceValue = _prop.GetArrayElementAtIndex(index).objectReferenceValue;
        }

        public void Remove(int index)
        {
            _prop.DeleteArrayElementAtIndex(index);
        }

        public void Move(int sourceIndex, int destIndex)
        {
            _prop.MoveArrayElement(sourceIndex, destIndex);
        }

        public void Clear()
        {
            _prop.ClearArray();
        }

        public void BeginGUI()
        {
            
        }

        public void EndGUI()
        {

        }

        public void DrawItemBackground(Rect position, int index)
        {

        }

        public void DrawItem(Rect position, int index)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            GUI.Label(position, _target.sceneIndices[index].ToString());
            position.y += position.height;
            EditorGUI.BeginChangeCheck();
            Object tObj = EditorGUI.ObjectField(position, _prop.GetArrayElementAtIndex(index).objectReferenceValue,
                typeof(SceneAsset), false);
            if (EditorGUI.EndChangeCheck())
                _prop.GetArrayElementAtIndex(index).objectReferenceValue = tObj;
        }

        public float GetItemHeight(int index)
        {
            return EditorGUIUtility.singleLineHeight * 2;
        }

        public SceneAsset Get(int index)
        {
            return (SceneAsset)_prop.GetArrayElementAtIndex(index).objectReferenceValue;
        }
    }
}
