﻿using UnityEngine;
using UnityEditor;
using RhoTools.ReorderableList;

[CustomEditor(typeof(CStringList))]
public class CStringListEditor : Editor
{
    SerializedObject _obj;
    StringsAdaptor _adaptor;

    private void OnEnable()
    {
        _obj = new SerializedObject(target);
        _adaptor = new StringsAdaptor(
            _obj.FindProperty("titles"),
            _obj.FindProperty("strings"));
    }

    public override void OnInspectorGUI()
    {
        if (_obj != null)
        {
            ReorderableListGUI.Title("Strings");
            ReorderableListGUI.ListField(_adaptor);
            _obj.ApplyModifiedProperties();
        }
    }

    class StringsAdaptor : IReorderableListAdaptor
    {
        SerializedProperty _propTitles;
        SerializedProperty _propStrings;
        CStringList _target;

        public int Count
        {
            get
            {
                return _propTitles.arraySize;
            }
        }

        public StringsAdaptor(SerializedProperty aPropTitles, SerializedProperty aPropStrings)
        {
            _propTitles = aPropTitles;
            _propStrings = aPropStrings;
        }

        public bool CanDrag(int index)
        {
            return true;
        }

        public bool CanRemove(int index)
        {
            return true;
        }

        public void Add()
        {
            _propTitles.InsertArrayElementAtIndex(_propTitles.arraySize);
            _propStrings.InsertArrayElementAtIndex(_propStrings.arraySize);
        }

        public void Insert(int index)
        {
            _propTitles.InsertArrayElementAtIndex(index);
            _propStrings.InsertArrayElementAtIndex(index);
        }

        public void Duplicate(int index)
        {
            Add();
            _propTitles.GetArrayElementAtIndex(Count - 1).stringValue = _propTitles.GetArrayElementAtIndex(index).stringValue;
            _propStrings.GetArrayElementAtIndex(Count - 1).stringValue = _propStrings.GetArrayElementAtIndex(index).stringValue;
        }

        public void Remove(int index)
        {
            _propTitles.DeleteArrayElementAtIndex(index);
            _propStrings.DeleteArrayElementAtIndex(index);
        }

        public void Move(int sourceIndex, int destIndex)
        {
            _propTitles.MoveArrayElement(sourceIndex, destIndex);
            _propStrings.MoveArrayElement(sourceIndex, destIndex);
        }

        public void Clear()
        {
            _propTitles.ClearArray();
            _propStrings.ClearArray();
        }

        public void BeginGUI()
        {

        }

        public void EndGUI()
        {

        }

        public void DrawItemBackground(Rect position, int index)
        {

        }

        void DrawString(Rect position, SerializedProperty prop, int index, string text)
        {
            EditorGUI.BeginChangeCheck();
            string t_Val = EditorGUI.TextField(position, text, prop.GetArrayElementAtIndex(index).stringValue);
            if (EditorGUI.EndChangeCheck())
                prop.GetArrayElementAtIndex(index).stringValue = t_Val;
        }

        public void DrawItem(Rect position, int index)
        {
            position.height = EditorGUIUtility.singleLineHeight;
            DrawString(position, _propTitles, index, "Title:");
            position.y += position.height;
            DrawString(position, _propStrings, index, "String:");
        }

        public float GetItemHeight(int index)
        {
            return EditorGUIUtility.singleLineHeight * 2;
        }
    }
}