﻿using UnityEngine;

[CreateAssetMenu(fileName = "SpriteList", menuName = "Data/Sprite List")]
public class CSpriteList : ScriptableObject
{
    public Sprite[] sprites;
}
