﻿public class CGlobalTexts
{
    public const string VICTORY_MSG = "WIN_MSG";
    public const string COLOR_VAR = "%color";
    public const string TIE_MSG = "TIE_MSG";
    public const string WHITE_PIECES = "WHITE_PIECES";
    public const string BLACK_PIECES = "BLACK_PIECES";
}
