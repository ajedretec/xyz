﻿using DoozyUI;
using UnityEngine;

public class UIMenu : MonoBehaviour
{
    [SerializeField]
    UIElement[] _menues;

    protected virtual void Start()
    {
        foreach (UIElement tMenu in _menues)
            tMenu.gameObject.SetActive(true);
        for (int i = 1; i < _menues.Length; i++)
            _menues[i].Hide(true);
        _menues[0].Show(true);
    }

    public void ShowMenu(UIElement aMenu)
    {
        foreach (UIElement tElement in _menues)
        {
            if (tElement != aMenu)
                tElement.Hide(false);
        }
        aMenu.Show(false);
    }
}
