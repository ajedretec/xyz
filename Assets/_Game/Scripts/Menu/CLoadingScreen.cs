﻿using DoozyUI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CLoadingScreen : MonoBehaviour
{
    [SerializeField]
    UIElement _content;
    [SerializeField]
    bool _waitForUserLoad;
    public static bool WaitForUserLoad
    {
        get { return Inst._waitForUserLoad; }
        set { Inst._waitForUserLoad = value; }
    }
    [SerializeField]
    float _minWaitTime = 2f;

    int _sceneIndex;
    AsyncOperation _operation;
    float _timer;
    bool _unload;
    bool _loading;

    static CLoadingScreen _inst;
    public static CLoadingScreen Inst
    {
        get
        {
            if (_inst == null)
                _inst = CGlobal.Inst.GetComponentInChildren<CLoadingScreen>(true);
            return _inst;
        }
    }

    private void Awake()
    {
        if (_inst != null && _inst != this)
        {
            Destroy(gameObject);
            return;
        }
        _inst = this;
    }

    private void Start()
    {
        _content.OnInAnimationsFinish.AddListener(OnInAnimationFinish);
    }

    private void Update()
    {
        if (_loading)
        {
            if (!_waitForUserLoad)
            {
                if (_operation != null && _content.isVisible)
                {
                    if (_operation.isDone)
                        _unload = true;
                }
            }

            if (_unload && _timer >= _minWaitTime)
            {
                _content.Hide(false);
                _unload = false;
            }
            else
                _timer += Time.unscaledDeltaTime;
        }
    }

    public static void LoadingDone()
    {
        Inst._unload = true;
    }

    void OnInAnimationFinish()
    {
        Debug.Log("Animation finish");
        _operation = SceneManager.LoadSceneAsync(_sceneIndex);
    }

    public static void LoadScene(int aSceneIndex)
    {
        SceneManager.LoadScene(aSceneIndex);
        /*
        if (!Inst.gameObject.activeSelf)
            Inst.gameObject.SetActive(true);
        Inst._sceneIndex = aSceneIndex;
        Inst._loading = true;
        Inst._unload = false;
        Inst._content.Hide(true);
        CGlobal.ExecuteAfterTime(() =>
        {
            Inst._content.Show(false);
        }, .1f);
        */
    }

    public static void LoadScene(string aSceneName)
    {
        SceneManager.LoadScene(aSceneName);
    }
}
