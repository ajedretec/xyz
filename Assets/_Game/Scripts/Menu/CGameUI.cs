﻿using DoozyUI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class CGameUI : MonoBehaviour
{
    [SerializeField]
    TextMeshProUGUI _gameTitle;
    [SerializeField]
    Graphic _titleBG;
    [SerializeField]
    bool _confirm;
    [SerializeField]
    UIElement _confirmMenu;
    [SerializeField, Header("Tutorial")]
    UIElement _tutorialPanel;
    [SerializeField]
    Transform _tutorialRoot;
    [SerializeField, Header("Game Over")]
    UIElement _shade;
    [SerializeField]
    GameObject _victoryMsg;
    [SerializeField]
    GameObject _defeatMsg;
    [SerializeField, Header("Pause")]
    UIElement _pauseMenu;

    CGameManager _currentManager;
    System.Action<bool> _onPause;
    System.Action _alternateBackFun;

    private void Awake()
    {
        ResetFeedback();
        _shade.gameObject.SetActive(true);
        _tutorialPanel.gameObject.SetActive(true);
        _pauseMenu.gameObject.SetActive(true);
    }

    public void ResetFeedback()
    {
        if (_shade.gameObject.activeSelf)
            _shade.Hide(!_shade.isVisible);
        _victoryMsg.SetActive(false);
        _defeatMsg.SetActive(false);
    }

    public void SetTitle(string aGameName, Color aTitleColor, Color aBGColor)
    {
        _gameTitle.text = aGameName;
        _gameTitle.color = aTitleColor;
        _titleBG.color = aBGColor;
    }

    public void SetManager(CGameManager aManager)
    {
        _currentManager = aManager;
    }

    public void GoToMainMenu()
    {
        if (_alternateBackFun != null)
            _alternateBackFun();
        else
            CGlobal.GoToMainMenu();
    }

    public void ShowVictory()
    {
        _shade.gameObject.SetActive(true);
        _shade.Show(false);
        _victoryMsg.SetActive(true);
    }

    public void ShowDefeat()
    {
        _shade.gameObject.SetActive(true);
        _shade.Show(false);
        _defeatMsg.SetActive(true);
    }

    public void Restart()
    {
        _currentManager.Restart();
    }

    public void PositiveFeedback()
    {
        // TODO: Confeti
    }

    public void NegativeFeedback()
    {
        // TODO: Bad feedback
    }

    public void SetTutorial(GameObject aPrefab)
    {
        Instantiate(aPrefab, _tutorialRoot, false);
    }

    public void ShowTutorial()
    {
        _tutorialPanel.Show(false);
    }

    public void HideTutorial()
    {
        _tutorialPanel.Hide(false);
    }

    public void Pause()
    {
        //_pauseMenu.Show(false);
        Time.timeScale = 0;
        if (_onPause != null)
            _onPause(true);
    }

    public void Unpause()
    {
        CGlobal.ExecuteNextFrame(() =>
        {
            //_pauseMenu.Hide(false);
            Time.timeScale = 1;
            if (_onPause != null)
                _onPause(false);
        });
    }

    public void AddOnPauseCallback(System.Action<bool> aCallback)
    {
        _onPause -= aCallback;
        _onPause += aCallback;
    }

    public bool IsBlocked()
    {
        return _pauseMenu.isVisible || _tutorialPanel.isVisible;
    }

    public void ConfirmGoToMenu()
    {
        if (_confirm)
        {
            Pause();
            _confirmMenu.gameObject.SetActive(true);
            _confirmMenu.Show(false);
        }
        else
            GoToMainMenu();
    }

    public void SetAlternateBackFunction(System.Action aFun)
    {
        _alternateBackFun = aFun;
    }
}
