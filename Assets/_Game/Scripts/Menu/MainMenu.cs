﻿using DoozyUI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : UIMenu
{
    public const string CARDS = "cartas";
    public const string POWER = "poder";
    public const string CARDS_PLUS_POWER = "cartasmaspoder";
    readonly static string[] CARD_TYPES = { CARDS, POWER, CARDS_PLUS_POWER };

    protected override void Start()
    {
        base.Start();
        // If there is a network manager, destroy it
        CustomNetLobbyManager.DestroyObj();
    }

    public void LoadScene(int aSceneIndex)
    {
        CGlobal.LoadScene(aSceneIndex);
    }

    public void SetCardType(int aType)
    {
        if (aType < 0)
            aType = 0;
        else if (aType >= CARD_TYPES.Length)
            aType = CARD_TYPES.Length - 1;
        Turnos.setTipoCarta(CARD_TYPES[aType]);
    }

    public void Quit()
    {
        Application.Quit();
    }
}
