﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using RhoTools;

public class CTiling3D : EditorWindow
{
    #region Window definition
    [MenuItem(CConstants.ROOT_MENU  +"Tiling...")]
    public static CTiling3D ShowWindow()
    {
        return GetWindow(typeof(CTiling3D)) as CTiling3D;
    }
    #endregion

    const float MIN_TILE_SIZE = 0.01f;

    Transform _rootTransform;
    CTilemapRoot _rootTilemap;
    Transform[] _objectsList;
    BoxCollider[] _collidersList;
    List<BoxCollider> _colliders = new List<BoxCollider>();
    bool _dirty;

    void OnGUI()
    {
        EditorGUI.BeginChangeCheck();
        Transform tRootTransform = EditorGUILayout.ObjectField("Root", _rootTransform, typeof(Transform), true) as Transform;

        if (EditorGUI.EndChangeCheck())
        {
            _rootTransform = tRootTransform;
            if (_rootTransform != null)
            {
                _rootTilemap = _rootTransform.GetComponent<CTilemapRoot>();
                if (_rootTilemap == null)
                    _rootTilemap = _rootTransform.gameObject.AddComponent<CTilemapRoot>();
                CapSize();
            }
        }
        if (_rootTilemap != null)
        {
            EditorGUI.BeginChangeCheck();
            Vector3 tTileSize = EditorGUILayout.Vector3Field("Tile size", _rootTilemap.tileSize);
            GUILayout.BeginHorizontal();
            bool tUseColliders = GUILayout.Toggle(_rootTilemap.useColliders, "Use colliders");
            bool tForceColliderSize = GUILayout.Toggle(_rootTilemap.forceColliderSize, "Force collider size");
            GUILayout.EndHorizontal();
            if (EditorGUI.EndChangeCheck())
            {
                _rootTilemap.tileSize = tTileSize;
                _rootTilemap.useColliders = tUseColliders;
                _rootTilemap.forceColliderSize = tForceColliderSize;
                CapSize();
                Undo.RecordObject(_rootTilemap, "Change tilemap");
                _dirty = true;
            }
        }
        else
        {
            if (GUILayout.Button("Create root"))
            {
                _rootTransform = new GameObject("Root").transform;
                _rootTilemap = _rootTransform.gameObject.AddComponent<CTilemapRoot>();
                CapSize();
            }
        }
    }

    void CapSize()
    {
        if (_rootTilemap.tileSize.x < MIN_TILE_SIZE)
            _rootTilemap.tileSize.x = MIN_TILE_SIZE;
        if (_rootTilemap.tileSize.y < MIN_TILE_SIZE)
            _rootTilemap.tileSize.y = MIN_TILE_SIZE;
        if (_rootTilemap.tileSize.z < MIN_TILE_SIZE)
            _rootTilemap.tileSize.z = MIN_TILE_SIZE;
    }

    void Update()
    {
        if (_rootTransform != null)
        {
            int tTiles = GetTilesAmount();
            if (_objectsList == null || tTiles != _objectsList.Length)
            {
                GenerateObjectsList();
            }

            for (int i = 0; i < _objectsList.Length; i++)
            {
                Transform tTransform = _objectsList[i];
                Vector3 tNewPos;
                if (_rootTilemap.useColliders)
                {
                    BoxCollider tBoxCollider = _collidersList[i];

                    if (_rootTilemap.forceColliderSize)
                        tBoxCollider.size = _rootTilemap.tileSize;

                    tNewPos = Vector3.zero;
                    Vector3 tPos = tTransform.localPosition + tBoxCollider.center - tBoxCollider.size * .5f;

                    tNewPos.x = Mathf.Round(tPos.x / _rootTilemap.tileSize.x) * _rootTilemap.tileSize.x;
                    tNewPos.y = Mathf.Round(tPos.y / _rootTilemap.tileSize.y) * _rootTilemap.tileSize.y;
                    tNewPos.z = Mathf.Round(tPos.z / _rootTilemap.tileSize.z) * _rootTilemap.tileSize.z;
                    tNewPos = tNewPos + tBoxCollider.size * .5f - tBoxCollider.center;
                }
                else
                {
                    tNewPos = Vector3.zero;

                    tNewPos.x = Mathf.Round(tTransform.localPosition.x / _rootTilemap.tileSize.x) * _rootTilemap.tileSize.x;
                    tNewPos.y = Mathf.Round(tTransform.localPosition.y / _rootTilemap.tileSize.y) * _rootTilemap.tileSize.y;
                    tNewPos.z = Mathf.Round(tTransform.localPosition.z / _rootTilemap.tileSize.z) * _rootTilemap.tileSize.z;
                }

                tTransform.localPosition = tNewPos;
            }

            if (_dirty)
            {
                Undo.RecordObjects(_objectsList, "Adjust_position");
                _dirty = false;
            }
        }
    }

    int GetTilesAmount()
    {
        _colliders.Clear();
        int tTiles = 0;
        for (int i = 0; i < _rootTransform.childCount; i++)
        {
            BoxCollider tCol = _rootTransform.GetChild(i).GetComponent<BoxCollider>();
            if (tCol != null && tCol.transform.parent == _rootTransform)
            {
                _colliders.Add(tCol);
                tTiles++;
            }
        }
        return tTiles;
    }

    void GenerateObjectsList()
    {
        _objectsList = new Transform[_colliders.Count];
        _collidersList = new BoxCollider[_colliders.Count];
        for (int i = 0; i < _colliders.Count; i++)
        {
            BoxCollider tCol = _colliders[i];
            _objectsList[i] = tCol.transform;
            _collidersList[i] = tCol;
        }
    }

    public void SetRoot(Transform aRoot)
    {
        _rootTransform = aRoot;
        if (_rootTransform != null)
        {
            _rootTilemap = _rootTransform.GetComponent<CTilemapRoot>();
            if (_rootTilemap == null)
            {
                _rootTilemap = _rootTransform.gameObject.AddComponent<CTilemapRoot>();
            }
            CapSize();
        }
    }
}
