﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CTilemapRoot))]
public class CTilemapRootEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Open tiling tool"))
        {
            CTiling3D tTool = CTiling3D.ShowWindow();
            tTool.SetRoot((target as CTilemapRoot).transform);
        }
    }
}