﻿using UnityEngine;

public class CTilemapRoot : MonoBehaviour
{
    public Vector3 tileSize;
    public bool useColliders;
    public bool forceColliderSize;
}
