﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

namespace RhoTools
{
    public class CMassTextureEditor : EditorWindow
    {
        const string MENU_NAME = "Mass texture editor";
        const string USE_SELECTION_PREF = "MassTexture_useSelection";
        const string PLATFORM_PREF = "MassTexture_platform";
        const string SET_ADVANCED_PREF = "MassTexture_advanced";
        const string ONLY_AFFECT_ORIGINAL_PREF = "MassTexture_onlyAffectOriginal";
        readonly static string[] PLATFORMS = { "Default", "Web", "Standalone", "iPhone", "Android" };
        const int DEFAULT = 0;

        public static bool UseSelection
        {
            set
            {
                EditorPrefs.SetBool(USE_SELECTION_PREF, value);
            }
            get
            {
                return EditorPrefs.GetBool(USE_SELECTION_PREF);
            }
        }
        public static bool ForceTextureType
        {
            set
            {
                EditorPrefs.SetBool(SET_ADVANCED_PREF, value);
            }
            get
            {
                return EditorPrefs.GetBool(SET_ADVANCED_PREF);
            }
        }
        public static bool OnlyAffectOriginal
        {
            set
            {
                EditorPrefs.SetBool(ONLY_AFFECT_ORIGINAL_PREF, value);
            }
            get
            {
                return EditorPrefs.GetBool(ONLY_AFFECT_ORIGINAL_PREF);
            }
        }
        bool[] _platforms;
#if UNITY_4 || UNITY_5_1 || UNITY_5_2 || UNITY_5_3 || UNITY_5_4
        TextureImporterFormat _inFormat = TextureImporterFormat.AutomaticCompressed;
        TextureImporterFormat _outFormat = TextureImporterFormat.AutomaticTruecolor;
        TextureImporterType _forcedType = TextureImporterType.Advanced;
#else
        TextureImporterFormat _inFormat;
        TextureImporterFormat _outFormat;
        TextureImporterType _forcedType = TextureImporterType.Default;
#endif
        SceneAsset[] _scenes;
        System.Action[] _tabs;
        string[] _tabsTitles;
        int _selectedTab = 0;

        [MenuItem(CConstants.ROOT_MENU + MENU_NAME)]
        static void OpenWindow()
        {
            CMassTextureEditor tWindow = GetWindow<CMassTextureEditor>();
            GUIContent titleContent = new GUIContent("MassTexEdit");
            tWindow.titleContent = titleContent;
        }

        void OnEnable()
        {
            _platforms = new bool[PLATFORMS.Length];
            for (int i = 0; i < _platforms.Length; i++)
            {
                _platforms[i] = GetPlatformSelected(PLATFORMS[i]);
            }
            if (_scenes == null)
                _scenes = new SceneAsset[0];

            _tabs = new System.Action[3];
            _tabsTitles = new string[_tabs.Length];
            _tabs[0] = OnAutoPacker;
            _tabsTitles[0] = "Auto packer";
            _tabs[1] = OnFormatReplacer;
            _tabsTitles[1] = "Format replacer";
            _tabs[2] = OnTextureSelector;
            _tabsTitles[2] = "Texture selector";
        }

        void OnGUI()
        {
            _selectedTab = CEditorGUILayout.DrawTabs(_tabsTitles, _tabs, _selectedTab);
        }

        void OnAutoPacker()
        {
            GUILayout.Label("Auto packer", EditorStyles.boldLabel);

            if (!CEditorGUILayout.DropAreaGUI("Drag scene here to add", 30f, AddScene))
            {
                if (GUILayout.Button("Use scenes in build"))
                {
                    _scenes = new SceneAsset[0];
                    for (int i = 0; i < EditorBuildSettings.scenes.Length; i++)
                    {
                        EditorBuildSettingsScene tBuildScene = EditorBuildSettings.scenes[i];
                        if (tBuildScene.enabled)
                        {
                            ArrayUtility.Add<SceneAsset>(
                                ref _scenes,
                                AssetDatabase.LoadAssetAtPath<SceneAsset>(tBuildScene.path)
                            );
                        }
                    }
                }
                if (_scenes.Length == 0)
                {
                    CEditorGUILayout.Box(20f, "No scenes added");
                }
                for (int i = 0; i < _scenes.Length; i++)
                {
                    GUILayout.BeginHorizontal();
                    _scenes[i] = (SceneAsset)EditorGUILayout.ObjectField(_scenes[i], typeof(SceneAsset), false);
                    if (GUILayout.Button("X", GUILayout.MaxWidth(30)))
                    {
                        ArrayUtility.RemoveAt<SceneAsset>(ref _scenes, i);
                    }
                    GUILayout.EndHorizontal();
                }
            }

            if (GUILayout.Button("Auto pack"))
                PackTextures(_scenes);
        }

        void OnFormatReplacer()
        {
            GUILayout.Label("Format replacer", EditorStyles.boldLabel);

            EditorGUI.BeginChangeCheck();
            GUILayout.BeginVertical(GUI.skin.box);
            GUILayout.Label("Affected platforms");
            int tPrevIndent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 1;
            for (int i = 0; i < PLATFORMS.Length; i++)
            {
                _platforms[i] = EditorGUILayout.Toggle(PLATFORMS[i], _platforms[i]);
            }
            if (EditorGUI.EndChangeCheck())
            {
                for (int i = 0; i < PLATFORMS.Length; i++)
                {
                    SetPlatformSelected(PLATFORMS[i], _platforms[i]);
                }
            }
            EditorGUI.indentLevel = tPrevIndent;
            GUILayout.EndVertical();

            GUILayout.BeginHorizontal();
            GUIContent tButtonText = new GUIContent(
                "Get from selected",
                "Will retrieve the format from the selected texture in the first selected platform from the list above"
            );
            _inFormat = (TextureImporterFormat)EditorGUILayout.EnumPopup("Original format", _inFormat);
            if (GUILayout.Button(tButtonText, GUILayout.MaxWidth(120)))
            {
                _inFormat = GetFormatFromSelected(_platforms);
            }
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            _outFormat = (TextureImporterFormat)EditorGUILayout.EnumPopup("New format", _outFormat);
            if (GUILayout.Button(tButtonText, GUILayout.MaxWidth(120)))
            {
                _outFormat = GetFormatFromSelected(_platforms);
            }
            GUILayout.EndHorizontal();

            EditorGUI.BeginChangeCheck();

            GUILayout.BeginHorizontal();
            GUIContent tContent = new GUIContent("Only affect original format", "Otherwise it ignores the \"Original format\"");
            bool tOnlyAffectOriginal = EditorGUILayout.Toggle(tContent, OnlyAffectOriginal);
            if (tOnlyAffectOriginal)
                GUILayout.Label("Only affect" + (UseSelection ? " selected" : "") +  " textures with the original format");
            else
                GUILayout.Label("Affect ALL" + (UseSelection ? " selected" : "") + " textures (ignores \"Original format\")");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            tContent = new GUIContent("Use selection", "Otherwise it uses ALL textures");
            bool tUseSelection = EditorGUILayout.Toggle(tContent, UseSelection);
            if (tUseSelection)
                GUILayout.Label("Only affect selected textures");
            else
                GUILayout.Label("Affect ALL textures in the proyect (will take some time)");
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            tContent = new GUIContent("Force texture type", "Only sets affected textures");
            bool tSetAdvanced = EditorGUILayout.Toggle(tContent, ForceTextureType);
            if (OnlyAffectOriginal)
                GUILayout.Label("Only affects textures with the original format");
            else
                GUILayout.Label("Affects ALL" + (UseSelection ? " selected" : "") + " textures");
            GUILayout.EndHorizontal();
            EditorGUI.BeginDisabledGroup(!ForceTextureType);
            _forcedType = (TextureImporterType)EditorGUILayout.EnumPopup("Forced texture type", _forcedType);
            EditorGUI.EndDisabledGroup();

            if (EditorGUI.EndChangeCheck())
            {
                UseSelection = tUseSelection;
                ForceTextureType = tSetAdvanced;
                OnlyAffectOriginal = tOnlyAffectOriginal;
            }
            string tWord;
            if (tUseSelection)
                tWord = "selected";
            else
                tWord = "all";
            if (GUILayout.Button("Change " + tWord + " textures format"))
            {
                string[] tAssets;
                if (UseSelection)
                    tAssets = Selection.assetGUIDs;
                else
                    tAssets = AssetDatabase.FindAssets("t:Texture");

                ChangeAllTextureFormats(tAssets, _platforms, _inFormat, _outFormat, _forcedType);
            }
        }

        void OnTextureSelector()
        {
            _inFormat = (TextureImporterFormat)EditorGUILayout.EnumPopup(
                "Select all images with format:", _inFormat);
            if (GUILayout.Button("Select"))
            {
                string[] tAssets = AssetDatabase.FindAssets("t:Texture");
                SelectTexturesWithFormat(tAssets, _platforms, _inFormat);
            }
        }

        public static void PackTextures(SceneAsset[] aScenes)
        {
            Dictionary<Texture2D, bool[]> tDict = new Dictionary<Texture2D, bool[]>();
            int tInd = 0;

            int tTotal = 0;
            List<Object[]> tDependenciesList = new List<Object[]>();
            for (int i = 0; i < aScenes.Length; i++)
            {
                Object[] tScene = { aScenes[i] };
                Object[] tDependencies = EditorUtility.CollectDependencies(tScene);
                tDependenciesList.Add(tDependencies);
                tTotal += tDependencies.Length;
            }

            for (int i = 0; i < aScenes.Length; i++)
            {
                Object[] tDependencies = tDependenciesList[i];
                for (int j = 0; j < tDependencies.Length; j++)
                {
                    Object tObj = tDependencies[j];
                    if (tObj is Texture2D)
                    {
                        Texture2D tTex = tObj as Texture2D;
                        if (tDict.ContainsKey(tTex))
                        {
                            tDict[tTex][i] = true;
                        }
                        else
                        {
                            bool[] tScenes = new bool[aScenes.Length];
                            tScenes[i] = true;
                            tDict.Add(tTex, tScenes);
                        }
                    }
                    if (EditorUtility.DisplayCancelableProgressBar(
                            "Configuring sprites",
                            "Finding Texture2D assets",
                            tInd / (float)tTotal))
                    {
                        EditorUtility.ClearProgressBar();
                        return;
                    }
                    tInd++;
                }
            }

            tTotal = tDict.Keys.Count;
            tInd = 0;
            foreach (KeyValuePair<Texture2D, bool[]> tKey in tDict)
            {
                string tTag = "";
                for (int i = 0; i < tKey.Value.Length; i++)
                {
                    if (tKey.Value[i])
                        tTag += aScenes[i].name;
                }
                string tAssetPath = AssetDatabase.GetAssetPath(tKey.Key);
                if (tAssetPath.StartsWith("Assets"))
                {
                    TextureImporter tTexture = AssetImporter.GetAtPath(
                        tAssetPath) as TextureImporter;
                    tTexture.spritePackingTag = tTag;
                    tTexture.SaveAndReimport();
                }

                if (EditorUtility.DisplayCancelableProgressBar(
                        "Configuring sprites",
                        "Setting packing tags",
                        tInd / (float)tTotal))
                {
                    EditorUtility.ClearProgressBar();
                    return;
                }
                tInd++;
            }
            EditorUtility.ClearProgressBar();
        }

        public static void SelectTexturesWithFormat(string[] aGUIDs, bool[] aPlatforms, TextureImporterFormat aFormat)
        {
            List<Object> tNewSelection = new List<Object>();
            List<string> tPlatforms = new List<string>();
            for (int i = 0; i < aPlatforms.Length; i++)
            {
                if (aPlatforms[i])
                    tPlatforms.Add(PLATFORMS[i]);
            }

            for (int i = 0; i < aGUIDs.Length; i++)
            {
                TextureImporter tAsset = AssetImporter.GetAtPath(
                    AssetDatabase.GUIDToAssetPath(aGUIDs[i])) as TextureImporter;
                
                for (int j = 0; j < tPlatforms.Count; j++)
                {
                    TextureImporterPlatformSettings tSettings =
                        tAsset.GetPlatformTextureSettings(tPlatforms[j]);
                    if (tSettings.format == aFormat)
                    {
                        tNewSelection.Add(AssetDatabase.LoadAssetAtPath(
                            AssetDatabase.GUIDToAssetPath(aGUIDs[i]),
                            typeof(Texture2D)));
                        break;
                    }
                }

                if (EditorUtility.DisplayCancelableProgressBar(
                        "Finding textures",
                        "Finding textures with format " + aFormat,
                        i / (float)aGUIDs.Length))
                {
                    EditorUtility.ClearProgressBar();
                    break;
                }
            }
            Selection.objects = tNewSelection.ToArray();
            EditorUtility.ClearProgressBar();
        }

        public static void ChangeAllTextureFormats(string[] aGUIDs, bool[] aPlatforms, TextureImporterFormat aOriginalFormat, TextureImporterFormat aNewFormat, TextureImporterType aForcedType)
        {
            int tTotal = aGUIDs.Length;
            for (int i = 0; i < tTotal; i++)
            {
                AssetImporter tAsset = AssetImporter.GetAtPath(
                    AssetDatabase.GUIDToAssetPath(aGUIDs[i]));
                if (tAsset != null && tAsset is TextureImporter)
                {
                    TextureImporter tTexture = tAsset as TextureImporter;
                    ReplaceTextureFormat(
                        tTexture,
                        aPlatforms,
                        aOriginalFormat,
                        aNewFormat
                    );

                    if (ForceTextureType)
                        tTexture.textureType = aForcedType;
                }
                
                if (EditorUtility.DisplayCancelableProgressBar(
                        "Configuring textures",
                        "Changing compression format",
                        i / (float)tTotal))
                {
                    EditorUtility.ClearProgressBar();
                    break;
                }
            }
            EditorUtility.ClearProgressBar();
        }

        public static void ReplaceTextureFormat(TextureImporter aTexture, bool[] aPlatforms, TextureImporterFormat aOriginalFormat, TextureImporterFormat aNewFormat)
        {
            int tMaxTexSize;
            TextureImporterFormat tFormat;
            
            /*
            if (aPlatforms[0])
            {
                aTexture.GetPlatformTextureSettings()
                if (aTexture.textureFormat == aOriginalFormat)
                    aTexture.textureFormat = aNewFormat;
            }
            */
            for (int j = 0; j < aPlatforms.Length; j++)
            {
                if (aPlatforms[j])
                {
                    string tPlatform = PLATFORMS[j];
                    aTexture.GetPlatformTextureSettings(tPlatform, out tMaxTexSize, out tFormat);
                    if (!OnlyAffectOriginal || tFormat == aOriginalFormat)
                    {
                        TextureImporterPlatformSettings tSettings = new TextureImporterPlatformSettings()
                        {
                            name = tPlatform,
                            maxTextureSize = tMaxTexSize,
                            format = aNewFormat
                        };
                        aTexture.SetPlatformTextureSettings(tSettings);
                    }
                }
            }
            aTexture.SaveAndReimport();
        }

        public static TextureImporterFormat GetFormatFromSelected(bool[] aPlatforms)
        {
            if (Selection.activeObject != null && Selection.activeObject is Texture2D)
            {
                TextureImporter tTexture = AssetImporter.GetAtPath(AssetDatabase.GetAssetPath(Selection.activeObject)) as TextureImporter;
                Debug.Log("Selected: " + Selection.activeObject.name);
                /*
                if (aPlatforms[0])
                {
                    return tTexture.textureFormat;
                }
                */
                for (int i = 0; i < aPlatforms.Length; i++)
                {
                    if (aPlatforms[i])
                    {
                        TextureImporterFormat tFormat;
                        int tSize;
                        tTexture.GetPlatformTextureSettings(PLATFORMS[i], out tSize, out tFormat);
                        Debug.Log(PLATFORMS[i] + ": " + tFormat);
                        return tFormat;
                    }
                }
                tTexture.GetAutomaticFormat(PLATFORMS[0]);
            }
            return TextureImporterFormat.ARGB16;
        }

        void AddScene(Object aObj)
        {
            if (aObj is SceneAsset)
            {
                ArrayUtility.Add<SceneAsset>(ref _scenes, aObj as SceneAsset);
            }
        }

        public static bool GetPlatformSelected(string aPlatform)
        {
            return EditorPrefs.GetBool(PLATFORM_PREF + aPlatform);
        }

        public static void SetPlatformSelected(string aPlatform, bool aValue)
        {
            EditorPrefs.SetBool(PLATFORM_PREF + aPlatform, aValue);
        }
    }
}
