﻿using UnityEngine;
using UnityEditor;

namespace RhoTools
{
    public static class CurveEditor
    {
        public static void DrawMultipleCurves(Rect position, params AnimationCurve[] curves)
        {
            
        }

        public static void DrawCurve(Rect position, AnimationCurve curve, Color color)
        {
            if (curve.length == 0)
                return;
            DrawSegment(position, curve.keys[0], curve.keys[1], color);

            for (int i = 1; i < curve.length - 1; i++)
            {
                DrawSegment(position, curve.keys[i], curve.keys[i + 1], color);
            }
        }

        static void DrawSegment(Rect position, Keyframe aFirst, Keyframe aSecond, Color color)
        {
            Vector2 tOrigin = position.position + Vector2.up * position.height;
            Vector2 tPos1 = new Vector2(aFirst.time * position.width,
                aFirst.value * -position.height) + tOrigin;
            Vector2 tPos2 = new Vector2(aSecond.time * position.width,
                aSecond.value * -position.height) + tOrigin;
            Vector2 tStartTan = GetTangent(aFirst.outTangent);
            tStartTan.y *= -1;
            Vector2 tEndTan = GetTangent(aSecond.inTangent);
            tEndTan.x *= -1;
            Handles.DrawBezier(tPos1, tPos2, tPos1 + tStartTan, tPos2 + tEndTan, color, null, 1);
        }

        static Vector2 GetTangent(float aTan)
        {
            return Vector2.right.Rotate(Mathf.Atan(aTan) * Mathf.Rad2Deg) * 10;
        }
    }
}