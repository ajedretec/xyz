﻿using UnityEditor;
using UnityEngine;

namespace RhoTools
{
    public static class CSerializedArrayTools
    {
        public static void AddElement(SerializedProperty aProperty, Object aObject)
        {
            if (aProperty.isArray)
            {
                aProperty.arraySize++;
                int tIndex = aProperty.arraySize - 1;
                Debug.Log("Size: " + aProperty.arraySize + ", index: " + tIndex);

                // Get the array size
                //aProperty.InsertArrayElementAtIndex(tIndex);
                aProperty.GetArrayElementAtIndex(tIndex).objectReferenceValue = aObject;
            }
        }
    }
}
