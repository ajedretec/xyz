﻿using UnityEngine;
# if UNITY_EDITOR
using UnityEditor;
#endif

public class CGizmos
{
    public static void DrawCube(Vector3 aPosition, Quaternion aRotation, Vector3 aScale)
    {
            Matrix4x4 cubeTransform = Matrix4x4.TRS(aPosition, aRotation, aScale);
            Matrix4x4 oldGizmosMatrix = Gizmos.matrix;
            Gizmos.matrix *= cubeTransform;
            Gizmos.DrawCube(Vector3.zero, Vector3.one);
            Gizmos.matrix = oldGizmosMatrix;
    }
    
    public static void DrawIndicator(Vector3 aPosition, float aSize)
    {
# if UNITY_EDITOR
        GameObject tIconObj = AssetDatabase.LoadAssetAtPath<GameObject>("Assets/RhoTools/Tools/Prefabs/indicator.prefab");
        Mesh tMesh = tIconObj.GetComponent<MeshFilter>().sharedMesh;
        
        Gizmos.DrawMesh(tMesh, aPosition, Quaternion.identity, Vector3.one * aSize * 100);
#endif
    }
}