﻿using UnityEngine;
using System.Collections;

namespace RhoTools
{
    public class CJSonReader
    {
        public static SimpleJSON.JSONNode Load(string aPath)
        {
            string tJSON = CTxtManager.Read(aPath);
            if (tJSON == null)
                return null;
            return SimpleJSON.JSON.Parse(tJSON);
        }
    }
}
