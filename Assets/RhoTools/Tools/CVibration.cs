﻿using UnityEngine;

namespace RhoTools
{
    [System.Serializable]
    public class CVibration
    {
        [HideInInspector]
        public bool active;
        [HideInInspector]
        public float time;
        public float intensity;
        public float delay;
        public float decay;

        public CVibration Copy()
        {
            CVibration tCopy = new CVibration
            {
                active = active,
                intensity = intensity,
                time = time,
                delay = delay,
                decay = decay
            };
            return tCopy;
        }
    }
}
