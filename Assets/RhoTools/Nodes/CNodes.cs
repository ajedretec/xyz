﻿using UnityEngine;
using System.Collections.Generic;
using RhoTools;

public class CNodes : MonoBehaviour
{
    [System.Serializable]
    public class Node
    {
        public Node next;
        public Node previous;
        public Vector3 position;
    }
    
    public List<Node> nodes;

    public int GetNodeIndex(Node aNode)
    {
        for (int i = 0; i < nodes.Count; i++)
        {
            if (nodes[i] == aNode)
                return i;
        }
        return -1;
    }
}
