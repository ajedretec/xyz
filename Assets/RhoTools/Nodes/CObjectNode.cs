﻿using UnityEngine;
using System.Collections;

public class CObjectNode : MonoBehaviour
{
    [SerializeField, ReadOnly]
    public CObjectNode _previous;
    [SerializeField, ReadOnly]
    public CObjectNode _next;

    protected Transform _transform;

    public CObjectNode Next
    {
        get
        {
            return _next;
        }
    }

    public CObjectNode Previous
    {
        get
        {
            return _previous;
        }
    }

    void Awake()
    {
        _transform = transform;
    }

    public void OnlySetNext(CObjectNode aNode)
    {
        _next = aNode;
    }

    public void OnlySetPrevious(CObjectNode aNode)
    {
        _previous = aNode;
    }

    public void SetNext(CObjectNode aNode)
    {
        _next = aNode;
        if (aNode)
            aNode.OnlySetPrevious(this);
    }

    public void SetPrevious(CObjectNode aNode)
    {
        _previous = aNode;
        if (aNode)
            aNode.OnlySetNext(this);
    }
    
    public void UnlinkPrevious()
    {
        _previous.OnlySetNext(null);
        _previous = null;
    }
    
    public void UnlinkNext()
    {
        _next.OnlySetPrevious(null);
        _next = null;
    }

    public Vector3 GetPos()
    {
        return _transform.position;
    }

    public CObjectNode GetFirstNode()
    {
        if (Previous == null)
            return this;
        return Previous.GetFirstNode();
    }

    public CObjectNode GetLastNode()
    {
        if (Next == null)
            return this;
        return Next.GetLastNode();
    }
    
    public Quaternion GetRotation()
    {
        return _transform.rotation;
    }

    // Editor
    public void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position, 0.1f);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, 0.2f);
        if (Next)
        {
            Gizmos.color = Color.white;
            Gizmos.DrawLine(transform.position, Next.transform.position);
            
            Gizmos.color = Color.blue;
            Vector3 tNextPos = Next.transform.position - transform.position;
            tNextPos.Normalize();
            tNextPos *= 0.2f;
            DrawCube(transform.position + tNextPos, Quaternion.LookRotation(tNextPos) * Quaternion.Euler(Vector3.right * 45), Vector3.right * 0.01f + (Vector3.forward + Vector3.up) * 0.1f);
        }
        if (Previous)
        {
            Gizmos.color = Color.blue;
            Vector3 tPrevPos = Previous.transform.position - transform.position;
            tPrevPos.Normalize();
            tPrevPos *= 0.2f;
            DrawCube(transform.position + tPrevPos, Quaternion.LookRotation(tPrevPos) * Quaternion.Euler(Vector3.right * 45), Vector3.right * 0.01f + (Vector3.forward + Vector3.up) * 0.1f);
        }
    }
    
    void DrawCube(Vector3 aPosition, Quaternion aRotation, Vector3 aScale)
    {
            Matrix4x4 cubeTransform = Matrix4x4.TRS(aPosition, aRotation, aScale);
            Matrix4x4 oldGizmosMatrix = Gizmos.matrix;
            Gizmos.matrix *= cubeTransform;
            Gizmos.DrawCube(Vector3.zero, Vector3.one);
            Gizmos.matrix = oldGizmosMatrix;
    }
}