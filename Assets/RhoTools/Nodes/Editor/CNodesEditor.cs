﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

[CustomEditor(typeof(CNodes))]
public class CNodesEditor : Editor
{
    Color SELECTED_COLOR = new Color(0, 1, .5f);
    const float handleSize = 0.04f;
    const float pickSize = 0.06f;

    Vector2 _nodeScrollPos = Vector2.zero;
    CNodes _target;
    int _selected = -1;
    static Texture2D tex;
    Transform _handleTransform;
    Quaternion _handleRotation;

    void OnEnable()
    {
        _target = target as CNodes;
        if (_target.nodes == null)
            _target.nodes = new List<CNodes.Node>();

        tex = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        tex.SetPixel(0, 0, SELECTED_COLOR);
        tex.Apply();

        _handleTransform = _target.transform;
        _handleRotation = Tools.pivotRotation == PivotRotation.Local ?
            _handleTransform.rotation : Quaternion.identity;
    }

    public override void OnInspectorGUI()
    {
        bool tSelected = false;
        if (_selected >= 0 && _selected < _target.nodes.Count)
        {
            tSelected = true;
            DrawSelectedNodeInspector();
        }

        _nodeScrollPos = EditorGUILayout.BeginScrollView(
            _nodeScrollPos,
            GUILayout.Width(EditorGUIUtility.currentViewWidth - 20),
            GUILayout.Height(150)
        );

        for (int i = 0; i < _target.nodes.Count; i++)
        {
            float tWidth = EditorGUIUtility.currentViewWidth - 70;
            CNodes.Node tNode = _target.nodes[i];

            GUILayout.BeginHorizontal();
            if (_selected != i)
            {
                if (tSelected)
                {
                    tWidth -= 157;
                }
                tWidth -= 55;
            }
            else
            {
                GUI.DrawTexture(
                    new Rect(
                        0,
                        i * (EditorGUIUtility.singleLineHeight + 4) + 4,
                        EditorGUIUtility.currentViewWidth - 20,
                        EditorGUIUtility.singleLineHeight
                    ),
                    tex,
                    ScaleMode.StretchToFill
                );
            }

            EditorGUILayout.LabelField(
                i.ToString(),
                GUILayout.MaxWidth(tWidth),
                GUILayout.MinWidth(10)
            );

            if (_selected != i)
            {
                if (tSelected)
                {
                    if (GUILayout.Button("Make next", GUILayout.MaxWidth(70)))
                    {
                        Undo.RecordObject(_target, "Change next node");
                        EditorUtility.SetDirty(_target);
                        _target.nodes[_selected].next = tNode;
                    }
                    if (GUILayout.Button("Make previous", GUILayout.MaxWidth(80)))
                    {
                        Undo.RecordObject(_target, "Change prev node");
                        EditorUtility.SetDirty(_target);
                        _target.nodes[_selected].previous = tNode;
                    }
                }
                if (GUILayout.Button("Select", GUILayout.MaxWidth(50)))
                    _selected = i;
            }

            if (GUILayout.Button("x", GUILayout.MaxWidth(20)))
                _target.nodes.RemoveAt(i);
            GUILayout.EndHorizontal();
        }
        EditorGUILayout.EndScrollView();

        if (GUILayout.Button("New node"))
        {
            CNodes.Node tNode = new CNodes.Node();
            tNode.position = Vector3.zero;
            _target.nodes.Add(tNode);
        }
    }

    void OnSceneGUI()
    {
        for (int i = 0; i < _target.nodes.Count; i++)
        {
            Vector3 tPoint = ShowPoint(i);
            CNodes.Node tNode = _target.nodes[i];
            if (tNode.next != null)
                Handles.DrawLine(
                    tPoint,
                    _handleTransform.TransformPoint(tNode.next.position)
                );
        }
    }

    void DrawSelectedNodeInspector()
    {
        GUILayout.Label("Selected: " + _selected.ToString());
        EditorGUI.BeginChangeCheck();
        CNodes.Node tNode = _target.nodes[_selected];
        Vector3 tPoint = EditorGUILayout.Vector3Field("Position", tNode.position);
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(_target, "Move node");
            EditorUtility.SetDirty(_target);
            tNode.position = tPoint;
        }

        EditorGUI.BeginChangeCheck();
        GUILayout.BeginHorizontal();
        GUILayout.Label("Previous: " + _target.GetNodeIndex(tNode.previous));
        GUILayout.Label("Next: " + _target.GetNodeIndex(tNode.next));
        GUILayout.EndHorizontal();
        if (EditorGUI.EndChangeCheck())
        {
            Undo.RecordObject(_target, "Change prev and next nodes");
            EditorUtility.SetDirty(_target);
            
        }
    }

    private Vector3 ShowPoint(int index)
    {
        CNodes.Node tNode = _target.nodes[index];
        Vector3 tPoint = _handleTransform.TransformPoint(tNode.position);
        //Handles.color = modeColors[(int)spline.GetControlPointMode(index)];
        float size = HandleUtility.GetHandleSize(tPoint);
#if UNITY_2017_1_OR_NEWER
        if (Handles.Button(tPoint, _handleRotation, size * handleSize, size * pickSize, Handles.DotHandleCap))
#else
        if (Handles.Button(tPoint, _handleRotation, size * handleSize, size * pickSize, Handles.DotCap))
#endif
        {
            Repaint();
            _selected = index;
        }
        if (_selected == index)
        {
            EditorGUI.BeginChangeCheck();
            tPoint = Handles.DoPositionHandle(tPoint, _handleRotation);
            if (EditorGUI.EndChangeCheck())
            {
                Undo.RecordObject(_target, "Move Point");
                EditorUtility.SetDirty(_target);
                tNode.position = _handleTransform.InverseTransformPoint(tPoint);
            }
        }
        return tPoint;
    }
}
