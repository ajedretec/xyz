﻿using UnityEngine;
using UnityEditor;
using System.IO;

namespace RhoTools
{
    public class CAssetRenamer : EditorWindow
    {
        #region Window definition
        [MenuItem(CConstants.ROOT_MENU + "Asset Renamer...")]
        public static void ShowWindow()
        {
            EditorWindow tWindow = GetWindow(typeof(CAssetRenamer));
            GUIContent titleContent = new GUIContent("Renamer");
            tWindow.titleContent = titleContent;
        }
        #endregion

        string _replace;
        string _with;

        void OnGUI()
        {
            _replace = EditorGUILayout.TextField("Replace", _replace);
            _with = EditorGUILayout.TextField("with", _with);
            EditorGUI.BeginDisabledGroup(Selection.objects.Length == 0);
            if (GUILayout.Button("Rename selected"))
            {
                Object[] tSelection = Selection.objects;
                for (int i = 0; i < tSelection.Length; i++)
                {
                    Object tObj = tSelection[i];

                    string tPath = AssetDatabase.GetAssetPath(tObj);
                    if (tPath != "")
                    {
                        string tName = Path.GetFileNameWithoutExtension(tPath);
                        string tNewName = tName.Replace(_replace, _with);
                        AssetDatabase.RenameAsset(tPath, tNewName);
                    }

                    if (!EditorUtility.DisplayCancelableProgressBar(
                            "Renaming",
                            "Replacing " + _replace + " with " + _with,
                            i / (float)tSelection.Length
                        ))
                    {
                        break;
                    }
                }
                AssetDatabase.SaveAssets();
                EditorUtility.ClearProgressBar();
            }
            EditorGUI.EndDisabledGroup();
        }
    }
}
