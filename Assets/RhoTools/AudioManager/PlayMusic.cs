﻿using RhoTools.Audio;
using UnityEngine;
using UnityEngine.Audio;

public class PlayMusic : MonoBehaviour
{
    [SerializeField]
    AudioClip _music;
    [SerializeField]
    float _transitionTime = 1f;
    [SerializeField]
    bool _useSnapshots;
    [SerializeField]
    AudioMixerSnapshot[] _snapshots;
    [SerializeField]
    float[] _weights;
    
    void Start()
    {
        AudioManager.PlayMusic(_music, _transitionTime);
        if (_useSnapshots)
            AudioManager.Inst.audioMixer.TransitionToSnapshots(_snapshots,
                _weights, _transitionTime);
    }
}
