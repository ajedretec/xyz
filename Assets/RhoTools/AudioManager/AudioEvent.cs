using UnityEngine;

public abstract class AudioEvent : ScriptableObject
{
    const int DEFAULT_PRIORITY = 128;
    [Range(0, 255)]
    public int priority = DEFAULT_PRIORITY;

    public abstract void Play(AudioSource source);
}