﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using RhoTools.ReorderableList;

[CustomEditor(typeof(AudioEvent), true)]
public class AudioEventEditor : Editor
{
    [SerializeField]
    private AudioSource _previewer;

    protected virtual void OnEnable()
    {
        _previewer = EditorUtility.CreateGameObjectWithHideFlags("Audio preview",
            HideFlags.HideAndDontSave, typeof(AudioSource)).GetComponent<AudioSource>();
    }

    protected virtual void OnDisable()
    {
        DestroyImmediate(_previewer.gameObject);
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUI.BeginDisabledGroup(serializedObject.isEditingMultipleObjects);
        if (GUILayout.Button("Preview"))
        {
            ((AudioEvent) target).Play(_previewer);
        }
        EditorGUI.EndDisabledGroup();
    }
}

[CustomEditor(typeof(SimpleAudioEvent), true)]
public class SimpleAudioEventEditor : AudioEventEditor
{
    private SerializedObject _obj;
    private SerializedProperty _clips;

    protected override void OnEnable()
    {
        base.OnEnable();
        _obj = new SerializedObject(target);
        _clips = _obj.FindProperty("clips");        
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (_obj != null)
        {
            ReorderableListGUI.Title("Clips");
            ReorderableListGUI.ListField(_clips);
            _obj.ApplyModifiedProperties();
        }
    }
}

[CustomEditor(typeof(CompositeAudioEvent), true)]
public class CompositeAudioEventEditor : AudioEventEditor
{
    private SerializedObject _obj;
    private SerializedProperty _entries;

    protected override void OnEnable()
    {
        base.OnEnable();
        _obj = new SerializedObject(target);
        _entries = _obj.FindProperty("Entries");        
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        if (_obj != null)
        {
            ReorderableListGUI.Title("Entries");
            ReorderableListGUI.ListField(_entries);
            _obj.ApplyModifiedProperties();
        }
    }
}

[CustomPropertyDrawer(typeof(CompositeAudioEvent.CompositeEntry))]
public class CompositeEntryDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return 2 * EditorGUIUtility.singleLineHeight;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        position.height = EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("Event"));

        position.y += position.height;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("Weight"));
    }
}