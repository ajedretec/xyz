﻿using UnityEngine;
using UnityEditor;
using UnityEngine.Audio;

namespace RhoTools.Audio
{
    [CustomEditor(typeof(AudioManager))]
    public class AudioManagerEditor : Editor
    {
        #region Menu item
        [MenuItem("GameObject/Audio/Audio Manager")]
        public static void CreateAudioManager()
        {
            string[] ids = AssetDatabase.FindAssets("t:AudioMixer");
            AudioManager tMngr = new GameObject("AudioManager",
                typeof(AudioManager)).GetComponent<AudioManager>();
            if (ids.Length > 0)
            {
                AudioMixer tMixer = AssetDatabase.LoadAssetAtPath<AudioMixer>(
                    AssetDatabase.GUIDToAssetPath(ids[0]));
                tMngr.audioMixer = tMixer;
            }
        }
        #endregion

        public static GUIStyle _boxStyle;
        public static GUIStyle boxStyle
        {
            get
            {
                if (_boxStyle == null)
                {
                    _boxStyle = new GUIStyle("box");
                }
                return _boxStyle;
            }
        }
        public static GUIStyle _centeredLabel;
        public static GUIStyle centeredLabel
        {
            get
            {
                if (_centeredLabel == null)
                {
                    _centeredLabel = GUI.skin.GetStyle("Label");
                    _centeredLabel.alignment = TextAnchor.UpperCenter;
                }
                return _centeredLabel;
            }
        }

        AudioManager _target;

        private void OnEnable()
        {
            _target = target as AudioManager;
        }

        public override void OnInspectorGUI()
        {
            EditorGUI.BeginChangeCheck();
            base.OnInspectorGUI();

            /*
            GUILayout.Label("Music fade:");
            Rect position = GUILayoutUtility.GetLastRect();
            position.y += position.height;
            position.height = 100;
            EditorGUI.DrawRect(position, Color.gray);
            //CurveEditor.DrawCurve(position, _target.fadeOut, Color.red);
            //CurveEditor.DrawCurve(position, _target.fadeIn, Color.blue);
            GUILayout.Space(100);
            if (GUILayout.Button("Validate curves"))
            {
                ValidateCurve(ref _target.fadeOut, 1, 0);
                ValidateCurve(ref _target.fadeIn, 0, 1);
            }
            */

            if (EditorGUI.EndChangeCheck())
            {
                if (_target.preloadAmount > _target.maxTotalSources)
                    _target.maxTotalSources = _target.preloadAmount;
            }

            if (_target.maxTotalSources < _target.preloadAmount)
                _target.preloadAmount = _target.maxTotalSources;

            //_obj.ApplyModifiedProperties();
        }

        void ValidateCurve(ref AnimationCurve aCurve, float aStartValue, float aEndValue)
        {
            Keyframe[] keys = aCurve.keys;
            Keyframe tStartFrame;
            if (aCurve.length == 0)
            {
                tStartFrame = new Keyframe(0, aStartValue);
                keys = new Keyframe[2];
                keys[0] = tStartFrame;
            }
            else
                tStartFrame = aCurve.keys[0];

            Keyframe tEndFrame;
            if (aCurve.length < 2)
            {
                tEndFrame = new Keyframe(1, aEndValue);
                if (keys.Length < 2)
                {
                    keys = new Keyframe[2];
                    keys[0] = aCurve.keys[0];
                }
                keys[1] = tEndFrame;
            }
            else
                tEndFrame = aCurve.keys[aCurve.length - 1];
        
            keys[0].time = 0;
            keys[0].value = aStartValue;
            keys[keys.Length - 1].time = 1;
            keys[keys.Length - 1].value = aEndValue;
            aCurve.keys = keys;
        }
    }
}
