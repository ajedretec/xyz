﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Audio;

namespace RhoTools.Audio
{
    [AddComponentMenu("Audio/Audio Manager")]
    public class AudioManager : MonoBehaviour
    {
        // Constants
        const int DEFAULT_PRIORITY = 128; // The default priprity
        const float MUTE_VOL = -80; // Volume in dB equivalent to 0

        [SerializeField, Header("Audio Mixer")]
        AudioMixer _audioMixer;
        public AudioMixer audioMixer
        {
            set { _audioMixer = value; }
            get { return _audioMixer; }
        }
        [SerializeField, Tooltip("Audio Mixer Group applied to music")]
        AudioMixerGroup _musicGroup;
        /// <summary>
        /// This group will be set to music audio sources
        /// </summary>
        public AudioMixerGroup musicGroup
        {
            set
            {
                _musicGroup = value;
                if (_musicSource != null)
                    _musicSource.outputAudioMixerGroup = _musicGroup;
                if (_musicSource2 != null)
                    _musicSource2.outputAudioMixerGroup = _musicGroup;
            }
            get { return _musicGroup; }
        }
        [SerializeField, Tooltip("Audio Mixer Group applied to SFX")]
        AudioMixerGroup _fxGroup;
        /// <summary>
        /// This group will be set to FX audio sources
        /// </summary>
        public AudioMixerGroup fxGroup
        {
            set
            {
                _fxGroup = value;
                if (_fxAvailable != null)
                {
                    for (int i = 0; i < _fxAvailable.Count; i++)
                        _fxAvailable[i].outputAudioMixerGroup = _fxGroup;
                }
                if (_fxPlaying != null)
                {
                    for (int i = 0; i < _fxPlaying.Count; i++)
                        _fxPlaying[i].outputAudioMixerGroup = _fxGroup;
                }
            }
            get { return _fxGroup; }
        }
        [SerializeField, Tooltip("Name of exposed variable that handles SFX volume")]
        string _fxVolumeName = "fxVol";
        [SerializeField, Tooltip("Name of exposed variable that handles music volume")]
        string _musicVolumeName = "musicVol";
        [SerializeField, Tooltip("Names of exposed variables that handle custom volumes")]
        string[] _customVolumeNames;
        [SerializeField, Tooltip("Custom groups to be used")]
        AudioMixerGroup[] _customGroups;
        [Header("Sources")]
        [Tooltip("Everytime sources are preloaded, it preloads this amount (Can't be greater than max total sources)")]
        public int preloadAmount = 10;
        [Tooltip("If true, will preload new sources when there are none available. Otherwise it will stop the source with the lowest priority and use that one")]
        public bool preloadWhenFinished;
        [Tooltip("It won't create more sources than this")]
        public int maxTotalSources;
        /*
        [Header("Fading")]
        [SerializeField, Tooltip("Fading out curve (it must start in (0,1) and end in (1,0))")]
        public AnimationCurve fadeOut = AnimationCurve.EaseInOut(0, 1, 1, 0);
        [SerializeField, Tooltip("Fading in curve (it must start in (0,0) and end in (1,1))")]
        public AnimationCurve fadeIn = AnimationCurve.EaseInOut(0, 0, 1, 1);
        */

        /// <summary>
        /// The amount of AudioSources preloaded
        /// </summary>
        public int fxSourceCount { get; private set; }
        public int customSourceCount { get; private set; }
        List<AudioSource> _fxAvailable; // List of available audio sources
        List<AudioSource> _fxPlaying; // List of audio sources playing
        List<AudioSource> _customAvailable;
        List<AudioSource> _customPlaying;
        AudioSource _musicSource; // AudioSource for music
        AudioSource _musicSource2; // Second AudioSource for music used for fading
        float _fadeTime;
        float _timer;
        bool _fadingMusic;
        Transform _availableRoot;
        #region Singleton
        static AudioManager _inst;
        public static AudioManager Inst
        {
            get
            {
                if (_inst == null)
                    _inst = FindObjectOfType<AudioManager>();
                if (_inst == null)
                    _inst = new GameObject("AudioManager",
                        typeof(AudioManager)).GetComponent<AudioManager>();
                return _inst;
            }
            private set { _inst = value; }
        }
        #endregion

        private void Awake()
        {
            if (Inst != null && Inst != this)
            {
                Destroy(gameObject);
                return;
            }
            Inst = this;

            // Create GameObject to put available AudioSources in
            _availableRoot = new GameObject("Available").transform;
            _availableRoot.parent = transform;
            // Create FX lists
            _fxAvailable = new List<AudioSource>();
            _fxPlaying = new List<AudioSource>();
            // Create custom lists
            _customAvailable = new List<AudioSource>();
            _customPlaying = new List<AudioSource>();
            // Reset fx source count
            fxSourceCount = 0;
            // Preload sources
            PreloadFX(preloadAmount);
            // Create music sources
            _musicSource = CreateSource("Music", _musicGroup);
            _musicSource.loop = true;
            _musicSource.transform.SetParent(transform);
            _musicSource2 = CreateSource("Music2", _musicGroup);
            _musicSource2.loop = true;
            _musicSource2.transform.SetParent(transform);
        }

        void Update()
        {
            // Check if fx finished playing
            for (int i = _fxPlaying.Count - 1; i >= 0; i --)
            {
                AudioSource tSource = _fxPlaying[i];
                // If the source has played for the clip's length in time, the
                // play ended
                // Ignore if it's looping
                // Otherwise check if it was manually stopped
                if ((!tSource.loop &&
                    tSource.time >= tSource.clip.length)
                    || !tSource.isPlaying)
                {
                    // Set clip as available
                    SetAvailable(i);
                }
            }
            // Check if fx finished playing
            for (int i = _customPlaying.Count - 1; i >= 0; i --)
            {
                AudioSource tSource = _customPlaying[i];
                // If the source has played for the clip's length in time, the
                // play ended
                // Ignore if it's looping
                // Otherwise check if it was manually stopped
                if ((!tSource.loop &&
                    tSource.time >= tSource.clip.length)
                    || !tSource.isPlaying)
                {
                    // Set clip as available
                    SetAvailableCustom(i);
                }
            }
            
            // Music transition
            if (_fadingMusic)
            {
                if (_timer < _fadeTime) // if fade time hasn't finished
                {
                    // Transition volumes
                    _musicSource.volume = 1 - _timer / _fadeTime;
                    _musicSource2.volume = _timer / _fadeTime;
                }
                else // When time's finished
                {
                    // Exchange music sources so that the new one becomes the main one
                    _musicSource2.volume = 1;
                    _musicSource.Stop();
                    AudioSource tSource = _musicSource;
                    _musicSource = _musicSource2;
                    _musicSource2 = tSource;
                    _fadingMusic = false; // Transition ended
                }
                _timer += Time.deltaTime;
            }
        }

        AudioSource CreateSource(string aName, AudioMixerGroup aGroup)
        {
            // Create new object with AudioSource
            AudioSource tSource = new GameObject(aName,
                typeof(AudioSource)).GetComponent<AudioSource>();
            // Set Audio Mixer Group
            tSource.outputAudioMixerGroup = aGroup;
            // Stop it from playing on Awake
            tSource.playOnAwake = false;
            // Set the default priority
            tSource.priority = DEFAULT_PRIORITY;
            return tSource;
        }

        void PreloadFX(int aNum)
        {
            // Check how many we can create, we can't create more than maxTotalSources
            int tLen = Mathf.Min(aNum, maxTotalSources - fxSourceCount);
            for (int i = 0; i < tLen; i++)
            {
                // Create source
                AudioSource tSource = CreateSource("AudioFX" + fxSourceCount, _fxGroup);
                // Add it to available list
                _fxAvailable.Add(tSource);
                // Set it as child of available root
                tSource.transform.parent = _availableRoot;
                // Move the counter
                fxSourceCount++;
            }
        }

        void PreloadCustom(int aNum)
        {
            // Check how many we can create, we can't create more than maxTotalSources
            int tLen = Mathf.Min(aNum, maxTotalSources - customSourceCount);
            for (int i = 0; i < tLen; i++)
            {
                // Create source
                AudioSource tSource = CreateSource("AudioCustom" + customSourceCount, _fxGroup);
                // Add it to available list
                _customAvailable.Add(tSource);
                // Set it as child of available root
                tSource.transform.parent = _availableRoot;
                // Move the counter
                customSourceCount++;
            }
        }

        void SetAvailable(int aIndex)
        {
            // Get source from index
            AudioSource tSource = _fxPlaying[aIndex];
            // Add to available list
            _fxAvailable.Add(tSource);
            // Remove from playing list
            _fxPlaying.RemoveAt(aIndex);
            // Set as child of available root
            tSource.transform.parent = _availableRoot;
        }

        void SetPlaying(ref AudioSource aSource)
        {
            // Add to playing list
            _fxPlaying.Add(aSource);
            // Set priority as default
            aSource.priority = DEFAULT_PRIORITY;
            // Set volume as 1
            aSource.volume = 1;
            // Set pitch as 1
            aSource.pitch = 1;
        }

        void SetAvailableCustom(int aIndex)
        {
            // Get source from index
            AudioSource tSource = _customPlaying[aIndex];
            // Add to available list
            _customAvailable.Add(tSource);
            // Remove from playing list
            _customPlaying.RemoveAt(aIndex);
            // Set as child of available root
            tSource.transform.parent = _availableRoot;
        }

        void SetPlayingCustom(ref AudioSource aSource)
        {
            // Add to playing list
            _customPlaying.Add(aSource);
            // Set priority as default
            aSource.priority = DEFAULT_PRIORITY;
            // Set volume as 1
            aSource.volume = 1;
            // Set pitch as 1
            aSource.pitch = 1;
        }

        AudioSource GetFXWithLowestPriority()
        {
            // If playing list is empty, there's nothing we can do
            if (_fxPlaying.Count == 0)
                return null;

            // Get first and set it as lowest
            AudioSource tLowest = _fxPlaying[0];
            for (int i = 1; i < _fxPlaying.Count; i++)
            {
                // Compare every source to lowest and replace if lower
                AudioSource tSource = _fxPlaying[i];
                if (tSource.priority > tLowest.priority)
                    tLowest = tSource;
            }

            return tLowest;
        }

        AudioSource GetCustomWithLowestPriority()
        {
            // If playing list is empty, there's nothing we can do
            if (_customPlaying.Count == 0)
                return null;

            // Get first and set it as lowest
            AudioSource tLowest = _customPlaying[0];
            for (int i = 1; i < _customPlaying.Count; i++)
            {
                // Compare every source to lowest and replace if lower
                AudioSource tSource = _customPlaying[i];
                if (tSource.priority > tLowest.priority)
                    tLowest = tSource;
            }

            return tLowest;
        }

        AudioSource GetFXSource()
        {
            AudioSource tSource;
            if (_fxAvailable.Count > 0) // If there are available, return first in list
            {
                tSource = _fxAvailable[0];
                _fxAvailable.RemoveAt(0); // Remove from available list
                tSource.transform.parent = transform;
            }
            else // If there are none available
            {
                if (preloadWhenFinished && fxSourceCount < maxTotalSources)
                { // if we should preload sources and we haven't exceded the max
                    PreloadFX(preloadAmount); // Preload more sources and get a new fx
                    return GetFXSource();
                }
                else // Otherwise get the playing source with the lowest priority
                    return GetFXWithLowestPriority();
            }
            // Set source as playing
            SetPlaying(ref tSource);

            return tSource;
        }

        AudioSource GetCustomSource()
        {
            AudioSource tSource;
            if (_customAvailable.Count > 0) // If there are available, return first in list
            {
                tSource = _customAvailable[0];
                _customAvailable.RemoveAt(0); // Remove from available list
                tSource.transform.parent = transform;
            }
            else // If there are none available
            {
                if (preloadWhenFinished && customSourceCount < maxTotalSources)
                { // if we should preload sources and we haven't exceded the max
                    PreloadCustom(preloadAmount); // Preload more sources and get a new fx
                    return GetCustomSource();
                }
                else // Otherwise get the playing source with the lowest priority
                    return GetCustomWithLowestPriority();
            }
            // Set source as playing
            SetPlaying(ref tSource);

            return tSource;
        }

        static float GetVolume(float aVol)
        {
            // Clamp value between 0 and 1
            Mathf.Clamp01(aVol);
            if (aVol == 0) // if volume is 0, return MUTE_VOL (otherwise there's error)
                return MUTE_VOL;
            // Convert linear value into dB value
            return 20 * Mathf.Log10(aVol);
        }

        #region Public methods
        /// <summary>
        /// Play SFX from AudioClip
        /// </summary>
        /// <param name="aClip">Clip to play</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <param name="aPriority">Set a priority. Low: 255, High: 0, Default: 128</param>
        /// <returns>Audio Source</returns>
        public static AudioSource PlayFX(AudioClip aClip, bool aLoop = false,
            int aPriority = DEFAULT_PRIORITY)
        {
            AudioSource tSource = Inst.GetFXSource();
            tSource.clip = aClip;
            tSource.Play();
            tSource.priority = aPriority;
            tSource.loop = aLoop;
            return tSource;
        }

        /// <summary>
        /// Play SFX from AudioClip
        /// </summary>
        /// <param name="aClip">Clip to play</param>
        /// <param name="aPos">Position in world</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <param name="aPriority">Set a priority. Low: 255, High: 0, Default: 128</param>
        /// <returns>Audio Source</returns>
        public static AudioSource PlayFX(AudioClip aClip, Vector3 aPos,
            bool aLoop = false, int aPriority = DEFAULT_PRIORITY)
        {
            AudioSource tSource = PlayFX(aClip, aLoop, aPriority);
            tSource.transform.position = aPos;
            return tSource;
        }

        /// <summary>
        /// Play random SFX from AudioClip list
        /// </summary>
        /// <param name="aClip">List of clips</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <param name="aPriority">Set a priority. Low: 255, High: 0, Default: 128</param>
        /// <returns>Audio Source</returns>
        public static AudioSource PlayRandomFX(IList<AudioClip> aClip,
            bool aLoop = false, int aPriority = DEFAULT_PRIORITY)
        {
            return PlayFX(aClip[Random.Range(0, aClip.Count)], aLoop, aPriority);
        }

        /// <summary>
        /// Play random SFX from AudioClip list
        /// </summary>
        /// <param name="aClip">List of clips</param>
        /// <param name="aPos">Position in world</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <param name="aPriority">Set a priority. Low: 255, High: 0, Default: 128</param>
        /// <returns>Audio Source</returns>
        public static AudioSource PlayRandomFX(IList<AudioClip> aClip, Vector3 aPos,
            bool aLoop = false, int aPriority = DEFAULT_PRIORITY)
        {
            AudioSource tSource = PlayRandomFX(aClip, aLoop, aPriority);
            tSource.transform.position = aPos;
            return tSource;
        }

        /// <summary>
        /// Play SFX from an AudioEvent
        /// </summary>
        /// <param name="aEvent">AudioEvent to play</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <param name="aPriority">Set a priority. Low: 255, High: 0, Default: 128</param>
        /// <returns>Audio Source</returns>
        public static AudioSource PlayFX(AudioEvent aEvent, bool aLoop, int aPriority)
        {
            AudioSource tSource = Inst.GetFXSource();
            aEvent.Play(tSource);
            tSource.priority = aPriority;
            tSource.loop = aLoop;
            return tSource;
        }

        /// <summary>
        /// Play SFX from an AudioEvent, it will take the priority from the AudioEvent
        /// </summary>
        /// <param name="aEvent">AudioEvent to play</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <returns>Audio Source</returns>
        public static AudioSource PlayFX(AudioEvent aEvent, bool aLoop = false)
        {
            return PlayFX(aEvent, aLoop, aEvent.priority);
        }

        /// <summary>
        /// Play SFX from an AudioEvent, it will take the priority from the AudioEvent
        /// </summary>
        /// <param name="aEvent">AudioEvent to play</param>
        /// <param name="aPos">Position in world</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <returns>Audio Source</returns>
        public static AudioSource PlayFX(AudioEvent aEvent, Vector3 aPos,
            bool aLoop = false)
        {
            AudioSource tSource = PlayFX(aEvent, aLoop);
            tSource.transform.position = aPos;
            return tSource;
        }

        /// <summary>
        /// Play SFX from an AudioEvent
        /// </summary>
        /// <param name="aEvent">AudioEvent to play</param>
        /// <param name="aPos">Position in world</param>
        /// <param name="aLoop">Indicates if the clip should loop</param>
        /// <param name="aPriority">Set a priority. Low: 255, High: 0, Default: 128</param>
        /// <returns></returns>
        public static AudioSource PlayFX(AudioEvent aEvent, Vector3 aPos,
            bool aLoop, int aPriority)
        {
            AudioSource tSource = PlayFX(aEvent, aLoop, aPriority);
            tSource.transform.position = aPos;
            return tSource;
        }

        /// <summary>
        /// Play music
        /// </summary>
        /// <param name="aClip">Clip to play</param>
        public static void PlayMusic(AudioClip aClip)
        {
            // If it's the same clip, don't do anything
            if (Inst._musicSource.clip == aClip)
                return;
            Inst._musicSource.clip = aClip;
            Inst._musicSource.volume = 1;
            Inst._musicSource.Play();
        }

        /// <summary>
        /// Play music with transition (there won't be a transition
        /// if there is no music playing)
        /// </summary>
        /// <param name="aClip">Clip to play</param>
        /// <param name="aFadeTime">Fade time</param>
        public static void PlayMusic(AudioClip aClip, float aFadeTime)
        {
            if (Inst._musicSource.isPlaying && Inst._musicSource.clip != aClip)
            {
                Inst._fadeTime = aFadeTime;
                Inst._musicSource2.volume = 0;
                Inst._musicSource2.clip = aClip;
                Inst._musicSource2.Play();

                if (!Inst._fadingMusic)
                    Inst._timer = 0;
                Inst._fadingMusic = true;
            }
            else
                PlayMusic(aClip);
        }

        /// <summary>
        /// Play a clip in a custom group
        /// </summary>
        /// <param name="aClip">Audio clip to play</param>
        /// <param name="aCustomGroup">Index of custom group</param>
        /// <param name="aLoop">Should the clip loop?</param>
        /// <param name="aPriority">Priority (default = 128)</param>
        /// <returns></returns>
        public static AudioSource PlayCustom(AudioClip aClip, int aCustomGroup,
            bool aLoop = false, int aPriority = DEFAULT_PRIORITY)
        {
            AudioSource tSource = Inst.GetFXSource();
            tSource.outputAudioMixerGroup = GetCustomGroup(aCustomGroup);
            tSource.clip = aClip;
            tSource.Play();
            tSource.priority = aPriority;
            tSource.loop = aLoop;
            return tSource;
        }

        /// <summary>
        /// Sets master volume
        /// </summary>
        /// <param name="aVol">Volume</param>
        public static void SetMasterVol(float aVol)
        {
            AudioListener.volume = aVol;
        }

        /// <summary>
        /// Returns master volume
        /// </summary>
        /// <returns>Volume</returns>
        public static float GetMasterVol()
        {
            return AudioListener.volume;
        }

        /// <summary>
        /// Set SFX volume (only works if the AudioMixer is set and the variable
        /// for fx volume is exposed)
        /// </summary>
        /// <param name="aVol">Volume</param>
        public static void SetFXVol(float aVol)
        {
            if (Inst._audioMixer != null)
                Inst._audioMixer.SetFloat(Inst._fxVolumeName, GetVolume(aVol));
        }

        /// <summary>
        /// Set music volume (only works if the AudioMixer is set and the variable
        /// for music volume is exposed)
        /// </summary>
        /// <param name="aVol">Volume</param>
        public static void SetMusicVol(float aVol)
        {
            if (Inst._audioMixer != null)
                Inst._audioMixer.SetFloat(Inst._musicVolumeName, GetVolume(aVol));
        }

        /// <summary>
        /// Set custom volume (only works if the AudioMixer is set and the variable
        /// for the custom volume is exposed and set in the list)
        /// </summary>
        /// <param name="aIndex">Index in the custom volumes list</param>
        /// <param name="aVol">Volume</param>
        public static void SetCustomVol(int aIndex, float aVol)
        {
            if (Inst._audioMixer != null && aIndex >= 0 && aIndex < Inst._customVolumeNames.Length)
                Inst._audioMixer.SetFloat(Inst._customVolumeNames[aIndex], GetVolume(aVol));
        }

        /// <summary>
        /// Get custom Audio Mixer Group
        /// </summary>
        /// <param name="aIndex">Index of group</param>
        /// <returns>Audio mixer group</returns>
        public static AudioMixerGroup GetCustomGroup(int aIndex)
        {
            if (Inst._customGroups.Length > aIndex && aIndex > 0)
                return Inst._customGroups[aIndex];
            return null;
        }
        #endregion
    }
}
