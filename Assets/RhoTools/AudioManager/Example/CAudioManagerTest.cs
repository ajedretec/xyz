﻿using UnityEngine;
using System.Collections;
using RhoTools.Audio;

public class CAudioManagerTest : MonoBehaviour
{
    [SerializeField]
    AudioClip _music1;
    [SerializeField]
    AudioClip _music2;
    [SerializeField]
    AudioEvent[] _fx;
    [SerializeField]
    float _fadeTime = 2f;
    
    public void PlayMusic1()
    {
        AudioManager.PlayMusic(_music1, _fadeTime);
    }
    
    public void PlayMusic2()
    {
        AudioManager.PlayMusic(_music2, _fadeTime);
    }

    public void PlayFX(int aFX)
    {
        if (aFX >= 0 && _fx.Length > aFX)
            AudioManager.PlayFX(_fx[aFX], true);
    }
}
