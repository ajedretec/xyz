﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using RhoTools;
using System.IO;

public class CAnimationTools : EditorWindow
{
    const string PROPERTY_SPRITE = "m_Sprite";
    const string MENU_NAME = "Animations/";
    const string SPRITE_TO_IMAGE = "Sprite to image";
    const string IMAGE_TO_SPRITE = "Image to sprite";
    static readonly System.Type[] TYPES = 
    {
        typeof(SpriteRenderer),
        typeof(Image),
        typeof(Transform),
        typeof(RectTransform),
        typeof(ParticleSystem),
    };
    static string[] _typeList;
    public static string[] typeList
    {
        get
        {
            if (_typeList == null)
            {
                _typeList = new string[TYPES.Length];
                for (int i = 0; i < TYPES.Length; i++)
                    _typeList[i] = TYPES[i].Name;
            }
            return _typeList;
        }
    }

    delegate ObjectReferenceKeyframe[] GetFrames(AnimationClip aClip, EditorCurveBinding aBinding);

    public static CAnimationTools Inst { get; private set; }
    public static bool IsOpen { get { return Inst != null; } }

    static GUIStyle _titleStyle;
    public static GUIStyle titleStyle
    {
        get
        {
            if (_titleStyle == null)
            {
                _titleStyle = new GUIStyle(EditorStyles.largeLabel);
                _titleStyle.fontStyle = FontStyle.Bold;
            }
            return _titleStyle;
        }
    }

    public class ClipData
    {
        public AnimationClip clip;
        public CurveData[] objectReferenceCurves;
        public CurveData[] floatCurves;
        public bool dirty;
    }

    public class CurveData
    {
        public EditorCurveBinding binding;
        public string propertyName;
        public string path;
        public bool objectRef;
        public AnimationCurve curve;
        public ObjectReferenceKeyframe[] frames;
        public System.Type propertyType;

        public void GetData(AnimationClip aClip)
        {
            if (objectRef)
            {
                frames = AnimationUtility.GetObjectReferenceCurve(aClip, binding);
                System.Type tType = GetPropertyType(binding.type, propertyName);
                if (tType != null)
                    propertyType = tType;
                else
                    propertyType = typeof(Object);
            }
            else
            {
                curve = AnimationUtility.GetEditorCurve(aClip, binding);
            }
        }
    }

    AnimationClip _animationClip;
    AnimationClip _animationClip2;
    ClipData _clip1Data;
    ClipData _clip2Data;
    Vector2 _scrollPos1;
    Vector2 _scrollPos2;


    #region Window definition
    [MenuItem(CConstants.ROOT_MENU + MENU_NAME + "Animation tools...")]
    [MenuItem(CConstants.ASSETS_MENU + MENU_NAME + "Animation tools...")]
    public static CAnimationTools ShowWindow()
    {
        CAnimationTools tWindow = GetWindow(typeof(CAnimationTools)) as CAnimationTools;
        // Loads an icon from an image stored at the specified path
        MonoScript tScript = MonoScript.FromScriptableObject(tWindow);
        string tPath = AssetDatabase.GetAssetPath(tScript);
        tPath = Path.GetDirectoryName(tPath) + "/icon.png";
        Texture tIcon = AssetDatabase.LoadAssetAtPath<Texture>(tPath);

        // Create the instance of GUIContent to assign to the window. Gives the title "RBSettings" and the icon
        GUIContent titleContent = new GUIContent("AnimTools", tIcon);
        tWindow.titleContent = titleContent;

        Object tObj = Selection.activeObject;
        if (tObj is AnimationClip)
        {
            tWindow.SetAnimation(tObj as AnimationClip);
            tWindow.Refresh();
        }

        return tWindow;
    }
    #endregion

    void OnEnable()
    {
        Inst = this;
        _clip1Data = new ClipData { dirty = false };
        _clip2Data = new ClipData { dirty = false };
        Refresh();
    }

    void OnDestroy()
    {
        CloseSubWindows();
        Inst = null;
    }

    void CloseSubWindows()
    {
        if (CKeyframesEditor.IsOpen)
            CKeyframesEditor.Inst.Close();
    }

    void OnGUI()
    {
        GUILayout.BeginHorizontal();

        GUILayout.BeginVertical();
        EditorGUI.BeginChangeCheck();
        _animationClip = (AnimationClip)EditorGUILayout.ObjectField("Animation clip", _animationClip, typeof(AnimationClip), false);
        if (GUILayout.Button("Refresh") || EditorGUI.EndChangeCheck())
        {
            GetCurvesData(_animationClip, ref _clip1Data);
        }
        ShowClipData(_clip1Data, ref _scrollPos1);
        GUILayout.EndVertical();

        GUILayout.BeginVertical();
        EditorGUI.BeginChangeCheck();
        _animationClip2 = (AnimationClip)EditorGUILayout.ObjectField("Animation clip", _animationClip2, typeof(AnimationClip), false);
        if (GUILayout.Button("Refresh") || EditorGUI.EndChangeCheck())
        {
            GetCurvesData(_animationClip2, ref _clip2Data);
        }
        ShowClipData(_clip2Data, ref _scrollPos2);
        GUILayout.EndVertical();

        GUILayout.EndHorizontal();

    }

    void ShowClipData(ClipData aData, ref Vector2 aScrollPos)
    {
        if (aData.floatCurves != null && aData.objectReferenceCurves != null)
        {
            aScrollPos = GUILayout.BeginScrollView(aScrollPos);
            GUILayout.Label("Object reference curve bindings", titleStyle);
            ShowBindingGUI(aData, aData.objectReferenceCurves);

            GUILayout.Box("", new GUILayoutOption[] { GUILayout.ExpandWidth(true), GUILayout.Height(1) });

            GUILayout.Label("Float curve bindings", titleStyle);
            ShowBindingGUI(aData, aData.floatCurves);
            GUILayout.EndScrollView();

            EditorGUI.BeginDisabledGroup(!aData.dirty);
            if (GUILayout.Button("Apply"))
            {
                GUI.FocusControl("");
                ModifyCurves(aData.objectReferenceCurves, aData.clip);
                ModifyCurves(aData.floatCurves, aData.clip);
                aData.dirty = false;
                Refresh(true);
                CloseSubWindows();
            }
            EditorGUI.EndDisabledGroup();
        }
    }

    void ModifyCurves(CurveData[] aCurves, AnimationClip aClip)
    {
        for (int i = 0; i < aCurves.Length; i++)
        {
            CurveData tCurve = aCurves[i];
            EditorCurveBinding tBinding = tCurve.binding;
            EditorCurveBinding tNewBinding = new EditorCurveBinding();
            tNewBinding.path = tCurve.path;
            tNewBinding.propertyName = tCurve.propertyName;
            tNewBinding.type = tBinding.type;
            if (tCurve.objectRef)
            {
                System.Array.Sort(tCurve.frames, FrameComparer);
                //ObjectReferenceKeyframe[] tFrames = AnimationUtility.GetObjectReferenceCurve(aClip, tBinding);
                DeleteCurve(aClip, tBinding, true);
                AnimationUtility.SetObjectReferenceCurve(aClip, tNewBinding, tCurve.frames);
            }
            else
            {
                //AnimationCurve tAnimCurve = AnimationUtility.GetEditorCurve(aClip, tBinding);
                DeleteCurve(aClip, tBinding, false);
                AnimationUtility.SetEditorCurve(aClip, tNewBinding, tCurve.curve);
            }
        }
    }

    public void Refresh(bool ifNotDirty = false)
    {
        if (!ifNotDirty || !_clip1Data.dirty)
            GetCurvesData(_animationClip, ref _clip1Data);
        if (!ifNotDirty || !_clip2Data.dirty)
            GetCurvesData(_animationClip2, ref _clip2Data);
    }

    public void GetCurvesData(AnimationClip aClip, ref ClipData aClipData)
    {
        aClipData = new ClipData();
        if (aClip != null)
        {
            aClipData.clip = aClip;
            // Object reference curves
            EditorCurveBinding[] tBindings = AnimationUtility.GetObjectReferenceCurveBindings(aClip);
            aClipData.objectReferenceCurves = GetCruvesData(aClip, tBindings, true);
            // Float curves
            tBindings = AnimationUtility.GetCurveBindings(aClip);
            aClipData.floatCurves = GetCruvesData(aClip, tBindings, false);
        }
        aClipData.dirty = false;
    }

    CurveData[] GetCruvesData(AnimationClip aClip, EditorCurveBinding[] aBindings, bool aObjectRef)
    {
        if (aBindings != null)
        {
            CurveData[] aCurveList = new CurveData[aBindings.Length];
            for (int i = 0; i < aBindings.Length; i++)
            {
                EditorCurveBinding tBinding = aBindings[i];
                CurveData tData = new CurveData();
                tData.binding = tBinding;
                tData.path = tBinding.path;
                tData.propertyName = tBinding.propertyName;
                tData.objectRef = aObjectRef;
                tData.GetData(aClip);
                aCurveList[i] = tData;
            }
            return aCurveList;
        }
        else
            return null;
    }

    void ShowBindingGUI(ClipData aClip, CurveData[] aData)
    {
        for (int i = 0; i < aData.Length; i++)
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);
            CurveData tCurve = aData[i];
            EditorCurveBinding tBinding = tCurve.binding;
            GUILayout.BeginHorizontal();
            GUILayout.Label(
                "Curve " + i.ToString() + " (" + tBinding.type.Name + ")",
                EditorStyles.boldLabel
            );

            // Change type
            EditorGUI.BeginChangeCheck();
            int tType = EditorGUILayout.Popup(GetTypeIndex(tBinding.type), typeList);
            if (EditorGUI.EndChangeCheck())
            {
                    if (aClip.dirty)
                    {
                        if (EditorUtility.DisplayDialog("Apply changes", "Changes must be applied before copying a curve", "Apply changes", "Cancel"))
                        {
                            ModifyCurves(aData, aClip.clip);
                        }
                    }
                    EditorCurveBinding tNewBinding = new EditorCurveBinding();
                    tNewBinding.path = tCurve.path;
                    tNewBinding.propertyName = tCurve.propertyName;
                    tNewBinding.type = TYPES[tType];
                    if (tCurve.objectRef)
                    {
                        ObjectReferenceKeyframe[] tFrames = AnimationUtility.GetObjectReferenceCurve(aClip.clip, tBinding);
                        DeleteCurve(aClip.clip, tBinding, tCurve.objectRef);
                        AnimationUtility.SetObjectReferenceCurve(aClip.clip, tNewBinding, tFrames);
                    }
                    else
                    {
                        AnimationCurve tFrames = AnimationUtility.GetEditorCurve(aClip.clip, tBinding);
                        DeleteCurve(aClip.clip, tBinding, tCurve.objectRef);
                        AnimationUtility.SetEditorCurve(aClip.clip, tNewBinding, tFrames);
                    }
                    Refresh(true);
            }

            // Copy curve
            if (_animationClip != null && _animationClip2 != null && GUILayout.Button("Copy", GUILayout.MaxWidth(40)))
            {
                AnimationClip tOtherClip;
                if (aClip.clip == _animationClip)
                    tOtherClip = _animationClip2;
                else
                    tOtherClip = _animationClip;
                if (tCurve.objectRef)
                {
                    if (aClip.dirty)
                    {
                        if (EditorUtility.DisplayDialog("Apply changes", "Changes must be applied before copying a curve", "Apply changes", "Cancel"))
                        {
                            ModifyCurves(aData, aClip.clip);
                        }
                    }

                    ObjectReferenceKeyframe[] tTarget = AnimationUtility.GetObjectReferenceCurve(tOtherClip, tBinding);
                    if (!aClip.dirty && (tTarget == null || (tTarget != null && EditorUtility.DisplayDialog("Replace curve", "The target curve will be replaced", "Replace", "Cancel"))))
                    {
                        ObjectReferenceKeyframe[] tFrames = AnimationUtility.GetObjectReferenceCurve(aClip.clip, tBinding);
                        AnimationUtility.SetObjectReferenceCurve(tOtherClip, tBinding, tFrames);
                    }
                }
                else
                {
                    if (aClip.dirty)
                    {
                        if (EditorUtility.DisplayDialog("Apply changes", "Changes must be applied before copying a curve", "Apply changes", "Cancel"))
                        {
                            ModifyCurves(aData, aClip.clip);
                        }
                    }

                    AnimationCurve tTarget = AnimationUtility.GetEditorCurve(tOtherClip, tBinding);
                    if (tTarget == null || (tTarget != null && EditorUtility.DisplayDialog("Replace curve", "The target curve will be replaced", "Replace", "Cancel")))
                    {
                        AnimationCurve tAnimCurve = AnimationUtility.GetEditorCurve(aClip.clip, tBinding);
                        AnimationUtility.SetEditorCurve(tOtherClip, tBinding, tAnimCurve);
                    }
                }
                Refresh();
            }

            // Delete
            if (GUILayout.Button("X", GUILayout.MaxWidth(20)))
            {
                if (EditorUtility.DisplayDialog("Changes will be lost", "Are you sure you want to delete this curve?\nYou can't take it back and unapplied changes will be lost.", "Delete", "Cancel"))
                {
                    DeleteCurve(aClip.clip, tCurve.binding, tCurve.objectRef);
                    Refresh(true);
                }
            }

            GUILayout.EndHorizontal();
            // Parameters
            EditorGUI.BeginChangeCheck();
            tCurve.propertyName = EditorGUILayout.TextField("Property name:", tCurve.propertyName);
            tCurve.path = EditorGUILayout.TextField("Path:", tCurve.path);
            if (EditorGUI.EndChangeCheck())
                aClip.dirty = true;
            if (tCurve.objectRef)
            {
                if (GUILayout.Button("Edit frames"))
                {
                    CKeyframesEditor.Show(aClip, tCurve);
                }
            }
            else
            {
                EditorGUI.BeginChangeCheck();
                tCurve.curve = EditorGUILayout.CurveField("Animation curve:", tCurve.curve);
                if (EditorGUI.EndChangeCheck())
                    aClip.dirty = true;
            }
            GUILayout.EndVertical();
        }
    }

    public void SetAnimation(AnimationClip aClip)
    {
        _animationClip = aClip;
    }

    public void DeleteCurve(AnimationClip aClip, EditorCurveBinding aBinding, bool aObjectRef)
    {
        if (aObjectRef)
            AnimationUtility.SetObjectReferenceCurve(aClip, aBinding, null);
        else
            AnimationUtility.SetEditorCurve(aClip, aBinding, null);
    }

    int FrameComparer(ObjectReferenceKeyframe aFrame1, ObjectReferenceKeyframe aFrame2)
    {
        if (aFrame1.time > aFrame2.time)
            return 1;
        if (aFrame1.time < aFrame2.time)
            return -1;
        return 0;
    }

    int GetTypeIndex(System.Type aType)
    {
        for (int i = 0; i < TYPES.Length; i++)
        {
            if (TYPES[i] == aType)
                return i;
        }
        return 0;
    }

    #region Menu items
    [MenuItem(CConstants.ASSETS_MENU + MENU_NAME + SPRITE_TO_IMAGE)]
    [MenuItem(CConstants.ROOT_MENU + MENU_NAME + SPRITE_TO_IMAGE, false, 1)]
    public static void SpriteToImage()
    {
        for (int j = 0; j < Selection.objects.Length; j++)
        {
            Object tObj = Selection.objects[j];
            if (tObj is AnimationClip)
            {
                ChangeAnimationType<SpriteRenderer, Image>(tObj as AnimationClip, PROPERTY_SPRITE);
            }
            else
            {
                Debug.LogWarning("Object " + AssetDatabase.GetAssetPath(tObj) + " is not an AnimationClip");
            }
        }
    }

    [MenuItem(CConstants.ASSETS_MENU + MENU_NAME + IMAGE_TO_SPRITE)]
    [MenuItem(CConstants.ROOT_MENU + MENU_NAME + IMAGE_TO_SPRITE, false, 1)]
    public static void ImageToSprite()
    {
        for (int j = 0; j < Selection.objects.Length; j++)
        {
            Object tObj = Selection.objects[j];
            if (tObj is AnimationClip)
            {
                ChangeAnimationType<Image, SpriteRenderer>(tObj as AnimationClip, PROPERTY_SPRITE);
            }
            else
            {
                Debug.LogWarning("Object " + AssetDatabase.GetAssetPath(tObj) + " is not an AnimationClip");
            }
        }
    }

    [MenuItem(CConstants.ASSETS_MENU + MENU_NAME + SPRITE_TO_IMAGE, true)]
    [MenuItem(CConstants.ROOT_MENU + MENU_NAME + SPRITE_TO_IMAGE, true, 1)]
    [MenuItem(CConstants.ASSETS_MENU + MENU_NAME + IMAGE_TO_SPRITE, true)]
    [MenuItem(CConstants.ROOT_MENU + MENU_NAME + IMAGE_TO_SPRITE, true, 1)]
    public static bool CheckAnimationSelected()
    {
        Object tObj = Selection.activeObject;
        return tObj is AnimationClip;
    }

    #endregion

    #region Methods
    public static void ChangeAnimationType<T1, T2>(AnimationClip aClip, string aPropertyName)
    {
        string tAssetPath = AssetDatabase.GetAssetPath(aClip);
        AnimationClip tNewAnimation = new AnimationClip();
        tNewAnimation.frameRate = aClip.frameRate;
        tNewAnimation.wrapMode = aClip.wrapMode;

        EditorCurveBinding tCurveBinding;

        EditorCurveBinding[] tOriginalBindings = AnimationUtility.GetObjectReferenceCurveBindings(aClip);
        int tAmount = 0;
        for (int i = 0; i < tOriginalBindings.Length; i++)
        {
            EditorCurveBinding tBinding = tOriginalBindings[i];
            if (tBinding.type == typeof(T1))
            {
                tCurveBinding = new EditorCurveBinding();
                tCurveBinding.type = typeof(T2);
                tCurveBinding.propertyName = aPropertyName;
                tCurveBinding.path = tBinding.path;
                ObjectReferenceKeyframe[] tOriginalFrames = AnimationUtility.GetObjectReferenceCurve(aClip, tOriginalBindings[i]);
                if (tOriginalFrames != null)
                {
                    ObjectReferenceKeyframe[] tNewFrames = new ObjectReferenceKeyframe[tOriginalFrames.Length];
                    for (int j = 0; j < tOriginalFrames.Length; j++)
                    {
                        tNewFrames[j] = tOriginalFrames[j];
                    }
                    AnimationUtility.SetObjectReferenceCurve(tNewAnimation, tCurveBinding, tNewFrames);
                }
                tAmount++;
            }
        }

        if (tAmount > 0)
        {
            tNewAnimation.name = aClip.name + "_" + typeof(T2).Name;
            string tNewPath = tAssetPath.Replace(aClip.name, tNewAnimation.name);
            AssetDatabase.CreateAsset(tNewAnimation, tNewPath);
            AssetDatabase.SaveAssets();
        }
        else
        {
            Debug.LogWarning("Animation " + tAssetPath + " doesn't have a " + typeof(T1).Name + " animation");
        }
    }

    public static System.Type GetPropertyType(System.Type aType, string aPropertyName)
    {
        System.Reflection.FieldInfo tInfo = aType.GetField(aPropertyName);
        if (tInfo != null)
            return tInfo.FieldType;
        return null;

    }
    #endregion
}

public class CKeyframesEditor : EditorWindow
{
    public static CKeyframesEditor Inst { get; private set; }
    public static bool IsOpen { get { return Inst != null; } }

    CAnimationTools.CurveData _curveData;
    CAnimationTools.ClipData _clipData;
    Vector2 _scrollPos;
    string _curveName;

    public static void Show(CAnimationTools.ClipData aClipData, CAnimationTools.CurveData aCurveData)
    {
        CKeyframesEditor tWindow = GetWindow<CKeyframesEditor>();
        tWindow.SetData(aClipData, aCurveData);
    }

    public void SetData(CAnimationTools.ClipData aClipData, CAnimationTools.CurveData aCurveData)
    {
        _curveData = aCurveData;
        _clipData = aClipData;
        string tPath = _curveData.binding.path;
        if (tPath != "")
            tPath += ".";
        _curveName = _clipData.clip.name + " (" + tPath + _curveData.binding.type.Name + "." + _curveData.binding.propertyName + ")";
    }

    void OnEnable()
    {
        Inst = this;
    }

    void OnDestroy()
    {
        Inst = null;
    }

    void OnGUI()
    {
        if (!CAnimationTools.IsOpen || _curveData == null || _clipData == null)
        {
            Close();
            return;
        }

        GUILayout.Label(_curveName, CAnimationTools.titleStyle);
        _scrollPos = GUILayout.BeginScrollView(_scrollPos);
        for (int i = 0; i < _curveData.frames.Length; i++)
        {
            GUILayout.BeginVertical(EditorStyles.helpBox);
            EditorGUI.BeginChangeCheck();
            ObjectReferenceKeyframe tFrame = _curveData.frames[i];
            float tTime = EditorGUILayout.FloatField("Time:", tFrame.time);
            Object tValue = EditorGUILayout.ObjectField("Value:", tFrame.value, _curveData.propertyType, false);
            if (EditorGUI.EndChangeCheck())
            {
                _clipData.dirty = true;
                tFrame.time = tTime;
                tFrame.value = tValue;
                _curveData.frames[i] = tFrame;
                CAnimationTools.Inst.Repaint();
            }
            GUILayout.EndVertical();
        }
        GUILayout.EndScrollView();
    }
}