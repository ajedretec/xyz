﻿#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using UnityEngine;
using XInputDotNetPure;

namespace RhoTools.RhoInput
{
    public class CXInput
    {
        const int JOYSTICKS = 4;

        private static CInputControl.GamePadVibration[] _vibrations;

        static GamePadState[] _prevStates;
        static GamePadState[] _states;

        public static void Init()
        {
            _states = new GamePadState[JOYSTICKS];
            _prevStates = new GamePadState[JOYSTICKS];
            _vibrations = new CInputControl.GamePadVibration[JOYSTICKS];
            for (int i = 0; i < JOYSTICKS; i++)
            {
                PlayerIndex tIndex = (PlayerIndex)i;
                _states[i] = GamePad.GetState(tIndex);

                CInputControl.GamePadVibration tData = new CInputControl.GamePadVibration();
                tData.left = new CVibration();
                tData.right = new CVibration();
                tData.left.active = false;
                tData.right.active = false;
                _vibrations[i] = tData;
            }
        }

        public static void Update()
        {
            for (int i = 0; i < _states.Length; i++)
            {
                PlayerIndex tIndex = (PlayerIndex)i;

                // Set previous
                GamePadState tState = _states[i];
                _prevStates[i] = tState;
                _states[i] = GamePad.GetState(tIndex);

                // Vibrations
                CInputControl.GamePadVibration tVibration = _vibrations[i];
                if (tVibration.left.active || tVibration.right.active)
                {
                    DoVibration(tVibration.left);
                    DoVibration(tVibration.right);

                    SetVibration(tIndex, tVibration.left.intensity, tVibration.right.intensity);
                    tVibration.left.time += Time.unscaledDeltaTime;
                    tVibration.right.time += Time.unscaledDeltaTime;
                }
            }
        }

        public static void SetButtonStates(CJoystick.Axis aButton, int aIndex, ref ButtonState aPrevButtonState, ref ButtonState aCurButtonState)
        {
            GamePadState tPrevState = _prevStates[aIndex];
            GamePadState tCurState = _states[aIndex];
            switch (aButton)
            {
                case CJoystick.Axis.BTN1:
                    aPrevButtonState = tPrevState.Buttons.A;
                    aCurButtonState = tCurState.Buttons.A;
                    break;
                case CJoystick.Axis.BTN2:
                    aPrevButtonState = tPrevState.Buttons.B;
                    aCurButtonState = tCurState.Buttons.B;
                    break;
                case CJoystick.Axis.BTN3:
                    aPrevButtonState = tPrevState.Buttons.X;
                    aCurButtonState = tCurState.Buttons.X;
                    break;
                case CJoystick.Axis.BTN4:
                    aPrevButtonState = tPrevState.Buttons.Y;
                    aCurButtonState = tCurState.Buttons.Y;
                    break;
                case CJoystick.Axis.L1:
                    aPrevButtonState = tPrevState.Buttons.LeftShoulder;
                    aCurButtonState = tCurState.Buttons.LeftShoulder;
                    break;
                case CJoystick.Axis.R1:
                    aPrevButtonState = tPrevState.Buttons.RightShoulder;
                    aCurButtonState = tCurState.Buttons.RightShoulder;
                    break;
                case CJoystick.Axis.L3:
                    aPrevButtonState = tPrevState.Buttons.LeftStick;
                    aCurButtonState = tCurState.Buttons.LeftStick;
                    break;
                case CJoystick.Axis.R3:
                    aPrevButtonState = tPrevState.Buttons.RightStick;
                    aCurButtonState = tCurState.Buttons.RightStick;
                    break;
                case CJoystick.Axis.START:
                    aPrevButtonState = tPrevState.Buttons.Start;
                    aCurButtonState = tCurState.Buttons.Start;
                    break;
                case CJoystick.Axis.SELECT:
                    aPrevButtonState = tPrevState.Buttons.Back;
                    aCurButtonState = tCurState.Buttons.Back;
                    break;
            }
        }

        // Button
        public static bool GetButton(int aIndex, CJoystick.Axis aButton)
        {
            ButtonState tPrevButtonState = ButtonState.Released;
            ButtonState tCurButtonState = ButtonState.Released;
            SetButtonStates(aButton, aIndex, ref tPrevButtonState, ref tCurButtonState);

            return tCurButtonState == ButtonState.Pressed;
        }

        public static bool GetButton(PlayerIndex aIndex, CJoystick.Axis aButton)
        {
            return GetButton((int)aIndex, aButton);
        }

        // Button down
        public static bool GetButtonDown(int aIndex, CJoystick.Axis aButton)
        {
            ButtonState tPrevButtonState = ButtonState.Released;
            ButtonState tCurButtonState = ButtonState.Released;
            SetButtonStates(aButton, aIndex, ref tPrevButtonState, ref tCurButtonState);

            return tPrevButtonState == ButtonState.Released && tCurButtonState == ButtonState.Pressed;
        }

        public static bool GetButtonDown(PlayerIndex aIndex, CJoystick.Axis aButton)
        {
            return GetButtonDown((int)aIndex, aButton);
        }

        // Button up
        public static bool GetButtonUp(int aIndex, CJoystick.Axis aButton)
        {
            ButtonState tPrevButtonState = ButtonState.Released;
            ButtonState tCurButtonState = ButtonState.Released;
            SetButtonStates(aButton, aIndex, ref tPrevButtonState, ref tCurButtonState);

            return tPrevButtonState == ButtonState.Pressed && tCurButtonState == ButtonState.Released;
        }

        public static bool GetButtonUp(PlayerIndex aIndex, CJoystick.Axis aButton)
        {
            return GetButtonUp((int)aIndex, aButton);
        }

        public static float GetStateAxis(GamePadState aState, CJoystick.Axis aAxis)
        {
            switch (aAxis)
            {
                case CJoystick.Axis.LH:
                    return aState.ThumbSticks.Left.X;
                case CJoystick.Axis.LV:
                    return aState.ThumbSticks.Left.Y;
                case CJoystick.Axis.RH:
                    return aState.ThumbSticks.Right.X;
                case CJoystick.Axis.RV:
                    return aState.ThumbSticks.Right.Y;
                case CJoystick.Axis.L2:
                    return aState.Triggers.Left;
                case CJoystick.Axis.R2:
                    return aState.Triggers.Right;
                case CJoystick.Axis.DPADH:
                    float tValue1 = 0;
                    if (aState.DPad.Right == ButtonState.Pressed)
                        tValue1 += 1;
                    if (aState.DPad.Left == ButtonState.Pressed)
                        tValue1 -= 1;
                    return tValue1;
                case CJoystick.Axis.DPADV:
                    float tValue2 = 0;
                    if (aState.DPad.Up == ButtonState.Pressed)
                        tValue2 += 1;
                    if (aState.DPad.Down == ButtonState.Pressed)
                        tValue2 -= 1;
                    return tValue2;
            }
            return 0;
        }

        // Axis
        public static float GetAxis(int aIndex, CJoystick.Axis aAxis)
        {
            GamePadState tState = _states[aIndex];
            return GetStateAxis(tState, aAxis);
        }

        public static float GetAxis(PlayerIndex aIndex, CJoystick.Axis aAxis)
        {
            return GetAxis((int)aIndex, aAxis);
        }

        // Trigger
        public static bool GetTrigger(int aIndex, CJoystick.Axis aTrigger, float aThreshold = 1)
        {
            return GetAxis(aIndex, aTrigger) >= aThreshold;
        }

        public static bool GetTrigger(PlayerIndex aIndex, CJoystick.Axis aTrigger, float aThreshold = 1)
        {
            return GetTrigger((int)aIndex, aTrigger, aThreshold);
        }

        // Trigger down
        public static bool GetTriggerDown(int aIndex, CJoystick.Axis aTrigger, float aThreshold = 1)
        {
            GamePadState tState = _states[aIndex];
            GamePadState tPrevState = _prevStates[aIndex];
            return GetStateAxis(tPrevState, aTrigger) < aThreshold && GetStateAxis(tState, aTrigger) >= aThreshold;
        }

        public static bool GetTriggerDown(PlayerIndex aIndex, CJoystick.Axis aTrigger, float aThreshold = 1)
        {
            return GetTriggerDown((int)aIndex, aTrigger, aThreshold);
        }

        // Axis down
        public static bool GetAxisPositiveDown(int aIndex, CJoystick.Axis aAxis, float aThreshold = 0)
        {
            GamePadState tState = _states[aIndex];
            GamePadState tPrevState = _prevStates[aIndex];
            return GetStateAxis(tPrevState, aAxis) <= aThreshold && GetStateAxis(tState, aAxis) > aThreshold;
        }

        public static bool GetAxisPositiveDown(PlayerIndex aIndex, CJoystick.Axis aAxis, float aThreshold = 0)
        {
            return GetAxisPositiveDown((int)aIndex, aAxis, aThreshold);
        }

        public static bool GetAxisNegativeDown(int aIndex, CJoystick.Axis aAxis, float aThreshold = 0)
        {
            GamePadState tState = _states[aIndex];
            GamePadState tPrevState = _prevStates[aIndex];
            return -GetStateAxis(tPrevState, aAxis) <= aThreshold && -GetStateAxis(tState, aAxis) > aThreshold;
        }

        public static bool GetAxisNegativeDown(PlayerIndex aIndex, CJoystick.Axis aAxis, float aThreshold = 0)
        {
            return GetAxisNegativeDown((int)aIndex, aAxis, aThreshold);
        }

        // Vibrations
        public static void SetVibration(int aIndex, float aLeft, float aRight)
        {
            GamePad.SetVibration((PlayerIndex)aIndex, aLeft, aRight);
        }

        public static void SetVibration(PlayerIndex aIndex, float aLeft, float aRight)
        {
            GamePad.SetVibration(aIndex, aLeft, aRight);
        }

        static void DoVibration(CVibration aData)
        {
            if (aData.active)
            {
                if (aData.intensity > 0)
                {
                    if (aData.time >= aData.delay)
                        aData.intensity -= aData.decay * Time.unscaledDeltaTime;
                }
                else
                {
                    aData.intensity = 0;
                    aData.active = false;
                }
            }
        }

        public static void VibrateMotor(int aIndex, int aMotor, float aIntensity, float aDecay, float aDelay)
        {
            CInputControl.GamePadVibration tData = _vibrations[aIndex];
            if (aMotor == 0) // Left motor
            {
                if (tData.left.intensity < aIntensity)
                {
                    tData.left.intensity = aIntensity;
                    tData.left.decay = aDecay;
                    tData.left.delay = aDelay;
                    tData.left.active = true;
                }
            }
            else if (aMotor == 1) // Right motor
            {
                if (tData.right.intensity < aIntensity)
                {
                    tData.right.intensity = aIntensity;
                    tData.right.decay = aDecay;
                    tData.right.delay = aDelay;
                    tData.right.active = true;
                }
            }
        }

        public static void VibrateMotor(PlayerIndex aIndex, int aMotor, float aIntensity, float aDecay, float aDelay)
        {
            VibrateMotor((int)aIndex, aMotor, aIntensity, aDecay, aDelay);
        }

        public static void VibrateMotor(int aIndex, int aMotor, CVibration aData)
        {
            VibrateMotor(aIndex, aMotor, aData.intensity, aData.decay, aData.delay);
        }

        public static void VibrateMotor(PlayerIndex aIndex, int aMotor, CVibration aData)
        {
            VibrateMotor(aIndex, aMotor, aData.intensity, aData.decay, aData.delay);
        }

        public static GamePadState[] GetStates()
        {
            return _states;
        }
    }
}
#endif
