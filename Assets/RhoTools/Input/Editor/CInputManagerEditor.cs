﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CInputManager))]
public class CInputManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (Application.isPlaying)
        {
            if (GUILayout.Button("Print maps"))
            {
                (target as CInputManager).PrintMaps();
            }
        }
    }
}
