﻿using UnityEditor;
using UnityEngine;
using RhoTools.ReorderableList;

[CustomEditor(typeof(CJoystickMap))]
public class CJoystickMapEditor : Editor
{
    SerializedObject _serializedObject;
    SerializedProperty _buttonsProperty;
    SerializedProperty _axesProperty;
    SerializedProperty _axisMulProperty;
    SerializedProperty _nameProperty;
    SerializedProperty _spritesProperty;
    CJoystickMap _target;

    void OnEnable()
    {
        _serializedObject = new SerializedObject(target);
        _buttonsProperty = _serializedObject.FindProperty("buttonList");
        _axesProperty = _serializedObject.FindProperty("axisList");
        _axisMulProperty = _serializedObject.FindProperty("axisMultiplierList");
        _nameProperty = _serializedObject.FindProperty("namesList");
        _spritesProperty = _serializedObject.FindProperty("spritesList");
        _target = target as CJoystickMap;
    }

    public override void OnInspectorGUI()
    {
        _target.mapName = EditorGUILayout.TextField("Name", _target.mapName);
        _target.sprite = (Sprite)EditorGUILayout.ObjectField(
            "Controller sprite",
            _target.sprite,
            typeof(Sprite),
            false
        );

        EditorGUI.BeginChangeCheck();

        if (_serializedObject != null)
        {
            GUILayout.Space(5f);
            _serializedObject.Update();

            ReorderableListGUI.Title("Buttons");
            ReorderableListGUI.ListField(_buttonsProperty);

            ReorderableListGUI.Title("Axes");
            ReorderableListGUI.ListField(_axesProperty);

            ReorderableListGUI.Title("Axis multipliers");
            ReorderableListGUI.ListField(_axisMulProperty);

            ReorderableListGUI.Title("Axis names");
            ReorderableListGUI.ListField(_nameProperty);

            ReorderableListGUI.Title("Axis sprites");
            ReorderableListGUI.ListField(_spritesProperty);

            _serializedObject.ApplyModifiedProperties();
        }

        if (EditorGUI.EndChangeCheck())
        {
            (target as CJoystickMap).SetChanged();
        }

        if (GUILayout.Button("Cache Data"))
        {
            (target as CJoystickMap).CacheData();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Save as JSON"))
        {
            CJoystickMap tTarget = target as CJoystickMap;
            string tFilePath = EditorUtility.SaveFilePanel("Save as JSON", "", tTarget.mapName, "json");
            if (tFilePath != "")
                tTarget.SaveAsJSON(tFilePath);
        }
        if (GUILayout.Button("Load JSON"))
        {
            CJoystickMap tTarget = target as CJoystickMap;
            string tFilePath = EditorUtility.OpenFilePanel("Open joystick config", "", "json");
            if (tFilePath != "")
                tTarget.LoadJSON(tFilePath);
        }
        GUILayout.EndHorizontal();
    }
}

[CustomPropertyDrawer(typeof(CJoystickMap.AxisAssign))]
public class CAxisAssignEditor : PropertyDrawer
{
    readonly string[] _choices = new[]
    {
        "0",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "7",
        "8",
        "9",
        "10",
        "11",
        "12",
        "13",
        "14",
        "15",
        "16",
        "17",
        "18",
        "19",
    };
    const float SPACE = 5f;
    const float BUTTON_WIDTH = 90f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float tWidth = position.width;
        position.width = tWidth - BUTTON_WIDTH;

        EditorGUIUtility.labelWidth = 30;
        EditorGUI.PropertyField(
            position,
            property.FindPropertyRelative("axis")
        );

        position.x += position.width + SPACE;
        position.width = BUTTON_WIDTH;

        EditorGUIUtility.labelWidth = 50;
        SerializedProperty tButton = property.FindPropertyRelative("button");
        tButton.intValue = EditorGUI.Popup(position, "Button", tButton.intValue, _choices, EditorStyles.popup);

        EditorGUIUtility.labelWidth = 0;
    }
}

[CustomPropertyDrawer(typeof(CJoystickMap.AxisMultiplier))]
public class CAxisMultiplierEditor : PropertyDrawer
{
    const float SPACE = 5f;
    const float BUTTON_WIDTH = 95f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float tWidth = position.width;
        position.width = tWidth - BUTTON_WIDTH;

        EditorGUIUtility.labelWidth = 30;
        EditorGUI.PropertyField(
            position,
            property.FindPropertyRelative("axis")
        );

        position.x += position.width + SPACE;
        position.width = BUTTON_WIDTH;

        EditorGUIUtility.labelWidth = 60;
        EditorGUI.PropertyField(
            position,
            property.FindPropertyRelative("multiplier")
        );

        EditorGUIUtility.labelWidth = 0;
    }
}

[CustomPropertyDrawer(typeof(CJoystickMap.AxisName))]
public class CAxisNameEditor : PropertyDrawer
{
    const float SPACE = 5f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float tWidth = position.width;
        position.width = tWidth * .5f;

        EditorGUIUtility.labelWidth = 30;
        EditorGUI.PropertyField(
            position,
            property.FindPropertyRelative("axis")
        );

        position.x += position.width + SPACE;
        position.width = tWidth * .5f - SPACE;

        EditorGUIUtility.labelWidth = 45;
        EditorGUI.PropertyField(
            position,
            property.FindPropertyRelative("name")
        );

        EditorGUIUtility.labelWidth = 0;
    }
}

[CustomPropertyDrawer(typeof(CJoystickMap.AxisSprite))]
public class CAxisSpriteEditor : PropertyDrawer
{
    const float SPACE = 5f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float tWidth = position.width;
        position.width = tWidth * .5f;

        EditorGUIUtility.labelWidth = 30;
        EditorGUI.PropertyField(
            position,
            property.FindPropertyRelative("axis")
        );

        position.x += position.width + SPACE;
        position.width = tWidth * .5f - SPACE;

        EditorGUIUtility.labelWidth = 45;
        EditorGUI.PropertyField(
            position,
            property.FindPropertyRelative("sprite")
        );

        EditorGUIUtility.labelWidth = 0;
    }
}