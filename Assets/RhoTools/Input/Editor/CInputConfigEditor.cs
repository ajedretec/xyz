﻿using UnityEditor;
using UnityEngine;
using RhoTools.ReorderableList;

[CustomEditor(typeof(CInputConfig))]
public class CInputConfigEditor : Editor
{
    SerializedObject _serializedObject;
    SerializedProperty _mapsProperty;
    SerializedProperty _xboxMap;
    SerializedProperty _defaultMap;
    SerializedProperty _xboxNames;
    SerializedProperty _defaultSprite;
    SerializedProperty _spritesProperty;

    void OnEnable()
    {
        _serializedObject = new SerializedObject(target);
        _mapsProperty = _serializedObject.FindProperty("maps");
        _xboxMap = _serializedObject.FindProperty("xboxMap");
        _defaultMap = _serializedObject.FindProperty("defaultMap");
        _xboxNames = _serializedObject.FindProperty("xboxNames");
        _defaultSprite = _serializedObject.FindProperty("defaultControllerSprite");
        _spritesProperty = _serializedObject.FindProperty("defaultAxisSprites");
    }

    public override void OnInspectorGUI()
    {
        if (_serializedObject != null)
        {
            GUILayout.Space(5f);
            _serializedObject.Update();

            ReorderableListGUI.Title("Maps");
            ReorderableListGUI.ListField(_mapsProperty);

            EditorGUILayout.PropertyField(_xboxMap);
            ReorderableListGUI.Title("XBox Names");
            ReorderableListGUI.ListField(_xboxNames);

            EditorGUILayout.PropertyField(_defaultMap);

            EditorGUILayout.PropertyField(_defaultSprite);

            ReorderableListGUI.Title("Default xis sprites");
            ReorderableListGUI.ListField(_spritesProperty);


            if (GUILayout.Button("Capitalize"))
            {
                (target as CInputConfig).Capitalize();
            }

            _serializedObject.ApplyModifiedPropertiesWithoutUndo();
        }
    }
}

[CustomPropertyDrawer(typeof(CInputConfig.JoystickMap))]
public class CJoystickMapDrawer : PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight * 3
            + ReorderableListGUI.CalculateListFieldHeight(property.FindPropertyRelative("names"));
    }
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        SerializedProperty tList = property.FindPropertyRelative("names");
        position.height = ReorderableListGUI.CalculateListFieldHeight(tList);
        ReorderableListGUI.ListFieldAbsolute(
            position,
            tList
        );
        position.y += position.height;
        position.height = EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("map"));
        position.y += position.height;
        position.height = EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("linuxMap"));
        position.y += position.height;
        position.height = EditorGUIUtility.singleLineHeight;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("macMap"));
    }
}