﻿using UnityEngine;
using UnityEditor;
using SimpleJSON;
using XInputDotNetPure;
using RhoTools;
using RhoTools.RhoInput;

public class CInputManagerWindow : EditorWindow
{
    #region Window definition
    [MenuItem(CConstants.ROOT_MENU + "Input Manager...")]
    public static void ShowWindow()
    {
        GetWindow(typeof(CInputManagerWindow));
    }
    #endregion
    #region Menu Items
    [MenuItem(CConstants.ROOT_MENU + "Input/CreateMap")]
    public static void CreateModeList()
    {
        CustomAssetUtility.CreateAsset<CJoystickMap>();
    }
    [MenuItem(CConstants.ROOT_MENU + "Input/CreateInputConfig")]
    public static void CreateInputConfig()
    {
        CustomAssetUtility.CreateAsset<CInputConfig>();
    }
    #endregion

    // Constants
    const string INPUT_MANAGER_FILE = "ProjectSettings/InputManager.asset";
    const string AXES_PROPERTY = "m_Axes";
    const string KEY_OR_MOUSE_BUTTON = "KeyOrMouseButton";
    const string MOUSE_MOVEMENT = "MouseMovement";
    const string JOYSTICK_AXIS = "JoystickAxis";
    const int NUMBER_OF_JOYSTICKS = 8;
    const int NUMBER_OF_AXIS = 28;

    float _gravity = 1;
    float _deadZone = 0.19f;
    float _sensitivity = 1;
    bool _showDelete = false;

    public enum AxisType
    {
        KeyOrMouseButton = 0,
        MouseMovement = 1,
        JoystickAxis = 2
    };
    public class InputAxis
    {
        public string AxisName;
        public string name;
        public string descriptiveName;
        public string descriptiveNegativeName;
        public string negativeButton;
        public string positiveButton;
        public string altNegativeButton;
        public string altPositiveButton;

        public float gravity;
        public float dead;
        public float sensitivity;

        public bool snap = false;
        public bool invert = false;

        public AxisType type;

        public int axis;
        public int joyNum;

        public virtual void SetName(string aName, int aNum)
        {
            name = aName + "_" + aNum.ToString() + "_" + AxisName;
            joyNum = aNum;
        }
    }


    // GUI
    void OnGUI()
    {
        if (GUILayout.Button("Open Input menu"))
        {
            EditorApplication.ExecuteMenuItem("Edit/Project Settings/Input");
        }
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Show controllers list (unity)"))
        {
            string[] tNames = Input.GetJoystickNames();
            for (int i = 0; i < tNames.Length; i++)
                Debug.Log(i + ": " + tNames[i]);
        }
        if (GUILayout.Button("Show controllers list"))
        {
            if (Application.isPlaying)
            {
                string[] tNames = null;
                CInputManager.Inst.GetJoystickNames(ref tNames);
                for (int i = 0; i < tNames.Length; i++)
                    Debug.Log(i + ": " + tNames[i]);
            }
            else
                Debug.Log("Only works on play");
        }
        GUILayout.EndHorizontal();
        _showDelete = EditorGUILayout.Foldout(_showDelete, "Delete configuration");
        if (_showDelete)
        {
            if (GUILayout.Button("Delete current configuration (no turning back)"))
            {
                DeleteCurrentConfig();
            }
        }

        if (GUILayout.Button("Open config file (json)"))
        {
            OpenJsonConfig();
        }
        if (GUILayout.Button("Save config file (json)"))
        {
            SaveJsonConfig();
        }
        _gravity = EditorGUILayout.FloatField(
            "Axis gravity:",
            _gravity
        );
        _deadZone = EditorGUILayout.FloatField(
            "Dead zone:",
            _deadZone
        );
        _sensitivity = EditorGUILayout.FloatField(
            "Sensitivity:",
            _sensitivity
        );
        if (GUILayout.Button("Add axes"))
        {
            AddAxes();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Vibrate 1"))
        {
            CInputControl.Vibrate(PlayerIndex.One, 1, 1);
        }
        if (GUILayout.Button("Vibrate 2"))
        {
            CInputControl.Vibrate(PlayerIndex.Two, 1, 1);
        }
        if (GUILayout.Button("Vibrate 3"))
        {
            CInputControl.Vibrate(PlayerIndex.Three, 1, 1);
        }
        if (GUILayout.Button("Vibrate 4"))
        {
            CInputControl.Vibrate(PlayerIndex.Four, 1, 1);
        }
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Stop vibrations"))
        {
            for (int i = 0; i < 4; i++)
                CInputControl.Vibrate((PlayerIndex)i, 0, 0);
        }
    }

    private void DeleteCurrentConfig()
    {
        SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath(INPUT_MANAGER_FILE)[0]);
        SerializedProperty axesProperty = serializedObject.FindProperty(AXES_PROPERTY);
        axesProperty.ClearArray();
        serializedObject.ApplyModifiedProperties();
    }

    void OpenJsonConfig()
    {
        string tPath = EditorUtility.OpenFilePanel("Chose a json input config file", "/", "json");
        if (tPath == "")
            return;
        Debug.Log("Loading " + tPath);
        JSONNode tJson = CJSonReader.Load(tPath);
        // Options
        JSONNode tOptions = tJson["options"];
        if (tOptions["delete"].AsBool)
        {
            Debug.Log("Deleting current config");
            DeleteCurrentConfig();
        }
        // Axes
        JSONArray tAxes = tJson["axes"].AsArray;
        for (int i = 0; i < tAxes.Count; i++)
        {
            InputAxis tAxis = new InputAxis();
            // Strings
            if (tAxes[i]["name"].Value != "")
                tAxis.name = tAxes[i]["name"].Value;
            if (tAxes[i]["descriptiveName"].Value != "")
                tAxis.descriptiveName = tAxes[i]["descriptiveName"].Value;
            if (tAxes[i]["descriptiveNegativeName"].Value != "")
                tAxis.descriptiveNegativeName = tAxes[i]["descriptiveNegativeName"].Value;
            if (tAxes[i]["negativeButton"].Value != "")
                tAxis.negativeButton = tAxes[i]["negativeButton"].Value;
            if (tAxes[i]["positiveButton"].Value != "")
                tAxis.positiveButton = tAxes[i]["positiveButton"].Value;
            if (tAxes[i]["altNegativeButton"].Value != "")
                tAxis.altNegativeButton = tAxes[i]["altNegativeButton"].Value;
            if (tAxes[i]["altPositiveButton"].Value != "")
                tAxis.altPositiveButton = tAxes[i]["altPositiveButton"].Value;
            // Floats
            if (tAxes[i]["gravity"].Value != "")
                tAxis.gravity = tAxes[i]["gravity"].AsFloat;
            if (tAxes[i]["dead"].Value != "")
                tAxis.dead = tAxes[i]["dead"].AsFloat;
            if (tAxes[i]["sensitivity"].Value != "")
                tAxis.sensitivity = tAxes[i]["sensitivity"].AsFloat;
            // Booleans
            if (tAxes[i]["snap"].Value != "")
                tAxis.snap = tAxes[i]["snap"].AsBool;
            if (tAxes[i]["invert"].Value != "")
                tAxis.invert = tAxes[i]["invert"].AsBool;
            // Axis types
            if (tAxes[i]["type"].Value != "")
                tAxis.type = (AxisType)tAxes[i]["type"].AsInt;
            // Ints
            if (tAxes[i]["axis"].Value != "")
                tAxis.axis = tAxes[i]["axis"].AsInt;
            if (tAxes[i]["joyNum"].Value != "")
                tAxis.joyNum = tAxes[i]["joyNum"].AsInt;

            // Add the axis
            AddAxis(tAxis);
        }
    }

    void SaveJsonConfig()
    {
        string tFile = EditorUtility.SaveFilePanel("Chose a location to save your input config file", "/", "InputConfig.json", "json");
        if (tFile == "")
            return;
        JSONNode tJson = JSON.Parse("{}");
        SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath(INPUT_MANAGER_FILE)[0]);
        SerializedProperty axesProperty = serializedObject.FindProperty(AXES_PROPERTY);
        tJson["axes"] = new JSONArray();
        tJson["options"]["delete"].AsBool = true;
        for (int i = 0; i < axesProperty.arraySize; i++)
        {
            SerializedProperty axisProperty = axesProperty.GetArrayElementAtIndex(i);
            tJson["axes"][i]["name"] = GetChildProperty(axisProperty, "m_Name").stringValue;
            tJson["axes"][i]["descriptiveName"] = GetChildProperty(axisProperty, "descriptiveName").stringValue;
            tJson["axes"][i]["descriptiveNegativeName"] = GetChildProperty(axisProperty, "descriptiveNegativeName").stringValue;
            tJson["axes"][i]["negativeButton"] = GetChildProperty(axisProperty, "negativeButton").stringValue;
            tJson["axes"][i]["positiveButton"] = GetChildProperty(axisProperty, "positiveButton").stringValue;
            tJson["axes"][i]["altNegativeButton"] = GetChildProperty(axisProperty, "altNegativeButton").stringValue;
            tJson["axes"][i]["altPositiveButton"] = GetChildProperty(axisProperty, "altPositiveButton").stringValue;
            tJson["axes"][i]["gravity"].AsFloat = GetChildProperty(axisProperty, "gravity").floatValue;
            tJson["axes"][i]["dead"].AsFloat = GetChildProperty(axisProperty, "dead").floatValue;
            tJson["axes"][i]["sensitivity"].AsFloat = GetChildProperty(axisProperty, "sensitivity").floatValue;
            tJson["axes"][i]["snap"].AsBool = GetChildProperty(axisProperty, "snap").boolValue;
            tJson["axes"][i]["invert"].AsBool = GetChildProperty(axisProperty, "invert").boolValue;
            tJson["axes"][i]["type"].AsInt = GetChildProperty(axisProperty, "type").intValue;
            tJson["axes"][i]["axis"].AsInt = GetChildProperty(axisProperty, "axis").intValue;
            tJson["axes"][i]["joyNum"].AsInt = GetChildProperty(axisProperty, "joyNum").intValue;
        }
        string tJsonString = CPrettyJSON.FormatJson(tJson.ToString());
        Debug.Log(tJsonString);
        CTxtManager.Write(tFile, tJsonString);
    }

    void AddAxes()
    {
        for (int i = 0; i < NUMBER_OF_JOYSTICKS; i++)
        {
            for (int j = 0; j < NUMBER_OF_AXIS; j++)
            {
                InputAxis tAxis = new InputAxis
                {
                    name = "Joystick" + (i + 1).ToString() + "Axis" + (j + 1).ToString(),
                    axis = j,
                    joyNum = i+1,
                    gravity = _gravity,
                    dead = _deadZone,
                    sensitivity = _sensitivity,
                    snap = false,
                    invert = false,
                    type = AxisType.JoystickAxis,
                };

                AddAxis(tAxis);
            }
        }
    }

    #region InputManager functions
    private static SerializedProperty GetChildProperty(SerializedProperty parent, string name)
    {
        SerializedProperty child = parent.Copy();
        child.Next(true);
        do
        {
            if (child.name == name) return child;
        }
        while (child.Next(false));
        return null;
    }

    private static bool AxisDefined(string axisName)
    {
        SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath(INPUT_MANAGER_FILE)[0]);
        SerializedProperty axesProperty = serializedObject.FindProperty(AXES_PROPERTY);

        axesProperty.Next(true);
        axesProperty.Next(true);
        while (axesProperty.Next(false))
        {
            SerializedProperty axis = axesProperty.Copy();
            axis.Next(true);
            if (axis.stringValue == axisName) return true;
        }
        return false;
    }

    public static void AddAxis(InputAxis axis)
    {
        SerializedObject serializedObject = new SerializedObject(AssetDatabase.LoadAllAssetsAtPath(INPUT_MANAGER_FILE)[0]);
        SerializedProperty axesProperty = serializedObject.FindProperty(AXES_PROPERTY);

        axesProperty.arraySize++;
        serializedObject.ApplyModifiedProperties();

        SerializedProperty axisProperty = axesProperty.GetArrayElementAtIndex(axesProperty.arraySize - 1);

        GetChildProperty(axisProperty, "m_Name").stringValue = axis.name;
        GetChildProperty(axisProperty, "descriptiveName").stringValue = axis.descriptiveName;
        GetChildProperty(axisProperty, "descriptiveNegativeName").stringValue = axis.descriptiveNegativeName;
        GetChildProperty(axisProperty, "negativeButton").stringValue = axis.negativeButton;
        GetChildProperty(axisProperty, "positiveButton").stringValue = axis.positiveButton;
        GetChildProperty(axisProperty, "altNegativeButton").stringValue = axis.altNegativeButton;
        GetChildProperty(axisProperty, "altPositiveButton").stringValue = axis.altPositiveButton;
        GetChildProperty(axisProperty, "gravity").floatValue = axis.gravity;
        GetChildProperty(axisProperty, "dead").floatValue = axis.dead;
        GetChildProperty(axisProperty, "sensitivity").floatValue = axis.sensitivity;
        GetChildProperty(axisProperty, "snap").boolValue = axis.snap;
        GetChildProperty(axisProperty, "invert").boolValue = axis.invert;
        GetChildProperty(axisProperty, "type").intValue = (int)axis.type;
        GetChildProperty(axisProperty, "axis").intValue = axis.axis;
        GetChildProperty(axisProperty, "joyNum").intValue = axis.joyNum;

        serializedObject.ApplyModifiedProperties();
    }
    #endregion
}
