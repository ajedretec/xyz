﻿using UnityEngine;
using UnityEditor;
using RhoTools.ReorderableList;
using RhoTools.RhoInput;

[CustomPropertyDrawer(typeof(CControllerScheme.Button))]
public class CControllerSchemeEditor : PropertyDrawer
{
    const int HEIGHT = 5;

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return EditorGUIUtility.singleLineHeight * HEIGHT;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        float tWidth = position.width - 20;
        float tLineHeight = EditorGUIUtility.singleLineHeight;
        position.height = tLineHeight;

        EditorGUI.LabelField(position, property.displayName);

        position.width = tWidth;
        position.x += 20;
        position.y += position.height;


        EditorGUI.LabelField(position, "Joystick");
        position.y += position.height;

        SerializedProperty tProp = property.FindPropertyRelative("axes");
        position.height = ReorderableListGUI.CalculateListFieldHeight(tProp);
        ReorderableListGUI.ListFieldAbsolute(
            position,
            tProp
        );
        position.y += position.height;

        position.height = tLineHeight;
        EditorGUI.LabelField(position, "Keyboard");
        position.y += position.height;

        tProp = property.FindPropertyRelative("keys");
        position.height = ReorderableListGUI.CalculateListFieldHeight(tProp);
        ReorderableListGUI.ListFieldAbsolute(
            position,
            tProp
        );

    }
}
