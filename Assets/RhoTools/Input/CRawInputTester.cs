﻿using UnityEngine;
using RhoTools.RhoInput;

public class CRawInputTester : MonoBehaviour
{
    const float LINE_HEIGHT = 20;
    const float LINE_WIDTH = 150;

    [SerializeField]
    int _joystickNum = 0;

    Vector2 _scrollPos = Vector2.zero;
    CInputTester _inputTester;

    void Awake()
    {
        _inputTester = GetComponent<CInputTester>();
    }

    void OnGUI()
    {
        string[] tJoyNames = Input.GetJoystickNames();
        if (_joystickNum < 0)
            _joystickNum = 0;
        else if (_joystickNum >= tJoyNames.Length)
            _joystickNum = tJoyNames.Length - 1;

        // Upper bar
        Rect tPos = new Rect(30, 5, Screen.width, 30);
        Rect tButtonPos = new Rect(tPos.x + 5, tPos.y, 20, 20);
        for (int i = 0; i < tJoyNames.Length; i++)
        {
            if (GUI.Button(tButtonPos, (i + 1).ToString()))
            {
                _joystickNum = i;
            }
            tButtonPos.x += tButtonPos.width + 5;
        }
        if (_inputTester != null)
        {
            tButtonPos.width = 80;
            if (GUI.Button(tButtonPos, "RhoInput"))
            {
                _inputTester.enabled = true;
                enabled = false;
            }
            tButtonPos.x += tButtonPos.width + 5;
        }

        // Controller info
        tPos.y += tPos.height;
        GUI.Label(tPos, (_joystickNum + 1).ToString() + ": " + tJoyNames[_joystickNum]);

        tPos.y += LINE_HEIGHT;
        tPos.width = Screen.width - 125;
        tPos.height = Screen.height - 45 - LINE_HEIGHT;

        float tHeight = tPos.height;

        float tButtonsWidth = Mathf.Ceil(20 * LINE_HEIGHT / tHeight) * LINE_WIDTH;
        float tAxesWidth = Mathf.Ceil(28 * LINE_HEIGHT / tHeight) * LINE_WIDTH;

        using (var scrollViewScope = new GUI.ScrollViewScope(tPos, _scrollPos, new Rect(0, 0, tButtonsWidth + tAxesWidth, tPos.height - 25)))
        {
            _scrollPos = scrollViewScope.scrollPosition;
            tPos.x = 0;
            tPos.y = 0;

            //Buttons
            for (int i = 0; i < 20; i++)
            {
                GUI.Label(tPos, "Button " + i.ToString() + ": "
                    + Input.GetKey(CJoystick.JOYSTICK_BUTTONS[_joystickNum][i]).ToString());
                tPos.y += LINE_HEIGHT;
                if (tPos.y >= tHeight - LINE_HEIGHT - 25)
                {
                    tPos.y = 0;
                    tPos.x += LINE_WIDTH;
                }
            }

            tPos.y = 0;
            tPos.x += LINE_WIDTH;
            for (int i = 0; i < 28; i++)
            {
                GUI.Label(tPos, "Axis " + i.ToString() + ": "
                    + Input.GetAxis(CJoystick.JOYSTICK_AXES[_joystickNum][i].ToString()));
                tPos.y += LINE_HEIGHT;
                if (tPos.y >= tHeight - LINE_HEIGHT - 25)
                {
                    tPos.y = 0;
                    tPos.x += LINE_WIDTH;
                }
            }
        }

        //GUI.EndScrollView();
    }
}
