﻿using UnityEngine;
using RhoTools;
using RhoTools.RhoInput;
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using XInputDotNetPure;
#endif

public class CInputManager : MonoBehaviour
{
    public const int KEYBOARD_NUM = -2;
    const string XINPUT_NAME = "XBox 360 Joystick (CXInput)";

    [SerializeField]
    CInputConfig _inputConfig;
    [SerializeField]
    public bool useXInput;

    CJoystick[] _joysticks;
    public static CJoystick[] joysticks
    {
        get
        {
            return Inst._joysticks;
        }
    }
    string[] _joystickNames;
    System.Action _joysticksChangeCallback;
    Sprite[] _defaultAxisSprites;

    public static CInputManager Inst { get; private set; }

    bool _unityUpdate = true;

    void Awake()
    {
        if (Inst != null && Inst != this)
        {
            Destroy(gameObject);
            return;
        }
        Inst = this;

        if (_inputConfig.defaultMap == null)
            Debug.LogError("You must set a default joystick map");
        _inputConfig.Capitalize();
        
        // Cache sprites list
        CJoystick.Axis[] tAxes = CEnumUtils.GetValues<CJoystick.Axis>();
        _defaultAxisSprites = new Sprite[tAxes.Length];
        for (int i = 0; i < tAxes.Length; i++)
        {
            CJoystick.Axis tAxis = tAxes[i];
            int tIndex = (int)tAxis;

            _defaultAxisSprites[tIndex] = CJoystickMap.GetValue(_inputConfig.defaultAxisSprites, tAxis);
        }
        _unityUpdate = true;
    }

    void Start()
    {
        CInputControl.Init();
        CInputControl.useXInput = useXInput;
        CheckJoysticks();
    }

    void Update()
    {
        if (_unityUpdate)
            InUpdate();
    }

    public void APIUpdate()
    {
        _unityUpdate = false;
        InUpdate();
    }

    void InUpdate()
    {
        for (int i = 0; i < _joysticks.Length; i++)
        {
            CJoystick tJoystick = _joysticks[i];
            if (tJoystick != null)
                tJoystick.Update();
        }

        CInputControl.Update();


        string[] tJoysticks = Input.GetJoystickNames();
        if (ListsAreDifferent(tJoysticks, _joystickNames))
        {
            Debug.Log("Rechecking joysticks");
            if (_joysticksChangeCallback != null)
                _joysticksChangeCallback();
            _joystickNames = tJoysticks;
            CheckJoysticks();
        }

    }

    public CJoystickMap GetJoystickMap(string aName)
    {
        string tName = aName.ToUpper();

        for (int i = 0; i < _inputConfig.maps.Length; i++)
        {
            CInputConfig.JoystickMap tMapData = _inputConfig.maps[i];
            for (int j = 0; j < tMapData.names.Length; j++)
            {
                if (tName.Contains(tMapData.names[j]))
                {
                    CJoystickMap tMap = tMapData.map;
#if UNITY_EDITOR_LINUX || UNITY_STANDALONE_LINUX
                    if (tMapData.linuxMap != null)
                        tMap = tMapData.linuxMap;
#endif
#if UNITY_EDITOR_OSX || UNITY_STANDALONE_OSX
                    if (tMapData.macMap != null)
                        tMap = tMapData.macMap;
#endif
                    if (!tMap.cached)
                    {
                        tMap.CacheData();
                    }
                    return tMap;
                }
            }
        }

        if (!_inputConfig.defaultMap.cached)
            _inputConfig.defaultMap.CacheData();
        return _inputConfig.defaultMap;
    }

    public CJoystick GetJoystick(int aIndex)
    {
        if (aIndex >= 0 && aIndex < _joysticks.Length)
            return _joysticks[aIndex];
        else
            return null;
    }

    public void CheckJoysticks()
    {
        _joystickNames = Input.GetJoystickNames();
        int tXboxJoysNum = 0;

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        GamePadState[] tStates = CXInput.GetStates();
        _joysticks = new CJoystick[tStates.Length + _joystickNames.Length];
        // Put XBox joysticks at the begining on windows
        tXboxJoysNum = tStates.Length;
        for (int i = 0; i < tStates.Length; i++)
        {
            if (!_inputConfig.xboxMap.cached)
                _inputConfig.xboxMap.CacheData();
            CXBox360 tJoystick = new CXBox360();
            tJoystick.SetMap(_inputConfig.xboxMap, i);
            tJoystick.SetPlayerIndex(i);
            tJoystick.name = XINPUT_NAME;
            _joysticks[i] = tJoystick;
        }
#else
        _joysticks = new CJoystick[_joystickNames.Length];

#endif

        bool tSkip = false;
        for (int i = 0; i < _joystickNames.Length; i++)
        {
            string tName = _joystickNames[i];
            if (tName != "")
            {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                string tUpperName = tName.ToUpper();
                for (int j = 0; j < _inputConfig.xboxNames.Length; j++)
                {
                    if (tUpperName.Contains(_inputConfig.xboxNames[j]))
                    {
                        _joysticks[tXboxJoysNum + i] = null;
                        tSkip = true;
                    }
                }
#endif
                if (!tSkip)
                {
                    CJoystickMap tMap = GetJoystickMap(tName);
                    CJoystick tJoystick = new CJoystick();
                    tJoystick.SetMap(tMap, i);
                    tJoystick.name = tName;
                    _joysticks[tXboxJoysNum + i] = tJoystick;
                }
            }
            else
                _joysticks[tXboxJoysNum + i] = null;

            tSkip = false;
        }
#if UNITY_EDITOR
        Debug.Log("Joysticks connected:");
        for (int i = 0; i < _joysticks.Length; i++)
        {
            if (_joysticks[i] != null)
                Debug.Log(i + " - " + _joysticks[i].name);
            else
                Debug.Log(i + " - null");
        }
#endif
    }

    static bool ListsAreDifferent(string[] aList1, string[] aList2)
    {
        if (aList1.Length != aList2.Length)
            return true;
        for (int i = 0; i < aList1.Length; i++)
        {
            if (aList1[i] != aList2[i])
                return true;
        }
        return false;
    }

    public void SetJoystickChangeCallback(System.Action aCallback)
    {
        _joysticksChangeCallback = aCallback;
    }

    public void PrintMaps()
    {
        for (int i = 0; i < _joysticks.Length; i++)
        {
            _joysticks[i].GetMap().PrintMap();
        }
    }

    public void GetJoystickNames(ref string[] aList)
    {
        aList = new string[_joysticks.Length];
        for (int i = 0; i < _joysticks.Length; i++)
        {
            CJoystick tJoy = _joysticks[i];
            if (tJoy != null)
                aList[i] = _joysticks[i].name;
            else
                aList[i] = "EMPTY";
        }
    }

    public Sprite GetDefaultSprite(CJoystick.Axis aKey)
    {
        return _defaultAxisSprites[(int)aKey];
    }

    public Sprite GetDefaultControllerSprite()
    {
        return _inputConfig.defaultControllerSprite;
    }
}
