﻿using UnityEngine;

namespace RhoTools.RhoInput
{
    public class CControllerScheme : ScriptableObject
    {
        [System.Serializable]
        public class Button
        {
            public CJoystick.Axis[] axes;
            public KeyCode[] keys;
        }

        public bool automaticAssign;
        public string defaultController;

        public virtual void SetDefaults()
        {
            automaticAssign = false;
            defaultController = "NONE";
        }
    }
}
