﻿using UnityEngine;

public class CInputConfig : ScriptableObject
{
    [System.Serializable]
    public class JoystickMap
    {
        public string[] names;
        public CJoystickMap map;
        public CJoystickMap linuxMap;
        public CJoystickMap macMap;
    }

    public CJoystickMap xboxMap;
    public string[] xboxNames;
    public JoystickMap[] maps;
    public CJoystickMap defaultMap;
    public Sprite defaultControllerSprite;
    public CJoystickMap.AxisSprite[] defaultAxisSprites;

    public void Capitalize()
    {
        for (int i = 0; i < maps.Length; i++)
        {
            string[] tNames = maps[i].names;
            for (int j = 0; j < tNames.Length; j++)
                tNames[j] = tNames[j].ToUpper();
        }
    }
}
