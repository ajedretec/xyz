﻿#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using XInputDotNetPure;
#endif

namespace RhoTools.RhoInput
{
    public static class CInputControl
    {
        public class GamePadVibration
        {
            public CVibration left;
            public CVibration right;
        }

        public delegate bool ButtonQuery(int aNum);
        public delegate bool AxisQuery(int aNum, CJoystick.Axis aAxis, float aThreshold);

        private static bool _useXInput;
        public static bool useXInput { get { return _useXInput; } set { _useXInput = value; } }

        public static void Init()
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            CXInput.Init();
#endif
        }

        public static void Update()
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            CXInput.Update();
#endif
        }
        
        // Get joystick data
        #region Get Axis
        public static float GetAxis(int aNum, CJoystick.Axis aAxis)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetAxis(aAxis);
            return 0;
        }
        #endregion

        #region GetButton
        public static bool GetButton(int aNum, CJoystick.Axis aButton)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetButton(aButton);
            return false;
        }

        public static bool GetButtonDown(int aNum, CJoystick.Axis aButton)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetButtonDown(aButton);
            return false;
        }

        public static bool GetButtonUp(int aNum, CJoystick.Axis aButton)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetButtonUp(aButton);
            return false;
        }
        #endregion

        #region GetTrigger
        public static bool GetTrigger(int aNum, CJoystick.Axis aTrigger, float aThreshold = 1)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetTrigger(aTrigger, aThreshold);
            return false;
        }

        // only works if Update is run on the game's loop
        public static bool GetTriggerDown(int aNum, CJoystick.Axis aTrigger, float aThreshold = 1)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetTriggerDown(aTrigger, aThreshold);
            return false;
        }
        #endregion

        #region GetAxis Down
        // only works if Update is run on the game's loop
        public static bool GetAxisPositiveDown(int aNum, CJoystick.Axis aAxis, float aThreshold = 0)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetAxisPositiveDown(aAxis);
            return false;
        }

        // only works if Update is run on the game's loop
        public static bool GetAxisNegativeDown(int aNum, CJoystick.Axis aAxis, float aThreshold = 0)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                return tJoystick.GetAxisNegativeDown(aAxis);
            return false;
        }
        #endregion

        // Checks connected joysticks and updates Joysticks list
        #region Get Joystick Down
        public static int GetJoystickDown(CJoystick.Axis aButton)
        {
            for (int i = 0; i < CInputManager.joysticks.Length; i++)
            {
                if (GetButtonDown(i, aButton))
                {
                    return i;
                }
            }
            return -1;
        }

        public static int GetJoystickDown(ButtonQuery aButtonQuery)
        {
            for (int i = 0; i < CInputManager.joysticks.Length; i++)
            {
                if (aButtonQuery(i))
                {
                    return i;
                }
            }
            return -1;
        }

        public static int GetJoystickPressed(CJoystick.Axis aButton)
        {
            for (int i = 0; i < CInputManager.joysticks.Length; i++)
            {
                if (GetButton(i, aButton))
                {
                    return i;
                }
            }
            return -1;
        }
        #endregion

        public static void Vibrate(int aNum, float aValueLeft, float aValueRight)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                tJoystick.Vibrate(aValueLeft, aValueRight);
        }

        public static void VibrateMotor(int aNum, int aMotor, float aIntensity, float aDecay, float aDelay)
        {
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                tJoystick.VibrateMotor(aMotor, aIntensity, aDecay, aDelay);
        }

        public static void VibrateMotor(int aNum, int aMotor, CVibration aData)
        {
            UnityEngine.Debug.Log("Vibrate: " + aNum);
            CJoystick tJoystick = CInputManager.Inst.GetJoystick(aNum);
            if (tJoystick != null)
                tJoystick.VibrateMotor(aMotor, aData);
        }

#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        public static void Vibrate(PlayerIndex aIndex, float aValueLeft, float aValueRight)
        {
            if (_useXInput)
                CXInput.SetVibration(aIndex, aValueLeft, aValueRight);
        }

        public static void VibrateMotor(PlayerIndex aIndex, int aNum, int aMotor, float aIntensity, float aDecay, float aDelay)
        {
            if (_useXInput)
                CXInput.VibrateMotor(aIndex, aMotor, aIntensity, aDecay, aDelay);
        }
#endif
    }
}
