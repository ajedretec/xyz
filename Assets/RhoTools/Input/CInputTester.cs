﻿using UnityEngine;
using RhoTools;
using RhoTools.RhoInput;

public class CInputTester : MonoBehaviour
{
    const float LINE_HEIGHT = 20;

    [SerializeField]
    int _joystickNum = 0;

    Vector2 _scrollPos1 = Vector2.zero;
    Vector2 _scrollPos2 = Vector2.zero;
    CRawInputTester _rawInputTester;

    void Awake()
    {
        _rawInputTester = GetComponent<CRawInputTester>();
    }

    void OnGUI()
    {
        // Upper bar
        Rect tPos = new Rect(30, 5, Screen.width, 30);
        Rect tButtonPos = new Rect(tPos.x + 5, tPos.y, 20, 20);
        for (int i = 0; i < CInputManager.joysticks.Length; i++)
        {
            if (GUI.Button(tButtonPos, (i+1).ToString()))
            {
                _joystickNum = i;
            }
            tButtonPos.x += tButtonPos.width + 5;
        }
        if (_rawInputTester != null)
        {
            tButtonPos.width = 80;
            if (GUI.Button(tButtonPos, "Unity Input"))
            {
                _rawInputTester.enabled = true;
                enabled = false;
            }
            tButtonPos.x += tButtonPos.width + 5;
        }
        tButtonPos.width = 95;
        if (GUI.Button(tButtonPos, (CInputControl.useXInput ? "Disable" : "Enable") + " XInput"))
        {
            CInputControl.useXInput = !CInputControl.useXInput;
        }
        tButtonPos.x += tButtonPos.width + 5;

        // Controller info
        tPos.y += tPos.height;
        float tInfoY = tPos.y;

        CJoystick tHandler = CInputManager.Inst.GetJoystick(_joystickNum);
        if (tHandler != null)
        {
            GUI.Label(tPos, (_joystickNum + 1).ToString() + ": " + tHandler.name + ": " + tHandler.mapName);

            tPos.y += LINE_HEIGHT;
            tPos.width = 450;
            CJoystick.Axis[] tButtons = tHandler.GetButtonsList();
            CJoystick.Axis[] tAxes = tHandler.GetAxisList();
            //Buttons
            tPos.height = Screen.height - tPos.y;
            _scrollPos1 = GUI.BeginScrollView(
                tPos,
                _scrollPos1,
                new Rect(0, 0, tPos.width - 25, Mathf.Max(tButtons.Length, tAxes.Length) * LINE_HEIGHT)
            );
            tPos.x = LINE_HEIGHT + 5;
            tPos.y = 0;
            Rect tSprPos = new Rect(0, 0, LINE_HEIGHT, LINE_HEIGHT);
            for (int i = 0; i < tButtons.Length; i++)
            {
                CJoystick.Axis tButton = tButtons[i];

                DrawSprite(tHandler.GetSprite(tButton), tSprPos);
                GUI.Label(tPos, tHandler.GetAxisHumanName(tButton) + ": " + tHandler.GetButton(tButton).ToString());
                tPos.y += LINE_HEIGHT;
                tSprPos.y += LINE_HEIGHT;
            }

            tPos.y = 0;
            tPos.x = 250;
            for (int i = 0; i < tAxes.Length; i++)
            {
                tPos.y += 20;
                CJoystick.Axis tButton = tAxes[i];
                GUI.Label(tPos, tHandler.GetAxisHumanName(tButton) + ": " + tHandler.GetAxis(tButton).ToString());
            }
            GUI.EndScrollView();

            CJoystick.Axis[] tAxisList = CEnumUtils.GetValues<CJoystick.Axis>();
            tPos.y = tInfoY;
            tPos.x = 510;
            tPos.width = 300;
            GUI.Label(tPos, "NAME: IS AXIS - IS BUTTON");
            tPos.y += LINE_HEIGHT;

            _scrollPos2 = GUI.BeginScrollView(
                tPos,
                _scrollPos2,
                new Rect(0, 0, tPos.width - 25, LINE_HEIGHT * tAxisList.Length)
            );
            tPos.x = 0;
            tPos.y = 0;
            tPos.height = LINE_HEIGHT;
            for (int i = 0; i < tAxisList.Length; i++)
            {
                CJoystick.Axis tAxis = tAxisList[i];
                bool tIsAxis = tHandler.GetMap().GetAxis(tAxis) > 0;
                bool tIsButton = tHandler.GetButtonKey(tAxis) != KeyCode.None;
                GUI.Label(tPos, tHandler.GetAxisHumanName(tAxis) + ": " + tIsAxis.ToString() + " - " + tIsButton.ToString());
                tPos.y += LINE_HEIGHT;
            }
            GUI.EndScrollView();
        }
        else
        {
            GUI.Label(tPos, "No joystick");
        }

        /*
        _scrollPos = GUI.BeginScrollView(new Rect(10, 300, 100, 100), _scrollPos, new Rect(0, 0, 220, 200));
        GUI.Button(new Rect(0, 0, 100, 20), "Top-left");
        GUI.Button(new Rect(120, 0, 100, 20), "Top-right");
        GUI.Button(new Rect(0, 180, 100, 20), "Bottom-left");
        GUI.Button(new Rect(120, 180, 100, 20), "Bottom-right");
        GUI.EndScrollView();*/
    }

    void DrawSprite(Sprite aSprite, Rect aPos)
    {
        if (aSprite != null)
        {
            Texture t = aSprite.texture;
            Rect tr = aSprite.textureRect;
            Rect r = new Rect(tr.x / t.width, tr.y / t.height, tr.width / t.width, tr.height / t.height);
            GUI.DrawTextureWithTexCoords(aPos, t, r);
        }
    }
}
