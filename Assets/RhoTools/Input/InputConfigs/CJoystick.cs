﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace RhoTools.RhoInput
{
    public class CJoystick
    {
        #region Constants
        public const string AXIS_NONE = "";

        public enum Axis
        {
            BTN1,
            BTN2,
            BTN3,
            BTN4,
            START,
            SELECT,
            L1,
            L2,
            L3,
            R1,
            R2,
            R3,
            LH,
            LV,
            RH,
            RV,
            DPADH,
            DPADV,
            DPAD_UP,
            DPAD_DOWN,
            DPAD_LEFT,
            DPAD_RIGHT,
            AXIS_7,
            AXIS_8,
            AXIS_9,
            AXIS_10,
            AXIS_11,
            AXIS_12,
            AXIS_13,
            AXIS_14,
            AXIS_15,
            AXIS_16,
            AXIS_17,
            AXIS_18,
            AXIS_19,
            AXIS_20,
            AXIS_21,
            AXIS_22,
            AXIS_23,
            AXIS_24,
            AXIS_25,
            AXIS_26,
            AXIS_27,
            AXIS_28,
            BUTTON_10,
            BUTTON_11,
            BUTTON_12,
            BUTTON_13,
            BUTTON_14,
            BUTTON_15,
            BUTTON_16,
            BUTTON_17,
            BUTTON_18,
            BUTTON_19,
            BUTTON_20,
        }

        public static readonly Axis[] BUTTONS =
        {
            Axis.BTN1,
            Axis.BTN2,
            Axis.BTN3,
            Axis.BTN4,
            Axis.START,
            Axis.SELECT,
            Axis.L1,
            Axis.L2,
            Axis.L3,
            Axis.R1,
            Axis.R2,
            Axis.R3,
        };

        public static readonly Axis[] AXES =
        {
            Axis.LH,
            Axis.LV,
            Axis.RH,
            Axis.RV,
            Axis.DPADH,
            Axis.DPADV,
        };

        public static readonly Axis[] HORIZONTAL_AXES =
        {
            Axis.LH,
            Axis.RH,
            Axis.DPADH,
        };

        public static readonly Axis[] VERTICAL_AXES =
        {
            Axis.LV,
            Axis.RV,
            Axis.DPADV,
        };

        public static readonly Axis[] TRIGGERS =
        {
            Axis.L2,
            Axis.R2,
        };

        public static readonly Dictionary<Axis, int> DEFAULT_BUTTONS = new Dictionary<Axis, int>()
        {
            {Axis.BTN1, 0},
            {Axis.BTN2, 1},
            {Axis.BTN3, 2},
            {Axis.BTN4, 3},
            {Axis.DPAD_UP, 4},
            {Axis.DPAD_DOWN, 5},
            {Axis.DPAD_RIGHT, 6},
            {Axis.DPAD_LEFT, 7},
            {Axis.L1, 8},
            {Axis.L2, 9},
            {Axis.R1, 10},
            {Axis.R2, 11},
            {Axis.L3, 12},
            {Axis.R3, 13},
            {Axis.SELECT, 18},
            {Axis.START, 19},
            {Axis.BUTTON_10, 9},
            {Axis.BUTTON_11, 10},
            {Axis.BUTTON_12, 11},
            {Axis.BUTTON_13, 12},
            {Axis.BUTTON_14, 13},
            {Axis.BUTTON_15, 14},
            {Axis.BUTTON_16, 15},
            {Axis.BUTTON_17, 16},
            {Axis.BUTTON_18, 17},
            {Axis.BUTTON_19, 18},
            {Axis.BUTTON_20, 19},
        };

        public static readonly Dictionary<Axis, string> AXIS_NAMES = new Dictionary<Axis, string>()
        {
            {Axis.BTN1, "BUTTON 1"},
            {Axis.BTN2, "BUTTON 2"},
            {Axis.BTN3, "BUTTON 3"},
            {Axis.BTN4, "BUTTON 4"},
            {Axis.DPADH, "DPAD HORIZONTAL"},
            {Axis.DPADV, "DPAD VERTICAL"},
            {Axis.DPAD_UP, "DPAD UP"},
            {Axis.DPAD_DOWN, "DPAD DOWN"},
            {Axis.DPAD_RIGHT, "DPAD RIGHT"},
            {Axis.DPAD_LEFT, "DPAD LEFT"},
            {Axis.L1, "LEFT BUMPER"},
            {Axis.L2, "LEFT TRIGGER"},
            {Axis.R1, "RIGHT BUMPER"},
            {Axis.R2, "RIGHT TRIGGER"},
            {Axis.L3, "LEFT STICK CLICK"},
            {Axis.R3, "RIGHT STICK CLICK"},
            {Axis.LH, "LEFT STICK HORIZONTAL"},
            {Axis.LV, "LEFT STICK VERTICAL"},
            {Axis.RH, "RIGHT STICK HORIZONTAL"},
            {Axis.RV, "RIGHT STICK VERTICAL"},
            {Axis.SELECT, "SELECT"},
            {Axis.START, "START"},
            {Axis.AXIS_7, "AXIS 7"},
            {Axis.AXIS_8, "AXIS 8"},
            {Axis.AXIS_9, "AXIS 9"},
            {Axis.AXIS_10, "AXIS 10"},
            {Axis.AXIS_11, "AXIS 11"},
            {Axis.AXIS_12, "AXIS 12"},
            {Axis.AXIS_13, "AXIS 13"},
            {Axis.AXIS_14, "AXIS 14"},
            {Axis.AXIS_15, "AXIS 15"},
            {Axis.AXIS_16, "AXIS 16"},
            {Axis.AXIS_17, "AXIS 17"},
            {Axis.AXIS_18, "AXIS 18"},
            {Axis.AXIS_19, "AXIS 19"},
            {Axis.AXIS_20, "AXIS 20"},
            {Axis.AXIS_21, "AXIS 21"},
            {Axis.AXIS_22, "AXIS 22"},
            {Axis.AXIS_23, "AXIS 23"},
            {Axis.AXIS_24, "AXIS 24"},
            {Axis.AXIS_25, "AXIS 25"},
            {Axis.AXIS_26, "AXIS 26"},
            {Axis.AXIS_27, "AXIS 27"},
            {Axis.AXIS_28, "AXIS 28"},
            {Axis.BUTTON_10, "BUTTON 10"},
            {Axis.BUTTON_11, "BUTTON 11"},
            {Axis.BUTTON_12, "BUTTON 12"},
            {Axis.BUTTON_13, "BUTTON 13"},
            {Axis.BUTTON_14, "BUTTON 14"},
            {Axis.BUTTON_15, "BUTTON 15"},
            {Axis.BUTTON_16, "BUTTON 16"},
            {Axis.BUTTON_17, "BUTTON 17"},
            {Axis.BUTTON_18, "BUTTON 18"},
            {Axis.BUTTON_19, "BUTTON 19"},
            {Axis.BUTTON_20, "BUTTON 20"},
        };
        #endregion

        #region Joystick buttons
        public static readonly KeyCode[] JOYSTICK_1_BUTTONS =
        {
            KeyCode.Joystick1Button0,
            KeyCode.Joystick1Button1,
            KeyCode.Joystick1Button2,
            KeyCode.Joystick1Button3,
            KeyCode.Joystick1Button4,
            KeyCode.Joystick1Button5,
            KeyCode.Joystick1Button6,
            KeyCode.Joystick1Button7,
            KeyCode.Joystick1Button8,
            KeyCode.Joystick1Button9,
            KeyCode.Joystick1Button10,
            KeyCode.Joystick1Button11,
            KeyCode.Joystick1Button12,
            KeyCode.Joystick1Button13,
            KeyCode.Joystick1Button14,
            KeyCode.Joystick1Button15,
            KeyCode.Joystick1Button16,
            KeyCode.Joystick1Button17,
            KeyCode.Joystick1Button18,
            KeyCode.Joystick1Button19,
        };

        public static readonly KeyCode[] JOYSTICK_2_BUTTONS =
        {
            KeyCode.Joystick2Button0,
            KeyCode.Joystick2Button1,
            KeyCode.Joystick2Button2,
            KeyCode.Joystick2Button3,
            KeyCode.Joystick2Button4,
            KeyCode.Joystick2Button5,
            KeyCode.Joystick2Button6,
            KeyCode.Joystick2Button7,
            KeyCode.Joystick2Button8,
            KeyCode.Joystick2Button9,
            KeyCode.Joystick2Button10,
            KeyCode.Joystick2Button11,
            KeyCode.Joystick2Button12,
            KeyCode.Joystick2Button13,
            KeyCode.Joystick2Button14,
            KeyCode.Joystick2Button15,
            KeyCode.Joystick2Button16,
            KeyCode.Joystick2Button17,
            KeyCode.Joystick2Button18,
            KeyCode.Joystick2Button19,
        };

        public static readonly KeyCode[] JOYSTICK_3_BUTTONS =
        {
            KeyCode.Joystick3Button0,
            KeyCode.Joystick3Button1,
            KeyCode.Joystick3Button2,
            KeyCode.Joystick3Button3,
            KeyCode.Joystick3Button4,
            KeyCode.Joystick3Button5,
            KeyCode.Joystick3Button6,
            KeyCode.Joystick3Button7,
            KeyCode.Joystick3Button8,
            KeyCode.Joystick3Button9,
            KeyCode.Joystick3Button10,
            KeyCode.Joystick3Button11,
            KeyCode.Joystick3Button12,
            KeyCode.Joystick3Button13,
            KeyCode.Joystick3Button14,
            KeyCode.Joystick3Button15,
            KeyCode.Joystick3Button16,
            KeyCode.Joystick3Button17,
            KeyCode.Joystick3Button18,
            KeyCode.Joystick3Button19,
        };

        public static readonly KeyCode[] JOYSTICK_4_BUTTONS =
        {
            KeyCode.Joystick4Button0,
            KeyCode.Joystick4Button1,
            KeyCode.Joystick4Button2,
            KeyCode.Joystick4Button3,
            KeyCode.Joystick4Button4,
            KeyCode.Joystick4Button5,
            KeyCode.Joystick4Button6,
            KeyCode.Joystick4Button7,
            KeyCode.Joystick4Button8,
            KeyCode.Joystick4Button9,
            KeyCode.Joystick4Button10,
            KeyCode.Joystick4Button11,
            KeyCode.Joystick4Button12,
            KeyCode.Joystick4Button13,
            KeyCode.Joystick4Button14,
            KeyCode.Joystick4Button15,
            KeyCode.Joystick4Button16,
            KeyCode.Joystick4Button17,
            KeyCode.Joystick4Button18,
            KeyCode.Joystick4Button19,
        };

        public static readonly KeyCode[] JOYSTICK_5_BUTTONS =
        {
            KeyCode.Joystick5Button0,
            KeyCode.Joystick5Button1,
            KeyCode.Joystick5Button2,
            KeyCode.Joystick5Button3,
            KeyCode.Joystick5Button4,
            KeyCode.Joystick5Button5,
            KeyCode.Joystick5Button6,
            KeyCode.Joystick5Button7,
            KeyCode.Joystick5Button8,
            KeyCode.Joystick5Button9,
            KeyCode.Joystick5Button10,
            KeyCode.Joystick5Button11,
            KeyCode.Joystick5Button12,
            KeyCode.Joystick5Button13,
            KeyCode.Joystick5Button14,
            KeyCode.Joystick5Button15,
            KeyCode.Joystick5Button16,
            KeyCode.Joystick5Button17,
            KeyCode.Joystick5Button18,
            KeyCode.Joystick5Button19,
        };

        public static readonly KeyCode[] JOYSTICK_6_BUTTONS =
        {
            KeyCode.Joystick6Button0,
            KeyCode.Joystick6Button1,
            KeyCode.Joystick6Button2,
            KeyCode.Joystick6Button3,
            KeyCode.Joystick6Button4,
            KeyCode.Joystick6Button5,
            KeyCode.Joystick6Button6,
            KeyCode.Joystick6Button7,
            KeyCode.Joystick6Button8,
            KeyCode.Joystick6Button9,
            KeyCode.Joystick6Button10,
            KeyCode.Joystick6Button11,
            KeyCode.Joystick6Button12,
            KeyCode.Joystick6Button13,
            KeyCode.Joystick6Button14,
            KeyCode.Joystick6Button15,
            KeyCode.Joystick6Button16,
            KeyCode.Joystick6Button17,
            KeyCode.Joystick6Button18,
            KeyCode.Joystick6Button19,
        };

        public static readonly KeyCode[] JOYSTICK_7_BUTTONS =
        {
            KeyCode.Joystick7Button0,
            KeyCode.Joystick7Button1,
            KeyCode.Joystick7Button2,
            KeyCode.Joystick7Button3,
            KeyCode.Joystick7Button4,
            KeyCode.Joystick7Button5,
            KeyCode.Joystick7Button6,
            KeyCode.Joystick7Button7,
            KeyCode.Joystick7Button8,
            KeyCode.Joystick7Button9,
            KeyCode.Joystick7Button10,
            KeyCode.Joystick7Button11,
            KeyCode.Joystick7Button12,
            KeyCode.Joystick7Button13,
            KeyCode.Joystick7Button14,
            KeyCode.Joystick7Button15,
            KeyCode.Joystick7Button16,
            KeyCode.Joystick7Button17,
            KeyCode.Joystick7Button18,
            KeyCode.Joystick7Button19,
        };

        public static readonly KeyCode[] JOYSTICK_8_BUTTONS =
        {
            KeyCode.Joystick8Button0,
            KeyCode.Joystick8Button1,
            KeyCode.Joystick8Button2,
            KeyCode.Joystick8Button3,
            KeyCode.Joystick8Button4,
            KeyCode.Joystick8Button5,
            KeyCode.Joystick8Button6,
            KeyCode.Joystick8Button7,
            KeyCode.Joystick8Button8,
            KeyCode.Joystick8Button9,
            KeyCode.Joystick8Button10,
            KeyCode.Joystick8Button11,
            KeyCode.Joystick8Button12,
            KeyCode.Joystick8Button13,
            KeyCode.Joystick8Button14,
            KeyCode.Joystick8Button15,
            KeyCode.Joystick8Button16,
            KeyCode.Joystick8Button17,
            KeyCode.Joystick8Button18,
            KeyCode.Joystick8Button19,
        };

        public static readonly KeyCode[][] JOYSTICK_BUTTONS =
        {
            JOYSTICK_1_BUTTONS,
            JOYSTICK_2_BUTTONS,
            JOYSTICK_3_BUTTONS,
            JOYSTICK_4_BUTTONS,
            JOYSTICK_5_BUTTONS,
            JOYSTICK_6_BUTTONS,
            JOYSTICK_7_BUTTONS,
            JOYSTICK_8_BUTTONS,
        };
        #endregion

        #region Joystick axes
        public static string[] JOYSTICK_1_AXES =
        {
            "Joystick1Axis1",
            "Joystick1Axis2",
            "Joystick1Axis3",
            "Joystick1Axis4",
            "Joystick1Axis5",
            "Joystick1Axis6",
            "Joystick1Axis7",
            "Joystick1Axis8",
            "Joystick1Axis9",
            "Joystick1Axis10",
            "Joystick1Axis11",
            "Joystick1Axis12",
            "Joystick1Axis13",
            "Joystick1Axis14",
            "Joystick1Axis15",
            "Joystick1Axis16",
            "Joystick1Axis17",
            "Joystick1Axis18",
            "Joystick1Axis19",
            "Joystick1Axis20",
            "Joystick1Axis21",
            "Joystick1Axis22",
            "Joystick1Axis23",
            "Joystick1Axis24",
            "Joystick1Axis25",
            "Joystick1Axis26",
            "Joystick1Axis27",
            "Joystick1Axis28",
        };
        public static string[] JOYSTICK_2_AXES =
        {
            "Joystick2Axis1",
            "Joystick2Axis2",
            "Joystick2Axis3",
            "Joystick2Axis4",
            "Joystick2Axis5",
            "Joystick2Axis6",
            "Joystick2Axis7",
            "Joystick2Axis8",
            "Joystick2Axis9",
            "Joystick2Axis10",
            "Joystick2Axis11",
            "Joystick2Axis12",
            "Joystick2Axis13",
            "Joystick2Axis14",
            "Joystick2Axis15",
            "Joystick2Axis16",
            "Joystick2Axis17",
            "Joystick2Axis18",
            "Joystick2Axis19",
            "Joystick2Axis20",
            "Joystick2Axis21",
            "Joystick2Axis22",
            "Joystick2Axis23",
            "Joystick2Axis24",
            "Joystick2Axis25",
            "Joystick2Axis26",
            "Joystick2Axis27",
            "Joystick2Axis28",
        };
        public static string[] JOYSTICK_3_AXES =
        {
            "Joystick3Axis1",
            "Joystick3Axis2",
            "Joystick3Axis3",
            "Joystick3Axis4",
            "Joystick3Axis5",
            "Joystick3Axis6",
            "Joystick3Axis7",
            "Joystick3Axis8",
            "Joystick3Axis9",
            "Joystick3Axis10",
            "Joystick3Axis11",
            "Joystick3Axis12",
            "Joystick3Axis13",
            "Joystick3Axis14",
            "Joystick3Axis15",
            "Joystick3Axis16",
            "Joystick3Axis17",
            "Joystick3Axis18",
            "Joystick3Axis19",
            "Joystick3Axis20",
            "Joystick3Axis21",
            "Joystick3Axis22",
            "Joystick3Axis23",
            "Joystick3Axis24",
            "Joystick3Axis25",
            "Joystick3Axis26",
            "Joystick3Axis27",
            "Joystick3Axis28",
        };
        public static string[] JOYSTICK_4_AXES =
        {
            "Joystick4Axis1",
            "Joystick4Axis2",
            "Joystick4Axis3",
            "Joystick4Axis4",
            "Joystick4Axis5",
            "Joystick4Axis6",
            "Joystick4Axis7",
            "Joystick4Axis8",
            "Joystick4Axis9",
            "Joystick4Axis10",
            "Joystick4Axis11",
            "Joystick4Axis12",
            "Joystick4Axis13",
            "Joystick4Axis14",
            "Joystick4Axis15",
            "Joystick4Axis16",
            "Joystick4Axis17",
            "Joystick4Axis18",
            "Joystick4Axis19",
            "Joystick4Axis20",
            "Joystick4Axis21",
            "Joystick4Axis22",
            "Joystick4Axis23",
            "Joystick4Axis24",
            "Joystick4Axis25",
            "Joystick4Axis26",
            "Joystick4Axis27",
            "Joystick4Axis28",
        };
        public static string[] JOYSTICK_5_AXES =
        {
            "Joystick5Axis1",
            "Joystick5Axis2",
            "Joystick5Axis3",
            "Joystick5Axis4",
            "Joystick5Axis5",
            "Joystick5Axis6",
            "Joystick5Axis7",
            "Joystick5Axis8",
            "Joystick5Axis9",
            "Joystick5Axis10",
            "Joystick5Axis11",
            "Joystick5Axis12",
            "Joystick5Axis13",
            "Joystick5Axis14",
            "Joystick5Axis15",
            "Joystick5Axis16",
            "Joystick5Axis17",
            "Joystick5Axis18",
            "Joystick5Axis19",
            "Joystick5Axis20",
            "Joystick5Axis21",
            "Joystick5Axis22",
            "Joystick5Axis23",
            "Joystick5Axis24",
            "Joystick5Axis25",
            "Joystick5Axis26",
            "Joystick5Axis27",
            "Joystick5Axis28",
        };
        public static string[] JOYSTICK_6_AXES =
        {
            "Joystick6Axis1",
            "Joystick6Axis2",
            "Joystick6Axis3",
            "Joystick6Axis4",
            "Joystick6Axis5",
            "Joystick6Axis6",
            "Joystick6Axis7",
            "Joystick6Axis8",
            "Joystick6Axis9",
            "Joystick6Axis10",
            "Joystick6Axis11",
            "Joystick6Axis12",
            "Joystick6Axis13",
            "Joystick6Axis14",
            "Joystick6Axis15",
            "Joystick6Axis16",
            "Joystick6Axis17",
            "Joystick6Axis18",
            "Joystick6Axis19",
            "Joystick6Axis20",
            "Joystick6Axis21",
            "Joystick6Axis22",
            "Joystick6Axis23",
            "Joystick6Axis24",
            "Joystick6Axis25",
            "Joystick6Axis26",
            "Joystick6Axis27",
            "Joystick6Axis28",
        };
        public static string[] JOYSTICK_7_AXES =
        {
            "Joystick7Axis1",
            "Joystick7Axis2",
            "Joystick7Axis3",
            "Joystick7Axis4",
            "Joystick7Axis5",
            "Joystick7Axis6",
            "Joystick7Axis7",
            "Joystick7Axis8",
            "Joystick7Axis9",
            "Joystick7Axis10",
            "Joystick7Axis11",
            "Joystick7Axis12",
            "Joystick7Axis13",
            "Joystick7Axis14",
            "Joystick7Axis15",
            "Joystick7Axis16",
            "Joystick7Axis17",
            "Joystick7Axis18",
            "Joystick7Axis19",
            "Joystick7Axis20",
            "Joystick7Axis21",
            "Joystick7Axis22",
            "Joystick7Axis23",
            "Joystick7Axis24",
            "Joystick7Axis25",
            "Joystick7Axis26",
            "Joystick7Axis27",
            "Joystick7Axis28",
        };
        public static string[] JOYSTICK_8_AXES =
        {
            "Joystick8Axis1",
            "Joystick8Axis2",
            "Joystick8Axis3",
            "Joystick8Axis4",
            "Joystick8Axis5",
            "Joystick8Axis6",
            "Joystick8Axis7",
            "Joystick8Axis8",
            "Joystick8Axis9",
            "Joystick8Axis10",
            "Joystick8Axis11",
            "Joystick8Axis12",
            "Joystick8Axis13",
            "Joystick8Axis14",
            "Joystick8Axis15",
            "Joystick8Axis16",
            "Joystick8Axis17",
            "Joystick8Axis18",
            "Joystick8Axis19",
            "Joystick8Axis20",
            "Joystick8Axis21",
            "Joystick8Axis22",
            "Joystick8Axis23",
            "Joystick8Axis24",
            "Joystick8Axis25",
            "Joystick8Axis26",
            "Joystick8Axis27",
            "Joystick8Axis28",
        };
        public static string[][] JOYSTICK_AXES =
        {
            JOYSTICK_1_AXES,
            JOYSTICK_2_AXES,
            JOYSTICK_3_AXES,
            JOYSTICK_4_AXES,
            JOYSTICK_5_AXES,
            JOYSTICK_6_AXES,
            JOYSTICK_7_AXES,
            JOYSTICK_8_AXES,
        };
        #endregion

        public string mapName
        {
            get
            {
                if (_map != null)
                    return _map.mapName;
                return "Joystick not set";
            }
        }

        public string name { get; set; }

        class Key : IEquatable<Key>
        {
            public int key { get; set; }
            public bool state { get; set; }
            public bool previousState { get; set; }

            public override string ToString()
            {
                return "Key: " + key.ToString() + "   State: " + state.ToString();
            }
            public override bool Equals(object obj)
            {
                if (obj == null) return false;
                Key objAsPart = obj as Key;
                if (objAsPart == null) return false;
                else return Equals(objAsPart);
            }
            public override int GetHashCode()
            {
                return key.GetHashCode();
            }
            public bool Equals(Key other)
            {
                if (other == null) return false;
                return (this.key.Equals(other.key));
            }
            // Should also override == and != operators.
        }

        protected CJoystickMap _map;
        protected int _num;
        string[] _axisList;
        KeyCode[] _buttonList;
        List<Key> _triggerDown;
        List<Key> _axisPosDown;
        List<Key> _axisNegDown;

        public virtual void SetMap(CJoystickMap aMap, int aNum)
        {
            _map = aMap;
            _num = aNum;
            _axisList = JOYSTICK_AXES[_num];
            _buttonList = JOYSTICK_BUTTONS[_num];
            _axisPosDown = new List<Key>();
            _axisNegDown = new List<Key>();
            _triggerDown = new List<Key>();
        }

        public virtual void Update()
        {
            UpdateKeys(_axisNegDown);
            UpdateKeys(_axisPosDown);
            UpdateKeys(_triggerDown);
        }

        void UpdateKeys(List<Key> aList)
        {
            for (int i = 0; i < aList.Count; i++)
            {
                Key tKey = aList[i];
                tKey.previousState = tKey.state;
            }
        }

        bool GetKeyDown(List<Key> aList, int aKey, bool aState)
        {
            Key tCurKey;
            if (aList.Exists(x => x.key == aKey))
            {
                tCurKey = aList.Find(x => x.key == aKey);
            }
            else
            {
                tCurKey = new Key();
                tCurKey.key = aKey;
                aList.Add(tCurKey);
            }
            tCurKey.state = aState;
            return tCurKey.state && !tCurKey.previousState;
        }

        public virtual string GetAxisHumanName(Axis aAxis)
        {
            return _map.GetName(aAxis);
        }

        public virtual KeyCode GetButtonKey(Axis aButton)
        {
            int tButton = _map.GetButton(aButton);
            if (tButton >= 0 && tButton < _buttonList.Length)
                return _buttonList[tButton];
            return KeyCode.None;
        }

        public virtual string GetAxisName(Axis aAxis)
        {
            int tAxis = _map.GetAxis(aAxis);
            if (tAxis >= 0 && tAxis < _axisList.Length)
                return _axisList[tAxis];
            return AXIS_NONE;
        }

        public virtual bool GetButton(Axis aButton)
        {
            return Input.GetKey(GetButtonKey(aButton));
        }

        public virtual bool GetButtonDown(Axis aButton)
        {
            return Input.GetKeyDown(GetButtonKey(aButton));
        }

        public virtual bool GetButtonUp(Axis aButton)
        {
            return Input.GetKeyUp(GetButtonKey(aButton));
        }

        public virtual float GetAxis(Axis aAxis)
        {
            string tAxisName = GetAxisName(aAxis);
            if (tAxisName != AXIS_NONE)
                return GetAxisMultiplier(aAxis) * Input.GetAxis(tAxisName);
            return 0;
        }

        public virtual bool GetTrigger(Axis aAxis, float aThreshold = 1)
        {
            return Input.GetAxis(GetAxisName(aAxis)) >= aThreshold;
        }

        public virtual Axis[] GetAxisList()
        {
            return _map.GetAxisList();
        }

        public virtual Axis[] GetButtonsList()
        {
            return _map.GetButtonList();
        }

        public virtual float GetAxisMultiplier(Axis aAxis)
        {
            return _map.GetAxisMultiplier(aAxis);
        }

        public virtual bool GetTriggerDown(Axis aAxis, float aThreshold = 1)
        {
            bool tState = GetTrigger(aAxis, aThreshold);
            return GetKeyDown(_triggerDown, (int)aAxis, tState);
        }

        public virtual bool GetAxisPositiveDown(Axis aAxis, float aThreshold = 0)
        {
            bool tState = GetAxis(aAxis) > aThreshold;
            return GetKeyDown(_axisPosDown, (int)aAxis, tState);
        }

        public virtual bool GetAxisNegativeDown(Axis aAxis, float aThreshold = 0)
        {
            bool tState = GetAxis(aAxis) < -aThreshold;
            return GetKeyDown(_axisNegDown, (int)aAxis, tState);
        }

        public CJoystickMap GetMap()
        {
            return _map;
        }

        public virtual void Vibrate(float aValueLeft, float aValueRight)
        {

        }

        public virtual void VibrateMotor(int aMotor, float aIntensity, float aDecay, float aDelay)
        {

        }

        public virtual void VibrateMotor(int aMotor, CVibration aData)
        {

        }

        public Sprite GetControllerSprite()
        {
            if (_map.sprite != null)
                return _map.sprite;

            return CInputManager.Inst.GetDefaultControllerSprite();
        }

        public Sprite GetSprite(Axis aKey)
        {
            Sprite tSprite = _map.GetSprite(aKey);
            if (tSprite != null)
                return tSprite;

            return CInputManager.Inst.GetDefaultSprite(aKey);
        }
    }
}
