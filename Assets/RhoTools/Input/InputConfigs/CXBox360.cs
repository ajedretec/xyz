﻿#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
using XInputDotNetPure;
#endif

namespace RhoTools.RhoInput
{
    public class CXBox360 : CJoystick
    {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
        PlayerIndex _playerIndex;
#endif

        public void SetPlayerIndex(int aIndex)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            _playerIndex = (PlayerIndex)aIndex;
#endif
        }

        public override bool GetButton(Axis aButton)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                return CXInput.GetButton(_playerIndex, aButton);
#endif
            return base.GetButton(aButton);
        }

        public override bool GetButtonDown(Axis aButton)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                return CXInput.GetButtonDown(_playerIndex, aButton);
#endif
            return base.GetButtonDown(aButton);
        }

        public override bool GetButtonUp(Axis aButton)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                return CXInput.GetButtonUp(_playerIndex, aButton);
#endif
            return base.GetButtonUp(aButton);
        }

        public override float GetAxis(Axis aAxis)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                return CXInput.GetAxis(_playerIndex, aAxis);
#endif
            return base.GetAxis(aAxis);
        }

        public override bool GetTrigger(Axis aAxis, float aThreshold = 1)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                return CXInput.GetTrigger(_playerIndex, aAxis);
#endif
            return base.GetTrigger(aAxis, aThreshold);
        }

        public override void Vibrate(float aValueLeft, float aValueRight)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                CXInput.SetVibration(_playerIndex, aValueLeft, aValueRight);
#endif
        }

        public override void VibrateMotor(int aMotor, float aIntensity, float aDecay, float aDelay)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                CXInput.VibrateMotor(_playerIndex, aMotor, aIntensity, aDecay, aDelay);
#endif
        }

        public override void VibrateMotor(int aMotor, CVibration aData)
        {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
            if (CInputControl.useXInput)
                CXInput.VibrateMotor(_playerIndex, aMotor, aData);
#endif
        }
    }
}
