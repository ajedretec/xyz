﻿using UnityEngine;
using RhoTools;
using RhoTools.RhoInput;
using System.Collections.Generic;
using SimpleJSON;

public class CJoystickMap : ScriptableObject
{
    #region Constants
    const string NAME = "name";
    const string TAG_LIST = "tag_list";
    const string BUTTONS = "buttons";
    const string AXES = "axes";
    const string AXIS_MULTIPLIERS = "axis_multipliers";
    const string NAMES = "names";
    const string INPUT = "input";
    const string BUTTON = "button";
    const string AXIS = "axis";
    const string MULTIPLIER = "multiplier";
    #endregion
    #region Public part
    [System.Serializable]
    public class AxisAssign
    {
        public CJoystick.Axis axis;
        public int button;
    }

    [System.Serializable]
    public class AxisMultiplier
    {
        public CJoystick.Axis axis;
        public float multiplier;
    }

    [System.Serializable]
    public class AxisName
    {
        public CJoystick.Axis axis;
        public string name;
    }

    [System.Serializable]
    public class AxisSprite
    {
        public CJoystick.Axis axis;
        public Sprite sprite;
    }

    public string mapName;
    public AxisAssign[] buttonList;
    public AxisAssign[] axisList;
    public AxisMultiplier[] axisMultiplierList;
    public AxisName[] namesList;
    public AxisSprite[] spritesList;
    public Sprite sprite;
    #endregion

    bool _cached = false;
    public bool cached { get { return _cached; } }
    int[] _buttonsMap;
    int[] _axesMap;
    float[] _axesMultipliers;
    string[] _names;
    Sprite[] _axisSprites;
    CJoystick.Axis[] _buttonList;
    CJoystick.Axis[] _axisList;

    public void CacheData()
    {
        CJoystick.Axis[] tAxes = CEnumUtils.GetValues<CJoystick.Axis>();
        _buttonsMap = new int[tAxes.Length];
        _axesMap = new int[tAxes.Length];
        _axesMultipliers = new float[tAxes.Length];
        _names = new string[tAxes.Length];
        _axisSprites = new Sprite[tAxes.Length];
        List<CJoystick.Axis> tButtonList = new List<CJoystick.Axis>();
        List<CJoystick.Axis> tAxisList = new List<CJoystick.Axis>();

        for (int i = 0; i < tAxes.Length; i++)
        {
            CJoystick.Axis tAxis = tAxes[i];
            int tIndex = (int)tAxis;

            int tButtonValue = GetValue(buttonList, tAxis);
            _buttonsMap[tIndex] = tButtonValue;

            int tAxisValue = GetValue(axisList, tAxis);
            _axesMap[tIndex] = tAxisValue;

            _axesMultipliers[tIndex] = GetValue(axisMultiplierList, tAxis);

            _names[tIndex] = GetValue(namesList, tAxis);

            _axisSprites[tIndex] = GetValue(spritesList, tAxis);

            if (tButtonValue >= 0)
                tButtonList.Add(tAxis);
            if (tAxisValue >= 0)
                tAxisList.Add(tAxis);
        }

        _buttonList = tButtonList.ToArray();
        _axisList = tAxisList.ToArray();
        _cached = true;
    }

    public static int GetValue(AxisAssign[] aList, CJoystick.Axis aKey)
    {
        for (int i = 0; i < aList.Length; i++)
        {
            AxisAssign tAssign = aList[i];
            if (tAssign.axis == aKey)
                return tAssign.button;
        }
        return -1;
    }

    public static float GetValue(AxisMultiplier[] aList, CJoystick.Axis aKey)
    {
        for (int i = 0; i < aList.Length; i++)
        {
            AxisMultiplier tMul = aList[i];
            if (tMul.axis == aKey)
                return tMul.multiplier;
        }
        return 1;
    }

    public static string GetValue(AxisName[] aList, CJoystick.Axis aKey)
    {
        for (int i = 0; i < aList.Length; i++)
        {
            AxisName tMul = aList[i];
            if (tMul.axis == aKey)
                return tMul.name;
        }
        return "";
    }

    public static Sprite GetValue(AxisSprite[] aList, CJoystick.Axis aKey)
    {
        for (int i = 0; i < aList.Length; i++)
        {
            AxisSprite tSprite = aList[i];
            if (tSprite.axis == aKey)
                return tSprite.sprite;
        }
        return null;
    }

    public CJoystick.Axis[] GetAxisList()
    {
        return _axisList;
    }

    public CJoystick.Axis[] GetButtonList()
    {
        return _buttonList;
    }

    public int GetButton(CJoystick.Axis aButton)
    {
        return _buttonsMap[(int)aButton];
    }

    public int GetAxis(CJoystick.Axis aAxis)
    {
        return _axesMap[(int)aAxis];
    }

    public float GetAxisMultiplier(CJoystick.Axis aAxis)
    {
        return _axesMultipliers[(int)aAxis];
    }

    public string GetName(CJoystick.Axis aAxis)
    {
        return _names[(int)aAxis];
    }

    public Sprite GetSprite(CJoystick.Axis aAxis)
    {
        return _axisSprites[(int)aAxis];
    }

    public void PrintMap()
    {
        Debug.Log(mapName);
        Debug.Log("    Axes:");
        for (int i = 0; i < _axesMap.Length; i++)
        {
            Debug.Log("    " + (CJoystick.Axis)i + ": " + _axesMap[i]);
        }
        Debug.Log("    Buttons:");
        for (int i = 0; i < _buttonsMap.Length; i++)
        {
            Debug.Log("    " + (CJoystick.Axis)i + ": " + _buttonsMap[i]);
        }
    }

    public void SetChanged()
    {
        _cached = false;
    }

    public void SaveAsJSON(string aDir)
    {
        JSONNode tData = JSON.Parse("{}");
        tData[NAME] = mapName;
        tData[TAG_LIST] = new JSONArray();
        tData[BUTTONS] = new JSONArray();
        tData[AXES] = new JSONArray();
        tData[AXIS_MULTIPLIERS] = new JSONArray();
        tData[NAMES] = new JSONArray();

        JSONNode tButtonsNode = tData[BUTTONS];
        for (int i = 0; i < buttonList.Length; i++)
        {
            AxisAssign tAssign = buttonList[i];
            tButtonsNode[i][INPUT] = tAssign.axis.ToString();
            tButtonsNode[i][BUTTON].AsInt = tAssign.button;
        }

        JSONNode tAxesNode = tData[AXES];
        for (int i = 0; i < axisList.Length; i++)
        {
            AxisAssign tAssign = axisList[i];
            tAxesNode[i][INPUT] = tAssign.axis.ToString();
            tAxesNode[i][AXIS].AsInt = tAssign.button;
        }

        JSONNode tAxisMultipliers = tData[AXIS_MULTIPLIERS];
        for (int i = 0; i < axisMultiplierList.Length; i++)
        {
            AxisMultiplier tMul = axisMultiplierList[i];
            tAxisMultipliers[i][INPUT] = tMul.axis.ToString();
            tAxisMultipliers[i][MULTIPLIER].AsFloat = tMul.multiplier;
        }

        JSONNode tNames = tData[NAMES];
        for (int i = 0; i < namesList.Length; i++)
        {
            AxisName tName = namesList[i];
            tNames[i][INPUT] = tName.axis.ToString();
            tNames[i][NAME] = tName.name;
        }

        string tJsonString = CPrettyJSON.FormatJson(tData.ToString());
        CTxtManager.Write(aDir, tJsonString);
    }

    public void LoadJSON(string aDir)
    {
        JSONNode tData = CJSonReader.Load(aDir);
        if (tData != null)
        {
            string tName = tData[NAME];
            JSONArray tButtonsNode = tData[BUTTONS].AsArray;
            JSONArray tAxesNode = tData[AXES].AsArray;
            JSONArray tAxisMultipliersNode = tData[AXIS_MULTIPLIERS].AsArray;
            JSONArray tNamesNode = tData[NAMES].AsArray;

            List<AxisAssign> tButtons = new List<AxisAssign>();
            for (int i = 0; i < tButtonsNode.Count; i++)
            {
                AxisAssign tButton = new AxisAssign();
                JSONNode tNode = tButtonsNode[i];
                if (CEnumUtils.TryParse<CJoystick.Axis>(tNode[INPUT], out tButton.axis))
                {
                    tButton.button = tNode[BUTTON].AsInt;
                    tButtons.Add(tButton);
                }
            }

            List<AxisAssign> tAxes = new List<AxisAssign>();
            for (int i = 0; i < tAxesNode.Count; i++)
            {
                AxisAssign tAxis = new AxisAssign();
                JSONNode tNode = tAxesNode[i];
                if (CEnumUtils.TryParse<CJoystick.Axis>(tNode[INPUT], out tAxis.axis))
                {
                    tAxis.button = tNode[AXIS].AsInt;
                    tAxes.Add(tAxis);
                }
            }

            List<AxisMultiplier> tMultipliers = new List<AxisMultiplier>();
            for (int i = 0; i < tAxisMultipliersNode.Count; i++)
            {
                AxisMultiplier tAxisMul = new AxisMultiplier();
                JSONNode tNode = tAxisMultipliersNode[i];
                if (CEnumUtils.TryParse<CJoystick.Axis>(tNode[INPUT], out tAxisMul.axis))
                {
                    tAxisMul.multiplier = tNode[MULTIPLIER].AsInt;
                    tMultipliers.Add(tAxisMul);
                }
            }

            List<AxisName> tNames = new List<AxisName>();
            for (int i = 0; i < tNamesNode.Count; i++)
            {
                AxisName tAxisName = new AxisName();
                JSONNode tNode = tNamesNode[i];
                if (CEnumUtils.TryParse<CJoystick.Axis>(tNode[INPUT], out tAxisName.axis))
                {
                    tAxisName.name = tNode[NAME];
                    tNames.Add(tAxisName);
                }
            }

            if (tName != null)
            {
                axisList = tAxes.ToArray();
                buttonList = tButtons.ToArray();
                axisMultiplierList = tMultipliers.ToArray();
                namesList = tNames.ToArray();
                CacheData();
            }
        }
    }
}
