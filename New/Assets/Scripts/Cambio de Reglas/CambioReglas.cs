﻿using UnityEngine;
using System.Collections;




public class CambioReglas : MonoBehaviour {

    
	// Use this for initialization
	private int rand=0;
	private float t = 0.0f; 
	private SpriteRenderer sr;
	private float x;
	private float y;
	private float y2;
	private int z;
	private Turnos tn;
    public Font KGCorneroftheSky;


    void Start () {

		rand = Random.Range (1, 9);
		t = Random.Range (30,50);
		sr = this.GetComponent<SpriteRenderer> ();
		x = Screen.width/3.0f;
		y= Screen.height/3;
		y2= y/2;
		z = Random.Range (3,9);
		tn = GameObject.Find ("pFondo").GetComponent<Turnos>();

        x = x * 0.9f;

       //KGCorneroftheSky = Instantiate(Resources.FindObjectsOfTypeAll(typeof(Font))[0]) as Font;


    }
	
	// Update is called once per frame
	void Update () {

		t -= Time.deltaTime;

		z = Random.Range (3,9);
		if(t.CompareTo(z)==-1){

			int i = (int) t;

			if ((i%2).Equals(0)){
				sr.color = Color.blue;
			}
			else {

				sr.color = Color.red;
			}

		}

		if(t.CompareTo(0)==-1){
			rand = Random.Range (1, 10);
			t = Random.Range (10,15);// cambiarlo a 10,15
			sr.color = Color.white;
		}

		print (t);
	}

	void OnGUI() {

      
        Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        GUIStyle myStyle = new GUIStyle();
        myStyle.font= myFont;
        myStyle.fontSize = 24;

        GUIStyle myStyle2 = new GUIStyle();
        myStyle2.font = myFont;
        myStyle2.fontSize = 56;
        


        GUI.color = Color.black; 
        
		GUI.skin.label.fontSize = 32;

		switch(rand){

		case 1: GUI.Label (new Rect (x, y2, 180, 200)," CAMBIO DE COLOR", myStyle2);
				GUI.skin.label.fontSize = 14;
              
                GUI.Label (new Rect (x, y, 180, 200), "Se da vuelta el tablero y mueve el color  \n de quien era el turno.", myStyle);

			break;
		case 2: GUI.Label (new Rect (x, y2, 180, 200),"   TORNADO", myStyle2);
		    	GUI.skin.label.fontSize = 14;
				GUI.Label (new Rect (x, y, 200, 200), "Todos los jugadores cambian hacia la \n silla de la derecha.", myStyle);
			break;
		case 3: GUI.Label (new Rect (x, y2, 200, 200),"  PASAPIEZAS", myStyle2);
				GUI.skin.label.fontSize = 14;
			GUI.Label (new Rect (x, y, 200, 200), "Todas las piezas que habíamos atrapado con \n anterioridad, pueden ser pasadas sin más \n al compañero de al lado.", myStyle);
			break;
		case 4: GUI.Label (new Rect (x, y2, 180, 200),"    MUTANTE", myStyle2);
				GUI.skin.label.fontSize = 14;
			    GUI.Label (new Rect (x, y, 180, 200), "De aquí en adelante, las piezas \n mueven de acuerdo  a la columna que ocupen. \n" +
                "Si un caballo está en la columna \n del alfil (“c” o “f”),  pues mueve \n como alfil," +
                "pero si desde ella se desplaza \n hasta  la columna “h”, inmediatamente \n pasa a ser torre…", myStyle);
			break;
		case 5: GUI.Label (new Rect (x, y2, 200, 200),"   COME COME", myStyle2);
				GUI.skin.label.fontSize = 14;
			    GUI.Label (new Rect (x, y, 180, 200), "La partida ahora cambia de objetivo, \n y habrá que entregar todas las piezas, \n "
                    + "excepto el rey, ya que si tenemos \n nuevo cambio de " +
                       "reglas y hemos \n entregado el rey, no podremos \n seguir en las nuevas condiciones.", myStyle);
			break;
		case 6: GUI.Label (new Rect (x, y2, 200, 200), "  MARSELLÉS", myStyle2);
				GUI.skin.label.fontSize = 14;
			GUI.Label (new Rect (x, y, 180, 200),"Cada jugador mueve dos veces por turno.", myStyle);
			break;
		case 7: GUI.Label (new Rect (x, y2, 200, 200)," AGREGAR PUNTOS", myStyle2);
				GUI.skin.label.fontSize = 14;
			GUI.Label (new Rect (x, y, 180, 200), "Se tira un dado y el número que éste \n indique determinará cuantos “puntos” \n" +
                "agrega cada jugador a su ejército, \n en la casilla que desee,  \n" + 
                 "y antes de retomar las acciones.", myStyle);
			break;
		case 8: GUI.Label (new Rect (x, y2, 200, 200),"   PROGRESIVO", myStyle2);
				GUI.skin.label.fontSize = 14;
			GUI.Label (new Rect (x, y, 200, 200), "De aquí en adelante, a quien corresponde \n mover arranca  con dos jugadas," +
                "su oponente con tres, y así \n sucesivamente", myStyle);
			break;
		case 9: //tn.setTipoCarta ("");
				tn.setCambioR(true);
				Application.LoadLevel ("Cartas");
			break;

		}
		
	}
}
