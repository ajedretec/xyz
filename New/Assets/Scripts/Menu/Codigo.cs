﻿using UnityEngine;
using System.Collections;

public class Codigo : MonoBehaviour
{


    private GameObject[] myObj = new GameObject[11];
    static private bool reloj = false;
    static private bool cartas = false;
    static private bool poder = false;
    static private bool tatedrez = false;
    static private bool neutral = false;
    static private bool trivia = false;
    static private bool puzzle = false;
    static private bool objetivos = false;
    static private bool cartasmaspoder = false;
    static private bool ajedrez = false;
    static private bool cambio = false;
    

    void Start()
    {
        var input = gameObject.GetComponent<UnityEngine.UI.InputField>();
        var se = new UnityEngine.UI.InputField.SubmitEvent();
        

        se.AddListener(SubmitName);
        input.onEndEdit = se;

        //la vuelta atrás no me debería desactivar
        
            myObj[0] = GameObject.Find("pBreloj"); //podría activarlo, ya que guardé la variable
            myObj[0].SetActive(reloj);
            myObj[1] = GameObject.Find("pAjedrezCartas");
            myObj[1].SetActive(cartas);
            myObj[2] = GameObject.Find("pCartasPoder");
            myObj[2].SetActive(poder);
            myObj[3] = GameObject.Find("pbTatedrez");
            myObj[3].SetActive(tatedrez);
            myObj[4] = GameObject.Find("pDamaNeutral");
            myObj[4].SetActive(neutral);
            myObj[5] = GameObject.Find("pbTrivia");
            myObj[5].SetActive(trivia);
            myObj[6] = GameObject.Find("p_AjedrezPuzzle");
            myObj[6].SetActive(puzzle);
            myObj[7] = GameObject.Find("pbObjS");
            myObj[7].SetActive(objetivos);
            myObj[8] = GameObject.Find("pCartasmasPoder");
            myObj[8].SetActive(cartasmaspoder);
            myObj[9] = GameObject.Find("pBAjedrez");
            myObj[9].SetActive(ajedrez);
            myObj[10] = GameObject.Find("pbCambioR");
            myObj[10].SetActive(cambio);
        
    }

    private void SubmitName(string arg0)
    {
        //Debug.Log(arg0);
        print(arg0);// de donde sale arg0?? PUEDE Y DEBE SER UN CASE!
        
        if (arg0.Equals("reloj"))
        {
            activar(0);
            reloj = true;
        }
        else
        if (arg0.Equals("cartas"))
        {
            activar(1);
            cartas = true;
        }
        else
        if (arg0.Equals("poder"))
        {
            activar(2);
            poder = true;
        }
        else
        if (arg0.Equals("tatedrez"))
        {
            activar(3);
            tatedrez = true;
        }
        else
        if (arg0.Equals("neutral"))
        {
            activar(4);
            neutral = true;
        }
        else
        if (arg0.Equals("trivia"))
        {
            activar(5);
            trivia = true;
        }
        else
        if (arg0.Equals("puzzle"))
        {
            activar(6);
            puzzle = true;
        }
        else
        if (arg0.Equals("objetivos"))
        {
            activar(7);
            objetivos = true;
        }
        else
        if (arg0.Equals("cartasmaspoder"))
        {
            activar(8);
            cartasmaspoder = true;
        }
        else
        if (arg0.Equals("ajedrez"))
        {
            activar(9);
            ajedrez = true;
        }
        else
        if (arg0.Equals("cambio"))
        {
            activar(10);
            cambio = true;
        }

    }

    public void activar(int i)
    {

        myObj[i].SetActive(true);

    }
}
