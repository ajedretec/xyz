﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Back : MonoBehaviour {
	Turnos tn;

	// Use this for initialization
	void Start () {

        tn = GameObject.Find("pFondo").GetComponent<Turnos>();
    }
	
	// Update is called once per frame
	void Update () {

	}

	void OnMouseDown(){

        //pBack Tambien sirve para configurar el Reloj

		if (this.gameObject.name.Equals ("pBack")) {

			tn.setTurno("inicio");            
            SceneManager.LoadScene("Menu");
            	
		}
		else
		if (this.gameObject.name.Equals ("pStop")) {
			print ("Stop");
			tn.setTurno("stop");
		}
		else
		if (this.gameObject.name.Equals ("pConf")) {
			tn.setTurno("stop");
            SceneManager.LoadScene("RelojConf");
        }
        else
        if (this.gameObject.name.Equals("pBack1"))
        {

            tn.setTurno("inicio");
            SceneManager.LoadScene("Menu 1");

        }
        else
        if (this.gameObject.name.Equals("pBack2"))
        {

            tn.setTurno("inicio");
            SceneManager.LoadScene("Menu 2");

        }

    }


}
