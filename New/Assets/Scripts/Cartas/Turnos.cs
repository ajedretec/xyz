﻿using UnityEngine;
using System.Collections;

public class Turnos : MonoBehaviour {


	string der="pMazo2";
	string izq="pMazo";
	static string TipoCarta="cartas";
	static string turno = "inicio";
	static int m1=1;
	static int m2=1;
	static int inc1=0;
	static int inc2=0;
	static bool c50 = true;
	static bool next=true;
	static bool masT=true;
	static bool mas1= false;
	static bool otravez= true;
	static bool cambioR= false;
	static bool blancas = true;
    private GameObject[] myObj = new GameObject[11];


    // Use this for initialization
    void Start () {

       

       // myObj[0] = GameObject.Find("pBreloj"); //podría activarlo, ya que guardé la variable
        //myObj[0].SetActive(false);
        /*myObj[1] = GameObject.Find("pAjedrezCartas");
        myObj[1].SetActive(false);
        myObj[2] = GameObject.Find("pCartasPoder");
        myObj[2].SetActive(false);
        myObj[3] = GameObject.Find("pbTatedrez");
        myObj[3].SetActive(false);
        myObj[4] = GameObject.Find("pDamaNeutral");
        myObj[4].SetActive(false);
        myObj[5] = GameObject.Find("pbTrivia");
        myObj[5].SetActive(false);
        myObj[6] = GameObject.Find("p_AjedrezPuzzle");
        myObj[6].SetActive(false);
        myObj[7] = GameObject.Find("pbObjS");
        myObj[7].SetActive(false);
        myObj[8] = GameObject.Find("pCartasmasPoder");
        myObj[8].SetActive(false);
        myObj[9] = GameObject.Find("pBAjedrez");
        myObj[9].SetActive(false);
        myObj[10] = GameObject.Find("pbCambioR");
        myObj[10].SetActive(false); */
    }

 

    public void setIzq(string nuevo_izq){
		izq = nuevo_izq;
	}

	public void setDer(string nuevo_der){
		der = nuevo_der;
	}

	public string getIzq(){
		return izq;
	}

	public string getDer(){
		return der;
	}

	public void setTipoCarta(string nuevo_tipoCarta){
		TipoCarta = nuevo_tipoCarta;
	}
	
	public string geTipoCarta(){
		return TipoCarta;
	}

	public void setTurno(string nuevo_turno){
		turno = nuevo_turno;
	}
	
	public string getTurno(){
		return turno;
	}
	public int getM1(){
		return m1;
	}

	public void setM1(int m){
		m1=m;
	}
	public int getM2(){
		return m2;
	}
	
	public void setM2(int m){
		m2=m;
	}

	public int getInc1(){
		return inc1;
	}
	
	public void setInc1(int i){
		inc1=i;
	}

	public int getInc2(){
		return inc2;
	}
	
	public void setInc2(int i){
		inc2=i;
	}

	public bool getc50(){
		return c50;
	}
	
	public void setc50(bool c){
		c50=c;
	}
	public bool getnext(){
		return next;
	}
	
	public void setnext(bool n){
		next= n;
	}
	public bool getmasT(){
		return masT;
	}
	
	public void setmasT(bool m){
		masT= m;
	}

	public bool getmas1(){
		return mas1;
	}
	
	public void setmas1(bool m){
		mas1= m;
	}
	public bool getotravez(){
		return otravez;
	}
	
	public void setotravez(bool o){
		otravez= o;
	}

	public bool getCambioR(){
		return cambioR;
	}
	
	public void setCambioR(bool c){
		cambioR= c;
	}

	public bool getBlancas(){
		return blancas;
	}
	
	public void setBlancas(bool c){
		blancas= c;
	}

 

    //arreglarlo más adelante
    public int getCounter(){
		return 0;
	}
	
	// Update is called once per frame
	void Update () {
//		print (turno);
	}
}
