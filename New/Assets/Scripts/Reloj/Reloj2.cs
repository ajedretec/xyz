﻿using UnityEngine;
using System.Collections;

public class Reloj2 : MonoBehaviour {

	private float Timer2 = 0.0f;
	private bool aux=true;
	private SpriteRenderer sr;
	private SpriteRenderer sr2;
	private SpriteRenderer sr3;
	private bool aux2 = false;
	private bool aux3 = false;
	private bool aux4 = false;
	private int minutes2;
	private int seconds2;
	private string niceTime2;
	private GameObject myObj;
	private GameObject actual;
	private GameObject myObj2;
	private GameObject myObj3;
	private GameObject fondo; 
	private Turnos tn2;
	private string der;
	private string izq;
	private bool derb;
	private string d = "der";
	private string i = "izq";
	private string turno;
	static Reloj r;
	private int inc2=0;
	private string t2;
	private float x;
	private float y;
    private Font myFont;

	void Start () {
		myObj2 = GameObject.Find ("pReloj1 (1)");		
		sr2 = myObj2.GetComponent<SpriteRenderer> ();
		sr2.color = Color.gray;
		tn2 = GameObject.Find ("pFondo").GetComponent<Turnos> ();
		minutes2 = tn2.getM2 ();
		inc2 = tn2.getInc2 ();
		x= (float)Screen.width/1.65f;
		y= (float)Screen.height/2.4f;

        myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
    }	

	
	void Update (){


		t2 = tn2.getTurno ();
		if ((t2.Equals ("stop"))) {
			aux4=false;
			myObj2 = GameObject.Find ("pReloj1 (1)");	
			sr2 = myObj2.GetComponent<SpriteRenderer> ();
			sr2.color = Color.gray;
		}

			if (aux4) {
				Timer2 -= Time.deltaTime;
			}

	}
	
	void OnGUI() {
		
		GUI.color = Color.black; 
		GUI.skin.label.fontSize = 100;
		seconds2 = Mathf.FloorToInt(Timer2);
		tn2 = GameObject.Find ("pFondo").GetComponent<Turnos> ();


       // Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));

        

        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 116;

        if ((minutes2.Equals (0))&&(seconds2.Equals(-1))){
			aux = false;
		}
		
		if ((seconds2.Equals (-1)) && !(minutes2.Equals (0))) {
			Timer2 = Timer2 + 60;
			minutes2 = minutes2 - 1;
		} 

		if (aux) {
			niceTime2 = string.Format ("{0:0}:{1:00}", minutes2, seconds2);
		} else {
			niceTime2 = string.Format ("{0:0}:{1:00}", 0, 0);

			tn2.setTurno("fin");

			myObj2 = GameObject.Find ("pReloj1 (1)");	
			sr2 = myObj2.GetComponent<SpriteRenderer> ();

		}


		//turno = tn.getTurno ();
		GUI.Label (new Rect (x, y, 250, 100), niceTime2, myStyle);
	}


	public void setReloj2(bool r){
		aux4 = true;
		tn2 = GameObject.Find ("pFondo").GetComponent<Turnos> ();
		tn2.setTurno("der");
	}

	void OnMouseDown(){


		tn2 = GameObject.Find ("pFondo").GetComponent<Turnos> ();
		turno = tn2.getTurno ();
		Vector3 mousePosition = Camera.main.ScreenToWorldPoint (Input.mousePosition);
		mousePosition.z = 0;
		if ((mousePosition.x > 0)) {
		if (!(turno.Equals ("fin"))) {

			if ((turno.Equals ("der"))||(turno.Equals ("inicio"))||(turno.Equals ("stop"))) {

				aux4 = false;
				myObj2 = GameObject.Find ("pReloj1 (1)");	
				sr2 = myObj2.GetComponent<SpriteRenderer> ();
				sr2.color = Color.gray;
				myObj3 = GameObject.Find ("pReloj1");
				r = myObj3.GetComponent<Reloj> ();
				Timer2 = Timer2 + inc2;	
				sr3 = myObj3.GetComponent<SpriteRenderer> ();
				sr3.color = Color.white;


				if (Timer2 > 59) {
					minutes2= minutes2+1;
					Timer2=Timer2-60;
				}
					r.setReloj (true);

				}   
 			}
		}
	}
}