﻿using UnityEngine;
using System.Collections;

public class Respuesta : MonoBehaviour {
	private Trivia t;
	private SpriteRenderer sr;
	private bool otravez= false;
	private string click;
	private Turnos tn;
	private bool activo;

	void Start () {

		t = GameObject.Find ("pCambioR").GetComponent<Trivia> ();	
		tn = GameObject.Find ("pFondo").GetComponent<Turnos>();

	}

	void Update () {

		otravez = tn.getmas1();	
		print (activo);
	}

	void OnMouseDown(){

		sr= this.GetComponent<SpriteRenderer>();
		click = this.gameObject.name;

		if (t.getActivo()){
			if (!(otravez)){

				if (this.gameObject.name.Equals (t.getR())){
					t.respuesta(1);
					sr.color= Color.green;
					tn.setmas1(false);
		//			activo=false;
					
				}
				else{
					t.respuesta(0);
					sr.color= Color.red;
					tn.setmas1(false);
		//			activo=false;
	    		}
			}
			else{
				if (this.gameObject.name.Equals (t.getR())){// la respuesta es correcta con otra chance
					t.respuesta(1);
					sr.color= Color.green;
					tn.setmas1(false);
		//			activo = false;
				}
				else{
						t.respuesta(2);
						sr.color= Color.blue;
						t.apagarBoton(click);
						tn.setmas1(false);
				}
			}
		}
	}
}
