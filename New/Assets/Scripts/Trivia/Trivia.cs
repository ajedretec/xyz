﻿using UnityEngine;
using System.Collections;

public class Trivia : MonoBehaviour {

	private string[] p = new string[4];
	private string[,] r = new string[4,5];
	private float Timer = 20.0f;
	private float Timer2 = 2.0f;
	int i;
	static int j=0;
	float x,y;
	static int minutes;
	static int seconds;
	private string niceTime;
	private SpriteRenderer sr;
	static bool aux, correct, incorrect, aux2, a, b, c, d, box, otravez;


	void Start () {

		box = true;
		a = true;
		b = true;
		c = true;
		d = true;
		otravez= false;
		aux= true;
		aux2=false;
		correct= false;
		incorrect= false;
		i=Random.Range(0,4);

		p[0]="Cuantas casillas tiene el tablero de ajedrez?";
		r[0,0]="64";
		r[0,1]="48";
		r[0,2]="68";
		r[0,3]="7";
		r[0,4]="a";
/*****************************************************************/
		p[1]="Cual es la suma de: 1 peon + 2 caballos + 1 alfil";
		r[1,0]="10";
		r[1,1]="6";
		r[1,2]="11";
		r[1,3]="5";
		r[1,4]="a";
/*****************************************************************/
		p[2]="Cual pieza se mueve como una L?";
		r[2,0]="Rey";
		r[2,1]="Torre";
		r[2,2]="Alfil";
		r[2,3]="Caballo";
		r[2,4]="d";
/*****************************************************************/
		p[3]="Cuantas filas tiene el tablero de ajedrez?";
		r[3,0]="64";
		r[3,1]="8";
		r[3,2]="12";
		r[3,3]="4";
		r[3,4]="b";
/*****************************************************************/	
	}


	void Update () {

		if (aux){
			Timer -= Time.deltaTime; 
		}

		if (aux2){
			Timer2-=Time.deltaTime;
			if (Timer2.CompareTo(0).Equals(-1)){
				j++;
				Application.LoadLevel ("Trivia");
			}
		}

		if (Timer.CompareTo(0).Equals(-1)){
			incorrect=true;
			aux2=true;
			aux=false;
			sr = this.gameObject.GetComponent<SpriteRenderer>();
			sr.color = Color.red;
		}
	}

	void OnGUI() {
		x = Screen.width/5f;
		y= Screen.height/4f;
		seconds =  Mathf.FloorToInt(Timer);
		GUI.color = Color.blue; 
		GUI.skin.label.fontSize = 35;	
		GUI.Label (new Rect (x*4.4f, y*0.15f, 300, 300),"#" + j.ToString());
		GUI.skin.label.fontSize = 22;
		GUI.color = Color.black;


        Font myFont = (Font)Resources.Load("K26CasualComics", typeof(Font));
        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 20;



        if (box){
			GUI.Label (new Rect (x, y*0.90f, 300, 300),p[i], myStyle);
		}
		else{
			GUI.Label (new Rect (x, y*0.90f, 300, 300),"Cambiando pregunta...");
		}
		if (d){
			GUI.Label (new Rect (x*3.2f, y*3, 300, 300),r[i,3]);
		}
		if (c){
			GUI.Label (new Rect (x*1.45f, y*3, 300, 300),r[i,2]);
		}
		if (b){
			GUI.Label (new Rect (x*3.2f, y*2.3f, 300, 300),r[i,1]);//b
		}
		if (a){
			GUI.Label (new Rect (x*1.45f, y*2.3f, 300, 300),r[i,0]); // a
	    }
		GUI.skin.label.fontSize = 40;

		if (aux){
			niceTime = string.Format ("{0:0}:{1:00}", minutes, seconds);
		}

		GUI.Label (new Rect (x*2, y/4f, 250, 100), niceTime);

		if (correct){
			otravez=false;
			GUI.skin.label.fontSize = 30;
			GUI.color= Color.green;	
			GUI.Label (new Rect (x*1.5f, y*1.7f, 300, 300),"CORRECTO");
		}

		if (incorrect){
			otravez=false;
			GUI.skin.label.fontSize = 30;
			GUI.color= Color.red;	
			GUI.Label (new Rect (x*1.4f, y*1.7f, 300, 300),"INCORRECTO");

		}

		if (otravez){
			GUI.skin.label.fontSize = 30;
			GUI.color= Color.blue;	
			GUI.Label (new Rect (x*1.4f, y*1.7f, 300, 300),"OTRA VEZ!");
			
		}
	}


	public string getR(){
		return r[i,4];
	} 

	public void upJ(){
		j++;
	} 

	public void respuesta(int r){

		if (aux){

			if (r.Equals(1)){//la respuesta es correcta
				aux = false;
				correct = true;
				sr = this.gameObject.GetComponent<SpriteRenderer>();
				sr.color = Color.green;
				aux2= true;
			}
			else
				if (r.Equals(0)){
					aux= false;
					incorrect= true;
					sr = this.gameObject.GetComponent<SpriteRenderer>();
					sr.color = Color.red;
					aux2= true;

		    	}
				else
					if (r.Equals(2)){
						otravez= true; 
						sr = this.gameObject.GetComponent<SpriteRenderer>();
						sr.color = Color.red;
			}
		}
	}

	public void apagarBoton(string s){
		if (s.Equals("a"))
			a= false;
		if (s.Equals("b"))
			b= false;
		if (s.Equals("c"))
			c= false;
		if (s.Equals("d"))
			d= false;
	}
	public void Reanudar(){
		aux2=true;
		aux=false;
		box= false;
	}

	public void masT(){
		Timer= Timer + 10;
	}

	public void mas1(){
		otravez= true;
	}

	public bool getActivo(){
		return aux;
	}
	//void OnMouseOver(){

	//}
}
