﻿using UnityEngine;
using System.Collections;

using UnityEngine;
using System.Collections;

public class caballo_t : MonoBehaviour
{
  

    private string[,] board = new string[7, 7];
    private string[,] movPos = new string[7, 7];
    private string casilla = "null";
    private int i, j;
    private bool term = false;
    private string color = "";
    private bool empate = false;
    static bool neutral_n = false;
    static bool neutral_b = false;
    private bool ganan_blancas = false;
    private bool ganan_negras = false;
    private string juego;
    private int p_blancas = 0;
    private int p_negras = 0;
    private Font myFont;



    private Cas[] lista = new Cas[37];//hay casillas de más
    private int tope = 0;


    void Start()
    {
        myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        Turnos2 tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
        tn2.setJuego("c"); //Setea el Juego

        for (int i = 0; i < 7; i++)
        {
            for (int j = 0; j < 7; j++)
            {
                board[i, j] = "c"+i.ToString()+j.ToString();
            }
        }

        board[2, 1] = "wk1";
        board[5, 6] = "bk2";

       
        juego = tn2.getJuego();
    }

    void Update()
    {


    }

    public void insertar(string p)
    {

        print(p);
        switch (casilla)
        {
            case "a1":
                board[1, 1] = p;
                i = 1;
                j = 1;
                break;
            case "a2":
                board[1, 2] = p;
                i = 1;
                j = 2;
                break;
            case "a3":
                board[1, 3] = p;
                i = 1;
                j = 3;
                break;
            case "a4":
                board[1, 4] = p;
                i = 1;
                j = 4;
                break;
            case "a5":
                board[1, 5] = p;
                i = 1;
                j = 5;
                break;
            case "a6":
                board[1, 6] = p;
                i = 1;
                j = 6;
                break;
            case "b1":
                board[2, 1] = p;
                i = 2;
                j = 1;
                break;
            case "b2":
                board[2, 2] = p;
                i = 2;
                j = 2;
                break;
            case "b3":
                board[2, 3] = p;
                i = 2;
                j = 3;
                break;
            case "b4":
                board[2, 4] = p;
                i = 2;
                j = 4;
                break;
            case "b5":
                board[2, 5] = p;
                i = 2;
                j = 5;
                break;
            case "b6":
                board[2, 6] = p;
                i = 2;
                j = 6;
                break;
            case "c1":
                board[3, 1] = p;
                i = 3;
                j = 1;
                break;
            case "c2":
                board[3, 2] = p;
                i = 3;
                j = 2;
                break;
            case "c3":
                board[3, 3] = p;
                i = 3;
                j = 3;
                break;
            case "c4":
                board[3, 4] = p;
                i = 3;
                j = 4;
                break;
            case "c5":
                board[3, 5] = p;
                i = 3;
                j = 5;
                break;
            case "c6":
                board[3, 6] = p;
                i = 3;
                j = 6;
                break;
            case "d1":
                board[4, 1] = p;
                i = 4;
                j = 1;
                break;
            case "d2":
                board[4, 2] = p;
                i = 4;
                j = 2;
                break;
            case "d3":
                board[4, 3] = p;
                i = 4;
                j = 3;
                break;
            case "d4":
                board[4, 4] = p;
                i = 4;
                j = 4;
                break;
            case "d5":
                board[4, 5] = p;
                i = 4;
                j = 5;
                break;
            case "d6":
                board[4, 6] = p;
                i = 4;
                j = 6;
                break;
            case "e1":
                board[5, 1] = p;
                i = 5;
                j = 1;
                break;
            case "e2":
                board[5, 2] = p;
                i = 5;
                j = 2;
                break;
            case "e3":
                board[5, 3] = p;
                i = 5;
                j = 3;
                break;
            case "e4":
                board[5, 4] = p;
                i = 5;
                j = 4;
                break;
            case "e5":
                board[5, 5] = p;
                i = 5;
                j = 5;
                break;
            case "e6":
                board[5, 6] = p;
                i = 5;
                j = 6;
                break;
            case "f1":
                board[6, 1] = p;
                i = 6;
                j = 1;
                break;
            case "f2":
                board[6, 2] = p;
                i = 6;
                j = 2;
                break;
            case "f3":
                board[6, 3] = p;
                i = 6;
                j = 3;
                break;
            case "f4":
                board[6, 4] = p;
                i = 6;
                j = 4;
                break;
            case "f5":
                board[6, 5] = p;
                i = 6;
                j = 5;
                break;
            case "f6":
                board[6, 6] = p;
                i = 6;
                j = 6;
                break;
        }

    }





    public int[] getCoord(string p)
    {
        int[] pos = new int[2];
        switch (casilla)
        {
            case "a1":
                pos[0] = 1;
                pos[1] = 1;

                break;
            case "a2":

                pos[0] = 1;
                pos[1] = 2;
                break;
            case "a3":

                pos[0] = 1;
                pos[1] = 3;
                break;
            case "a4":

                pos[0] = 1;
                pos[1] = 4;
                break;
            case "a5":

                pos[0] = 1;
                pos[1] = 5;
                break;
            case "a6":

                pos[0] = 1;
                pos[1] = 6;
                break;
            case "b1":

                pos[0] = 2;
                pos[1] = 1;
                break;
            case "b2":

                pos[0] = 2;
                pos[1] = 2;
                break;
            case "b3":

                pos[0] = 2;
                pos[1] = 3;
                break;
            case "b4":

                pos[0] = 2;
                pos[1] = 4;
                break;
            case "b5":

                pos[0] = 2;
                pos[1] = 5;
                break;
            case "b6":

                pos[0] = 2;
                pos[1] = 6;
                break;
            case "c1":

                pos[0] = 3;
                pos[1] = 1;
                break;
            case "c2":
                pos[0] = 3;
                pos[1] = 2;
                break;
            case "c3":                
                pos[0] = 3;
                pos[1] = 3;
                break;
            case "c4":
                pos[0] = 3;
                pos[1] = 4;
                break;
            case "c5":
                pos[0] = 3;
                pos[1] = 5;
                break;
            case "c6":

                pos[0] = 3;
                pos[1] = 6;
                break;
            case "d1":

                pos[0] = 4;
                pos[1] = 1;
                break;
            case "d2":

                pos[0] = 4;
                pos[1] = 2;
                break;
            case "d3":

                pos[0] = 4;
                pos[1] = 3;
                break;
            case "d4":

                pos[0] = 4;
                pos[1] = 4;
                break;
            case "d5":

                pos[0] = 4;
                pos[1] = 5;
                break;
            case "d6":

                pos[0] = 4;
                pos[1] = 6;
                break;
            case "e1":

                pos[0] = 5;
                pos[1] = 1;
                break;
            case "e2":
                pos[0] = 5;
                pos[1] = 2;
                break;
            case "e3":

                pos[0] = 5;
                pos[1] = 3;
                break;
            case "e4":
                pos[0] = 5;
                pos[1] = 4;
                break;
            case "e5":

                pos[0] = 5;
                pos[1] = 5;
                break;
            case "e6":

                pos[0] = 5;
                pos[1] = 6;
                break;

            case "f1":
                pos[0] = 6;
                pos[1] = 1;
                break;
            case "f2":
                pos[0] = 6;
                pos[1] = 2;
                break;
            case "f3":
                pos[0] = 6;
                pos[1] = 3;
                break;
            case "f4":
                pos[0] = 6;
                pos[1] = 4;
                break;
            case "f5":

                pos[0] = 6;
                pos[1] = 5;
                break;
            case "f6":

                pos[0] = 6;
                pos[1] = 6;
                break;

        }
        return pos;
    }


    public bool capturar_moneda(string casilla)
    {
        int[] pos = getCoord(casilla);

        if (board[pos[0], pos[1]].Contains("c")) {


            GameObject coin = GameObject.Find(board[pos[0], pos[1]]);
            string name = coin.GetComponent<UnityEngine.UI.Image>().sprite.name;

            print("Capturar moneda: " + name);
            int valor = 0;


            if (name.Contains("1")) 
                valor = 1;
            else if (name.Contains("2"))
                valor = 2;
            else if (name.Contains("3"))            
                valor = 3;
            else if (name.Contains("4"))
                valor = 4;
            else if (name.Contains("5"))
                valor = 5;

            Turnos2 tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();

            if (!tn2.getTurno()) {

                p_blancas = p_blancas + valor;
            }
            else
            {
                p_negras = p_negras + valor;
            }




            Destroy(coin);
            board[pos[0], pos[1]] = ""; //elimino la moneda del array tablero.

          return true;
        }            
        else
            return false;
       
        
    }

    public void eliminar(string p)
    {

        for (int i = 1; i < 7; i++)
        {
            for (int j = 1; j < 7; j++)
            {
                if (board[i, j].Equals(p))
                {
                    board[i, j] = "";
                }
            }
        }
    }


    public void SaltoPosible(int i, int j)
    {

        if ((j < 7) && (i < 7) && (i > 0) && (j > 0) && ((board[i, j].Equals("")) || (board[i, j].Contains("c"))))
        {
          
            Cas item = new Cas(i, j);
            lista[tope] = item;
            tope++;
        }
    }


    public void MovCaballo(int x, int y)
    {

        tope = 0;
        

            SaltoPosible(x + 2, y + 1);
            SaltoPosible(x + 2, y - 1);

            SaltoPosible(x - 2, y + 1);
            SaltoPosible(x - 2, y - 1);

            SaltoPosible(x + 1, y + 2);
            SaltoPosible(x - 1, y + 2);

            SaltoPosible(x + 1, y - 2);
            SaltoPosible(x - 1, y - 2);

    }



    public bool EnLista(int x, int y)
    {
      
        bool aux = false;
        int i = 0;

        while ((!aux) && (i < tope))
        {
            aux = ((lista[i].getX().Equals(x)) && (lista[i].getY().Equals(y))); //lo que hace es buscar en una lista                 
            i++;
        }
      
        return aux;
    }

    public int[] getPos(string p)
    {
        int[] pos = new int[2];
        for (int i = 1; i < 7; i++)
        {
            for (int j = 1; j < 7; j++)
            {
                if (board[i, j].Equals(p))
                {
                    pos[0] = i;
                    pos[1] = j;
                }
            }
        }

        return pos;
    }


    //Usar backtracking para mayor eficiencia


       public bool HayEmpate(bool turno)
    {
        //dependiendo del turno, debo verificar que las piezas posean movimientos posibles.


        int[] b_pos = new int[2];
        int[] n_pos = new int[2];
        int[] r_pos = new int[2];

        if (turno)
        {
            b_pos = getPos("wB");
            n_pos = getPos("wN");
            r_pos = getPos("wR");
        }
        else
        {
            b_pos = getPos("bB");
            n_pos = getPos("bN");
            r_pos = getPos("bR");
        }

        //MovAlfil(b_pos[0], b_pos[1]);
        if ((tope.Equals(0)))
        {
            // MovCaballo(n_pos[0], n_pos[1]);
            if ((tope.Equals(0)))
            {
             //   MovTorre(r_pos[0], r_pos[1]);
                if ((tope.Equals(0)))
                {
                    print("HAY EMPATE!!!!");
                    empate = true;
                }
            }

        }

        return empate;
    }



    public bool terminado(string c, bool turno, int jugadas)
    {
        bool aux = false;
        bool empate = false;

        ganan_blancas = false;
        ganan_negras = false;

        /*
        //verticales
        if ((turno) && ((board[1, 1].Contains("nQ")) || (board[2, 1].Contains("nQ")) || (board[3, 1].Contains("nQ")) || (board[4, 1].Contains("nQ")) || (board[5, 1].Contains("nQ"))))
        {
            aux = true;
            print("GANAN BLANCAS");
            ganan_blancas = true;
        }
        else
         if ((!turno) && ((board[1, 5].Contains("nQ")) || (board[2, 5].Contains("nQ")) || (board[3, 5].Contains("nQ")) || (board[4, 5].Contains("nQ")) || (board[5, 5].Contains("nQ"))))
        {
            aux = true;
            print("GANAN NEGRAS");
            ganan_negras = true;
        }

        if (aux)
        {

            if (c.Equals("w"))
            {
                color = "NEGRAS";
            }
            else
            {
                color = "BLANCAS";
            }
        }*/

        term = aux;
        return aux;
    }


    public Cas[] getListaPosible()
    {

        return lista;
    }


    public int getTope()
    {

        return this.tope;
    }


    void OnGUI()
    {
        float x = Screen.width / 5f;
        float y = Screen.height / 4f;
        GUI.color = Color.red;
        GUI.skin.label.fontSize = 10;
        int h = 0;
        x = Screen.width / 5;
        y = Screen.height;
        //print (y);
        int k = 0;

        /*
         GUI.Label(new Rect(x * 3.8f, 0.3f * x + 20, 90, 1000), "tope" + tope.ToString());
        
                for (int i = 1; i < 7; i++)
                {

                    k++;
                    for (int j = 1; j < 7; j++)
                    {
                        k++;
                        GUI.Label(new Rect(x * 0.3f, 0.005f*x + k*10, 80, 1000),"["+i+","+j+"]: " + board[i,j]);
                    }
                }

                for (int f = 0; f < tope; f++) {


                    GUI.Label(new Rect(x * 4.3f, 0.3f * x + 20*f , 90, 1000), "[" +lista[f].getX().ToString() + " " + lista[f].getY().ToString() +"]");

                }
                */

       
        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 36;
        myStyle.fontStyle = FontStyle.Bold;

        GUI.Label(new Rect(10, 10, 10, 10), juego);
        // print(juego);

        //GUI.Label(new Rect(x * 4.2f, x * 2.5f, 300, 300), "b : " + p_blancas.ToString(), myStyle);

//        GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 320), "n : " + p_negras.ToString(), myStyle);

        if (term)
        {
            //GUI.skin.label.fontSize = 36;
            //GUI.color = Color.green;

            if (ganan_negras)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "N\nE\nG\nR\nA\nS", myStyle);

            }
            else
                if (ganan_blancas)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "B\nL\nA\nN\nC\nA\nS", myStyle);

            }
        }



    }

    public void setCasilla(string cas)
    {
        //term = terminado(cas);
        casilla = cas;
    }
    public string getCasilla()
    {
        return casilla;
    }

    public void setNeutral_n(bool aux)
    {
        neutral_n = aux;
    }
    public void setNeutral_b(bool aux)
    {
        neutral_b = aux;
    }
    public bool getNeutral_n()
    {
        return neutral_n;
    }
    public bool getNeutral_b()
    {
        return neutral_b;
    }

}

