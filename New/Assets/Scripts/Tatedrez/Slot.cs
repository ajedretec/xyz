﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class Slot : MonoBehaviour, IDropHandler
{

    string juego;

    public GameObject item
    {
        get
        {
            if (transform.childCount > 0)
            {
                return transform.GetChild(0).gameObject;
            }
            return null;
        }
    }

    #region IDropHandler implementation
    public void OnDrop(PointerEventData eventData)
    {


        Turnos2 tn2;
        tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
        juego = tn2.getJuego();
        bool posible = tn2.getPosible();

        Tatedrez t = GameObject.Find("pFondo").GetComponent<Tatedrez>();
        DamaNeutral dm = GameObject.Find("pFondo").GetComponent<DamaNeutral>();
        caballo_t tr = GameObject.Find("pFondo").GetComponent<caballo_t>();

        string pieza = Drag.pieza; //esto me daría la pieza actual

        if (juego.Equals("t"))
        {

            t.setCasilla(this.name);// primero setea la casilla y luego vé si es posible el movimiento            
            int[] pos_ini;
            int[] pos_fin;

            
            pos_ini = t.getPos(pieza);
            string contenido = t.getContenido(this.name);
            //print("Slot: contenido " + contenido);
            bool aux;

            if (contenido.Equals(""))
                aux = true;
            else
                aux = false;

            posible = (posible) && (aux);

            bool blancas = tn2.getTurno();//compruebo si el turno es el correcto

            //compruebo si el turno es el correcto!
            posible = posible && (((blancas) && (pieza.Contains("w"))) || (!(blancas) && (pieza.Contains("b"))));
            

            if (((pieza.Equals("bR")) || (pieza.Equals("wR"))) && (tn2.getCounter() > 5)) 
            {
                
                t.MovTorre(pos_ini[0], pos_ini[1]);
                pos_fin = t.getCoord(this.name);
                bool enLista = t.EnLista(pos_fin[0], pos_fin[1]);
                posible = (posible) && (enLista);
            }

            else if (((pieza.Equals("bB")) || (pieza.Equals("wB"))) && (tn2.getCounter() > 5))
            {

                t.MovAlfil(pos_ini[0], pos_ini[1]);
                pos_fin = t.getCoord(this.name);
                bool enLista = t.EnLista(pos_fin[0], pos_fin[1]);
                posible = (posible) && (enLista);
            }
            else if (((pieza.Equals("bN")) || (pieza.Equals("wN"))) && (tn2.getCounter() > 5))
            {

                t.MovCaballo(pos_ini[0], pos_ini[1]);
                pos_fin = t.getCoord(this.name);
                bool enLista = t.EnLista(pos_fin[0], pos_fin[1]);
                posible = (posible) && (enLista);
            }


            if ((pieza.Contains("b")) && ((tn2.getCounter().Equals(0))))
            {
                posible = false;
            }
            //con esto evito que las negras empiecen primero, tengo que vichar codigo en MovPosible porque puede que esté obsoleto
        }

        else

       if (juego.Equals("c")) {

            //print("CABALLO TRAGAMONEDAS" + this.name);// tengo que setear la casillla, obtener el turno y luego cambiarlo
            //posible = false; // voy a tener que modificarlo

            tr.setCasilla(this.name);// primero setea la casilla y luego vé si es posible el movimiento            
            int[] pos_ini;
            int[] pos_fin;
            bool blancas = tn2.getTurno();//obtengo turno

            pos_ini = tr.getPos(pieza); //ojo con los nombres de las piezas!

            if (((pieza.Contains("wk"))) && ((blancas))) // Primero las blancas mueven la nQ
            {   
                posible = true;
                
                tr.MovCaballo(pos_ini[0], pos_ini[1]);

                pos_fin = tr.getCoord(this.name); //ACA OBTENGO LA POSICIÓN FINAL, PUEDO TESTEAR SI HAY UNA MONEDA.

                bool enLista = tr.EnLista(pos_fin[0], pos_fin[1]);
                posible = (posible) && (enLista);

                if (posible) {
                    tn2.setTurno(false);
                    print("Es Posible " + pos_fin[0] + pos_fin[1]);
                }
                else
                {
                    print("No es Posible " + pos_fin[0] + pos_fin[1] + "enLista ="+ enLista);
                }
                


            }
            else if (((pieza.Contains("bk"))) && (!(blancas))) // Primero las blancas mueven la nQ
            {
                posible = true;                
                tr.MovCaballo(pos_ini[0], pos_ini[1]);
                pos_fin = tr.getCoord(this.name);
                bool enLista = tr.EnLista(pos_fin[0], pos_fin[1]);

                posible = (posible) && (enLista);

                if (posible)
                {
                    tn2.setTurno(true);
                    print("Es Posible " + pos_fin[0] + pos_fin[1]);
                }
                else {
                    print("No es Posible " + pos_fin[0] + pos_fin[1] + "enLista =" + enLista);
                }

               
            }


        }
        else if (juego.Equals("d")) //ultimo en cambiar
        { //el juego es DAMA NEUTRAL

            print("DAMA NEUTRAL?");
            TurnoNeutral tNeutral;
            tNeutral = GameObject.Find("ptNuetral").GetComponent<TurnoNeutral>();

            dm.setCasilla(this.name);// primero setea la casilla y luego vé si es posible el movimiento            
            int[] pos_ini;
            int[] pos_fin;

            // voy a controlar el turno con par e impar.
            int turno = tn2.getCounter();
            posible = false;

            if (((pieza.Equals("nQ"))) && ((turno.Equals(0)))) // Las blancas no pueden iniciar con un movimiento de dama NEUTRAL
            {
                posible = false;
            }
            else
            {
                bool blancas = tn2.getTurno();//obtengo turno
                bool neutral_b = dm.getNeutral_b();
                bool neutral_n = dm.getNeutral_n();
                bool mismo_lugar = false;


 
                    pos_ini = dm.getPos(pieza); //posición inicial de la pieza 
                    pos_fin = dm.getCoord(this.name); // posición final de la pieza

                    mismo_lugar = ((pos_ini[0].Equals(pos_fin[0])) && (pos_ini[1].Equals(pos_fin[1])));
                    print("mismo lugar :" + mismo_lugar + pos_ini[0]+","+ pos_ini[1] +"   "+ pos_fin[0]+","+pos_fin[1]);

                    dm.MovAlfil(pos_ini[0], pos_ini[1]);
                    bool enLista = dm.EnLista(pos_fin[0], pos_fin[1]);

                    if (!enLista)
                    {
                        dm.MovTorre(pos_ini[0], pos_ini[1]);
                        enLista = dm.EnLista(pos_fin[0], pos_fin[1]);
                    }
  

                if (((pieza.Equals("nQ"))) && ((!blancas)) && (!neutral_n) && (!mismo_lugar) && (enLista)) //turno de las negras
                {
                    posible = true;

                    neutral_n = true; //significa que las negras movieron su dama neutral
                    dm.setNeutral_n(neutral_n);
                    
                    tNeutral.setNeutron("");//con true lo oculto 
                
                }
                else
                if (((pieza.Equals("nQ"))) && ((blancas)) && (!neutral_b) && (!mismo_lugar) && (enLista)) // Primero las blancas mueven la nQ
                {
                    posible = true;
                   
                    neutral_b = true; // significa que las blancas movieron su dama neutral
                    dm.setNeutral_b(neutral_b);

                    tNeutral.setNeutron(""); // con true lo oculto

                }
                else
                if (((pieza.Contains("wQ"))) && ((blancas))&& (!mismo_lugar) && (enLista)) //turno de las blancas
                {
                    
                    if (turno.Equals(0))
                    {
                        posible = true;
                        dm.setNeutral_n(neutral_n);
                        tNeutral.setNeutron("negras");

                    }
                    else
                    if (neutral_b)// si las blancas ya movieron su dama neutral..
                    {
                        
                        posible = true;
                        neutral_b = false;//estaba en true                      
                        dm.setNeutral_b(neutral_b);

                        neutral_n = false;

                        dm.setNeutral_n(neutral_n);                                                
                        tNeutral.setNeutron("negras");


                    }
                }
                else
                if (((pieza.Contains("bQ"))) && ((!blancas)) && (neutral_n)&& (!mismo_lugar) && (enLista))
                {
                    
                    posible = true;
                    neutral_n = false;
                    dm.setNeutral_n(neutral_n);                   
                    tNeutral.setNeutron("blancas");
                }

            }
        }

        if (/*(!item) &&*/ (posible))
        {
            
            Drag.itemBeingDragged.transform.SetParent(transform);
            t.setCasilla(this.name);
        }
        else
        {
            print("NULL");
            t.setCasilla("null");
        }
    }

    public string getSlot()
    {
        return this.name;
    }
    #endregion
}
