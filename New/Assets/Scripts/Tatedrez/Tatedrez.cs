﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Tatedrez : MonoBehaviour
{


    //Los movimientos de las piezas se pueden hacer en una clase aparte y reutilizarlo en dama neutral y tragamonedas.

    private string[,] board = new string[4, 4];
    private string[,] movPos = new string[4, 4];
    private string casilla = "null";
    private int i, j;
    private bool term = false;
    private string color = "";
    private bool empate = false;
    private Cas[] mov_legal = new Cas[30];
    private Cas[] mov_legal_n = new Cas[30];
    private int topeLegal = 0;
    private int topeLegal_n = 0;
    private int tope_c = 0;
    private int tope_a = 0;
    private int tope_t = 0;
    private int ntope_c = 0;
    private int ntope_a = 0;
    private int ntope_t = 0;
    private Cas[] mov_alfil = new Cas[10];
    private Cas[] mov_caballo = new Cas[10];
    private Cas[] mov_torre = new Cas[10];
    private Cas[] n_mov_alfil = new Cas[10];
    private Cas[] n_mov_caballo = new Cas[10];
    private Cas[] n_mov_torre = new Cas[10];
    private Cas[] mov_posibles = new Cas[15];
    private int[] pos_alfil = new int[2];
    private int[] pos_caballo = new int[2];
    private int[] pos_torre = new int[2];
    private Cas[] lista = new Cas[15];
    private int tope = 0;
    private int tope_ini = 0;
    private Cas[] historial =  new Cas[10];
    private int th = 0;
    private Cas item;

    // private ArrayList<Cas> 


    void Start()
    {
        Turnos2 tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
        tn2.setJuego("t");
        item = new Cas(0, 0);

        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                board[i, j] = "";
            }
        }
    }

    void Update()
    {


    }

    public void insertar(string p)
    {
      //  print(p);

        
        int a = 0;
        int b = 1;
        int k;

        bool aux;

     //   if (p.Contains("w"))
       // {
            aux = false;
        //}
        //else {
           // aux = true;
        //}


        
        //print(_alfaBeta());
        switch (casilla) //Cada vez que inserto una pieza blanca pongo a pensar a la maquina
        {
            case "a1":
                board[1, 1] = p;
                i = 1;
                j = 1;
               // movLegales();
               // mov_legal_ini();
// n_movLegales();


                if (p.Contains("w")) // NO SON NECESARIOS TANTOS IF
                {

                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                    th--;
                }

                break;
            case "a2":
                board[1, 2] = p;
                i = 1;
                j = 2;

                //movLegales();
             //   mov_legal_ini();
                //n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                }

                break;
            case "a3":
                board[1, 3] = p;
                i = 1;
                j = 3;

                //movLegales();
               // mov_legal_ini();
                //n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                }

                break;
            case "b1":
                board[2, 1] = p;
                i = 2;
                j = 1;
                //movLegales();
                //mov_legal_ini();
                //n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                }

                break;
            case "b2":
                board[2, 2] = p;
                i = 2;
                j = 2;
                //movLegales();
                //mov_legal_ini();
                //n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                }

                break;
            case "b3":
                board[2, 3] = p;
                i = 2;
                j = 3;
                //movLegales();
                //mov_legal_ini();
                //n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                }

                break;
            case "c1":
                board[3, 1] = p;
                i = 3;
                j = 1;
                //movLegales();
                //mov_legal_ini();
               // n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                }

                break;
            case "c2":
                board[3, 2] = p;
                i = 3;
                j = 2;
                //movLegales();
                //mov_legal_ini();
                //n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);
                }

                break;
            case "c3":
                board[3, 3] = p;
                i = 3;
                j = 3;
                //movLegales();
                //mov_legal_ini();
                //n_movLegales();
                if (p.Contains("w"))
                {
                    th++;
                    historial[th] = item;
                    k = _alfaBeta(item, 0, a, b, aux, p);
                    th--;
                    print("Algoritmo:" + k);

                }

                break;
        }
    }

    public int[] getCoord(string p)
    {
        int[] pos = new int[2];
        switch (casilla)
        {
            case "a1":
                pos[0] = 1;
                pos[1] = 1;

                break;
            case "a2":

                pos[0] = 1;
                pos[1] = 2;
                break;
            case "a3":

                pos[0] = 1;
                pos[1] = 3;
                break;
            case "b1":

                pos[0] = 2;
                pos[1] = 1;
                break;
            case "b2":

                pos[0] = 2;
                pos[1] = 2;
                break;
            case "b3":

                pos[0] = 2;
                pos[1] = 3;
                break;
            case "c1":

                pos[0] = 3;
                pos[1] = 1;
                break;
            case "c2":
                pos[0] = 3;
                pos[1] = 2;
                break;
            case "c3":

                pos[0] = 3;
                pos[1] = 3;
                break;
        }
        return pos;
    }


    public string getContenido(string casilla)
    {

        int[] pos = new int[2];

        pos = getCoord("");


        return board[pos[0], pos[1]];
    }

    public void eliminar(string p)
    {

        for (int i = 1; i < 4; i++)
        {
            for (int j = 1; j < 4; j++)
            {
                if (board[i, j].Equals(p))
                {
                    board[i, j] = "";
                }
            }
        }
    }

    public bool EnLista(int x, int y)
    {

        bool aux = false;
        int i = 0;

        while ((!aux) && (i < tope))
        {
            aux = ((lista[i].getX().Equals(x)) && (lista[i].getY().Equals(y)));
            i++;
        }

        return aux;
    }

    public int[] getPos(string p)
    {
        int[] pos = new int[2];
        for (int i = 1; i < 4; i++)
        {
            for (int j = 1; j < 4; j++)
            {
                if (board[i, j].Equals(p))
                {
                    pos[0] = i;
                    pos[1] = j;
                }
            }
        }

        return pos;
    }


    //Usar backtracking para mayor eficiencia

    public void MovTorre(int x, int y)
    {

        tope = 0;
        int i, j, x_ini, y_ini;
        bool libre;
        i = x;
        j = y;
        x_ini = x;
        y_ini = y;

       libre = true;



        //primero consulto abajo, itero sobre la columna j
        while ((j > 0) && (libre))
        {

            j--;

            if ((j > 0) && (board[x, j].Equals("")))
            {              
                Cas item = new Cas(x, j);
                item.set_bx(x_ini);
                item.set_by(y_ini);
                item.setPieza("Torre");
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia arriba

        while ((j < 4) && (libre))
        {

            j++;
            if ((j < 4) && (board[x, j].Equals("")))
            { //guardo la x y la j
              // print("ARRIBA" + x + " " + j);

                Cas item = new Cas(x, j);
                item.setPieza("Torre");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;

        while ((i < 4) && (libre))
        {

            i++;
            if ((i < 4) && (board[i, y].Equals("")))
            { //guardo la x y la j
                //print("DERECHA" + i + " " + y);

                Cas item = new Cas(i, y);
                item.setPieza("Torre");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;
        libre = true;

        while ((i > 0) && (libre))
        {
            i--;
            if ((i > 0) && (board[i, y].Equals("")))
            { //guardo la x y la j

                // print("IZQUIERDA" + i + " " + y);
                Cas item = new Cas(i, y);
                item.setPieza("Torre");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }


    }


    public void MovAlfil(int x, int y)
    {

        tope = 0;
        int i, j, x_ini, y_ini; ;

        bool libre;
        i = x;
        j = y;
        x_ini = x;
        y_ini = y;


        libre = true;

        //primero consulto abaja a la izquierda
        while ((j > 0) && (i > 0) && (libre))
        {

            j--;
            i--;

            if ((j > 0) && (i > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                // print("ABAJO - IZQUIERDA" + i + " " + j);

                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia ARRIBA - DERECHA

        while ((j < 4) && (i < 4) && (libre))
        {

            j++;
            i++;

            if ((j < 4) && (i < 4) && (board[i, j].Equals("")))
            { //guardo la x y la j
              // print("ARRIBA-DERECHA" + i + " " + j);

                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;


        //Subo la i y decremento la j
        while ((i < 4) && (j > 0) && (libre))
        {

            i++;
            j--;

            if ((i < 4) && (j > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("DERECHA- ABAJO" + i + " " + j);

                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;

        libre = true;

        //subo la i y decremento la j
        while ((i > 0) && (j < 4) && (libre))
        {
            i--;
            j++;
            if ((i > 0) && (j < 4) && (board[i, j].Equals("")))
            { //guardo la x y la j

                // print("IZQUIERDA - ARRIBA" + i + " " + j);
                Cas item = new Cas(i, j);
                item.setPieza("Alfil");
                item.set_bx(x_ini);
                item.set_by(y_ini);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        //print("MovTorre: " + tope);
    }




    public void SaltoPosible(int i, int j)
    {
        int x_ini = i;
        int y_ini = j;

        if ((j < 4) && (i < 4) && (i > 0) && (j > 0) && (board[i, j].Equals("")))
        {
            Cas item = new Cas(i, j);
            item.setPieza("Caballo");
            item.set_bx(x_ini);
            item.set_by(y_ini);
            lista[tope] = item;
            tope++;
        }
    }


    public void MovCaballo(int x, int y)
    {

        tope = 0;

        if (tope < 3)
        {
            SaltoPosible(x + 2, y + 1);
            SaltoPosible(x + 2, y - 1);
        }

        if (tope < 3)
        {
            SaltoPosible(x - 2, y + 1);
            SaltoPosible(x - 2, y - 1);
        }

        if (tope < 3)
        {
            SaltoPosible(x + 1, y + 2);
            SaltoPosible(x - 1, y + 2);

        }

        if (tope < 3)
        {
            SaltoPosible(x + 1, y - 2);
            SaltoPosible(x - 1, y - 2);
        }


    }


    public bool HayEmpate(bool turno)
    {
        //dependiendo del turno, debo verificar que las piezas posean movimientos posibles.


        int[] b_pos = new int[2];
        int[] n_pos = new int[2];
        int[] r_pos = new int[2];

        if (turno)
        {
            b_pos = getPos("wB");
            n_pos = getPos("wN");
            r_pos = getPos("wR");
        }
        else
        {
            b_pos = getPos("bB");
            n_pos = getPos("bN");
            r_pos = getPos("bR");
        }

        MovAlfil(b_pos[0], b_pos[1]);
        if ((tope.Equals(0)))
        {
            MovCaballo(n_pos[0], n_pos[1]);
            if ((tope.Equals(0)))
            {
                MovTorre(r_pos[0], r_pos[1]);
                if ((tope.Equals(0)))
                {
                    //print("HAY EMPATE!!!!");
                    empate = true;
                }
            }

        }

        return empate;
    }



    public bool terminado(string c, bool turno, int jugadas)
    {
        bool aux = false;
        bool empate = false;

        //verticales
        if ((board[1, 1].Contains(c)) && (board[1, 2].Contains(c)) && (board[1, 3].Contains(c)))
        {
            aux = true;
            print("[" + board[1, 1] + " " + board[1, 2] + " " + board[1, 3] + "]");
        }
        else
        if ((board[2, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[2, 3].Contains(c)))
        {
            aux = true;
            print("[" + board[2, 1] + " " + board[2, 2] + " " + board[2, 3] + "]");
        }
        else
        if ((board[3, 1].Contains(c)) && (board[3, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = true;
            print("[" + board[3, 1] + " " + board[3, 2] + " " + board[3, 3] + "]");
        }
        else
        //laterales
        if ((board[1, 1].Contains(c)) && (board[2, 1].Contains(c)) && (board[3, 1].Contains(c)))
        {
            aux = true;
            print("[" + board[1, 1] + " " + board[2, 1] + " " + board[3, 1] + "]");
        }
        else
        if ((board[1, 2].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 2].Contains(c)))
        {
            aux = true;
            print("[" + board[1, 2] + " " + board[2, 2] + " " + board[3, 2] + "]");
        }
        else
        if ((board[1, 3].Contains(c)) && (board[2, 3].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = true;
            print("[" + board[1, 3] + " " + board[2, 3] + " " + board[3, 3] + "]");
        }
        else

        //diagonales
        if ((board[1, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = true;
            print("[" + board[1, 1] + " " + board[2, 2] + " " + board[3, 3] + "]");
        }
        else
        if ((board[1, 3].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 1].Contains(c)))
        {
            aux = true;
            print("[" + board[1, 3] + " " + board[2, 2] + " " + board[3, 1] + "]");
        }

        if (aux)
        {
            //lista = MovTorre(2, 2);
            if (c.Equals("b"))
            {
                color = "NEGRAS";
            }
            else
            {
                color = "BLANCAS";
            }
        }
        else
        {
            if (jugadas > 4)
            {
                empate = HayEmpate(turno);
                if (empate)
                {
                   // print("COLOR = EMPATE");
                    color = "EMPATE";
                }
            }
        }

        term = (aux) | (empate); // no terminó pero hay empate

        return term;
    }


    public Cas[] getListaPosible()
    {

        return lista;
    }


    public int getTope()
    {

        return this.tope;
    }

    //una opcion es siempre ejectarlo independientemente de si es blancas o negras
    public Cas[] movLegales() //tengo que saber DE DONDE VENGO!!!
    {

        pos_alfil = getPos("wB");
        pos_caballo = getPos("wN");
        pos_torre = getPos("wR");


        if (!(pos_alfil[0].Equals(0)) && !(pos_alfil[1].Equals(0)))
        {
            MovAlfil(pos_alfil[0], pos_alfil[1]);
            mov_alfil = (Cas[])getListaPosible().Clone();
            tope_a = getTope();
        }

        if (!(pos_caballo[0].Equals(0)) && !(pos_caballo[1].Equals(0)))
        {
            MovCaballo(pos_caballo[0], pos_caballo[1]);
            mov_caballo = (Cas[])getListaPosible().Clone();
            tope_c = getTope();
        }

        if (!(pos_torre[0].Equals(0)) && !(pos_torre[1].Equals(0)))
        {
            MovTorre(pos_torre[0], pos_torre[1]);
            mov_torre = (Cas[])getListaPosible().Clone();

            
            tope_t = getTope();
        }

        topeLegal = tope_c + tope_a + tope_t;
        //topeLegal_n = ntope_c + ntope_a + ntope_t;
        /* Comienza la copia */

        for (int i = 0; i < tope_a; i++)
        {
            Cas cas = new Cas(mov_alfil[i].getX(), mov_alfil[i].getY());
            cas.setPieza(mov_alfil[i].getPieza());

            cas.set_bx(mov_alfil[i].get_bx());
            cas.set_by(mov_alfil[i].get_by());

            mov_legal[i] = cas;
        }

//        int aux = tope_a;

        for (int i = 0; i < tope_c; i++)
        {
            Cas cas = new Cas(mov_caballo[i].getX(), mov_caballo[i].getY());
            cas.setPieza(mov_caballo[i].getPieza());

            cas.set_bx(mov_caballo[i].get_bx());
            cas.set_by(mov_caballo[i].get_by());

            mov_legal[tope_a + i] = cas;            
        }
  //      int aux2 = tope_a + tope_c;
        for (int i = 0; i < tope_t; i++)
        {
            Cas cas = new Cas(mov_torre[i].getX(), mov_torre[i].getY());
            cas.setPieza(mov_torre[i].getPieza());

            cas.set_bx(mov_torre[i].get_bx());
            cas.set_by(mov_torre[i].get_by());

            mov_legal[tope_a + i + tope_c] = cas;      
        }

        topeLegal = tope_c + tope_a + tope_t;
        mov_legal_ini();

        return mov_legal;
    }


    public Cas[] n_movLegales()
    {
            pos_alfil = getPos("bB");
            pos_caballo = getPos("bN");
            pos_torre = getPos("bR");

        if (!(pos_alfil[0].Equals(0)) && !(pos_alfil[1].Equals(0)))
        {
            MovAlfil(pos_alfil[0], pos_alfil[1]);
            n_mov_alfil = (Cas[])getListaPosible().Clone();                          
            ntope_a = getTope();           
        }

        if (!(pos_caballo[0].Equals(0)) && !(pos_caballo[1].Equals(0)))
        {
            MovCaballo(pos_caballo[0], pos_caballo[1]);
            n_mov_caballo = (Cas[])getListaPosible().Clone();                        
            ntope_c = getTope();            
        }

        if (!(pos_torre[0].Equals(0)) && !(pos_torre[1].Equals(0)))
        {
            MovTorre(pos_torre[0], pos_torre[1]);
            n_mov_torre = (Cas[])getListaPosible().Clone();           
            ntope_t = getTope();            
        }

       // topeLegal_n = ntope_c + ntope_a + ntope_t;
       
        for (int i = 0; i < ntope_a; i++)
        {
            Cas cas = new Cas(n_mov_alfil[i].getX(), n_mov_alfil[i].getY());
            cas.setPieza(n_mov_alfil[i].getPieza());

            cas.set_bx(n_mov_alfil[i].get_bx());
            cas.set_by(n_mov_alfil[i].get_by());

            mov_legal_n[i] = cas;            
        }

       // int aux = ntope_a;

        for (int i = 0; i < ntope_c; i++)
        {
            Cas cas = new Cas(n_mov_caballo[i].getX(), n_mov_caballo[i].getY());
            cas.setPieza(n_mov_caballo[i].getPieza());

            cas.set_bx(mov_caballo[i].get_bx());
            cas.set_by(mov_caballo[i].get_by());

            mov_legal_n[ntope_a + i] = cas;          
        }

      //  int aux2 = ntope_a + ntope_c;
        for (int i = 0; i < ntope_t; i++)
        {
            Cas cas = new Cas(n_mov_torre[i].getX(), n_mov_torre[i].getY());
            cas.setPieza(n_mov_torre[i].getPieza());

            cas.set_bx(mov_torre[i].get_bx());
            cas.set_by(mov_torre[i].get_by());

            mov_legal_n[ntope_a + i + ntope_c] = cas; 
            
        }

        topeLegal_n = ntope_c + ntope_a + ntope_t;
        n_mov_legal_ini();

        print("Tope Legal:" + topeLegal);
        return mov_legal_n;
    }


    //al inicio siempre hay una 
    void mov_legal_ini() {
        int k = 0;
        Turnos2 tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
        tn2.setJuego("t");
        int c = tn2.getCounter();
        if (c < 5)
        {
            for (int i = 1; i < 4; i++)
            {
                for (int j = 1; j < 4; j++)
                {
                    if (board[i, j].Equals(""))
                    {
                        Cas cas = new Cas(i, j);
                        cas.setPieza("-");
                        cas.set_bx(0);
                        cas.set_by(0);

                        //print("PATO APARATO!!");
                        mov_legal[topeLegal + k] = cas;
                        k = k + 1;
                    }
                }
            }
        }
        topeLegal = topeLegal + k;
    }

    void n_mov_legal_ini()
    {
        int k = 0;
        Turnos2 tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
        tn2.setJuego("t");
        int c = tn2.getCounter();
        if (c < 6)
        {
            for (int i = 1; i < 4; i++)
            {
                for (int j = 1; j < 4; j++)
                {
                    if (board[i, j].Equals(""))
                    {
                        Cas cas = new Cas(i, j);
                        cas.setPieza("-");

                        cas.set_bx(0);
                        cas.set_by(0);

                        //print("PATO APARATO!!");
                        mov_legal_n[topeLegal_n + k] = cas;
                        k = k + 1;
                    }
                }
            }
        }
        topeLegal_n = topeLegal_n + k;
    }




    void OnGUI()
    {
        float x = Screen.width / 5f;
        float y = Screen.height / 4f;
        GUI.color = Color.red;
        GUI.skin.label.fontSize = 10;
        int h = 0;

        x = Screen.width / 5;
        y = Screen.height;
        //print (y);
        int k = 0;
        int j = 5;
        float p = 0.1f;
        //  topeLegal = tope_c + tope_a + tope_t;
        // GUI.Label(new Rect(x * 3.8f, 0.3f * x + 20, 90, 1000), topeLegal.ToString());

        int t = 0;
        //el tope Legal para ambas listas no necesariamente debe ser el mismo.

        for (int i = 0; i < topeLegal; i++)
        {
            t++;
            GUI.Label(new Rect(x * 0.5f, 1.3f * x + 20 + t * 15, 90, 1000), "w" + i + " " + "[" + mov_legal[i].getX() + "," + mov_legal[i].getY() + "]: " + mov_legal[i].getPieza());
            //GUI.Label(new Rect(x * 4.0f, 1.3f * x + 20 + t * 15, 90, 1000), "b" + i + " " + "[" + mov_legal_n[i].getX() + "," + mov_legal_n[i].getY() + "]: " + mov_legal_n[i].getPieza());
        }

        
        int s = 0;
        for (int i = 0; i < topeLegal_n; i++)
        {
            s++;            
            GUI.Label(new Rect(x * 4.0f, 1.3f * x + 20 + s * 15, 90, 1000), "b" + i + " " + "[" + mov_legal_n[i].getX() + "," + mov_legal_n[i].getY() + "]: " + mov_legal_n[i].getPieza());
        }
        
        


        if (term)
        {

            Cas[] mov_legal = new Cas[10];
            Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
            GUIStyle myStyle = new GUIStyle();
            myStyle.font = myFont;
            myStyle.fontSize = 112;
            myStyle.fontStyle = FontStyle.Bold;

            if (color.Equals("EMPATE" + mov_legal.ToString()))
            {
                GUI.Label(new Rect(x * 1.0f, x * 2.5f, 300, 300), "¡EMPATE!", myStyle);
                

            }

            if (color.Equals("BLANCAS" + mov_legal.ToString()))
            {
                GUI.Label(new Rect(x * 1.0f, x * 2.5f, 300, 300), "¡GANAN " + color + "!", myStyle);

            }
            else
                if (color.Equals("NEGRAS" + mov_legal.ToString()))
            {
                GUI.Label(new Rect(x * 1.0f, x * 0.01f, 300, 300), "¡GANAN " + color + "!", myStyle);
            }
        }

    }


        private int evaluar(string c) {



        int aux =0;

        if ((board[1, 1].Contains(c)) && (board[1, 2].Contains(c)) && (board[1, 3].Contains(c)))
        {
            aux = 1;
            print("[" + board[1, 1] + " " + board[1, 2] + " " + board[1, 3] + "]");
        }
        else
       if ((board[2, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[2, 3].Contains(c)))
        {
            aux = 1;
            print("[" + board[2, 1] + " " + board[2, 2] + " " + board[2, 3] + "]");

        }
        else
       if ((board[3, 1].Contains(c)) && (board[3, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = 1;
            print("[" + board[3, 1] + " " + board[3, 2] + " " + board[3, 3] + "]");

        }
        else
       //laterales
       if ((board[1, 1].Contains(c)) && (board[2, 1].Contains(c)) && (board[3, 1].Contains(c)))
        {
            aux = 1;
            print("[" + board[1, 1] + " " + board[2, 1] + " " + board[3, 1] + "]");

        }
        else
       if ((board[1, 2].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 2].Contains(c)))
        {

            aux = 1;
            print("[" + board[1, 2] + " " + board[2, 2] + " " + board[3, 3] + "]");

        }
        else
       if ((board[1, 3].Contains(c)) && (board[2, 3].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = 1;
            print("[" + board[1, 3] + " " + board[2, 3] + " " + board[3, 3] + "]");

        }

        if ((board[1, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[3, 3].Contains(c)))
        {
            aux = 1;
            print("[" + board[1, 1] + " " + board[2, 2] + " " + board[3, 3] + "]");

        }
        if ((board[3, 1].Contains(c)) && (board[2, 2].Contains(c)) && (board[1, 3].Contains(c)))
        {
            aux = 1;
            print("[" + board[3, 1] + " " + board[2, 2] + " " + board[1, 3] + "]");

        }



        return aux;
    }


    private void insertarCasTablero(Cas cas, string c) {

        string pieza = "";

        string aux = cas.getPieza();

        if (aux.Equals("Alfil"))
        {

            pieza = c + "B";
        }
        else if (aux.Equals("Caballo")) {

            pieza = c + "k";

        }
        else if (aux.Equals("Torre"))
        {

            pieza = c + "R";

        }

        th++;
        historial[th] = cas;

        //eliminar(pieza); // a la hora de calcular, debo eliminar la pieza del escaque anterior, PERO TAMBIEN DEBO AGREGARLA DESPUES!!!
                print("Insertada pieza " + cas.getPieza() + "en: " + "["+ cas.getX()+","+ cas.getY() + "]");
        board[cas.getX(), cas.getY()] = pieza;
    }

    private void eliminarCasTablero(Cas cas)
    {
        th--;
        board[cas.getX(), cas.getY()] = "";
    }
    

    private int _alfaBeta(Cas node, int depth, int alpha = int.MinValue, int beta = int.MaxValue, bool maximizing = true, string c="")
        {

        print("-----------------------------------------------------------------------------------------------------------------------");
            //_board.move(node); //Falta que se mueva!
            if (depth == 0)
            {

               //print("profundidad 0");
                if (c.Contains("w")) //la ultima pieza insertada es blanca, por lo que ahora le toca a las negras
                 {
                     c = "b";
                 }
                else {                
                     c = "w";       
                 }

            // int val = evaluar(c);

            //print("EVALUAR: " + c+ ": "+ val);
            //print("[" + board[1, 1]+" "+ board[2, 2] + " " + board[3, 3] + "]");

            int val = evaluar("w");
            int val2 = evaluar("b");

            for (int i = 1; i <= th; i++) {
               // print("TH: " + th);
                print("-[" + historial[i].getPieza() + historial[i].getX() + historial[i].getY() + "]");
            }

            print("                                                                               -->EVALUAR BLANCAS: " + val);
            print("                                                                               -->EVALUAR NEGRAS: " + val2);


            return val;
            }

            if (maximizing)
            {
            //print("Entro a Maximizar");
                int v = int.MinValue;

                Cas[] replies = movLegales();

                for (int i = 0; i < topeLegal_n; i++)
                {

                    insertarCasTablero(replies[i],"w");


                int candidate = _alfaBeta(replies[i], depth - 1, alpha, beta, false, "b");
                    eliminarCasTablero(replies[i]);

                v = candidate > v ? candidate : v;

                    alpha = alpha > v ? alpha : v;
                    if (beta < alpha)
                    {
                        break;
                    }
                }
               // _board.revert();
                return v;
            }
            else
            {
              //  print("Entro a Maximizar");
                int v = int.MaxValue;

                Cas[] replies = n_movLegales();
                
                for (int i = 0; i < topeLegal_n ; i++)
                {


                    insertarCasTablero(replies[i],"b");

                    int candidate = _alfaBeta(replies[i], depth - 1, alpha, beta, true,"w");
                    eliminarCasTablero(replies[i]);

                    v = candidate < v ? candidate : v;
                    if (beta > v)
                    {
                        beta = v;
                        //node.bestResponse = reply;
                    }
                    if (beta < alpha)
                    {
                        break;
                    }
                }
                //_board.revert();
                return v;
            }
            
        }


    public void setCasilla(string cas)
    {
        //term = terminado(cas);
        casilla = cas;
    }
    public string getCasilla()
    {
        return casilla;
    }

}
