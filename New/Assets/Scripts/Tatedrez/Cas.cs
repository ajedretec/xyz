﻿using UnityEngine;
using System.Collections;

public class Cas : MonoBehaviour {

    private int x, b_x;
    private int y, b_y;
    private string pieza; 


     public Cas(int x, int y) {

        this.x = x;
        this.y = y;

    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public string getPieza() {

        return pieza;
    }

    public void setPieza(string p)
    {
        pieza = p;
    }


    public int getX() {
        return x;
    }

    public int getY(){
        return y;
    }

    public int get_bx()
    {
        return b_x;
    }

    public int get_by()
    {
        return b_y;
    }

    public void set_bx(int x)
    {
        b_x = x;
    }

    public void set_by(int y)
    {
        b_y = y;
    }




}
