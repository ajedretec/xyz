﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;




public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
{
    #region IBeginDragHandler implementation

    public static GameObject itemBeingDragged;
    Vector3 startPosition;
    Transform startParent;
    Turnos2 tn2;
    bool turno;
    string pieza_conc = "";
    string color;
    bool aux4;
    public static int var_x = 0;
    public static int var_y = 0;
    public bool torre;
    static string cas = "";
    private Cas[] lista = new Cas[5];
    private int tope = 0;
    private int c = 0;
    private bool es_posible = true;
    public static string pieza;
    private string juego;

   
    Transform canvas;

    public void OnBeginDrag(PointerEventData eventData)
    {
        itemBeingDragged = gameObject;
        startPosition = transform.position;
        startParent = transform.parent;
        GetComponent<CanvasGroup>().blocksRaycasts = false;
        canvas = GameObject.FindGameObjectWithTag("Canvas").transform;
        transform.parent = canvas;

        tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
    }

    

    #endregion

    #region IDragHandler implementation

    public void OnDrag(PointerEventData eventData)
    {
       
        
        //CUIDADO CON ESTO, ESTOY LLAMANDO A FUNCIONES CONSTANTEMENTE SIEMPRE QUE ESTOY DRAGEANDO. ARREGLAR URGENTE!!
        tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();//esto no es necesario llamarlo
        es_posible = movPosible("");
        tn2.setPosible(es_posible);

        Image p = GetComponent<CanvasGroup>().GetComponent<Image>();
        //seteo la pieza actual
        pieza = p.name;

       
        if (tn2.getCounter() > 5)
        {
            pieza_conc = "";
        }
        else
        {
            var_x = 0;
            var_y = 0;
        }


        transform.position = Input.mousePosition;


    }

    public bool movPosible(string casilla_final)
    {

        cas = casilla_final;
        tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
        Image p = GetComponent<CanvasGroup>().GetComponent<Image>();
        string pieza = p.name;
        bool posible;
        Tatedrez t = GameObject.Find("pFondo").GetComponent<Tatedrez>();
        turno = tn2.getTurno();
        posible = false;
        string contenido = t.getContenido(casilla_final);

        //print("CasillaFinal: " + casilla_final + "-" + contenido );

        if ((casilla_final.Equals("null")))        {
            posible = false;
        }

        else
        {
            
            if (!(pieza_conc.Contains(pieza)))
            {
                if ((((pieza.Contains("bN")) || (pieza.Equals("bB"))) || (pieza.Equals("bR")) || (pieza.Contains("bQ")) && (!turno)))
                {
                    posible = true;

                }
                else
                {
                    posible = false;
        //            print("Esa jugadita no es posible");
                }

                if ((((pieza.Contains("wN")) || (pieza.Equals("wB")) || (pieza.Equals("wR")) || (pieza.Contains("wQ"))) && (turno)))
                {
                    posible = true;
                }
            }

        }


        return posible;
    }

    #endregion

    #region IEndDragHandler implementation



    void OnGUI()
    {


        float x = Screen.width / 5f;
        float y = Screen.height / 4f;

        GUI.color = Color.red;
        GUI.skin.label.fontSize = 25;

        x = Screen.width / 5;
        y = Screen.height;

    }






    public void OnEndDrag(PointerEventData eventData)
    {
        //NO TIENE SENTIDO INVOCAR A MOV POSIBLE EN ON EN DRAG
        //OJO CON TN2! LOMEJOR SERIA BUSCARLO EN EL INICIO


        if (tn2.getJuego().Equals("t"))//IDENTIFICA QUE EL JUEGO ES EL TATEDREZ
        {
            //-print("Drag - EL JUEGO ES TATEDREZ!");


            
         //-   Debug.Log(transform.parent + " == " + canvas);
            itemBeingDragged = null;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            if (transform.parent == canvas){
                    transform.position = startPosition;
                    transform.parent = startParent;// <------------------------------- new addition
             }
            



            tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();

            string casilla;
            Tatedrez t = GameObject.Find("pFondo").GetComponent<Tatedrez>();
            casilla = t.getCasilla();
            bool posible = false;

            Image p = GetComponent<CanvasGroup>().GetComponent<Image>();
            string pieza = p.name;

            
           // print(posible + "POSIBLE: " + contenido);

            if (casilla.Equals("null"))
            {
                posible = false;
            }
            else
            {
               
                posible = true; //no deberia ser posible si hay una pieza anterior.
            }

            

            int contador = tn2.getCounter();

            string contenido = t.getContenido(casilla);

           //- print("CASILA: " + casilla + " contenido: " + contenido); 

            if ((contenido.Equals(""))) {
                //posible = false;
            }



            if ((posible) && (transform.parent != startParent))
            {
                //   Tatedrez t = GameObject.Find("pFondo").GetComponent<Tatedrez>(); //hay que ver como lo soluciono
                t.eliminar(pieza);//elimino pieza slot anterior
                t.insertar(pieza);

               //- print("entro al IF");
                if ((pieza.Contains("b")))
                {
                    color = "b";
                }
                else
                {
                    color = "w";
                }



                if ((pieza.Equals("bN")) || (pieza.Equals("bR")) || (pieza.Equals("bB")) || (pieza.Contains("bQ")))
                {
                    tn2.setTurno(true);
                }
                else if ((pieza.Equals("wN")) || (pieza.Equals("wR")) || (pieza.Equals("wB")) || (pieza.Contains("wQ")))
                {
                    tn2.setTurno(false);
                }


                t.terminado(color, tn2.getTurno(), contador);


                if ((tn2.getCounter().CompareTo(7).Equals(-1)) && !(pieza_conc.Contains(pieza)))
                {

                    pieza_conc = pieza_conc + pieza;
                    string slot_n = "";
                    switch (pieza)
                    {

                        case "bR":
                            slot_n = "Slot6";
                            break;
                        case "bN":
                            slot_n = "Slot5";
                            break;
                        case "bB":
                            slot_n = "Slot4";
                            break;
                        case "wR":
                            slot_n = "Slot3";
                            break;
                        case "wN":
                            slot_n = "Slot2";
                            break;
                        case "wB":
                            slot_n = "Slot1";
                            break;

                    }

                    if (transform.parent != startParent)
                    {
                       //- print("hola");
                        
                        if (!(slot_n.Equals("")))
                        {

                            //con esto elimino los slots iniciales de las piezas
                            Destroy((GameObject.Find(slot_n)).gameObject);
                            slot_n = "";
                        }
                    }

                }
            }
            else
            {
                //-print("No entro al if");
                transform.position = startPosition;
               // print("Vueta 1 EsPosible = " + posible + "contenido: " + contenido);

            }

        }
        if (tn2.getJuego().Equals("c")) {

            //-print("el juego es Tragamonedas!!!");

            Debug.Log(transform.parent + " == " + canvas);
            itemBeingDragged = null;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            if (transform.parent == canvas)
            {
                transform.position = startPosition;
                transform.parent = startParent;// <------------------------------- new addition
            }

            tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();

            string casilla;
            caballo_t tr = GameObject.Find("pFondo").GetComponent<caballo_t>();
            casilla = tr.getCasilla();

            //bool moneda = tr.capturar_moneda(casilla); 
            
            Image p = GetComponent<CanvasGroup>().GetComponent<Image>();
            string pieza = p.name;
           // print("CASILLA: " + casilla + " " + moneda + " " + pieza);

            bool posible = false;


            if (casilla.Equals("null"))
            {
                posible = false;
            }
            else
            {
                posible = true;
            }

            int contador = tn2.getCounter();

            if ((posible) && (transform.parent != startParent))
            {
                bool moneda = tr.capturar_moneda(casilla);
                tr.eliminar(pieza);//elimino pieza slot anterior
                tr.insertar(pieza);

                if ((pieza.Contains("b")))
                {
                    color = "b";
                }
                else
                {
                    color = "w";
                }

                if ((pieza.Contains("bK")))
                {
                    tn2.setTurno(true);
                }
                else if ((pieza.Contains("wK")))
                {
                    tn2.setTurno(false);
                }

            }
            else
            {

                transform.position = startPosition;

            }
        
    }
        // else if (juego.Equals("d"))
        else if (tn2.getJuego().Equals("d"))
        {// EL JUEGO ES DAMA NEUTRAL. ORDENAR CODIGO, EMPROLIJARLO

            Debug.Log(transform.parent + " == " + canvas);
            itemBeingDragged = null;
            GetComponent<CanvasGroup>().blocksRaycasts = true;
            if (transform.parent == canvas)
            {
                transform.position = startPosition;
                transform.parent = startParent;// <------------------------------- new addition
            }


            tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();

            string casilla;

            DamaNeutral dm = GameObject.Find("pFondo").GetComponent<DamaNeutral>();
            casilla = dm.getCasilla();

            bool posible = false;

            Image p = GetComponent<CanvasGroup>().GetComponent<Image>();
            string pieza = p.name;

            if (casilla.Equals("null"))
            {
                posible = false;
            }
            else
            {
                posible = true;
            }

            int contador = tn2.getCounter();

            if ((posible) && (transform.parent != startParent))
            {
                
                dm.eliminar(pieza);//elimino pieza slot anterior
                dm.insertar(pieza);

                if ((pieza.Contains("b")))
                {
                    color = "b";
                }
                else
                {
                    color = "w";
                }

                if ((pieza.Contains("bQ")))
                {
                    tn2.setTurno(true);
                }
                else if ((pieza.Contains("wQ")))
                {
                    tn2.setTurno(false);
                }


                dm.terminado(color, tn2.getTurno(), contador);

            }
            else
            {

                transform.position = startPosition;
                //-print("Vueta 1 EsPosible = " + posible);

            }
        }

    }

    #endregion

}
