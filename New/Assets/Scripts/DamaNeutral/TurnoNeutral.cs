﻿using UnityEngine;
using System.Collections;

public class TurnoNeutral : MonoBehaviour {

    static string neutron;
    private GameObject myObj;
    private SpriteRenderer sr;
    private Vector3 pos_negras;
    private Vector3 pos_blancas;

    void Start () {

        neutron = ""; 
        myObj = GameObject.Find("ptNuetral");
        sr = myObj.GetComponent<SpriteRenderer>();

        pos_negras = new Vector3(-5f, 3f, 0f);
        pos_blancas = new Vector3(-5f, -3f, 0f);

    }
	
	// Update is called once per frame
	void Update () {

        if (neutron.Equals(""))
        {
            
            sr.transform.position = new Vector3(100f, 100f, 0f);
            
        }
        else if (neutron.Equals("negras"))
        {
            sr.transform.position = pos_negras;
            
        }
        else if (neutron.Equals("blancas"))
        {
            sr.transform.position = pos_blancas;

        }

    }

     public void setNeutron(string aux) {
        neutron = aux;
    }

}
