﻿using UnityEngine;
using System.Collections;

public class DamaNeutral : MonoBehaviour
{
   //Se utilizan las clases Drag y Slot de Dama Neutral

    private string[,] board = new string[6, 6];
    private string[,] movPos = new string[6, 6];
    private string casilla = "null";
    private int i, j;
    private bool term = false;
    private string color = "";
    private bool empate = false;
    private bool neutron = false; 
    static bool neutral_n = false;
    static bool neutral_b = false;
    private bool ganan_blancas = false;
    private bool ganan_negras = false;
    private Cas[] lista = new Cas[16];
    private int tope = 0;


    void Start()
    {
        Turnos2 tn2 = GameObject.Find("turno_b").GetComponent<Turnos2>();
        tn2.setJuego("d"); // 

        for (int i = 0; i < 6; i++)
        {
            for (int j = 0; j < 6; j++)
            {
                board[i, j] = "";
            }
        }

          board[3, 3] = "nQ";
          board[1, 1] = "wQ1";
          board[2, 1] = "wQ2";
          board[3, 1] = "wQ3";
          board[4, 1] = "wQ4";
          board[5, 1] = "wQ5";

          board[1, 5] = "bQ1";
          board[2, 5] = "bQ2";
          board[3, 5] = "bQ3";
          board[4, 5] = "bQ4";
          board[5, 5] = "bQ5";


}

    void Update()
    {


    }

    public void insertar(string p)
    {

        print(p);
        switch (casilla)
        {
            case "a1":
                board[1, 1] = p;
                i = 1;
                j = 1;
                break;
            case "a2":
                board[1, 2] = p;
                i = 1;
                j = 2;
                break;
            case "a3":
                board[1, 3] = p;
                i = 1;
                j = 3;
                break;
            case "a4":
                board[1, 4] = p;
                i = 1;
                j = 4;
                break;
            case "a5":
                board[1, 5] = p;
                i = 1;
                j = 5;
                break;
            case "b1":
                board[2, 1] = p;
                i = 2;
                j = 1;
                break;
            case "b2":
                board[2, 2] = p;
                i = 2;
                j = 2;
                break;
            case "b3":
                board[2, 3] = p;
                i = 2;
                j = 3;
                break;
            case "b4":
                board[2, 4] = p;
                i = 2;
                j = 4;
                break;
            case "b5":
                board[2, 5] = p;
                i = 2;
                j = 5;
                break;
            case "c1":
                board[3, 1] = p;
                i = 3;
                j = 1;
                break;
            case "c2":
                board[3, 2] = p;
                i = 3;
                j = 2;
                break;
            case "c3":
                board[3, 3] = p;
                i = 3;
                j = 3;
                break;
            case "c4":
                board[4, 2] = p;
                i = 4;
                j = 2;
                break;
            case "c5":
                board[5, 3] = p;
                i = 5;
                j = 3;
                break;
            case "d1":
                board[4, 1] = p;
                i = 4;
                j = 1;
                break;
            case "d2":
                board[4, 2] = p;
                i = 4;
                j = 2;
                break;
            case "d3":
                board[4, 3] = p;
                i = 4;
                j = 3;
                break;
            case "d4":
                board[4, 4] = p;
                i = 4;
                j = 4;
                break;
            case "d5":
                board[4, 5] = p;
                i = 4;
                j = 5;
                break;
            case "e1":
                board[5, 1] = p;
                i = 5;
                j = 1;
                break;
            case "e2":
                board[5, 2] = p;
                i = 5;
                j = 2;
                break;
            case "e3":
                board[5, 3] = p;
                i = 5;
                j = 3;
                break;
            case "e4":
                board[5, 4] = p;
                i = 5;
                j = 4;
                break;
            case "e5":
                board[5, 5] = p;
                i = 5;
                j = 5;
                break;
        }

    }


    public int[] getCoord(string p)
    {
        int[] pos = new int[2];
        switch (casilla)
        {
            case "a1":
                pos[0] = 1;
                pos[1] = 1;

                break;
            case "a2":

                pos[0] = 1;
                pos[1] = 2;
                break;
            case "a3":

                pos[0] = 1;
                pos[1] = 3;
                break;
            case "a4":

                pos[0] = 1;
                pos[1] = 4;
                break;
            case "a5":

                pos[0] = 1;
                pos[1] = 5;
                break;
            case "b1":

                pos[0] = 2;
                pos[1] = 1;
                break;
            case "b2":

                pos[0] = 2;
                pos[1] = 2;
                break;
            case "b3":

                pos[0] = 2;
                pos[1] = 3;
                break;
            case "b4":

                pos[0] = 2;
                pos[1] = 4;
                break;
            case "b5":

                pos[0] = 2;
                pos[1] = 5;
                break;
            case "c1":

                pos[0] = 3;
                pos[1] = 1;
                break;
            case "c2":
                pos[0] = 3;
                pos[1] = 2;
                break;
            case "c3":

                pos[0] = 3;
                pos[1] = 3;
                break;
            case "c4":
                pos[0] = 3;
                pos[1] = 4;
                break;
            case "c5":

                pos[0] = 3;
                pos[1] = 5;
                break;
            case "d1":

                pos[0] = 4;
                pos[1] = 1;
                break;
            case "d2":

                pos[0] = 4;
                pos[1] = 2;
                break;
            case "d3":

                pos[0] = 4;
                pos[1] = 3;
                break;
            case "d4":

                pos[0] = 4;
                pos[1] = 4;
                break;
            case "d5":

                pos[0] = 4;
                pos[1] = 5;
                break;
            case "e1":

                pos[0] = 5;
                pos[1] = 1;
                break;
            case "e2":
                pos[0] = 5;
                pos[1] = 2;
                break;
            case "e3":

                pos[0] = 5;
                pos[1] = 3;
                break;
            case "e4":
                pos[0] = 5;
                pos[1] = 4;
                break;
            case "e5":

                pos[0] = 5;
                pos[1] = 5;
                break;


        }
        return pos;
    }

    public void eliminar(string p)
    {

        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                if (board[i, j].Equals(p))
                {
                    board[i, j] = "";
                }
            }
        }
    }

    public bool EnLista(int x, int y)
    {

        bool aux = false;
        int i = 0;

        while ((!aux) && (i < tope))
        {
            aux = ((lista[i].getX().Equals(x)) && (lista[i].getY().Equals(y)));
            i++;
        }

        return aux;
    }

    public int[] getPos(string p)
    {
        int[] pos = new int[2];
        for (int i = 1; i < 6; i++)
        {
            for (int j = 1; j < 6; j++)
            {
                if (board[i, j].Equals(p))
                {
                    pos[0] = i;
                    pos[1] = j;
                }
            }
        }

        return pos;
    }


    //Nota: Mejor usar backtracking para mayor eficiencia

    public void MovTorre(int x, int y)
    {

        tope = 0;
        int i, j;
        bool libre;
        i = x;
        j = y;
        libre = true;

        //primero consulto abajo, itero sobre la columna j
        while ((j > 0) && (libre))
        {

            j--;

            if ((j > 0) && (board[x, j].Equals("")))
            { //guardo la x y la j

                //print("ABAJO" + x + " " + j);

                Cas item = new Cas(x, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia arriba

        while ((j < 6) && (libre))
        {

            j++;
            if ((j < 6) && (board[x, j].Equals("")))
            { //guardo la x y la j
                //print("ARRIBA" + x + " " + j);

                Cas item = new Cas(x, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;

        while ((i < 6) && (libre))
        {

            i++;
            if ((i < 6) && (board[i, y].Equals("")))
            { //guardo la x y la j
                //print("DERECHA" + i + " " + y);

                Cas item = new Cas(i, y);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;
        libre = true;

        while ((i > 0) && (libre))
        {
            i--;
            if ((i > 0) && (board[i, y].Equals("")))
            { //guardo la x y la j

                //print("IZQUIERDA" + i + " " + y);
                Cas item = new Cas(i, y);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }


    }


    public void MovAlfil(int x, int y)
    {

        tope = 0;
        int i, j;
        bool libre;
        i = x;
        j = y;
        libre = true;

        //primero consulto abaja a la izquierda
        while ((j > 0) && (i > 0) && (libre))
        {

            j--;
            i--;

            if ((j > 0) && (i > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("ABAJO - IZQUIERDA" + i + " " + j);

                Cas item = new Cas(i, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;

        libre = true;
        //ahora itero hacia ARRIBA - DERECHA

        while ((j < 6) && (i < 6) && (libre))
        {

            j++;
            i++;

            if ((j < 6) && (i < 6) && (board[i, j].Equals("")))
            { //guardo la x y la j
               // print("ARRIBA-DERECHA" + i + " " + j);

                Cas item = new Cas(i, j);
                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        i = x;
        j = y;
        libre = true;


        //Subo la i y decremento la j
        while ((i < 6) && (j > 0) && (libre))
        {

            i++;
            j--;

            if ((i < 6) && (j > 0) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("DERECHA- ABAJO" + i + " " + j);

                Cas item = new Cas(i, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }

        }

        i = x;
        j = y;

        libre = true;

        //subo la i y decremento la j
        while ((i > 0) && (j < 6) && (libre))
        {
            i--;
            j++;
            if ((i > 0) && (j < 6) && (board[i, j].Equals("")))
            { //guardo la x y la j

                //print("IZQUIERDA - ARRIBA" + i + " " + j);
                Cas item = new Cas(i, j);

                lista[tope] = item;
                tope++;
            }
            else
            {
                libre = false;
            }
        }

        //print("MovTorre: " + tope);
    }
    
    public bool HayEmpate(bool turno)
    {
        //dependiendo del turno, debo verificar que las piezas posean movimientos posibles.


        int[] b_pos = new int[2];
        int[] n_pos = new int[2];
        int[] r_pos = new int[2];

        if (turno)
        {
            b_pos = getPos("wB");
            n_pos = getPos("wN");
            r_pos = getPos("wR");
        }
        else
        {
            b_pos = getPos("bB");
            n_pos = getPos("bN");
            r_pos = getPos("bR");
        }

        MovAlfil(b_pos[0], b_pos[1]);
        if ((tope.Equals(0)))
        {
           // MovCaballo(n_pos[0], n_pos[1]);
            if ((tope.Equals(0)))
            {
                MovTorre(r_pos[0], r_pos[1]);
                if ((tope.Equals(0)))
                {
                    print("HAY EMPATE!!!!");
                    empate = true;
                }
            }

        }

        return empate;
    }
    


    public bool terminado(string c, bool turno, int jugadas)
    {
        bool aux = false;
        bool empate = false;

        ganan_blancas = false;
        ganan_negras = false;

        //verticales
        if ((turno) && ((board[1, 1].Contains("nQ")) || (board[2, 1].Contains("nQ")) || (board[3, 1].Contains("nQ"))|| (board[4, 1].Contains("nQ")) || (board[5, 1].Contains("nQ"))))
        {
            aux = true;
            print("GANAN BLANCAS");
            ganan_blancas = true;
        }
        else
         if ((!turno) && ((board[1, 5].Contains("nQ")) || (board[2, 5].Contains("nQ")) || (board[3, 5].Contains("nQ")) || (board[4, 5].Contains("nQ")) || (board[5, 5].Contains("nQ"))))
            {
             aux = true;
             print("GANAN NEGRAS");
             ganan_negras = true;
           }

        if (aux)
        {
            
            if (c.Equals("w"))
            {
                color = "NEGRAS";
            }
            else
            {
                color = "BLANCAS";
            }
        }

        term = aux;
        return aux;
    }


    public Cas[] getListaPosible()
    {

        return lista;
    }


    public int getTope()
    {

        return this.tope;
    }


    void OnGUI()
    {
        float x = Screen.width / 5f;
        float y = Screen.height / 4f;
        int h = 0;

        x = Screen.width / 5;
        y = Screen.height;
        int k = 0;


        Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 36;
        myStyle.fontStyle = FontStyle.Bold;

        if (term)
        {
           
            if (ganan_negras)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "N\nE\nG\nR\nA\nS", myStyle);

            }
            else
                if (ganan_blancas)
            {
                GUI.Label(new Rect(x * 4.2f, x * 0.3f, 300, 300), "G\nA\nN\nA\nN", myStyle);
                GUI.Label(new Rect(x * 4.5f, x * 0.3f, 300, 300), "B\nL\nA\nN\nC\nA\nS", myStyle);
              
            }
        }



    }

    public void setCasilla(string cas)
    {
        casilla = cas;
    }
    public string getCasilla()
    {
        return casilla;
    }

    public void setNeutral_n(bool aux) {
        neutral_n = aux;
    }
    public void setNeutral_b(bool aux)
    {
        neutral_b = aux;
    }
    public bool getNeutral_n()
    {
        return neutral_n;
    }
    public bool getNeutral_b()
    {
        return neutral_b;
    }

}

