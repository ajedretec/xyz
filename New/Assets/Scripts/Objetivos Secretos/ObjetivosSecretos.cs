﻿using UnityEngine;
using System.Collections;

public class ObjetivosSecretos : MonoBehaviour {

	// Use this for initialization
	private string[] s = new string[19];
	int x,y,i;

	void Start () {

		s[0]= "• Dar tres jaques. \n\n• Capturar 16 puntos. \n\n• Poseer un peon en la sexta fila.";
		s[1]= "• Capturar una torre, un caballo y un alfíl. \n\n• Dar dos jaques. \n\n• Poseer dos piezas más allá de la quinta fila.";
		s[2]= "• Dar cinco jaques.\n\n • Capturar 10 puntos.";
		s[3]= "• Capturar los dos alfiles y cuatro peones.\n• Dar tres jaques.\n\n •Poseer una pieza en el centro del \n\n tablero.";
		s[4]="• Dar cuatro jaques.\n• Capturar 15 puntos.";
		s[5]="• Capturar 13 puntos.\n• Dar dos jaques.\n• Poseer una pieza en la séptima fila.";
		s[6]="• Capturar 21 puntos.\n• Dar un jaque.";
		s[7]="• Capturar seis peones.\n• Dar un jaque.";
		s[8]="• Capturar 16 puntos.\n• Ocupar tres casillas centrales.\n• Dar un jaque.";
		s[9]= "• Capturar 13 puntos.\n• Dar dos jaques.\n• Ocupar las casillas a5 y h5 (si juegas con blancas) \n o a4 y h4 (si juegas con negras).";
		s[10]="• Capturar la dama adversaria. \n• Poseer dos peones mas allá de la mitad del tablero.";
		s[11]="• Capturar 10 puntos.\n• Dar dos jaques.\n• Impedir el enroque corto adversario.";
		s[12]="• Dar dos jaques.\n• Capturar las dos torres adversarias.";
		s[13]="• Capturar 17 puntos.\n• Dar tres jaques.\n• Llevar tu rey a un rincón del tablero.";
		s[14]= "• Capturar cuatro peones y una torre.\n• Dar un jaque a la descubierta y otro jaque \n cualquiera.";
		s[15]= "• Dar un jaque y otro jaque cualquiera.\n• Poseer tres piezas(o peones) en el centro \n del tablero.";
		s[16]="• Capturar seis peones adversarios.\n• Dar un jaque.";
		s[17]="• Impedir el enroque (corto o largo) adversario.\n• Capturar una torre.\n• Dar un jaque.";
		s[18]="• Dar 8 jaques.";

		i= Random.Range(0,19);


	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnGUI() {
		float x = Screen.width/4.5f;
		y= Screen.height/3;

        Font myFont = (Font)Resources.Load("VistaSansBlack", typeof(Font));
        GUIStyle myStyle = new GUIStyle();
        myStyle.font = myFont;
        myStyle.fontSize = 20;

        

        GUI.color = Color.black; 
		GUI.skin.label.fontSize = 12;
        
       
		GUI.Label (new Rect (x, y, 300, 300),s[i], myStyle);
	}
}
